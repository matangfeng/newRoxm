//
//  CJCSectorCycleView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCSectorCycleView.h"
#import "CJCCommon.h"

@interface CJCSectorCycleView (){

    CGFloat viewWidth;
}

@property (nonatomic, assign) CGFloat progress;

@end

@implementation CJCSectorCycleView

- (void)drawProgress:(CGFloat )progress{

    _progress = progress;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();//获取上下文
    
    CGPoint center = CGPointMake((rect.size.width/2), (rect.size.height/2));  //设置圆心位置
    CGFloat radius = (rect.size.width - 10)/2;  //设置半径
    CGFloat startA = - M_PI_2;  //圆起点位置
    CGFloat endA = -M_PI_2 + M_PI * 2 * self.progress;  //圆终点位置
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];
    
    CGContextSetLineWidth(ctx, 5); //设置线条宽度
    [[UIColor whiteColor] setStroke]; //设置描边颜色
    
    CGContextAddPath(ctx, path.CGPath); //把路径添加到上下文
    
    CGContextStrokePath(ctx);  //渲染
    
}

//绘制扇形进度条
//- (void)drawRect:(CGRect)rect {
//    //    定义扇形中心
//    CGPoint origin = CGPointMake(kAdaptedValue(50), kAdaptedValue(50));
//    //    定义扇形半径
//    CGFloat radius = rect.size.width/2;
//    
//    //    设定扇形起点位置
//    CGFloat startAngle = - M_PI_2;
//    //    根据进度计算扇形结束位置
//    CGFloat endAngle = startAngle + self.progress * M_PI * 2;
//    
//    //    根据起始点、原点、半径绘制弧线
//    UIBezierPath *sectorPath = [UIBezierPath bezierPathWithArcCenter:origin radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
//    
//    //    从弧线结束为止绘制一条线段到圆心。这样系统会自动闭合图形，绘制一条从圆心到弧线起点的线段。
//    [sectorPath addLineToPoint:origin];
//    
//    //    设置扇形的填充颜色
//    [[UIColor lightGrayColor] set];
//    
//    //    设置扇形的填充模式
//    [sectorPath fill];
//}

@end
