//
//  CJCUploadManager.h
//  roxm
//
//  Created by lfy on 2017/8/31.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^uploadSuccessBlock)(NSString *QiniuKey);
typedef void (^groupUploadSuccessBlock)(NSArray *QiniuKeys);

@class QNUploadManager;

@interface CJCUploadManager : NSObject

-(void)uploadGroupImagesAndVideos:(NSArray *)groups successHandle:(groupUploadSuccessBlock)groupSuccess;

-(void)uploadVideo:(NSString *)videoPath successHandle:(uploadSuccessBlock)success;

-(void)uploadImage:(UIImage *)uploadImage successHandle:(uploadSuccessBlock)success;

@end
