//
//  CJCTools.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class CJCpersonalInfoModel;

@interface CJCTools : NSObject

///将生日转换成年龄
+ (NSString *)nameAndAgeStrFromInfoModel:(CJCpersonalInfoModel *)infoModel;

///比较俩个时间 返回多久之前
+ (NSString*)compareTwoTime:(long)time1 time2:(long)time2;

+ (NSString *)_convert2Mp4:(NSURL *)movUrl;

///获得指定font的字符串 在最大宽度时的size
+(CGSize)sizeWithString:(NSString *)string maxWidth:(CGFloat)width font:(UIFont *)font;

///随机颜色
+ (UIColor*)randomColor;

///判断手机号码格式是否正确
+ (BOOL)valiMobile:(NSString *)mobile;

///判断字符串是否为空  YES代表是空字符串
+ (BOOL) isBlankString:(NSString *)string;

///获取当前定位权限
+(CLAuthorizationStatus)locationStatus;

///获取当前显示的控制器
+ (UIViewController*)currentViewController;

///获得当前手机型号
+ (NSString *)iphoneType;
///获得当前时间的时间戳
+(NSString *)getDateTimeTOMilliSeconds;
///将时间戳转换为NSDate类型
+(NSDate *)getDateTimeFromMilliSeconds:(long long) miliSeconds;
///将时间戳转换为字符串
+(NSString *)getDateTimeStrFromMilliSeconds:(long long) miliSeconds;
///获得当前时间的时间 nsdate
+(NSString *)getDateStrTOMilliSeconds;
///将日期转换成年龄
+(NSString *)dateToAge:(NSString *)date;

///根据font获取一段短字符串的长度
+(CGFloat)getShortStringLength:(NSString *)string withFont:(UIFont *)font;

///将数字转换成  00:00 的时间格式
+ (NSString *)getNewTimeFromDurationSecond:(NSInteger)duration;

@end
