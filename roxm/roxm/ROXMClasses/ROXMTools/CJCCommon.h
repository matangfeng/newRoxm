//
//  CJCCommon.h
//  roxm
//
//  Created by lfy on 2017/8/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

//用户人际关系状态
typedef NS_ENUM(NSInteger,CJCPersonRelationType){
    
    CJCPersonRelationTypeNon                 = 0,///没有关系
    CJCPersonRelationTypeFollow              = 1,///关注
    CJCPersonRelationTypeCancleFollow        = 2,///取消关注
    CJCPersonRelationTypeDefriend            = 3,///拉黑
    CJCPersonRelationTypeFriend              = 4,///取消拉黑
};

#import "UIView+CJCEasyGetSet.h"
#import <YYKit.h>
#import <YYLabel.h>
#import "UIFont+AccodingVersion.h"
#import "CJCOprationButton.h"
#import "CJCErrorAletLabel.h"
#import "UIColor+StringToColor.h"
#import "CJCTools.h"
#import "MBManager.h"
#import "CJCHttpTool.h"
#include <Masonry.h>
#import "UIButton+addCanOpration.h"
#import "CJCpersonalInfoModel.h"
#import "AppDelegate.h"
#import "NSObject+YYModel.h"
#import "MJRefresh.h"
#import <NSObject+YYModel.h>
#import <UIImageView+YYWebImage.h>


#ifndef CJCCommon_h
#define CJCCommon_h

#endif /* CJCCommon_h */

//项目的url信息

#define kROXMBaseURL                       @"http://test.api.hojji.com/roxm/"

/* 
 *textField的tag  格式为 10001  五位数
 *button 的tag 格式为 1001  四位数
 *
 */

//支付宝的APPID
#define kALIPAYPID              @"2088421656359539"
#define kALIPAYSCHEME           @"alisdkdemo"
#define kALIPAYAPPID            @"2016090801867319"
#define kALIPAYSIYAO            @"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCkp27tuYJyxKXPvCl1503JZD9DfCPYJrpSvP8hGoPPC5M9ekEOPfjEedfdQgYhhdfF0LD9EEaNzeynIklP8FcGidMSCl+9upOimewUB4tcfdRG8BZyYJA9JqUhwMbyYALW03WGa48xAwyRiVj3R8Gjwetoy284B+mjkMDEaVbKmD6LlhuDqyyPB8A/CbknOarrpLWDENNUNp6oISvWrOswXBtXemEcnuOkDx25MpIIwU4QTgQGQ2o4EN/A6rujsaUCrt/6pBWXdLeEWl5b6l6KfNkBoVJ/yPUf84uwaIMGbF0LzQwh79C5dTApnmTrX+02Y4dwen+0lMs8MRaAGJQrAgMBAAECggEAa6Sei8HUu20+LYII5PDT1M654UeeM2iWNRMkBCNOeqRJnU54i3QOV3yEmDzavz2+I6/clYT3aytuBJIQtbcdXcPq/odTsjjw3cOdKDcKb+w/RCUft6TTh3blTBy4s6n7ETDrS3a0BUnGBLtzMy5xNLrJSRh+XHYn02wF6iInwIgZKr68k7FSpd+ZruSlcQqw9AvtW+4ZMdsYIt8uBw7fmrpVEKXuB+LhAu3VD8lXADQy30kXHcKChKANIW5W49zZynupXMdHuc1Wpq+eUVhyHTNhuAMwqEsp+etSsj3i0rm6Btu8DtKsCnr8jsqLVYAbBG3eTksiR5FQy6KKEmO+KQKBgQDzwPEkRVePvfPRrgrfranoPJbtHBjWuwRPNniimwTg2zOf6vV6+L8+t7SGK3biYNHio3LhXvwiqaDNUYvNNYE66pkScbiFfQuClBtKFVEAdenM/5D+0iak0yZom33P6kQ24/pkFlQZjAhY7dqPx7Q/4o7MzQqU2ncuvrm3zcEdNwKBgQCs7SUP/U0OUS/ZkTPhwItJBBK9WvG32ZyO7ECF/j7k1G0zv2MFwtVwgu/6y43pqg4f9S6zHlbLhSAS53jWUGMh7PwOsiNWNezyizgTkikixj0k+a/1KAujy2X8ihbtKOFe0DBHlMjW+cJKFKz9QulRfPUv3hLOHSnykpYEf4HarQKBgCEtKaUeKwTNfdoULjazlGSfmos7P8Y6PiPp7hFzt/5C14v0luTd5mnK5y87yzqUovyN8pzqKLcvD0vixXxww94ZACyafdrtYhurvOsga538bo4QWtPUFp5oftnOEBm0cQRnkzT8NQYlIR25gf3/2HC5jWYiokYQVhhurShvQNGRAoGAIjRKB26f5jwSzeMVswqlwkyG9GNfunDVscNzQ821XQfTjc/GY2ZVV4resum+aUCUyKdzlERlAJ6VaIfWc/W0lgpNFQ8RREUoWBVHgz9+2X0CrSSiNEi2fLCHvLsHq+NRKLLfSdlZHISMdKDAUtwAUHj5+nzflVtZZ7ys5bje04UCgYAxsExptEcVMCibZHDXKAtKJaWx5VpfVkIdhPK/glU0ULxwnTGSAkqVynvZIeaU6HEE1BrjqqmHi99syjF3lZ0m0Zuwb53SGyk6juQCSM2lAtX13W67+Yz8Xq0V4cFtY0jJEBD1D7FbM8uCLH+L0lrYzkFOIOhgrpXLSHODPab/+A=="

//极光推送的key
#define kJPUSHAPPKEY            @"d77c2477f4e81a1598456eab"

//高德地图的key
#define kAMAPKEY                @"c198164a9849715da4d136b897f36653"
#define kUMENGKey               @"59acaf498630f5718100191b"

//一些保存到本地的文件
#define kUSERINFOPLIST          @"userInfo.plist"

//存储相机权限的字段
#define kCAMERAAUTHORITY        @"cameraAuthorityIsOpen"

//偏好设置的键
#define kREGISTERSOURCE         @"linkedME_source"
#define kUSERUID                @"userUid"
#define kUSERTOKEN              @"userToken"
#define kHUANXINPASSWORD        @"emPassword"
#define kQINIUTOKEN             @"QINIUToken"
#define kQINIUDOMAIN            @"QINNIUDomain"
#define kCAMERASIZETYPE         @"kCameraSizeType"
#define kUSERINFOCOMPLETE       @"userInfoComplete"
#define kAPPSTARTCOUNT          @"appStartCount"
#define kUSERCITY               @"userCity"

///女士同意邀约  此时需要不断上传用户的位置
#define kACCEPTYAOYUEUPLOADLOCATION     @"acceptYaoyueUploadLocation"
#define kLOCATIONFRESHTIME              300.0

//设置界面需要的键
#define kSWITCHON               @"YES"    //开关打开
#define kSWITCHOFF              @"NO"     //开关关闭
#define kSETTINGHID             @"settingHid"  //是否开启隐身
#define kUSERCONTACTUPLOAD      @"userContactUpload" //是否开启屏蔽联系人
#define kCHATMESSAGENOTICE      @"settingChatNotice"    //是否通知对话消息
#define kYAOYUEMESSAGENOTICE    @"settingYaoyueNotice"  //是否通知邀约消息
#define kMESSAGEISSHOWDETAIL    @"settingShowMessageDetail" //通知消息是否显示详情
#define kNOTICEAUDIO            @"settingNoticeAudio"   //通知声音
#define kNOTICEVIBRATION        @"settingNoticeVibration"   //通知震动
#define kUNREADMESSAGECOUNT     @"UnreadMessageCount"//未读消息的数量

//环信拓展消息 用户消息
#define kHUANXINLOCATIONDISTANCE    @"huanxinLocationDistance"
#define kHUANXINLOCATIONNAME        @"huanxinLocationName"
#define kHUANXINLOCATIONIMAGE       @"huanxinLocationImage"
#define kHUANXINICONURLSENDER       @"huanxinIconURLSender"
#define kHUANXINNICKNAMESENDER      @"huanxinNickNameSender"
#define kHUANXINICONURLRECIVER      @"huanxinIconURLReciver"
#define kHUANXINNICKNAMERECIVER     @"huanxinNickNameReciver"
#define kHUANXINUIDRECIVER          @"huanxinUIDReciver"
#define kHUANXINIMAGESOURCE         @"huanxinImageSource"
#define kHUANXINIMAGECAMERA         @"huanxinImageCamera"
#define kHUANXINIMAGELIBERARY       @"huanxinImageLiberary"
#define kHUANXINIMAGEHEIGHT         @"huanxinImageHeight"
#define kHUANXINIMAGEWIDTH          @"huanxinImageWidth"

//环信扩展消息  邀约
#define kHXYAOYUEIDENTIGYCATION     @"HX_ROXM_Yaoyue_Identification"
#define kHXYAOYUEHOTELNAME          @"HX_ROXM_Yaoyue_hotelName"
#define kHXYAOYUEHOTELADDRESS       @"HX_ROXM_Yaoyue_address"
#define kHXYAOYUEHOTELLOCATIONLAT   @"HX_ROXM_Yaoyue_location_lat"
#define kHXYAOYUEHOTELLOCATIONLNG   @"HX_ROXM_Yaoyue_location_lng"
#define kHXYAOYUEHOTELIMAGE         @"HX_ROXM_Yaoyue_hotelImage"
#define kHXYAOYUEORDERID            @"HX_ROXM_Yaoyue_OrderId"
#define kHXYAOYUEORDERIDENTIGYCATION     @"HX_ROXM_Yaoyue_OrderIdentification"
#define kHXYAOYUEORDERSTATUS        @"HX_ROXM_Yaoyue_OrderStatus"

//常用宏＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
#define kUSERDEFAULT_STAND [NSUserDefaults standardUserDefaults]

#define Logins @"logins"
#define LoginUses @"LoginUses"

//拿到项目里的图片（R为文件全名带扩展名）
#define kGET_BUNDLE_IMAGE(R) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:(R) ofType:nil]]

//获取图片资源
#define kGetImage(imageName) [UIImage imageNamed:[NSString stringWithFormat:@"%@",imageName]]

#define SCREEN_WITDH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

//导航视图
#define ROOT_VIEWCONTROLLER (LFYNavigationC *)[UIApplication sharedApplication].keyWindow.rootViewController

//获得屏幕的宽高比  用于所有机型适配
#define kScreenWidthRatio  (SCREEN_WITDH / 375.0)

#define kAdaptedValue(x)  (ceilf((x) * kScreenWidthRatio))

//男士注册界面的进度条宽度
#define kMANREGISTERPROGRESSWIDTH     SCREEN_WITDH/2
//女士注册界面的进度条宽度
#define kWOMANREGISTERPROGRESSWIDTH     SCREEN_WITDH/5

//导航栏的centerY
#define  kNAVIVIEWCENTERY    42

// 判断当前系统版本
#define CJCSYSTEMVERSION CJCIOS8 [[UIDevice currentDevice].systemVersion floatValue]
#define CJCIOS8 [[UIDevice currentDevice].systemVersion floatValue] >= 8.0 && [[UIDevice currentDevice].systemVersion floatValue] < 9.0
#define CJCIOS9 [[UIDevice currentDevice].systemVersion floatValue] >= 9.0 && [[UIDevice currentDevice].systemVersion floatValue] < 10.0

#define LESSIOS9 [[UIDevice currentDevice].systemVersion floatValue] < 9.0
#define GREATERIOS9 [[UIDevice currentDevice].systemVersion floatValue] >= 9.0

#define Is_iOS10  [[UIDevice currentDevice].systemVersion hasPrefix:@"10"]

// 字体的宏
#define kPINGFANGFont_REGULAR            @"PingFangSC-Regular"
#define kPINGFANGFont_LIGHT              @"PingFangSC-Light"
#define kPINGFANGFont_MEDIUM             @"PingFangSC-Medium"

#define kFONTNAMELIGHT                   @"light"
#define kFONTNAMEREGULAR                 @"Regular"
#define kFONTNAMEMEDIUM                  @"medium"

#define kPINGFANGFont_REGULARWithSize(R)     [UIFont fontWithName: kPINGFANGFont_REGULAR size: (R)]

#define kPINGFANGFont_LIGHTWithSize(R) [UIFont fontWithName: kPINGFANGFont_LIGHT size: (R)]

#define kPINGFANGFont_MEDIUMWithSize(R) [UIFont fontWithName: kPINGFANGFont_MEDIUM size: (R)]

#define kSystemFontWithSize(R)     [UIFont systemFontOfSize:(R)]

//一像素的高度
#define OnePXLineHeight 1/[UIScreen mainScreen].scale

// 根据字符串获得颜色
#ifndef UIColorHex
#define UIColorHex(_hex_)   [UIColor colorWithHexString:((__bridge NSString *)CFSTR(#_hex_))]
#endif

// 根据RGB获取颜色
#define SYS_COLOR_RGB(R,G,B) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1.0]

#define SYS_COLOR_RGBA(R,G,B,A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
