//
//  CJCHttpTool.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^refreshSuccessBlock)(id responseObject);
typedef void (^refreshFailureBlock)(NSError *error);

@interface CJCHttpTool : NSObject

/**
 *  get方法请求网络数据
 *
 *  @param url     请求地址
 *  @param params  参数<字典类型>
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
+ (void)getWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  post方法请求网络数据
 *
 *  @param url     请求地址
 *  @param params  参数<字典类型>
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;


+ (void)postImagesAndVideosWithUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;

/**
 *  获取验证码
 *
 *  @param phone   手机号
 *  @param success 成功的回调
 *  @param failure 失败的回调
 */
+(void)getRoxmVerificationCodeWithPhone:(NSString *)phone success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;

///不添加其他的参数
+ (void)postWithNoAddParamsUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;

+ (void)getWithNoAddParamsUrl:(NSString *)url params:(NSDictionary *)params success:(void(^)(id responseObject))success failure:(void (^)(NSError *error))failure;

@end
