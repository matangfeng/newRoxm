//
//  CJCSectorCycleView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCSectorCycleView : UIView

- (void)drawProgress:(CGFloat )progress;

@end
