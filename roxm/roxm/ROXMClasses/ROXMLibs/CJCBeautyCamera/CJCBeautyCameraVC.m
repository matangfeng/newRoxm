//
//  CJCBeautyCameraVC.m
//  roxm
//
//  Created by lfy on 2017/8/25.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBeautyCameraVC.h"
#import "CJCCommon.h"
#import "GPUImageBeautifyFilter.h"
#import "KWBeautyFilter.h"
#import "CJCBeautyCameraManger.h"
#import "CJCVideoHandleManger.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "TBCycleView.h"

#define DURATION 0.7f
#define MAXVIDEOLENGH  10
#define k34RatioOffY   20

typedef NS_ENUM(NSInteger,CJCCameraType){
    
     CJCCameraTypePhoto                  = 0,
     CJCCameraTypeVideo                  = 1,
};

@interface CJCBeautyCameraVC ()<CJCVideoHandleMangerDelegate>{
    
    UIImage *takeImage;
    CJCCameraType cameraType;
    
    //3:4的比例时 展示view的高度
    CGFloat previewHeight;
    
    //改变拍照按钮的图片
    NSString *takePhotoImage;
    NSString *takingPhotoImage;
    CGPoint orginCenter;
    
    //录制完视频之后  可以预览拍摄的视频
    AVPlayer *videoPlayer;
    AVPlayerLayer *videoplayerLayer;
    AVPlayerItem *videoitem;
    
    CJCCameraSizeType imageSizeType;
    
    NSTimer *holdTimer;
    CGFloat cycleProgress;

    UILongPressGestureRecognizer *longPressGesture;
}

//视频剪切所用的类
@property (nonatomic ,strong) AVMutableComposition *composition;

//毛玻璃效果
@property (nonatomic ,strong) UIVisualEffectView *effectView;

@property (nonatomic ,strong) TBCycleView *cycleView;

@property (nonatomic ,strong) CJCBeautyCameraManger *cameraManger;

@property (nonatomic ,strong) UIView *middleHintView;

@property (nonatomic ,strong) UILabel *middleHintLabel;

@property (nonatomic ,strong) UIView *bottomHintView;

@property (nonatomic ,strong) UILabel *bottomHintLabel;

@property (nonatomic ,strong) UIImageView *takePhotoVideoImageView;

@property (nonatomic ,strong) UIButton *beautyIsOpenButton;

@property (nonatomic ,strong) UIButton *imageRatioButton;

@property (nonatomic ,strong) UIButton *reTakePhotoButton;

@property (nonatomic ,strong) UIButton *confirmButton;

@property (nonatomic ,strong) UIView *topCoverView;

@property (nonatomic ,strong) UIView *bottomCoverView;

@property (nonatomic ,copy) NSString *videoStr;
@property (nonatomic ,copy) NSString *audioStr;

@property (nonatomic ,copy) NSString *exportStr;

@end

@implementation CJCBeautyCameraVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self.cameraManger startCameraCapture];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    takePhotoImage = @"login_icon_viedo_stop";
    takingPhotoImage = @"photo_video_ing";
    
    if (![kUSERDEFAULT_STAND objectForKey:kCAMERASIZETYPE]) {
        
        imageSizeType = CJCCameraSizeTypeFull;
        [kUSERDEFAULT_STAND setObject:@"CJCCameraSizeTypeFull" forKey:kCAMERASIZETYPE];
        
        [kUSERDEFAULT_STAND synchronize];
    }else{
    
        imageSizeType = [self getLocalCameraSizeType];
    }
    
    cycleProgress = 0.0;
    previewHeight = SCREEN_HEIGHT - SCREEN_WITDH*4/3;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification object:nil]; //监听是否触发home键挂起程序.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil]; //监听是否重新进入程序程序.
    
    [self configBeautyCameraManger];
    
    [self setupNaviGationItem];
    
    [self setUpOprationView];
    
    [self cameraRatioWithImageSizeType:imageSizeType isChange11Frame:YES];
    self.effectView.hidden = YES;
}

-(CJCCameraSizeType)getLocalCameraSizeType{

    NSString *typeStr = [kUSERDEFAULT_STAND objectForKey:kCAMERASIZETYPE];
    
    if ([typeStr isEqualToString:@"CJCCameraSizeType34"]) {
        
        return CJCCameraSizeType34;
    }else if ([typeStr isEqualToString:@"CJCCameraSizeTypeFull"]){
    
        return CJCCameraSizeTypeFull;
    }else{
    
        return CJCCameraSizeType11;
    }
}

- (void)applicationWillResignActive:(NSNotification *)noti {
    
    [self reTakePhotoAction];
    [self.cameraManger stopCameraCapture];
}

- (void)applicationDidBecomeActive:(NSNotification *)noti {
    
    [self.cameraManger startCameraCapture];
}

-(void)configBeautyCameraManger{
    
    CJCBeautyCameraManger *manger = [CJCBeautyCameraManger manager];
    
    self.cameraManger = manger;
    
    manger.previewView.frame = self.view.bounds;
    
    [self.view insertSubview:manger.previewView atIndex:1];
 
    self.effectView.frame = self.view.bounds;
    self.effectView.hidden = YES;
}

-(void)setUpOprationView{

    //创建整体提示信息
    UIView *middleHintView = [[UIView alloc] init];
    
    middleHintView.backgroundColor = [UIColor toUIColorByStr:@"1A1A1A" andAlpha:0.35];
    
    middleHintView.frame = CGRectMake(kAdaptedValue(133.5), kAdaptedValue(314.5), kAdaptedValue(108), kAdaptedValue(38));
    
    middleHintView.layer.cornerRadius = kAdaptedValue(4);
    middleHintView.layer.masksToBounds = YES;
    
    self.middleHintView = middleHintView;
    [self.view addSubview:middleHintView];
    
    UILabel *middleHintLabel = [UIView getYYLabelWithStr:@"美颜开启" fontName:kFONTNAMEREGULAR size:12.5 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    middleHintLabel.textAlignment = NSTextAlignmentCenter;
    
    middleHintLabel.frame = CGRectMake(0, kAdaptedValue(8), middleHintView.width, kAdaptedValue(18));
    
    self.middleHintLabel = middleHintLabel;
    [middleHintView addSubview:middleHintLabel];
    
    self.middleHintView.hidden = YES;
    
    //创建操作提示信息View
    UIView *BottpmhintView = [[UIView alloc] init];
    
    BottpmhintView.backgroundColor = [UIColor toUIColorByStr:@"1A1A1A" andAlpha:0.35];
    
    BottpmhintView.frame = CGRectMake(kAdaptedValue(112), kAdaptedValue(523.5), kAdaptedValue(151.5), kAdaptedValue(30));
    
    BottpmhintView.layer.cornerRadius = kAdaptedValue(15);
    BottpmhintView.layer.masksToBounds = YES;
    
    self.bottomHintView = BottpmhintView;
    [self.view addSubview:BottpmhintView];
    
    UILabel *hintLabel = [UIView getYYLabelWithStr:@"轻触拍照 长按录像" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    hintLabel.frame = CGRectMake(0, kAdaptedValue(8), BottpmhintView.width, kAdaptedValue(14));
    
    self.bottomHintLabel = hintLabel;
    [BottpmhintView addSubview:hintLabel];
    
    //创建拍摄图片 视频的imageview
    UIImageView *takePhotoView = [[UIImageView alloc] initWithImage:kGetImage(takePhotoImage)];
    
    takePhotoView.frame = CGRectMake(kAdaptedValue(150.5), kAdaptedValue(575), kAdaptedValue(74), kAdaptedValue(72));
    
    takePhotoView.centerX = self.view.centerX;
    orginCenter = takePhotoView.center;
    
    takePhotoView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhotoAction)];
    
    [takePhotoView addGestureRecognizer:tapGesture];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(takeVideoAction:)];
    
    longGesture.minimumPressDuration = 0.3;
    
    longPressGesture = longGesture;
    [takePhotoView addGestureRecognizer:longGesture];
    
    self.takePhotoVideoImageView = takePhotoView;
    [self.view addSubview:takePhotoView];
    
    self.bottomHintView.bottom = self.takePhotoVideoImageView.y-kAdaptedValue(20);
    
    //打开 关闭美颜的button
    UIButton *beautyIsOpenButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [beautyIsOpenButton setImage:kGetImage(@"photo_icon_beautify_on") forState:UIControlStateNormal];
    [beautyIsOpenButton setImage:kGetImage(@"photo_icon_beautify_off") forState:UIControlStateSelected];
    
    beautyIsOpenButton.frame = CGRectMake(kAdaptedValue(53), kAdaptedValue(589), kAdaptedValue(44), kAdaptedValue(44));
    
    beautyIsOpenButton.right = takePhotoView.left - kAdaptedValue(53.5);
    beautyIsOpenButton.centerY = takePhotoView.centerY;
    
    [beautyIsOpenButton addTarget:self action:@selector(openAndCloseCameraBeauty:) forControlEvents:UIControlEventTouchUpInside];
    
    self.beautyIsOpenButton = beautyIsOpenButton;
    [self.view addSubview:beautyIsOpenButton];
    
    //改变显示图片尺寸的button
    UIButton *imageRatioButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [imageRatioButton setImage:kGetImage(@"photo_icon_full") forState:UIControlStateNormal];
    
    imageRatioButton.frame = CGRectMake(kAdaptedValue(299), kAdaptedValue(599.5), kAdaptedValue(44), kAdaptedValue(44));
    
    imageRatioButton.left = takePhotoView.right + kAdaptedValue(53.5);
    imageRatioButton.centerY = takePhotoView.centerY;
    
    [imageRatioButton addTarget:self action:@selector(cameraRatioChanged) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageRatioButton = imageRatioButton;
    [self.view addSubview:imageRatioButton];
    
    //重新拍摄的button
    UIButton *reTakePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [reTakePhotoButton setImage:kGetImage(@"photo_icon_again") forState:UIControlStateNormal];
    
    reTakePhotoButton.frame = CGRectMake(kAdaptedValue(42), kAdaptedValue(573.5), kAdaptedValue(73), kAdaptedValue(73));
    
    reTakePhotoButton.centerY = takePhotoView.centerY;
    
    [reTakePhotoButton addTarget:self action:@selector(reTakePhotoAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.reTakePhotoButton = reTakePhotoButton;
    [self.view addSubview:reTakePhotoButton];
    
    //确认选择的按钮
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [confirmButton setImage:kGetImage(@"photo_icon_done") forState:UIControlStateNormal];
    
    confirmButton.frame = CGRectMake(kAdaptedValue(260), kAdaptedValue(573.5), kAdaptedValue(73), kAdaptedValue(73));
    confirmButton.centerY = takePhotoView.centerY;
    
    [confirmButton addTarget:self action:@selector(takePhotoActionFinishConfirmAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.confirmButton = confirmButton;
    [self.view addSubview:confirmButton];
    
    [self takeVideoPhotoActionFinish:NO];
}

-(void)middleHintViewShowWithHint:(NSString *)hint{

    self.middleHintLabel.text = hint;
    self.middleHintView.hidden = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        self.middleHintView.hidden = YES;
    });
}



//允许多手势识别
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    
    return YES;
}

-(void)creatPreviewView{
    
    if (videoitem == nil) {
        
        videoitem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:self.exportStr]];
        
        [videoitem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    if (videoPlayer == nil) {
        
        videoPlayer = [[AVPlayer alloc] initWithPlayerItem:videoitem];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:videoitem];
    }
    
    if (videoplayerLayer == nil) {
        
        videoplayerLayer = [AVPlayerLayer playerLayerWithPlayer:videoPlayer];
        
        if(imageSizeType == CJCCameraSizeTypeFull){
        
            videoplayerLayer.frame = self.view.bounds;
        }else {
        
            videoplayerLayer.frame = CGRectMake(0, -kAdaptedValue(k34RatioOffY), SCREEN_WITDH, SCREEN_HEIGHT);
        }
        
        [self.view.layer addSublayer:videoplayerLayer];
    }
    
    [self.view bringSubviewToFront:self.reTakePhotoButton];
    [self.view bringSubviewToFront:self.confirmButton];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        
        if (status == AVPlayerStatusReadyToPlay) {
            
            [videoPlayer play];
        }
    }
    
    if([keyPath isEqualToString:@"adjustingFocus"]){
        BOOL adjustingFocus =[[change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1]];
        
        NSLog(@"adjustingFocus~~%d  change~~%@", adjustingFocus, change);
    }
}

-(void)playbackFinished:(NSNotification *)notification{
    NSLog(@"视频播放完成.");
    
    
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    [videoPlayer play];
}

#pragma mark =======相机的操作
-(void)takePhotoActionFinishConfirmAction{

    if (cameraType == CJCCameraTypePhoto) {
        
        if (self.takePhotoHandle) {
            
            self.takePhotoHandle(takeImage);
        }
        
    }else{
        
        if (self.takeVideoHandle) {
            
            self.takeVideoHandle([self getVideoPreViewImage],self.exportStr);
        }
    }

    [self backButtonDidClick];
}

- (UIImage*)getVideoPreViewImage
{

    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:self.exportStr] options:nil];
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    gen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    
    return img;
}

-(void)reTakePhotoAction{
    
    
    [videoitem removeObserver:self forKeyPath:@"status"];
    [videoplayerLayer removeFromSuperlayer];
    videoitem = nil;
    videoPlayer = nil;
    videoplayerLayer = nil;

    longPressGesture.enabled = YES;
    
    cycleProgress = 0;
    
    [self setTakePhotoViewImage:takePhotoImage];
    
    [self.cameraManger reTakePhoto];
    
    [self takeVideoPhotoActionFinish:NO];
}

-(void)takePhotoAction{
    
    [MBManager showLoadingInView:self.view];
    
    CGSize imageSize;
    
    if (self.bottomCoverView.y == SCREEN_HEIGHT) {
        
        imageSize = self.view.bounds.size;
        
    }else if(self.bottomCoverView.y == SCREEN_HEIGHT - kAdaptedValue(108.5)){
        
        imageSize = CGSizeMake(SCREEN_WITDH, SCREEN_HEIGHT-kAdaptedValue(108.5*2));
        
    }else{
        
        imageSize = CGSizeMake(SCREEN_WITDH, SCREEN_WITDH);
    }
    
    [self.cameraManger takePhotoActionWithSize:imageSize handle:^(UIImage *returnImage) {
        
        [MBManager hideAlert];
        
        takeImage = returnImage;
        
        cameraType = CJCCameraTypePhoto;
        
        [self takeVideoPhotoActionFinish:YES];
        
        //[self writeimagetoLibrary:returnImage];
    }];
}

-(void)writeimagetoLibrary:(UIImage *)image {

    __block ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    [lib writeImageToSavedPhotosAlbum:image.CGImage metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
        
        NSLog(@"assetURL = %@, error = %@", assetURL, error);
        lib = nil;
        
    }];
}

-(void)takeVideoAction:(UILongPressGestureRecognizer *)gesture{

    cameraType = CJCCameraTypeVideo;
    
    if (gesture.state == UIGestureRecognizerStateBegan){
    
        [self middleHintViewShowWithHint:@"开始录制视频"];
        
        [self.cameraManger startRecordVideo];
        
        [self takeVideoBegin];
        [self setTakePhotoViewImage:takingPhotoImage];
    }else if(gesture.state == UIGestureRecognizerStateEnded){
        
        [MBManager showPermanentAlert:@"请等待"];
        
        [self takeVideoEnd];
        
        [self.cameraManger stopCameraCapture];
        
        [self.cameraManger stopRecordVideoHandle:^{
            
            [self videoOprationAction];
        }];
    }else if(gesture.state == UIGestureRecognizerStateCancelled){
    
        [MBManager showPermanentAlert:@"请等待"];
        
        [self takeVideoEnd];
        
        [self.cameraManger stopCameraCapture];
        
        [self.cameraManger stopRecordVideoHandle:^{
            
            [self videoOprationAction];
        }];
        
    }
    
}

-(void)takeVideoBegin{

    self.bottomHintView.hidden = YES;
    
    self.cycleView.hidden = NO;
    [self.cycleView drawProgress:0];
    
    if (holdTimer == nil) {
        
        holdTimer = [NSTimer timerWithTimeInterval:0.01 target:self selector:@selector(changeCycleProgress) userInfo:nil repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:holdTimer forMode:NSRunLoopCommonModes];
    }

}

-(void)changeCycleProgress{

    if (cycleProgress > MAXVIDEOLENGH) {
        
        [self takeVideoEnd];
        
        longPressGesture.enabled = NO;
        
        return;
    }
    
    cycleProgress = cycleProgress+0.01;
    
    [self.cycleView drawProgress:cycleProgress];
}

-(void)takeVideoEnd{

    [holdTimer invalidate];
    holdTimer = nil;
    
    self.cycleView.hidden = YES;
    self.takePhotoVideoImageView.hidden = YES;
    self.beautyIsOpenButton.hidden = YES;
    self.imageRatioButton.hidden = YES;
    self.bottomHintView.hidden = YES;
}

-(void)setTakePhotoViewImage:(NSString *)imageName{

    self.takePhotoVideoImageView.image = kGetImage(imageName);
    
    if ([imageName isEqualToString:takePhotoImage]) {
        
        self.takePhotoVideoImageView.size = CGSizeMake(kAdaptedValue(74), kAdaptedValue(74));
    }else{
    
        self.takePhotoVideoImageView.size = CGSizeMake(kAdaptedValue(90), kAdaptedValue(90));
    }
    
    self.takePhotoVideoImageView.center = orginCenter;
}

-(void)videoOprationAction{

    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        CJCVideoHandleManger *composeCommand = [[CJCVideoHandleManger alloc] init];
        
        composeCommand.videoPaths = @[self.videoStr];
        composeCommand.audioPaths = @[self.audioStr];
        composeCommand.exportPath = self.exportStr;
        
        composeCommand.sizeType = imageSizeType;
        composeCommand.delegate = self;
        
        if (imageSizeType == CJCCameraSizeTypeFull) {
            
            [composeCommand mergeVideostoOnevideoWithFullRatio];
        }else if (imageSizeType == CJCCameraSizeType34){
        
            [composeCommand mergeVideostoOnevideoWith34Ratio];
        }else{
        
            [composeCommand mergeVideostoOnevideoWith11Ratio];
        }
        
    });
}

-(void)videoOprationDidFinish{

    [self performSelectorOnMainThread:@selector(takeVideoFinish) withObject:nil waitUntilDone:NO];
}

-(NSString *)exportStr{

    if (_exportStr == nil) {
        
        NSString *timeStamp = [CJCTools getDateTimeTOMilliSeconds];
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        
         NSString *createPath = [NSString stringWithFormat:@"%@/ROXM_Videos", videoStr];
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        if (![[NSFileManager defaultManager] fileExistsAtPath:createPath]) {
            [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
            
        } else {
            NSLog(@"FileDir is exists.");
        }
        
        videoStr = [NSString stringWithFormat:@"%@/%@.mp4",createPath,timeStamp];
        
        _exportStr = videoStr;
    }
    return _exportStr;
}

-(NSString *)videoStr{
    
    if (_videoStr == nil) {
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/ceshiMovie.mp4",videoStr];
        
        _videoStr = videoStr;
    }
    return _videoStr;
}

-(NSString *)audioStr{
    
    if (_audioStr == nil) {
        
        NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
        videoStr = [NSString stringWithFormat:@"%@/ceshiAudio.wav",videoStr];
        
        _audioStr = videoStr;
    }
    return _audioStr;
}

-(void)takeVideoFinish{

    [self creatPreviewView];
    
    [MBManager hideAlert];
    
    [self takeVideoPhotoActionFinish:YES];
    
    //NSLog(@"%@",[self getVideoPreViewImage]);
    
    //[self writeimagetoLibrary:[self getVideoPreViewImage]];
    
    //__block ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
    //[lib writeVideoAtPathToSavedPhotosAlbum:[NSURL fileURLWithPath:self.exportStr] completionBlock:^(NSURL *assetURL, NSError *error) {
        
      // NSLog(@"assetURL = %@, error = %@", assetURL, error);
      // lib = nil;
    //}];
}

-(void)takeVideoPhotoActionFinish:(BOOL)isFinish{

    self.takePhotoVideoImageView.hidden = isFinish;
    self.beautyIsOpenButton.hidden = isFinish;
    self.imageRatioButton.hidden = isFinish;
    self.bottomHintView.hidden = isFinish;
    
    self.reTakePhotoButton.hidden = !isFinish;
    self.confirmButton.hidden = !isFinish;
}

-(void)rorateCamera{
    
    [UIView animateWithDuration:DURATION animations:^{
        
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.cameraManger.previewView cache:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.cameraManger.beautyCamera rotateCamera];
        });
        
    } completion:^(BOOL finished) {
        
        
    }];
}

-(void)openAndCloseCameraBeauty:(UIButton *)button{

    button.selected = !button.selected;
    
    NSString *hintStr;
    
    if (button.selected) {
        
        hintStr = @"美颜关闭";
        [self.cameraManger closeBeauty];

    }else{
    
        hintStr = @"美颜开启";
        [self.cameraManger openBeauty];
    }
    
    [self middleHintViewShowWithHint:hintStr];
}

-(void)cameraRatioChanged{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if (imageSizeType == CJCCameraSizeTypeFull) {
            
            imageSizeType = CJCCameraSizeType34;
            
            [kUSERDEFAULT_STAND setObject:@"CJCCameraSizeType34" forKey:kCAMERASIZETYPE];
            
        }else if(imageSizeType == CJCCameraSizeType34){
        
            imageSizeType = CJCCameraSizeType11;
            
            [kUSERDEFAULT_STAND setObject:@"CJCCameraSizeType11" forKey:kCAMERASIZETYPE];
            
        }else{
    
            imageSizeType = CJCCameraSizeTypeFull;
            
            [kUSERDEFAULT_STAND setObject:@"CJCCameraSizeTypeFull" forKey:kCAMERASIZETYPE];
        }
        
        [kUSERDEFAULT_STAND synchronize];
        
        [self cameraRatioWithImageSizeType:imageSizeType isChange11Frame:NO];
        
    } completion:^(BOOL finished) {
        
        self.effectView.hidden = YES;
        
        [self.cameraManger startCameraCapture];
        
    }];
    
}

-(void)cameraRatioWithImageSizeType:(CJCCameraSizeType)sizeType isChange11Frame:(BOOL)change{

    if (sizeType == CJCCameraSizeTypeFull) {
        
        self.topCoverView.height = kAdaptedValue(0);
        
        self.bottomCoverView.y = SCREEN_HEIGHT;
        
        [self.cameraManger.previewView setFrame:self.view.bounds];
        
        [self.imageRatioButton setImage:kGetImage(@"photo_icon_full") forState:UIControlStateNormal];
        
        self.cameraManger.sizeType = CJCCameraSizeTypeFull;
        
        self.effectView.hidden = NO;
        self.effectView.frame = self.cameraManger.previewView.frame;
    }else if (sizeType == CJCCameraSizeType34){
    
        self.topCoverView.height = previewHeight/2-kAdaptedValue(k34RatioOffY);
        
        self.bottomCoverView.y = SCREEN_HEIGHT - previewHeight/2-kAdaptedValue(k34RatioOffY);
        
        [self.cameraManger.previewView setFrame:CGRectMake(0, previewHeight/2-kAdaptedValue(k34RatioOffY), SCREEN_WITDH, SCREEN_HEIGHT - previewHeight)];
        
        [self.imageRatioButton setImage:kGetImage(@"photo_icon_34") forState:UIControlStateNormal];
        
        self.cameraManger.sizeType = CJCCameraSizeType34;
        
        self.effectView.hidden = NO;
        self.effectView.frame = self.cameraManger.previewView.frame;
    }else{
    
        CGFloat height = SCREEN_HEIGHT - SCREEN_WITDH;
        
        self.topCoverView.height = kAdaptedValue(height/2)-kAdaptedValue(k34RatioOffY);
        
        self.bottomCoverView.y = SCREEN_WITDH+kAdaptedValue(height/2)-kAdaptedValue(k34RatioOffY);
        
        [self.imageRatioButton setImage:kGetImage(@"photo_icon_11") forState:UIControlStateNormal];
        
        if (change) {
            
            [self.cameraManger.previewView setFrame:CGRectMake(0, previewHeight/2-kAdaptedValue(20), SCREEN_WITDH, SCREEN_HEIGHT - previewHeight+kAdaptedValue(20))];
        }
        
        self.cameraManger.sizeType = CJCCameraSizeType11;
        
        self.effectView.hidden = NO;
        self.effectView.frame = self.cameraManger.previewView.frame;
    }
}

-(UIVisualEffectView *)effectView{

    if (_effectView == nil) {
        
        UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:effect];
        
        [self.view addSubview:effectView];
        
        _effectView = effectView;
    }
    return _effectView;
}

- (BOOL)prefersStatusBarHidden {
    
    return YES;
}

-(void)setupNaviGationItem{
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:kGetImage(@"nav_icon_back_black") forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(backButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    backButton.frame = CGRectMake(kAdaptedValue(16), kAdaptedValue(22), kAdaptedValue(40), kAdaptedValue(40));
    
    [self.view insertSubview:backButton atIndex:3];
    
    UIButton *rorateCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [rorateCameraButton setImage:kGetImage(@"photo_icon_turn") forState:UIControlStateNormal];
    
    [rorateCameraButton addTarget:self action:@selector(rorateCamera) forControlEvents:UIControlEventTouchUpInside];
    
    rorateCameraButton.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(60), 16, kAdaptedValue(44), kAdaptedValue(44));
    
    rorateCameraButton.centerY = backButton.centerY;
    
    [self.view addSubview:rorateCameraButton];
    
    self.topCoverView.height = kAdaptedValue(0);
    
    self.bottomCoverView.y = SCREEN_HEIGHT;
    
}

-(void)backButtonDidClick{

    [self reTakePhotoAction];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(TBCycleView *)cycleView{

    if (_cycleView == nil) {
        
        _cycleView = [[TBCycleView alloc] init];
        
        _cycleView.backgroundColor = [UIColor clearColor];
        
        _cycleView.maxLength = MAXVIDEOLENGH;
        
        _cycleView.frame = CGRectMake(200, 300, kAdaptedValue(100), kAdaptedValue(100));
        
        _cycleView.center = orginCenter;
        
        [self.view addSubview:_cycleView];
    }
    return _cycleView;
}

-(UIView *)topCoverView{

    if (_topCoverView == nil) {
        
        _topCoverView = [[UIView alloc] init];
        
        _topCoverView.backgroundColor = [UIColor blackColor];
        
        _topCoverView.frame = CGRectMake(0, 0, SCREEN_WITDH, 0);
        
        [self.view insertSubview:_topCoverView atIndex:1];
        
    }
    return _topCoverView;
}

-(UIView *)bottomCoverView{

    if (_bottomCoverView == nil) {
        
        _bottomCoverView = [[UIView alloc] init];
        
        _bottomCoverView.backgroundColor = [UIColor blackColor];
        
        _bottomCoverView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, SCREEN_HEIGHT);
        
        [self.view insertSubview:_bottomCoverView atIndex:2];
    }
    return _bottomCoverView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
