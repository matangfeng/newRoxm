//
//  CJCBeautyCameraVC.h
//  roxm
//
//  Created by lfy on 2017/8/25.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CJCtakePhotoHandle)(UIImage * returnImage);
typedef void(^CJCtakeVideoHandle)(UIImage * returnImage,NSString *videoPath);

@interface CJCBeautyCameraVC : UIViewController

@property (nonatomic ,copy) CJCtakePhotoHandle takePhotoHandle;
@property (nonatomic ,copy) CJCtakeVideoHandle takeVideoHandle;

@end
