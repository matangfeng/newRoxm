//
//  CJCPhotoCropManger.m
//  roxm
//
//  Created by lfy on 2017/8/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoCropManger.h"
#import "CJCCommon.h"

@implementation CJCPhotoCropManger

+(UIImage *)getFullImage:(UIImage *)image{

    CJCPhotoCropManger *manger = [[self alloc] init];
    
    CGFloat margin = SCREEN_WITDH/SCREEN_HEIGHT*image.size.height;
    
    CGRect clipRect = CGRectMake((image.size.width-margin)/2, 0, margin, image.size.height);
    
    UIImage *clipImage = [manger clipWithImageRect:clipRect clipImage:image];
    
    return clipImage;
}

+(UIImage *)getEqualWidthHeightImage:(UIImage *)image{

    CJCPhotoCropManger *manger = [[self alloc] init];

    CGFloat margin = image.size.height - image.size.width;
    
    CGRect clipRect = CGRectMake(0, (margin)/2, image.size.width, image.size.width);
    
    UIImage *clipImage = [manger clipWithImageRect:clipRect clipImage:image];
    
    return clipImage;
}

//将需要裁剪的图片重绘成屏幕尺寸的图片（这个方法没用，先留着）
- (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        else{
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}

//将图片裁剪成需要的尺寸
- (UIImage *)clipWithImageRect:(CGRect)clipRect clipImage:(UIImage *)clipImage;
{
    
    CGImageRef  imageRef = CGImageCreateWithImageInRect(clipImage.CGImage, clipRect);
    UIGraphicsBeginImageContext(clipRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, clipRect, imageRef);
    UIImage * clipImage1 = [UIImage imageWithCGImage:imageRef];
    UIGraphicsEndImageContext();
    
    return clipImage1;
}


@end
