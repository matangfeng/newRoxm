//
//  CJCPhotoCropManger.h
//  roxm
//
//  Created by lfy on 2017/8/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CJCPhotoCropManger : NSObject

+(UIImage *)getFullImage:(UIImage *)image;

+(UIImage *)getEqualWidthHeightImage:(UIImage *)image;

@end
