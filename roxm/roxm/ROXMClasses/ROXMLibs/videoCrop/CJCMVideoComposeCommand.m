//
//  CJCMVideoComposeCommand.m
//  roxm
//
//  Created by lfy on 2017/8/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMVideoComposeCommand.h"
#import <AVFoundation/AVFoundation.h>

@implementation CJCMVideoComposeCommand

-(AVMutableComposition *)mergeVideostoOnevideo
{
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableCompositionTrack *b_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    Float64 tmpDuration =0.0f;
    
    for (NSInteger i=0; i<self.videoPaths.count; i++)
    {
        
        NSURL *url = [NSURL fileURLWithPath:self.videoPaths[i]];
        
        AVURLAsset *videoAsset = [[AVURLAsset alloc]initWithURL:url options:nil];
        
        CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero,videoAsset.duration);
        
        NSURL *audioURL = [NSURL fileURLWithPath:self.audioPaths[i]];
        
        AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:audioURL options:nil];
        
        /**
         *  依次加入每个asset
         *
         *  @param TimeRange 加入的asset持续时间
         *  @param Track     加入的asset类型,这里都是video
         *  @param Time      从哪个时间点加入asset,这里用了CMTime下面的CMTimeMakeWithSeconds(tmpDuration, 0),timesacle为0
         *
         */
        NSError *error1,*error2;
        BOOL tbool = [a_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:CMTimeMakeWithSeconds(tmpDuration, 0) error:&error1];
        
        BOOL bbool = [b_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:CMTimeMakeWithSeconds(tmpDuration, 0) error:&error1];
        
        tmpDuration += CMTimeGetSeconds(videoAsset.duration);
        
        NSFileManager *manager = [NSFileManager defaultManager];
        
        [manager removeItemAtPath:self.videoPaths[i] error:nil];
        [manager removeItemAtPath:self.audioPaths[i] error:nil];
    }
    
    return mixComposition; 
}

@end
