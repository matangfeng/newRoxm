//
//  CJCMVideoComposeCommand.h
//  roxm
//
//  Created by lfy on 2017/8/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AVMutableComposition;

@interface CJCMVideoComposeCommand : NSObject

@property (nonatomic ,copy) NSArray *videoPaths;
@property (nonatomic ,copy) NSArray *audioPaths;

-(AVMutableComposition *)mergeVideostoOnevideo;

@end
