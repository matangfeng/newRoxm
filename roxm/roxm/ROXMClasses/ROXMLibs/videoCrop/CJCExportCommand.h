//
//  CJCExportCommand.h
//  roxm
//
//  Created by lfy on 2017/8/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCVideoOprationCommon.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface CJCExportCommand : CJCVideoOprationCommon

@property AVAssetExportSession *exportSession;

@property (nonatomic ,copy) NSString *exportPath;

@end
