//
//  CJCPickerView.m
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPickerView.h"
#import "CJCCommon.h"

@interface CJCPickerView ()

@property (nonatomic ,strong) UIView *containView;

@end

@implementation CJCPickerView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.bounds = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        self.layer.opacity = 0.0;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIView *topView = [[UIView alloc] init];
    
    topView.backgroundColor = [UIColor clearColor];
    
    topView.frame = CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT-kAdaptedValue(319));
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView)];
    
    [topView addGestureRecognizer:tapGesture];
    
    [self addSubview:topView];
    
    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, kAdaptedValue(309))];
    
    containView.backgroundColor = [UIColor whiteColor];
    
    self.containView = containView;
    
    [self addSubview:containView];
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(54), SCREEN_WITDH, kAdaptedValue(250))];
    
    self.pickerView = picker;
    
    [containView addSubview:picker];
    
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(kAdaptedValue(16), 0, kAdaptedValue(54), kAdaptedValue(54))];
    
    UILabel *cancleLabel = [[UILabel alloc] init];
    cancleLabel.text =@"取消";
    cancleLabel.textColor = [UIColor toUIColorByStr:@"222222"];
    cancleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:16];
    
    cancleLabel.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(70), 0, 50, 54);
    [cancleLabel sizeToFit];
    cancleLabel.left = kAdaptedValue(16);
    cancleLabel.centerY = kAdaptedValue(27);
    
    [containView addSubview:cancleLabel];

    [cancelButton addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:cancelButton];
    
    UIButton *confirmButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WITDH-kAdaptedValue(70), 0,kAdaptedValue(54) , kAdaptedValue(54))];
    
    UILabel *confirmLabel = [[UILabel alloc] init];
    confirmLabel.text =@"确定";
    confirmLabel.textColor = [UIColor toUIColorByStr:@"429CF0"];
    confirmLabel.font = [UIFont accodingVersionGetFont_regularWithSize:16];
    
    confirmLabel.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(70), 0, 50, 54);
     [confirmLabel sizeToFit];
    confirmLabel.right = SCREEN_WITDH - kAdaptedValue(16);
    confirmLabel.centerY = kAdaptedValue(27);
    
    [containView addSubview:confirmLabel];
    
    
    [confirmButton addTarget:self action:@selector(clickConfirmButton) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:confirmButton];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, kAdaptedValue(59), SCREEN_WITDH, OnePXLineHeight);
    
    [containView addSubview:lineView];
}

-(void)clickConfirmButton{

    [self dismissPickerView];
}

-(void)showPickerView{

    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setCenter:[UIApplication sharedApplication].keyWindow.center];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
    
    
    CGRect frameContent =  self.containView.frame;
    
    frameContent.origin.y -= self.containView.frame.size.height;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:1.0];
        self.containView.frame = frameContent;
        
    } completion:^(BOOL finished) {
    }];
    
}


-(void)dismissPickerView{

    CGRect frameContent =  self.containView.frame;
    frameContent.origin.y += self.containView.frame.size.height;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.layer setOpacity:0.0];
        self.containView.frame = frameContent;
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
    }];
}

@end
