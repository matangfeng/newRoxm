//
//  CJCWebVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCWebVC.h"
#import "CJCCommon.h"
#import <WebKit/WebKit.h>

@interface CJCWebVC ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>

@property (nonatomic ,strong) WKWebView *webView;

@end

@implementation CJCWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [self showWithLabelAnimation];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64)];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.requestURL]]];
    
    webView.navigationDelegate = self;
    
    self.webView = webView;
    [self.view addSubview:webView];
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    [self hidHUD];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
