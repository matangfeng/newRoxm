//
//  CJCWebVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@interface CJCWebVC : CommonVC

@property (nonatomic ,copy) NSString *requestURL;

@end
