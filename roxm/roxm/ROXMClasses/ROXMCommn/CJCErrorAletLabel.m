//
//  CJCErrorAletLabel.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCErrorAletLabel.h"
#import "CJCCommon.h"

@implementation CJCErrorAletLabel

-(instancetype)init{

    if (self = [super init]) {
        
        self.font = [UIFont accodingVersionGetFont_lightWithSize:12];
        
        self.textColor = [UIColor toUIColorByStr:@"FF3E3E"];
    }
    
    return self;
}

@end
