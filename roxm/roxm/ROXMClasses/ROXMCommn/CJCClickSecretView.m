//
//  CJCClickSecretView.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCClickSecretView.h"
#import "CJCCommon.h"

@interface CJCClickSecretView ()

@property (nonatomic ,strong) UIView *secretView;

@end

@implementation CJCClickSecretView

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)showViewShow{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    [window addSubview:self];
    
    self.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, SCREEN_HEIGHT);
    
    [UIView animateWithDuration:0.01 animations:^{
        
        self.frame = window.bounds;
        
    } completion:^(BOOL finished) {
        
        AppDelegate *dele = (AppDelegate *)[UIApplication sharedApplication].delegate;
        dele.secretView = self;
        
    }];
    
}

-(void)showViewHid{
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, SCREEN_HEIGHT);
        
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        
        AppDelegate *dele = (AppDelegate *)[UIApplication sharedApplication].delegate;
        dele.secretView = nil;
    }];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor blackColor];
        
        UILongPressGestureRecognizer *confirmSecretTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(confirmSecretTapHandle:)];
        
        confirmSecretTap.minimumPressDuration = 2.0;
        
        [self addGestureRecognizer:confirmSecretTap];
        
        [self setUpSecretArea];
    }
    return self;
}

-(void)setUpSecretArea{
    
    NSNumber *viewXNum = [kUSERDEFAULT_STAND objectForKey:@"secretAreaX"];
    NSNumber *viewYNum = [kUSERDEFAULT_STAND objectForKey:@"secretAreaY"];
    
    UIView *secretView = [[UIView alloc] init];
    
    secretView.backgroundColor = [UIColor redColor];
    
    secretView.frame = CGRectMake(viewXNum.floatValue, viewYNum.floatValue, 100, 100);
    
    self.secretView = secretView;
    [self addSubview:secretView];
}

-(void)confirmSecretTapHandle:(UIGestureRecognizer *)tap{
    
    if(tap.state == UIGestureRecognizerStateBegan){
    
        CGPoint centerPoint = [tap locationInView:tap.view];
        
        if (CGRectContainsPoint(self.secretView.frame, centerPoint)){
            
            [self showViewHid];
        }
    }
}

@end
