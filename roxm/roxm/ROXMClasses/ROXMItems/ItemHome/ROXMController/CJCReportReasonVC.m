//
//  CJCReportReasonVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCReportReasonVC.h"
#import "CJCCommon.h"
#import "CJCReportReasonCell.h"
#import "CJCReportSuccessVC.h"
#import "CJCCommentDateVC.h"

#define kTableViewCell          @"UITableViewCell"
#define kReportReasonCell       @"CJCReportReasonCell"

@interface CJCReportReasonVC ()<UITableViewDelegate,UITableViewDataSource>{

    NSArray *dataArray;
    
    NSInteger selectIndex;
}

@end

@implementation CJCReportReasonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    dataArray = @[@"请告诉我们你想举报该用户的理由",@"虚假账号，所示信息非本人",@"威胁辱骂或低俗淫秽用语",@"散布垃圾广告",@"无诚信，应邀不出现",@"约会体验与预期不符",@"酒托、饭托或其它欺诈行为",@"暴力、敲诈勒索等违法行为"];
    
    selectIndex = 1;
    
    [self setUpUI];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return;
    }
    
    if (indexPath.row == selectIndex) {
        
        return;
    }else{
    
        CJCReportReasonCell *cell = (CJCReportReasonCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        cell.isSelect = YES;
        
        CJCReportReasonCell *oldCell = (CJCReportReasonCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectIndex inSection:0]];
        
        oldCell.isSelect = NO;
        
        selectIndex = indexPath.row;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 47;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCell];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
        
        cell.textLabel.textColor = [UIColor toUIColorByStr:@"96A6B9"];
        
        cell.textLabel.text = dataArray[indexPath.row];
        
        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        return cell;
    }else{
    
        CJCReportReasonCell *cell = [tableView dequeueReusableCellWithIdentifier:kReportReasonCell];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.textLabel.text = dataArray[indexPath.row];
        
        if (indexPath.row == 1) {
            
            cell.isSelect = YES;
        }
        
        return cell;
    }
}

-(void)setUpUI{

    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kTableViewCell];
    [tableView registerClass:[CJCReportReasonCell class] forCellReuseIdentifier:kReportReasonCell];
    
    [self.view addSubview:tableView];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UIButton *oprationButton = [UIView getButtonWithStr:@"提交" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    oprationButton.frame = CGRectMake(200, 20, kAdaptedValue(40), kAdaptedValue(40));
    oprationButton.centerY = kNAVIVIEWCENTERY;
    oprationButton.right = SCREEN_WITDH - kAdaptedValue(16);
    
    [oprationButton addTarget:self action:@selector(oprationButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [self.naviView addSubview:oprationButton];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"用户举报"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

-(void)oprationButtonClickHandle{

    [self reportSomeBody];
}

-(void)reportSomeBody{

    NSString *reason = dataArray[selectIndex];
    
    NSString *commitURL= [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/complain"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.targetUID;
    params[@"content"] = reason;
    
    [CJCHttpTool postWithUrl:commitURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            [MBManager showGloomy:@"取消成功"];
            
            CJCReportSuccessVC *nextVC = [[CJCReportSuccessVC alloc] init];
            
            [self presentViewController:nextVC animated:YES completion:^{
                
                if (self.preVC) {
                    
                    [self.preVC reportSuccess];
                }
                
                [self.navigationController popViewControllerAnimated:NO];
            }];
            
        }else{
            
            [MBManager showGloomy:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
