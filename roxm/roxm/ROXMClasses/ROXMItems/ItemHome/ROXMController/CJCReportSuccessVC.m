//
//  CJCReportSuccessVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCReportSuccessVC.h"
#import "CJCCommon.h"

@interface CJCReportSuccessVC ()

@end

@implementation CJCReportSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)setUpUI{

    UIImageView *middleImageView = [[UIImageView alloc] init];
    
    middleImageView.backgroundColor = [UIColor redColor];
    
    middleImageView.frame = CGRectMake(137, 173, kAdaptedValue(100), kAdaptedValue(100));
    
    middleImageView.centerX = self.view.centerX;
    middleImageView.y = kAdaptedValue(174);
    
    [self.view addSubview:middleImageView];
    
    UILabel *middleLable = [UIView getSystemLabelWithStr:@"举报已提交，我们将尽快处理" fontName:kFONTNAMEREGULAR size:21 color:[UIColor toUIColorByStr:@"3C4F5E"]];
    
    middleLable.textAlignment = NSTextAlignmentCenter;
    
    middleLable.frame = CGRectMake(0, 334, SCREEN_WITDH, kAdaptedValue(30));
    middleLable.y = middleImageView.bottom + kAdaptedValue(60);
    
    [self.view addSubview:middleLable];
    
    NSString *middleStr = @"《濡沫平台行为规范》";
    
    UIFont *font = [UIFont accodingVersionGetFont_regularWithSize:14];
    
    UIButton *blueButton = [UIView getButtonWithStr:middleStr fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    CGFloat buttonWidth = [CJCTools getShortStringLength:middleStr withFont:font];
    
    blueButton.frame = CGRectMake(117, 378, buttonWidth, kAdaptedValue(21));
    blueButton.centerX = self.view.centerX;
    blueButton.y = middleLable.bottom+kAdaptedValue(15);
    
    [self.view addSubview:blueButton];
    
    UILabel *leftLabel = [UIView getSystemLabelWithStr:@"我们会依据" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"94A4B7"]];
    
    CGFloat lableWidth = (SCREEN_WITDH - buttonWidth)/2;
    
    leftLabel.textAlignment = NSTextAlignmentRight;
    
    leftLabel.frame = CGRectMake(0, 378, lableWidth, kAdaptedValue(21));
    
    leftLabel.centerY = blueButton.centerY;
    
    [self.view addSubview:leftLabel];
    
    UILabel *rightLabel = [UIView getSystemLabelWithStr:@"进行审核，" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"94A4B7"]];
    
    rightLabel.textAlignment = NSTextAlignmentLeft;
    
    rightLabel.frame = CGRectMake(0, 378, lableWidth, kAdaptedValue(21));
    
    rightLabel.x = blueButton.right;
    rightLabel.centerY = blueButton.centerY;
    
    [self.view addSubview:rightLabel];
    
    UILabel *bottomLabel = [UIView getSystemLabelWithStr:@"感谢您的举报" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"94A4B7"]];
    
    bottomLabel.textAlignment = NSTextAlignmentCenter;
    
    bottomLabel.frame = CGRectMake(0, 378, SCREEN_WITDH, kAdaptedValue(21));
    
    bottomLabel.y = blueButton.bottom;
    
    [self.view addSubview:bottomLabel];
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UIButton *oprationButton = [UIView getButtonWithStr:@"完成" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    oprationButton.frame = CGRectMake(200, 20, kAdaptedValue(40), kAdaptedValue(40));
    oprationButton.centerY = kNAVIVIEWCENTERY;
    oprationButton.right = SCREEN_WITDH - kAdaptedValue(16);
    
    [oprationButton addTarget:self action:@selector(oprationButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [self.naviView addSubview:oprationButton];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"举报成功"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

-(void)oprationButtonClickHandle{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
