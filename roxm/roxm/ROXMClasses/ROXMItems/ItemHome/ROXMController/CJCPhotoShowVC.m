//
//  CJCPhotoShowVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoShowVC.h"
#import "CJCCommon.h"
#import "CJCPhotoShowCell.h"
#import "CJCPhotoShowModel.h"
#import "CJCPhotoOprationHandle.h"

#define kPhotoShowCell          @"CJCPhotoShowCell"

@interface CJCPhotoShowVC ()<UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegate>{

    //当前collectionview展示的item 的index
    NSInteger currentItemIndex;
}

@property (nonatomic ,strong) UICollectionView *showCollectionView;

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *describLabel;

@property (nonatomic ,strong) CJCPhotoShowCell *selectCell;

@property (nonatomic ,strong) UITextView *editView;

@property (nonatomic ,strong) UIView *editInputView;

@end

@implementation CJCPhotoShowVC

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    [self scrollToIndex:self.currentIndex];
    currentItemIndex = self.currentIndex;
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",self.currentIndex+1,self.imageArr.count];
    
    [self changeDescribLabel];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)name:UIKeyboardWillHideNotification object:nil];
    
    [self setUpUI];
}

-(void)scrollToIndex:(NSInteger)index{

    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    [self.showCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{

    [self.selectCell videoStop];
    self.selectCell = nil;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat xOffset = scrollView.contentOffset.x;
    
    self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",(NSInteger)(xOffset/SCREEN_WITDH)+1,self.imageArr.count];
    
    currentItemIndex = (NSInteger)(xOffset/SCREEN_WITDH);
    
    [self changeDescribLabel];
}

-(void)changeDescribLabel{

    CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
    
    self.describLabel.text = model.text;
    
    self.describLabel.centerY = 30;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.editInputView.y<SCREEN_HEIGHT) {
        
        [self.view endEditing:YES];
        
        return;
    }
    
    CJCPhotoShowModel *tempModel = self.imageArr[indexPath.item];
    
    if (tempModel.type == 20) {
        
        return;
    }else{
    
        CJCPhotoShowCell *cell = (CJCPhotoShowCell *)[collectionView cellForItemAtIndexPath:indexPath];
        
        self.selectCell = cell;
        
        [cell videoPlay];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.imageArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    CJCPhotoShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPhotoShowCell forIndexPath:indexPath];
    
    CJCPhotoShowModel *model = self.imageArr[indexPath.row];
    
    cell.showModel = model;
    
    return cell;
}

-(void)setUpUI{

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    flowLayout.itemSize = CGSizeMake(SCREEN_WITDH, SCREEN_HEIGHT);
    
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT) collectionViewLayout:flowLayout];;
    
    imageCollectionView.backgroundColor = [UIColor whiteColor];
    
    imageCollectionView.dataSource = self;
    imageCollectionView.delegate = self;
    
    imageCollectionView.pagingEnabled = YES;
    
    [imageCollectionView registerClass:[CJCPhotoShowCell class] forCellWithReuseIdentifier:kPhotoShowCell];
    
    self.showCollectionView = imageCollectionView;
    [self.view addSubview:imageCollectionView];
    
    UIView *topView = [[UIView alloc] init];
    
    topView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.7];
    
    topView.frame = CGRectMake(0, 0, SCREEN_WITDH, 59);
    
    [self.view insertSubview:topView aboveSubview:imageCollectionView];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:kGetImage(@"nav_icon_back_black") forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(3, 9.5, 40, 40);
    
    [backButton addTarget:self action:@selector(backButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [topView addSubview:backButton];
    
    NSString *title = [NSString stringWithFormat:@"%ld/%ld",self.imageArr.count,self.imageArr.count];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:title fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    titleLabel.size = CGSizeMake(50, 17);
    titleLabel.centerX = self.view.centerX;
    titleLabel.centerY = 30;
    
    self.titleLabel = titleLabel;
    [topView addSubview:titleLabel];
    
    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.backgroundColor = [UIColor toUIColorByStr:@"000000" andAlpha:0.7];
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-80, SCREEN_WITDH, 80);
    
    [self.view insertSubview:bottomView aboveSubview:imageCollectionView];
    
    UILabel *describLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor whiteColor]];
    
    describLabel.frame = CGRectMake(14, 18, SCREEN_WITDH-70, 45);
    
    self.describLabel = describLabel;
    [bottomView addSubview:describLabel];
    
    if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
        
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [deleteButton setImage:kGetImage(@"nav_icon_more") forState:UIControlStateNormal];
        
        deleteButton.size = CGSizeMake(40, 40);
        deleteButton.centerY = 30;
        deleteButton.right = SCREEN_WITDH - 15;
        
        [deleteButton addTarget:self action:@selector(deleteButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [topView addSubview:deleteButton];
    
        UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [editButton setImage:kGetImage(@"nav_icon_more") forState:UIControlStateNormal];
        
        editButton.size = CGSizeMake(40, 40);
        editButton.centerY = 40;
        editButton.right = SCREEN_WITDH - 15;
        
        [editButton addTarget:self action:@selector(editButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [bottomView addSubview:editButton];
    }
}

-(void)editButtonClickHandle{

    [self.editView becomeFirstResponder];
}

-(void)deleteButtonClickHandle{

    if (self.imageArr.count<6) {
        
        [MBManager showBriefAlert:@"相册不得少于5张图片"];
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"确定要删除图片吗?" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        [MBManager showLoading];
        
        CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
        
        [CJCPhotoOprationHandle deletePhotoWithPhotoId:model.photoId success:^(id responsObject) {
            
            [MBManager hideAlert];
            
            NSNumber *rtNum = responsObject[@"rt"];
            
            if (rtNum.intValue == 0) {
                
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.imageArr];
                
                [tempArr removeObject:model];
                
                self.imageArr = tempArr.copy;
                
                self.titleLabel.text = [NSString stringWithFormat:@"%ld/%ld",currentItemIndex+1,self.imageArr.count];
                
                [self.showCollectionView reloadData];
                
                if (self.refreshHandle) {
                    
                    self.refreshHandle();
                }
                
            }else{
            
                [MBManager showBriefAlert:responsObject[@"msg"]];
            }
        }];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)backButtonClickHandle{

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  =====监听键盘的弹起 收回
- (void)keyboardWillShow:(NSNotification *)aNotification{
    //获取键盘的高度
    NSValue *aValue = [[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    self.editInputView.y = SCREEN_HEIGHT - self.editInputView.height - keyboardRect.size.height;
    
}

- (void)keyboardWillHide:(NSNotification *)aNotification{
    
    self.editInputView.y = SCREEN_HEIGHT;
}

-(UIView *)editInputView{

    if (_editInputView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WITDH, 60);
        
        [self.view insertSubview:view aboveSubview:self.showCollectionView];
        
        _editInputView = view;
        
        UIButton *completeButton = [UIView getButtonWithStr:@"完成" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
        
        completeButton.size = CGSizeMake(40, 20);
        completeButton.centerY = 30;
        completeButton.right = SCREEN_WITDH - 8;
        
        [completeButton addTarget:self action:@selector(completeButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:completeButton];
    }
    return _editInputView;
}

-(void)completeButtonClickHandle{

    if ([CJCTools isBlankString:self.editView.text]) {
        
        [MBManager showBriefAlert:@"请输入需要添加的描述"];
        
        return ;
    }
    
    [MBManager showLoading];
    CJCPhotoShowModel *model = self.imageArr[currentItemIndex];
    
    [CJCPhotoOprationHandle editCommentWithPhotoId:model.photoId withComment:self.editView.text success:^(id responsObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responsObject[@"rt"];
        
        if (rtNum.intValue == 0) {
            
            model.text = self.editView.text;
            
            [self changeDescribLabel];
            
            [self.view endEditing:YES];
            
            self.editView.text = @"";
            
        }else{
        
            [MBManager showBriefAlert:responsObject[@"msg"]];
        }
    }];
    
}

-(UITextView *)editView{

    if (_editView == nil) {
        
        UITextView *textView = [[UITextView alloc] init];
        
        textView.frame = CGRectMake(kAdaptedValue(14), 10, SCREEN_WITDH-kAdaptedValue(60), kAdaptedValue(45));
        
        textView.textColor = [UIColor toUIColorByStr:@"333333"];
        
        textView.backgroundColor = [UIColor toUIColorByStr:@"F8F8F8"];
        
        [self.editInputView addSubview:textView];
        
        _editView = textView;
    }
    return _editView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
