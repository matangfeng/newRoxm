//
//  CJCPersonalInfoVC.m
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonalInfoVC.h"
#import "CJCCommon.h"
#import "CJCPersonalInfoNameCell.h"
#import "CJCpersonalInfoModel.h"
#import "CJCPersonalInfoInfoCell.h"
#import "CJCPersonalInfoImagesCell.h"
#import "CJCSingleChatVC.h"
#import <EaseUI.h>
#import "CJCPhotoShowVC.h"
#import "CJCPhotoShowModel.h"
#import "CJCPhotoLibraryVC.h"
#import "CJCCommentModel.h"
#import "CJCCommentTableCell.h"
#import "XTPopView.h"
#import "CJCReportReasonVC.h"
#import "CJCInviteCreatOrderVC.h"
#import "CJCPersonImagesCollectionCell.h"
#import "CJCPhotoShowView.h"
#import "CJCReportAndDefriendVC.h"
#import <ReactiveObjC.h>

#define BottomButtonMargin  kAdaptedValue(14)
#define kPersonalInfoNameCell       @"CJCPersonalInfoNameCell"
#define kPersonalInfoInfoCell       @"CJCPersonalInfoInfoCell"
#define kPersonalInfoImagesCell     @"CJCPersonalInfoImagesCell"
#define kCommentTableCell           @"CJCCommentTableCell"

@interface CJCPersonalInfoVC ()<UITableViewDelegate,UITableViewDataSource,PersonalInfoImagesCellDelegate,SelectIndexPathDelegate>{

    CJCpersonalInfoModel *infoModel;
    
    NSMutableArray *imagesArray;
    
    NSMutableArray *fullImagesArr;
    
    NSMutableArray *commentArray;
    
    CGFloat collectionViewCellWidth;
    
    CGFloat commentTBHeight;
    
    BOOL isfinishLoadImage;
    
    BOOL isRefreshData;
    
    BOOL isfollowRelation;
    
    BOOL isBlackRelation;
    
    CGFloat _saveHeight;
}

@property (nonatomic ,strong) UITableView *tableView;

@property (atomic ,assign) NSInteger requestIndex;

@property (nonatomic ,strong) UICollectionView *imagesCollectionView;

@property (nonatomic ,strong) UIButton *talkButton;

@property (nonatomic ,strong) UIButton *inviteButton;

@end

@implementation CJCPersonalInfoVC

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if (isRefreshData) {
        
        [self getRelationship];
        
        [self getUserRelation];
        
        [self getPersonalInfo];
        
        [self getPersonalImages];
        
        [self getCommentList];
    }
}

- (void)controRight
{
    UIImage * img = [UIImage imageNamed:@"nav_icon_more"];
    UIButton * loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginButton setImage:img forState:0];
    [loginButton setImage:[self imageByApplyingAlpha:0.5 image:img] forState:1];
    [loginButton setImageEdgeInsets:(UIEdgeInsetsMake(0, 15, 0, -10))];
    loginButton.frame = CGRectMake(0, 0, 44, 44);
    [loginButton setTitleColor:[UIColor yellowColor] forState:(UIControlStateNormal)];
    [[loginButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        CJCReportAndDefriendVC *nextVC = [[CJCReportAndDefriendVC alloc] init];
        nextVC.targetUid = self.otherUID;
        nextVC.targetEMID = infoModel.emId;
        [self.navigationController pushViewController:nextVC animated:YES];
    }];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
    self.navigationItem.rightBarButtonItem = barButton;//为导航栏添加右侧按钮
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self controRight];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"CJCdeFriendHandle" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        CJCPersonalInfoNameCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.followButton.hidden =  YES;
        self.talkButton.hidden = YES;
        self.inviteButton.hidden = YES;
    }];
    
    [[[NSNotificationCenter defaultCenter ] rac_addObserverForName:@"CJCCancledeFriendHandle" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        CJCPersonalInfoNameCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.followButton.hidden =  NO;
        self.talkButton.hidden = NO;
        self.inviteButton.hidden = NO;
    }];
    imagesArray = [NSMutableArray array];
    commentArray = [NSMutableArray array];
    fullImagesArr = [NSMutableArray array];
    
    isRefreshData = YES;
    isfinishLoadImage = NO;
    self.requestIndex = 0;
    
    collectionViewCellWidth = (SCREEN_WITDH)/3;
    
    [self setUpNaviView];
    [self configTableView];
}

-(void)getUserRelation{
    
    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/is_black"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0是黑名单  1不是黑名单
            NSNumber *dataNum = responseObject[@"data"];
            
            isBlackRelation = dataNum.integerValue;
            
            self.requestIndex++;
            
            [self dataIsFinishLoad];
            
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getRelationship{

    [MBManager showLoadingInView:self.view];
    
    NSString *infoURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/is_follow"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:infoURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0是黑名单  1不是黑名单
            NSNumber *dataNum = responseObject[@"data"];
            
            isRefreshData = NO;
            
            isfollowRelation = dataNum.integerValue;
            
            self.requestIndex++;
            
            [self dataIsFinishLoad];
            
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

//获取个人信息
-(void)getPersonalInfo{

    [MBManager showLoadingInView:self.view];
    
    NSString *infoURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/get/other"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:infoURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
        
            isRefreshData = NO;
            
            infoModel = [CJCpersonalInfoModel modelWithDictionary:responseObject[@"data"][@"user"]];
            CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

            if ([localInfoModel.uid isEqualToString:infoModel.uid]) {
                
                infoModel.infoVCType = CJCPersonalInfoVCTypeOwn;
            }else{
            
                infoModel.infoVCType = self.infoVCType;
            }
            
            [MBManager hideAlert];
            
            self.requestIndex++;
            
            [self dataIsFinishLoad];
        }else{
            
            [MBManager hideAlert];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

//获得图片  视频 列表
-(void)getPersonalImages{

    [MBManager showLoadingInView:self.view];
    
    NSString *imagesURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/list/photo"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:imagesURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
        
            isfinishLoadImage = YES;
            isRefreshData = NO;
            
            [imagesArray removeAllObjects];
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCPhotoShowModel class] json:responseObject[@"data"]];
            
            [fullImagesArr addObjectsFromArray:tempArr];
            
            [imagesArray addObjectsFromArray:tempArr];
//            [imagesArray removeFirstObject];
            
            if (imagesArray.count > 17) {
                
                NSArray *tempArr1 = [imagesArray subarrayWithRange:NSMakeRange(0, 17)];
                
                [imagesArray removeAllObjects];
                [imagesArray addObjectsFromArray:tempArr1];
            }
            
            [MBManager hideAlert];
            
            self.requestIndex++;
            
            [self dataIsFinishLoad];
            
        }else{
            
            [MBManager hideAlert];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getCommentList{
    
    NSString *commentURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/comment/list/by_other"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"userId"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:commentURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            isRefreshData = NO;
            
            [CJCCommentModel getFormatModel:responseObject[@"data"] finish:^(NSArray *modelArr) {
               
                for (CJCCommentModel *mode in modelArr) {
                    
                    commentTBHeight = commentTBHeight+mode.cellHeight;
                }
                
                if (self.infoVCType != CJCPersonalInfoVCTypeOwn){
                
                    [commentArray addObjectsFromArray:modelArr];
                }
                
                self.requestIndex++;
                
                [self dataIsFinishLoad];
//                [self.tableView reloadRow:3 inSection:0 withRowAnimation:NO];
            }];
            
            
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)dataIsFinishLoad{

    if (self.requestIndex == 5) {
        
        if (![CJCTools isBlankString:infoModel.videoUrl]) {
            
            CJCPhotoShowModel *model = [[CJCPhotoShowModel alloc] init];
            model.url = infoModel.videoUrl;
            model.type = 21;
            
            [imagesArray insertObject:model atIndex:0];
            [fullImagesArr insertObject:model atIndex:0];
        }
        
        [self.tableView reloadData];
        
        if (self.infoVCType == CJCPersonalInfoVCTypeOther) {
            
            [self setUpBottomButton];
        }
    }
}

#pragma mark =======tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (self.requestIndex == 5){
    
        return 4;
    }
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return kAdaptedValue(143);
    }else if (indexPath.row == 1){
    
        if (isfinishLoadImage) {
            
            if (imagesArray.count == 0) {
                
                return collectionViewCellWidth;
            }else if (imagesArray.count<=3) {
                
                return collectionViewCellWidth+kAdaptedValue(44);
            }
            
            return 2*collectionViewCellWidth+kAdaptedValue(47);
        }else{
        
            return collectionViewCellWidth;
        }
    }else if (indexPath.row == 2){
    
        return _saveHeight;
    }else{
    
        return commentTBHeight;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        CJCPersonalInfoNameCell *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalInfoNameCell];
        
        cell.infoModel = infoModel;
        
        if (isfollowRelation == 0) {
            
            cell.relationStr = @"关注";
        }else{
        
            cell.relationStr = @"已关注";
        }
        
        __weak __typeof(self) weakSelf = self;
        
        cell.followHandle = ^(NSString *relation) {
            
            if ([relation isEqualToString:@"关注"]) {
                
                [weakSelf followHandle];
                
            }else{
            
                [weakSelf cancleFollowHandle];
            }
            
        };
        
        cell.tapHandle = ^(CJCPersonalInfoNameCell *cell) {
            
            UIWindow* window = [UIApplication sharedApplication].keyWindow;
            CGRect rectInWindow = [cell convertRect:cell.cycleIconImageView.frame toView:window];
            
            CJCPhotoShowView *showView = [[CJCPhotoShowView alloc] init];
            
            CJCPhotoShowModel *tempModel = [[CJCPhotoShowModel alloc]init];
            tempModel.type = 20;
            tempModel.url = infoModel.imageUrl;
            tempModel.thumbnilImage = cell.cycleIconImageView.image;
            tempModel.width = SCREEN_WITDH;
            tempModel.height = SCREEN_WITDH;
            
            showView.imageArr = @[tempModel];
            showView.currentIndex = 0;
            showView.showType = CJCPhotoShowViewTypeIcon;
            showView.originFrame = rectInWindow;
            
            [showView showViewShow];
        };
        
        return cell;
        
    }else if (indexPath.row == 1){
    
        CJCPersonalInfoImagesCell *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalInfoImagesCell];
        
        if (isfinishLoadImage) {
            
            cell.delegate = self;
            cell.itemHeight = collectionViewCellWidth;
            cell.infoModel = infoModel;
            cell.imagesArr = imagesArray;
            self.imagesCollectionView = cell.collectionView;
        }
        
        return cell;
    
    }else if (indexPath.row ==2){
    
        CJCPersonalInfoInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:kPersonalInfoInfoCell];
        
        cell.infoModel = infoModel;
        
        _saveHeight = CGRectGetMaxY(cell.saveBottomLine.frame) + 1;
        
        return cell;
    }else{
    
        CJCCommentTableCell *cell = [tableView dequeueReusableCellWithIdentifier:kCommentTableCell];
        
        cell.cellHeight = commentTBHeight;
        cell.comments = commentArray;
        
        return cell;
    
    }
}

-(void)followHandle{
    
    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/follow"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0是黑名单  1不是黑名单
            isfollowRelation = 1;
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
        }else{
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)cancleFollowHandle{
    
    [self showWithLabelAnimation];
    
    NSString *relationURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user_relationship/cancel/follow"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"targetUid"] = self.otherUID;
    
    [CJCHttpTool postWithUrl:relationURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            //不是黑名单关系 0不是黑名单  1是黑名单
            isfollowRelation = 0;
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
        }else{
            
            [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationNone];
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

#pragma mark ========cell的代理方法
-(void)imageCellClickHandle:(NSInteger)index{

    CJCPersonImagesCollectionCell *cell = (CJCPersonImagesCollectionCell*)[self.imagesCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    CGRect rectInWindow = [cell convertRect:cell.personalImageView.frame toView:window];
    
    CJCPhotoShowView *showView = [[CJCPhotoShowView alloc] init];
    
    showView.imageArr = imagesArray.copy;
    showView.currentIndex = index;
    
    if (self.infoVCType == CJCPersonalInfoVCTypeOwn) {
        
        showView.showType = CJCPhotoShowViewTypeCanOpration;
    }else{
    
        showView.showType = CJCPhotoShowViewTypeOnlyPreview;
    }
    showView.originFrame = rectInWindow;
    
    showView.scrollHandle = ^(NSInteger scrollIndex) {
        
        CJCPersonImagesCollectionCell *cell1 = (CJCPersonImagesCollectionCell*)[self.imagesCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:scrollIndex inSection:0]];
        
        UIWindow* window1 = [UIApplication sharedApplication].keyWindow;
        CGRect rectInWindow1 = [cell1 convertRect:cell.personalImageView.frame toView:window1];
        
        showView.originFrame = rectInWindow1;
    };
    
    [showView showViewShow];
}

-(void)openPhotoLibraryHandle{

    CJCPhotoLibraryVC *nextVC = [[CJCPhotoLibraryVC alloc] init];
    
    nextVC.imageArray = fullImagesArr.copy;
    nextVC.infoModel = infoModel;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark ========button的点击事件
//-(void)moreButtonDidClick{
//
//    CJCReportAndDefriendVC *nextVC = [[CJCReportAndDefriendVC alloc] init];
//
//    nextVC.targetUid = self.otherUID;
//    nextVC.targetEMID = infoModel.emId;
//
//    [self.navigationController pushViewController:nextVC animated:YES];
//
////    CGPoint point = CGPointMake(SCREEN_WITDH - 25, 65);
////
////    XTPopTableView *popView = [[XTPopTableView alloc] initWithOrigin:point Width:120 Height:30 * 3 Type:XTTypeOfUpRight Color:[UIColor colorWithRed:0.2737 green:0.2737 blue:0.2737 alpha:1.0]];
////    popView.dataArray       = @[@"举报", @"添加黑名单", @"取消黑名单"];
////    //    popView.images          = @[@"添加朋友", @"扫一扫", @"付款"];
////    popView.row_height      = 30;
////    popView.delegate        = self;
////    popView.titleTextColor  = [UIColor colorWithRed:0.2669 green:0.765 blue:1.0 alpha:1.0];
////    [popView popView];
//}

- (void)selectIndexPathRow:(NSInteger )index
{
    switch (index) {
        case 0:{
        
            CJCReportReasonVC *nextVC = [[CJCReportReasonVC alloc] init];
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
            break;
            
        case 1:{
            
            
        }
            break;
            
        case 2:{
            
            
        }
            break;
            
        default:
            break;
    }
}

-(void)setUpNaviView{
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    self.keyBoardHid = YES;
}

#pragma tableView
-(void)configTableView{

    UITableView * tableView = [[UITableView alloc] init];
    
    if (self.infoVCType == CJCPersonalInfoVCTypeOther) {
        tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64 - kAdaptedValue(60));
    }else{
        tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCPersonalInfoNameCell class] forCellReuseIdentifier:kPersonalInfoNameCell];
    [tableView registerClass:[CJCPersonalInfoInfoCell class] forCellReuseIdentifier:kPersonalInfoInfoCell];
    [tableView registerClass:[CJCPersonalInfoImagesCell class] forCellReuseIdentifier:kPersonalInfoImagesCell];
    [tableView registerClass:[CJCCommentTableCell class] forCellReuseIdentifier:kCommentTableCell];
}

- (void)setUpBottomButton{
    if (isBlackRelation == 1) {
        return;
    }
    if (infoModel.sex == 0) {
        CGFloat buttonWidth = (SCREEN_WITDH - 3*BottomButtonMargin)/2;
        CGFloat buttonHeight = kAdaptedValue(44.5);
        for (int i=0; i<2; i++) {
            UIButton *talkButton = [UIButton buttonWithType:UIButtonTypeCustom];
            talkButton.frame = CGRectMake(BottomButtonMargin+(buttonWidth+BottomButtonMargin)*i, 600, buttonWidth, buttonHeight);
            talkButton.bottom = SCREEN_HEIGHT - kAdaptedValue(10);
            talkButton.layer.cornerRadius = kAdaptedValue(6);
            talkButton.layer.masksToBounds = YES;
            talkButton.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
            [talkButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
            [talkButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5]  forState:1];
            [self.view addSubview:talkButton];
            
            if (i == 0) {
                [talkButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"1F1C1B"]] forState:0];
                [talkButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"1F1C1B" andAlpha:0.5]] forState:UIControlStateSelected];
                
                [talkButton setImage:kGetImage(@"preson_tapbar_talk") forState:UIControlStateNormal];
                [talkButton setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"preson_tapbar_talk")] forState:1];
                
                [talkButton setTitle:@"对话" forState:UIControlStateNormal];
                
                [[talkButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    CJCSingleChatVC *chatController = [[CJCSingleChatVC alloc] init];
                    chatController.conversationId = infoModel.emId;
                    chatController.location = self.location;
                    chatController.qunjuId = self.otherUID;
                    chatController.infoModel = infoModel;
                    chatController.nickName = infoModel.nickname;
                    chatController.iconImageUrl = infoModel.imageUrl;
                    [self.navigationController pushViewController:chatController animated:YES];
                }];
                
                self.talkButton = talkButton;
            }else{
                [talkButton setImage:kGetImage(@"preson_tapbar_invite") forState:UIControlStateNormal];
                [talkButton setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"preson_tapbar_invite")] forState:1];

                [talkButton setTitle:@"邀约" forState:UIControlStateNormal];
                [talkButton setTitle:@"邀约" forState:1];
                [talkButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:UIControlStateNormal];
                [talkButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.5]] forState:UIControlStateSelected];
                
                [[talkButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
                    CJCInviteCreatOrderVC *nextVC = [[CJCInviteCreatOrderVC alloc] init];
                    nextVC.images = fullImagesArr.copy;
                    nextVC.infoModel = infoModel;
                    nextVC.location = self.location;
                    nextVC.infoVC = self;
                    [self.navigationController pushViewController:nextVC animated:YES];
                }];
                self.inviteButton = talkButton;
            }
        }
    }else{
        CGFloat buttonWidth = SCREEN_WITDH - 2*kAdaptedValue(21);
        CGFloat buttonHeight = kAdaptedValue(44.5);
        UIButton *manButton = [UIButton buttonWithType:UIButtonTypeCustom];
        manButton.frame = CGRectMake(kAdaptedValue(21), 600, buttonWidth, buttonHeight);
        manButton.bottom = SCREEN_HEIGHT - kAdaptedValue(10);
        manButton.layer.cornerRadius = kAdaptedValue(6);
        manButton.layer.masksToBounds = YES;
        manButton.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
        [manButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
        [manButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
        [manButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"4BD3C5"]] forState:0];

        [manButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"4BD3C5" andAlpha:0.5]] forState:UIControlStateSelected];
        
        [manButton setImage:kGetImage(@"preson_tapbar_talk") forState:UIControlStateNormal];
        [manButton setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"preson_tapbar_talk")] forState:UIControlStateNormal];
        [manButton setTitle:@"对话" forState:UIControlStateNormal];
        self.talkButton = manButton;
        [self.view addSubview:manButton];
        [[manButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
            CJCInviteCreatOrderVC *nextVC = [[CJCInviteCreatOrderVC alloc] init];
            nextVC.images = fullImagesArr.copy;
            nextVC.infoModel = infoModel;
            nextVC.location = self.location;
            nextVC.infoVC = self;
            [self.navigationController pushViewController:nextVC animated:YES];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}

@end
