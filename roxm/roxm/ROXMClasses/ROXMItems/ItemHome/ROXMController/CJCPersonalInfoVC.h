//
//  CJCPersonalInfoVC.h
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//



#import "CommonVC.h"
#import "CJCpersonalInfoModel.h"

@class CLLocation;

@interface CJCPersonalInfoVC : CommonVC

@property (nonatomic ,assign) CJCSexType sexType;

@property (nonatomic ,assign) CJCPersonalInfoVCType infoVCType;

@property (nonatomic ,copy) NSString *otherUID;

@property (nonatomic ,strong) CLLocation *location;

//-(void)talkButtonDidClick;

@end
