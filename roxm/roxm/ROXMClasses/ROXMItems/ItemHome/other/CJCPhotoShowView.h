//
//  CJCPhotoShowView.h
//  roxm
//
//  Created by 陈建才 on 2017/10/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJCpersonalInfoModel.h"

typedef NS_ENUM(NSInteger,CJCPhotoShowViewType){
    
    CJCPhotoShowViewTypeIcon                        = 2,//头像预览
    CJCPhotoShowViewTypeCanOpration                 = 1,//自己的相册预览  可以操作
    CJCPhotoShowViewTypeOnlyPreview                 = 0,//观看别人的相册  只可预览
};

typedef void(^CJCPhotoScrollHandle)(NSInteger scrollIndex);

@interface CJCPhotoShowView : UIView

@property (nonatomic ,copy) NSArray *imageArr;

@property (nonatomic ,assign) NSInteger currentIndex;

@property (nonatomic ,assign) CJCPhotoShowViewType showType;

@property (nonatomic ,assign) CGRect originFrame;

@property (nonatomic ,copy) CJCPhotoScrollHandle  scrollHandle;

@property (nonatomic ,strong) UIImage *thumbnilImage;

-(void)reloadData;

-(void)showViewShow;

-(void)showViewHid;

@end
