//
//  CJCPhotoOprationHandle.h
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^oprationSuccessHandle)(id responsObject);

@interface CJCPhotoOprationHandle : NSObject

+(void)deletePhotoWithPhotoId:(NSString *)photoID success:(oprationSuccessHandle)success;

+(void)editCommentWithPhotoId:(NSString *)photoID withComment:(NSString *)commpent success:(oprationSuccessHandle)success;

@end
