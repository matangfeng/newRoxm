//
//  CJCCommentModel.m
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCommentModel.h"
#import "CJCCommon.h"
#import "CJCChatMessageHandle.h"

@implementation CJCCommentModel

+(void)getFormatModel:(NSDictionary *)dict finish:(void (^)(NSArray *))finishHandle{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSArray *tempArr = [NSArray modelArrayWithClass:[self class] json:dict];
        
        for (CJCCommentModel *mode in tempArr) {
            
            mode.nameWidth = [CJCTools getShortStringLength:mode.nickname withFont:[UIFont accodingVersionGetFont_regularWithSize:14]];
            
            CGFloat maxWidth = SCREEN_WITDH - 40;
            
            mode.textSize = [CJCTools sizeWithString:mode.content maxWidth:maxWidth font:[UIFont accodingVersionGetFont_lightWithSize:14]];
            
            mode.cellHeight = 74+mode.textSize.height;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
           
            if (finishHandle) {
                
                finishHandle(tempArr);
            }
            
        });
        
    });
}

@end
