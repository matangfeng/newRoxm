//
//  CJCPhotoShowCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCPhotoShowModel;

@protocol CJCPhotoShowCellDelegate <NSObject>

-(void)cellTapHandle;

@end

@interface CJCPhotoShowCell : UICollectionViewCell

@property (nonatomic ,weak) id<CJCPhotoShowCellDelegate> delegate;

@property (nonatomic ,strong) CJCPhotoShowModel *showModel;

@property (nonatomic ,assign) BOOL isVideoPlayerPlay;

-(void)videoPlay;

-(void)videoStop;

@end
