//
//  CJCPhotoOprationCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoOprationCell.h"
#import "CJCCommon.h"
#import "CJCPhotoShowModel.h"
#import "CJCSectorCycleView.h"
#import "CJCDownloadManger.h"

@interface CJCPhotoOprationCell ()

@property (nonatomic ,strong) UIImageView *playView;

@property (nonatomic ,strong) CJCSectorCycleView *cycleView;

@property (nonatomic ,strong) UIButton *selectButton;

@property (nonatomic ,strong) UILabel *videoLengthLable;

@end

@implementation CJCPhotoOprationCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.userInteractionEnabled = YES;
    
    imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    
    self.showImageView = imageView;
    [self.contentView addSubview:imageView];
}

-(void)setShowModel:(CJCPhotoShowModel *)showModel{
    
    _showModel = showModel;
    
    if (showModel.type == 20) {
        
        self.showImageView.hidden = NO;
        
        self.playView.hidden = YES;
        
        //        NSString *thumnailStr = [NSString stringWithFormat:@"?imageView2/2/w/%d/h/%d",(int)SCREEN_WITDH,(int)SCREEN_HEIGHT];
        NSString *thumnailStr = [NSString stringWithFormat:@"?imageView2/2/w/%d/q/50",(int)SCREEN_WITDH];
        
        NSString *thumbleImage = [NSString stringWithFormat:@"%@%@",showModel.url,thumnailStr];
        
        [self.showImageView setImageWithURL:[NSURL URLWithString:thumbleImage] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            showModel.thumbnilImage = image;
        }];
        
    }else if (showModel.type == 21){
        
        NSString *thumnailStr = [NSString stringWithFormat:@"?vframe/jpg/offset/0/w/%d",(int)SCREEN_WITDH];
        
        //        NSString *previewImage = [NSString stringWithFormat:@"%@%@",showModel.url,@"?vframe/jpg/offset/0/w/480/h/480"];
        NSString *previewImage = [NSString stringWithFormat:@"%@%@",showModel.url,thumnailStr];
        
        self.showImageView.hidden = NO;
        
        self.playView.hidden = NO;
        
        self.playView.center = self.contentView.center;
        
        //        [self.personalImageView setImageURL:[NSURL URLWithString:previewImage]];
        
        [self.showImageView setImageWithURL:[NSURL URLWithString:previewImage] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            showModel.thumbnilImage = image;
        }];
    }
    
    self.showImageView.frame = CGRectMake(0, 0, self.contentView.size.width, self.contentView.size.height);
    
}

-(CJCSectorCycleView *)cycleView{
    
    if (_cycleView == nil) {
        
        CJCSectorCycleView *view = [[CJCSectorCycleView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.size = CGSizeMake(kAdaptedValue(100), kAdaptedValue(100));
        view.center = self.contentView.center;
        
        [self.contentView addSubview:view];
        
        _cycleView = view;
    }
    return _cycleView;
}

-(UIImageView *)playView{
    
    if (_playView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_videoplay")];
        
        imageView.size = CGSizeMake(49, 49);
        imageView.center = self.contentView.center;
        
        [self.showImageView addSubview:imageView];
        
        _playView = imageView;
    }
    return _playView;
}

-(void)setIsEdit:(BOOL)isEdit{

    _isEdit = isEdit;
    
    self.selectButton.hidden = !isEdit;
    
    self.videoLengthLable.hidden = !isEdit;
    self.videoLengthLable.text = self.showModel.videoLength;
}

-(UILabel *)videoLengthLable{

    if (_videoLengthLable == nil) {
        
        UILabel *label = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor whiteColor]];
        
        label.size = CGSizeMake(50, 17);
        label.x = 7;
        label.bottom = self.contentView.size.width - 7;
        
        [self.showImageView addSubview:label];
        
        _videoLengthLable = label;
    }
    return _videoLengthLable;
}

-(UIButton *)selectButton{

    if (_selectButton == nil) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setImage:kGetImage(@"album_icon_photo_unchoose") forState:UIControlStateNormal];
        [button setImage:kGetImage(@"album_icon_photo_choose") forState:UIControlStateSelected];
        
        button.size = CGSizeMake(22, 22);
        button.bottom = self.contentView.size.height - 9;
        button.right = self.contentView.size.width - 9;
        
        [button addTarget:self action:@selector(selectButtonClickHandle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.showImageView addSubview:button];
        _selectButton = button;
    }
    return _selectButton;
}

-(void)selectButtonClickHandle:(UIButton *)button{

    button.selected = !button.selected;
    
    if ([self.delegate respondsToSelector:@selector(cellSelectButtonClickHandle:andIndex:)]) {
        
        [self.delegate cellSelectButtonClickHandle:button.selected andIndex:self.index];
    }
}

@end
