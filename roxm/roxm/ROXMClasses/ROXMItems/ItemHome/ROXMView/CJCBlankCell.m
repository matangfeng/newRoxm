//
//  CJCBlankCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBlankCell.h"
#import "CJCCommon.h"

@implementation CJCBlankCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UIView *topView = [[UIView alloc] init];
    
    topView.frame = CGRectMake(0, 10, SCREEN_WITDH, 20);
    
    topView.backgroundColor = [UIColor toUIColorByStr:@"838383"];
    
    [self.contentView addSubview:topView];
    
    UIView *middleView = [[UIView alloc] init];
    
    middleView.frame = CGRectMake(SCREEN_WITDH/3, 40, SCREEN_WITDH/3*2, 20);
    
    middleView.backgroundColor = [UIColor toUIColorByStr:@"838383"];
    
    [self.contentView addSubview:middleView];
    
    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.frame = CGRectMake(SCREEN_WITDH/3*2, 70, SCREEN_WITDH/3, 20);
    
    bottomView.backgroundColor = [UIColor toUIColorByStr:@"838383"];
    
    [self.contentView addSubview:bottomView];

}

@end
