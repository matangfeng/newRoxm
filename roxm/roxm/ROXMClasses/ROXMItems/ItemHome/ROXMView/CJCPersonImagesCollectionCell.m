//
//  CJCPersonImagesCollectionCell.m
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPersonImagesCollectionCell.h"
#import "CJCCommon.h"
#import "CJCDownloadManger.h"
#import "CJCPhotoShowModel.h"
#import "CJCSectorCycleView.h"

@interface CJCPersonImagesCollectionCell ()

@property (nonatomic ,strong) UIImageView *playView;

@end

@implementation CJCPersonImagesCollectionCell

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIImageView *personalImageView = [[UIImageView alloc] init];
    
    personalImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    personalImageView.contentMode = UIViewContentModeScaleAspectFill;
    personalImageView.layer.masksToBounds = YES;
    
    self.personalImageView = personalImageView;
    [self.contentView addSubview:personalImageView];
}

-(void)setShowModel:(CJCPhotoShowModel *)showModel{

    _showModel = showModel;
    
    if (showModel.type == 20) {
        
        self.personalImageView.hidden = NO;
        
        self.playView.hidden = YES;
        
//        NSString *thumnailStr = [NSString stringWithFormat:@"?imageView2/2/w/%d/h/%d",(int)SCREEN_WITDH,(int)SCREEN_HEIGHT];
        NSString *thumnailStr = [NSString stringWithFormat:@"?imageView2/2/w/%d/q/50",(int)SCREEN_WITDH];
        
        NSString *thumbleImage = [NSString stringWithFormat:@"%@%@",showModel.url,thumnailStr];
        
        [self.personalImageView setImageWithURL:[NSURL URLWithString:thumbleImage] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            showModel.thumbnilImage = image;
        }];
        
    }else if (showModel.type == 21){
        
        NSString *thumnailStr = [NSString stringWithFormat:@"?vframe/jpg/offset/0/w/%d",(int)SCREEN_WITDH];
        
//        NSString *previewImage = [NSString stringWithFormat:@"%@%@",showModel.url,@"?vframe/jpg/offset/0/w/480/h/480"];
        NSString *previewImage = [NSString stringWithFormat:@"%@%@",showModel.url,thumnailStr];
        
        self.personalImageView.hidden = NO;
        
        self.playView.hidden = NO;
        
        self.playView.center = self.contentView.center;
        
//        [self.personalImageView setImageURL:[NSURL URLWithString:previewImage]];
        
        [self.personalImageView setImageWithURL:[NSURL URLWithString:previewImage] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            showModel.thumbnilImage = image;
        }];
    }
    
    self.personalImageView.frame = CGRectMake(1.5, 1.5, self.contentView.size.width-3, self.contentView.size.height-3);
}

-(UIImageView *)playView{

    if (_playView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_videoplay")];
        
        imageView.size = CGSizeMake(49, 49);
        imageView.center = self.contentView.center;
        
        [self.personalImageView addSubview:imageView];
        
        _playView = imageView;
    }
    return _playView;
}

@end
