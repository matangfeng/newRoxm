//
//  CJCCommentCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCommentCell.h"
#import "CJCCommon.h"
#import "CJCCommentModel.h"

@interface CJCCommentCell ()

@property (nonatomic ,strong) UILabel *nameLabel;

@property (nonatomic ,strong) UIImageView *vipImageView;

@property (nonatomic ,strong) UIView *scoreView;

@property (nonatomic ,strong) UILabel *commentLabel;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation CJCCommentCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"222222"]];
    
    nameLabel.frame = CGRectMake(20, 20, 50, 16);
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UIImageView *vipImage= [[UIImageView alloc] initWithImage:kGetImage(@"preson_pic_vip")];
    
    vipImage.size = CGSizeMake(19, 14);
    vipImage.x = nameLabel.right+5;
    vipImage.centerY = nameLabel.centerY;
    
    self.vipImageView = vipImage;
    [self.contentView addSubview:vipImage];
    
    UIView *scoreView = [[UIView alloc] init];
    
    UIImage *grayImage = kGetImage(@"preson_pic_stargrey");
    
    for (int i =0; i<5; i++) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:grayImage];
        
        imageView.frame = CGRectMake(18*i, 0, 12, 12);
        
        [scoreView addSubview:imageView];
    }
    
    scoreView.size = CGSizeMake(84, 12);
    scoreView.x = vipImage.right+10;
    scoreView.centerY = nameLabel.centerY;
    
    self.scoreView = scoreView;
    [self.contentView addSubview:scoreView];
    
    UILabel *commentLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"222222"]];
    
    self.commentLabel = commentLabel;
    [self.contentView addSubview:commentLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(20, 100, SCREEN_WITDH-40, OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setCommentModel:(CJCCommentModel *)commentModel{

    _commentModel = commentModel;
    
    self.nameLabel.width = commentModel.nameWidth;
    self.nameLabel.text = commentModel.nickname;
    
    self.commentLabel.text = commentModel.content;
    self.commentLabel.size = commentModel.textSize;
    self.commentLabel.x = 20;
    self.commentLabel.y = self.nameLabel.bottom + 20;
    
    self.lineView.y = commentModel.cellHeight-1;
    
    self.vipImageView.x = self.nameLabel.right+5;
    
    self.scoreView.x = self.vipImageView.right+10;
    
    if (commentModel.score>0) {
        
        for (int i =0; i<commentModel.score; i++) {
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"preson_pic_staryellow")];
            
            imageView.frame = CGRectMake(18*i, 0, 12, 12);
            
            [self.scoreView addSubview:imageView];
        }
    }
}

@end
