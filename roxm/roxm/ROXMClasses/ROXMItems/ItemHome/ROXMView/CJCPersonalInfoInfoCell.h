//
//  CJCPersonalInfoInfoCell.h
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCpersonalInfoModel;

@interface CJCPersonalInfoInfoCell : UITableViewCell

@property (nonatomic , strong) UIView * saveBottomLine;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@end
