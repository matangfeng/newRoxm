//
//  CJCReportReasonCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCReportReasonCell : UITableViewCell

@property (nonatomic ,copy) NSString *reason;

@property (nonatomic ,assign) BOOL isSelect;

@end
