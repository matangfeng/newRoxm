//
//  CJCPersonalInfoNameCell.h
//  roxm
//
//  Created by lfy on 2017/9/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CJCCommon.h"

@class CJCPersonalInfoNameCell;

typedef void(^CJCFollowButtonHandle)(NSString *relation);

typedef void(^CJCIconTapHandle)(CJCPersonalInfoNameCell *cell);

@interface CJCPersonalInfoNameCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *cycleIconImageView;

@property (nonatomic ,strong) UIButton *followButton;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;


///用户关系  
@property (nonatomic ,copy) NSString *relationStr;

@property (nonatomic ,copy) CJCFollowButtonHandle followHandle;

@property (nonatomic ,copy) CJCIconTapHandle tapHandle;

@end
