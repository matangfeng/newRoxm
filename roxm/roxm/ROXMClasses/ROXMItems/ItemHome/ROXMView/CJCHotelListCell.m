//
//  CJCHotelListCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCHotelListCell.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "CJCCustomAmapPOI.h"

@interface CJCHotelListCell ()

@property (nonatomic ,strong) UILabel *hotelNameLable;

@property (nonatomic ,strong) UILabel *hotelAddressLabel;

@property (nonatomic ,strong) UIButton *selectButton;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation CJCHotelListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}


-(void)setUpUI{

    UILabel *nameLable = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    self.hotelNameLable = nameLable;
    [self.contentView addSubview:nameLable];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:11 color:[UIColor toUIColorByStr:@"333333"]];
    
    self.hotelAddressLabel = addressLabel;
    [self.contentView addSubview:addressLabel];

    
    UIButton *selectView = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [selectView setImage:kGetImage(@"all_check_smallblue") forState:UIControlStateSelected];
    
    selectView.frame = CGRectMake(337, 27, kAdaptedValue(10), kAdaptedValue(8));
    
    self.selectButton = selectView;
    [self.contentView addSubview:selectView];
    
    self.selectButton.centerY = kAdaptedValue(53/2);
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, kAdaptedValue(53), SCREEN_WITDH, OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setCustomAmapPOI:(CJCCustomAmapPOI *)customAmapPOI{

    AMapPOI *gaodePOI = customAmapPOI.gaodePOI;
    
    self.hotelNameLable.text = gaodePOI.name;
    
    self.hotelNameLable.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(10.5), SCREEN_WITDH-kAdaptedValue(80) , 15);
    
    self.hotelAddressLabel.text = gaodePOI.address;
    
    self.hotelAddressLabel.frame = CGRectMake(kAdaptedValue(23), self.hotelNameLable.bottom+kAdaptedValue(6), self.hotelNameLable.width, 12);
    
    if (customAmapPOI.isSelect) {
        
        self.selectButton.selected = YES;
    }else{
    
        self.selectButton.selected = NO;
    }
    
}

@end
