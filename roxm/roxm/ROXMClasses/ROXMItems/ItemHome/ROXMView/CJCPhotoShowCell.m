//
//  CJCPhotoShowCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPhotoShowCell.h"
#import "CJCCommon.h"
#import "CJCPhotoShowModel.h"
#import "CJCSectorCycleView.h"
#import "CJCDownloadManger.h"
#import <AVFoundation/AVFoundation.h>

#define PYPreviewPhotoMaxScale  2.0

@interface CJCPhotoShowCell ()<UIScrollViewDelegate>{
    
    AVPlayer *videoPlayer;
    AVPlayerLayer *videoplayerLayer;
    AVPlayerItem *videoitem;
    
    CGFloat pinchScal;
    
    CGRect orignRect;
}

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,strong) UIImageView *playView;

@property (nonatomic ,strong) CJCSectorCycleView *cycleView;

@property (nonatomic ,strong) UIScrollView *imageContainView;

@property (nonatomic ,strong) UITapGestureRecognizer *tapGes;

@end

@implementation CJCPhotoShowCell

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor blackColor];
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIScrollView *contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT)];
    // 取消滑动指示器
    contentScrollView.showsVerticalScrollIndicator = NO;
    contentScrollView.showsHorizontalScrollIndicator = NO;
    
    contentScrollView.delegate = self;
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id  _Nonnull sender) {
        
        if ([self.delegate respondsToSelector:@selector(cellTapHandle)]) {
            
            [self.delegate cellTapHandle];
        }
    }];
    
    self.tapGes = tapGes;
    [contentScrollView addGestureRecognizer:tapGes];
    
    [contentScrollView setMinimumZoomScale:1.0f];
    [contentScrollView setMaximumZoomScale:4.0f];
    [contentScrollView setZoomScale:1.0f animated:NO];
    
    self.imageContainView = contentScrollView;
    [self.contentView addSubview:contentScrollView];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    imageView.userInteractionEnabled = YES;
    
    self.showImageView = imageView;
    [self.imageContainView addSubview:imageView];
}

-(void)configGesture{

    // photoCell添加双击手势
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [self.imageContainView addGestureRecognizer:doubleTap];
    
    [self.tapGes requireGestureRecognizerToFail:doubleTap];
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gesture
{
    
    float newScale;
    
    if (self.imageContainView.zoomScale < 2) {
        
       newScale =  2.0;//zoomScale这个值决定了contents当前扩展的比例
        
    }else{
    
        newScale =  1.0;//zoomScale这个值决定了contents当前扩展的比例
    }
    
    [self zoomDoubleTapWithPoint:[gesture locationInView:gesture.view]];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    // 延中心点缩放
    CGRect rect = self.showImageView.frame;
    rect.origin.x = 0;
    rect.origin.y = 0;
    if (rect.size.width < self.imageContainView.width) {
        rect.origin.x = floorf((self.imageContainView.width - rect.size.width) / 2.0);
    }
    if (rect.size.height < self.imageContainView.height) {
        rect.origin.y = floorf((self.imageContainView.height - rect.size.height) / 2.0);
    }
    self.showImageView.frame = rect;
}

- (void)zoomDoubleTapWithPoint:(CGPoint)touchPoint
{
    if(self.imageContainView.zoomScale >= PYPreviewPhotoMaxScale)
    {
        [self.imageContainView setZoomScale:self.imageContainView.minimumZoomScale animated:YES];
    }
    else
    {
        CGFloat width = self.imageContainView.bounds.size.width / PYPreviewPhotoMaxScale;
        CGFloat height = self.imageContainView.bounds.size.height / PYPreviewPhotoMaxScale;
        [self.imageContainView zoomToRect:CGRectMake(touchPoint.x - width / 2, touchPoint.y - height / 2, width, height) animated:YES];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.showImageView;
}

-(void)setShowModel:(CJCPhotoShowModel *)showModel{

    _showModel = showModel;
    
    [videoitem removeObserver:self forKeyPath:@"status"];
    [videoplayerLayer removeFromSuperlayer];
    videoitem = nil;
    videoPlayer = nil;
    videoplayerLayer = nil;
    
    if (showModel.type == 20) {
        
        self.playView.hidden = YES;
        self.cycleView.hidden = NO;
        self.showImageView.hidden = NO;
        
        CGFloat imageWidth = SCREEN_WITDH;
        
        CGFloat imageHeight;
        
        if (showModel.height == 0||showModel.width == 0) {
            
            imageHeight = SCREEN_WITDH;
            
        }else{
        
            imageHeight = (CGFloat)showModel.height/showModel.width*imageWidth;
        }
        
        self.showImageView.size = CGSizeMake(imageWidth, imageHeight);
        self.showImageView.center = self.contentView.center;
        
        __weak __typeof(self) weakSelf = self;
        [self.showImageView setImageWithURL:[NSURL URLWithString:showModel.url] placeholder:showModel.thumbnilImage options:YYWebImageOptionShowNetworkActivity progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
            weakSelf.cycleView.hidden = NO;
            
            weakSelf.playView.hidden = YES;
            
            weakSelf.showImageView.hidden = NO;
            
            weakSelf.cycleView.center = weakSelf.contentView.center;
            
            [weakSelf.cycleView drawProgress:(CGFloat)receivedSize/expectedSize];
            
        } transform:nil completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            if (error) {
                
                return ;
            }
            
            weakSelf.cycleView.hidden = YES;
            weakSelf.playView.hidden = YES;
            
            CGFloat imageWidth = SCREEN_WITDH;
            CGFloat imageHeight = image.size.height/image.size.width*imageWidth;
            
            weakSelf.showImageView.size = CGSizeMake(imageWidth, imageHeight);
            weakSelf.showImageView.center = weakSelf.contentView.center;
            
            [weakSelf configGesture];
        }];
        
    }else{
    
        self.showImageView.hidden = NO;
        
        if (showModel.thumbnilImage) {
            
            self.showImageView.image = showModel.thumbnilImage;
            
            CGFloat imageWidth = SCREEN_WITDH;
            
            CGFloat imageHeight = showModel.thumbnilImage.size.height/showModel.thumbnilImage.size.width*imageWidth;
            
            self.showImageView.size = CGSizeMake(imageWidth, imageHeight);
            self.showImageView.center = self.contentView.center;
        }
        
        self.playView.hidden = YES;
        
        self.cycleView.hidden = NO;
        
        CJCDownloadManger *manger = [[CJCDownloadManger alloc] init];
        
        [manger downloadVideoWithVideoURL:showModel.url];
        
        _showModel.isFinishLoad = NO;
        
        manger.progressHandle = ^(CGFloat progress) {
            
            self.cycleView.hidden = NO;
            
            self.playView.hidden = YES;
            
            self.showImageView.hidden = NO;
            
            self.cycleView.center = self.contentView.center;
            
            if (progress<0.05) {
                
                progress = 0.05;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.cycleView drawProgress:progress];
            });
            
        };
        
        manger.successHandle = ^(NSString *loadPath, UIImage *returnImage, NSString *videoLength) {
            
            _showModel.videoPath = loadPath;
            _showModel.videoImage = returnImage;
            _showModel.isFinishLoad = YES;
            _showModel.videoLength = videoLength;
            
            CGFloat imageWidth = SCREEN_WITDH;
            CGFloat imageHeight = returnImage.size.height/returnImage.size.width*imageWidth;
            
            self.showImageView.size = CGSizeMake(imageWidth, imageHeight);
            self.showImageView.center = self.contentView.center;
            
            self.cycleView.hidden = YES;
            
            self.showImageView.hidden = YES;
            
            [self creatPreviewView];
        };
    }
}

-(void)creatPreviewView{
    
    [videoitem removeObserver:self forKeyPath:@"status"];
    [videoplayerLayer removeFromSuperlayer];
    videoitem = nil;
    videoPlayer = nil;
    videoplayerLayer = nil;
    
    videoitem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:_showModel.videoPath]];
    
    [videoitem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    
    videoPlayer = [[AVPlayer alloc] initWithPlayerItem:videoitem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:videoitem];
    
    videoplayerLayer = [AVPlayerLayer playerLayerWithPlayer:videoPlayer];
    
    videoplayerLayer.frame = self.contentView.bounds;
    
    [self.contentView.layer addSublayer:videoplayerLayer];
    
    self.playView.hidden = YES;
    
    self.playView.center = self.contentView.center;
    
    [self.contentView bringSubviewToFront:self.playView];
    
}

-(void)dealloc{

    [videoitem removeObserver:self forKeyPath:@"status"];
}

-(void)videoPlay{

    self.playView.hidden = YES;
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    [videoPlayer play];
}

-(void)videoStop{

    [videoPlayer pause];
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    
    if (videoPlayer) {
        
        self.playView.hidden = NO;
    }
}

-(void)playbackFinished:(NSNotification *)notification{
    NSLog(@"视频播放完成.");
    
    
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    self.playView.hidden = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        
        if (status == AVPlayerStatusReadyToPlay) {
            
            if (self.isVideoPlayerPlay) {
                
                [videoPlayer play];
                self.playView.hidden = YES;
            }else{
            
                [videoPlayer seekToTime:CMTimeMake(0, 1)];
                self.playView.hidden = NO;
            }
    
        }
    }
}

-(CJCSectorCycleView *)cycleView{
    
    if (_cycleView == nil) {
        
        CJCSectorCycleView *view = [[CJCSectorCycleView alloc] init];
        
        view.backgroundColor = [UIColor clearColor];
        
//        view.size = CGSizeMake(kAdaptedValue(100), kAdaptedValue(100));
        view.frame = CGRectMake(0, 0, kAdaptedValue(150), kAdaptedValue(150));
        view.center = self.contentView.center;
        
        [view drawProgress:0.05];
        
        [self.contentView addSubview:view];
        
        _cycleView = view;
    }
    return _cycleView;
}

-(UIImageView *)playView{
    
    if (_playView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_videoplay")];
        
        imageView.size = CGSizeMake(100, 100);
        imageView.center = self.contentView.center;
        
        imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            
            [videoPlayer play];
            imageView.hidden = YES;
        }];
        
        [imageView addGestureRecognizer:tap];
        
        [self.contentView addSubview:imageView];
        
        _playView = imageView;
    }
    return _playView;
}

@end
