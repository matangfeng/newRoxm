//
//  CJCReportReasonCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCReportReasonCell.h"
#import "CJCCommon.h"

@interface CJCReportReasonCell ()

@property (nonatomic ,strong) UILabel *reasonLabel;

@property (nonatomic ,strong) UIImageView *selectImageView;

@end

@implementation CJCReportReasonCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    self.textLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
    
    self.textLabel.textColor = [UIColor toUIColorByStr:@"3C4F5E"];
    
    UIImageView *image = [[UIImageView alloc] initWithImage:kGetImage(@"all_check_smallblue")];
    
    image.size = CGSizeMake(10, 7);
    image.centerY = 24;
    image.right = SCREEN_WITDH - 16;
    
    image.hidden = YES;
    
    self.selectImageView = image;
    [self.contentView addSubview:image];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(20, 46, SCREEN_WITDH-40, OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setReason:(NSString *)reason{

    self.textLabel.text = reason;
    
    self.textLabel.x = 20;
}

-(void)setIsSelect:(BOOL)isSelect{

    self.selectImageView.hidden = !isSelect;
}

@end
