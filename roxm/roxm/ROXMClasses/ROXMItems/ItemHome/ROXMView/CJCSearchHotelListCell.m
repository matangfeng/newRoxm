//
//  CJCSearchHotelListCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/12.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCSearchHotelListCell.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>

@interface CJCSearchHotelListCell ()

@property (nonatomic ,strong) UILabel *hotelNameLable;

@property (nonatomic ,strong) UILabel *hotelAddressLabel;

@property (nonatomic ,strong) UILabel *hotelDistanceLabel;

@end

@implementation CJCSearchHotelListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UILabel *nameLable = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    self.hotelNameLable = nameLable;
    [self.contentView addSubview:nameLable];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"333333"]];
    
    self.hotelAddressLabel = addressLabel;
    [self.contentView addSubview:addressLabel];
    
    UILabel *distanceLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"A4A4A4"]];
    
    distanceLabel.textAlignment = NSTextAlignmentRight;
    
    distanceLabel.frame = CGRectMake(kAdaptedValue(23), 14, kAdaptedValue(70), kAdaptedValue(20));
    
    distanceLabel.centerY = 33;
    distanceLabel.right = SCREEN_WITDH - kAdaptedValue(23);
    
    self.hotelDistanceLabel = distanceLabel;
    [self.contentView addSubview:distanceLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(65), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setAmapPOI:(AMapPOI *)AmapPOI{
    
    
    self.hotelNameLable.attributedText = [self attrStrFrom:AmapPOI.name searchStr:self.keys];
    
    self.hotelNameLable.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(14), SCREEN_WITDH-kAdaptedValue(80) , 18);
    
    self.hotelAddressLabel.text = AmapPOI.address;
    
    self.hotelAddressLabel.frame = CGRectMake(kAdaptedValue(23), self.hotelNameLable.bottom+kAdaptedValue(7), self.hotelNameLable.width, 13);
    
    if (self.showDistance) {
        
        CGFloat distance = (CGFloat)AmapPOI.distance/1000.0;
        
        self.hotelDistanceLabel.text = [NSString stringWithFormat:@"%.2fkm",distance];
    }
}

- (NSMutableAttributedString *)attrStrFrom:(NSString *)titleStr searchStr:(NSString *)searchStr
{
    NSMutableAttributedString *arrString = [[NSMutableAttributedString alloc]initWithString:titleStr];
    
    [arrString addAttributes:@{NSFontAttributeName:[UIFont accodingVersionGetFont_mediumWithSize:16],NSForegroundColorAttributeName:[UIColor toUIColorByStr:@"333333"]
                               }
                       range:NSMakeRange(0, titleStr.length)];
    
    // 设置前面几个字串的格式:粗体、红色
    [arrString addAttributes:@{NSFontAttributeName:[UIFont accodingVersionGetFont_mediumWithSize:16],NSForegroundColorAttributeName:[UIColor toUIColorByStr:@"429CF0"]
                               }
                       range:[titleStr rangeOfString:searchStr]];
    return arrString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
