//
//  CJCpersonalInfoModel.m
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCpersonalInfoModel.h"
#import "CJCCommon.h"
#import <Hyphenate/Hyphenate.h>


static dispatch_once_t onceToken;
static CJCpersonalInfoModel * manager = nil;

@implementation CJCpersonalInfoModel

+ (instancetype)infoModel{

    dispatch_once(&onceToken, ^{
        manager = [[CJCpersonalInfoModel alloc] init];
    });
    return manager;
}

- (void)setLoginInfoDict:(NSDictionary *)loginInfoDict
{
    [[NSUserDefaults standardUserDefaults] setObject:loginInfoDict forKey:Logins];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setLoginUserDict:(NSDictionary *)loginUserDict
{
    [[NSUserDefaults standardUserDefaults] setObject:loginUserDict forKey:LoginUses];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (NSDictionary *)loginUserDict
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:LoginUses];
}

- (NSDictionary *)loginInfoDict
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:Logins];
}

- (void)userLoginWithDict:(NSDictionary *)infoDict{
    
    [kUSERDEFAULT_STAND setObject:self.loginInfoDict[@"data"][@"token"] forKey:kUSERTOKEN];
    [kUSERDEFAULT_STAND setObject:self.loginInfoDict[@"data"][@"emPassword"] forKey:kHUANXINPASSWORD];
    [kUSERDEFAULT_STAND setObject:[self.loginUserDict[@"uid"] stringValue] forKey:kUSERUID];
    [kUSERDEFAULT_STAND synchronize];
    
    if (![CJCTools isBlankString:self.loginUserDict[@"lockScreenX"]]) {
        [kUSERDEFAULT_STAND setObject:@([self.loginUserDict[@"lockScreenX"] floatValue]) forKey:@"secretAreaX"];
        [kUSERDEFAULT_STAND setObject:@([self.loginUserDict[@"secretAreaY"] floatValue]) forKey:@"secretAreaY"];
    }
    
    [kUSERDEFAULT_STAND setObject:@"YES" forKey:kUSERINFOCOMPLETE];
    [kUSERDEFAULT_STAND synchronize];
    
    BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
    if (!isAutoLogin) {
        EMError *error = [[EMClient sharedClient] loginWithUsername:self.loginUserDict[@"emId"] password:self.loginInfoDict[@"data"][@"emPassword"]];
        if (!error) {
            NSLog(@"环信及其用户、登录成功");
            [[EMClient sharedClient].options setIsAutoLogin:YES];
        }else{
            NSLog(@"环信登录失败%@",error);
        }
    }
}

- (void)userLogout{
    
    onceToken = 0;
    
    [self removeLocalInfo];
    
    EMError *error = [[EMClient sharedClient] logout:YES];
    if (!error) {
        NSLog(@"退出成功");
    }
}


- (void)updateInfoJSONTolocal:(NSDictionary *)info{

    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/%@",videoStr,kUSERINFOPLIST];
    
    [info writeToFile:videoStr atomically:NO];
}

- (void)saveInfoJSONTolocal:(NSDictionary *)info{

    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/%@",videoStr,kUSERINFOPLIST];
    NSDictionary *userInfoDict = info[@"data"][@"user"];
    [userInfoDict writeToFile:videoStr atomically:NO];
}

- (void)removeLocalInfo{
    
    [kUSERDEFAULT_STAND setObject:nil forKey:kUSERUID];
    [kUSERDEFAULT_STAND setObject:nil forKey:kUSERTOKEN];
    [kUSERDEFAULT_STAND setObject:nil forKey:kHUANXINPASSWORD];
    [kUSERDEFAULT_STAND setObject:nil forKey:kUSERCONTACTUPLOAD];
    [kUSERDEFAULT_STAND setObject:nil forKey:kREGISTERSOURCE];
    [kUSERDEFAULT_STAND setObject:nil forKey:kUSERINFOCOMPLETE];
    
    [kUSERDEFAULT_STAND setObject:@(0) forKey:kUNREADMESSAGECOUNT];
    
    [kUSERDEFAULT_STAND setObject:nil forKey:@"secretAreaIsSet"];
    [kUSERDEFAULT_STAND setObject:nil forKey:@"secretAreaX"];
    [kUSERDEFAULT_STAND setObject:nil forKey:@"secretAreaY"];
    [kUSERDEFAULT_STAND setObject:nil forKey:kACCEPTYAOYUEUPLOADLOCATION];
    
    [kUSERDEFAULT_STAND synchronize];
    NSLog(@"clear user info success");
}

@end
