//
//  CJCForgetPassWordVC.m
//  roxm
//
//  Created by lfy on 2017/8/30.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCForgetPassWordVC.h"
#import "CJCCommon.h"
#import "CJCInputPassWordVC.h"

@interface CJCForgetPassWordVC ()<UITextFieldDelegate>{

    //记录手机号的长度 用于判断是输入 还是删除
    NSInteger phoneNUmLengh;
}

@property (nonatomic ,strong) UITextField *phoneNumTF;

@property (nonatomic ,strong) CJCOprationButton *loginButton;

@property (nonatomic ,strong) CJCErrorAletLabel *phoneErrorLabel;

@end

@implementation CJCForgetPassWordVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.phoneNumTF becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    [self setUpUI];
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getYYLabelWithStr:@"忘记密码？" fontName:kFONTNAMELIGHT size:24 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(88), kAdaptedValue(120), kAdaptedValue(24));
    
    [self.view addSubview:titleLabel];
    
    UILabel *hintLabel = [UIView getYYLabelWithStr:@"请输入您的手机号以查找您的账号" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    hintLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(130.5), kAdaptedValue(210), kAdaptedValue(14));
    
    [self.view addSubview:hintLabel];
    
    UILabel *phoneLabel = [UIView getYYLabelWithStr:@"手机号" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    phoneLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(200), kAdaptedValue(50), kAdaptedValue(14));
    
    [self.view addSubview:phoneLabel];
    
    UILabel *eightLabel = [UIView getYYLabelWithStr:@"+86" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"333333" andAlpha:0.5]];
    
    eightLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(224.5), kAdaptedValue(30), kAdaptedValue(18.5));
    
    [self.view addSubview:eightLabel];
    
    
    UITextField *phoneTF = [[UITextField alloc] init];
    
    self.phoneNumTF = phoneTF;
    
    phoneTF.frame = CGRectMake(eightLabel.right+kAdaptedValue(10), kAdaptedValue(221), kAdaptedValue(329), kAdaptedValue(31));
    phoneTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    phoneTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    phoneTF.placeholder = @"请输入手机号";
    
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    
    phoneTF.delegate = self;
    
    [phoneTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    phoneTF.tag = 10001;
    
    [self.view addSubview:phoneTF];
    
    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(252), kAdaptedValue(329), OnePXLineHeight);
    
    [self.view addSubview:topLineView];
    
    CJCErrorAletLabel *phoneErrorLabel = [[CJCErrorAletLabel alloc] init];
    
    phoneErrorLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(255), kAdaptedValue(200), kAdaptedValue(12));
    
    self.phoneErrorLabel = phoneErrorLabel;
    
    [self.view addSubview:phoneErrorLabel];
    
    CJCOprationButton *loginBtn = [[CJCOprationButton alloc] init];
    
    self.loginButton = loginBtn;
    
    loginBtn.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(284.5), kAdaptedValue(200), kAdaptedValue(45));
    
    [loginBtn setTitle:@"下一步" forState:UIControlStateNormal];
    
    loginBtn.centerX = self.view.centerX;
    
    loginBtn.canOpration = NO;
    
    [loginBtn addTarget:self action:@selector(loginButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:loginBtn];

}

//登陆按钮的点击事件
-(void)loginButtonDidClick{
    
    [self.view endEditing:YES];
    
    if (![CJCTools valiMobile:self.phoneNumTF.text]) {
        
        [MBManager showBriefAlert:@"请输入正确的手机号" inView:self.view];
        
        return;
    }
    
    NSString *phoneNum = [self.phoneNumTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CJCInputPassWordVC *nextVC = [[CJCInputPassWordVC alloc] init];
    
    nextVC.phoneNum = phoneNum;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark =======TextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 10001) {
        
        self.phoneErrorLabel.text = @"";
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag == 10001) {
        
        if (![CJCTools isBlankString:textField.text]) {
            
            if (![CJCTools valiMobile:textField.text]) {
                
                self.phoneErrorLabel.hidden = NO;
                self.phoneErrorLabel.text = @"请填写正确的手机号";
            }else{
                
                self.phoneErrorLabel.hidden = YES;
            }
        }
    }
    
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{
    
    //手机号输入的一些限制 按照344 中间加空格的格式展示
    //需要判断手机号是否为合法手机号（正则判断）
    //多于13位时输入不了
    if (textField.tag == 10001) {
        
        if (phoneNUmLengh == 0) {
            
            phoneNUmLengh = textField.text.length;
        }
        //是在输入
        if (phoneNUmLengh < textField.text.length) {
            
            if (textField.text.length == 3 || textField.text.length == 8) {
                
                NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                
                [tempStr appendString:@" "];
                
                textField.text = tempStr.copy;
            }
            
            if (textField.text.length == 4) {
                
                if (![textField.text containsString:@" "]) {
                    
                    NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                    
                    [tempStr insertString:@" " atIndex:3];
                    
                    textField.text = tempStr.copy;
                }
                
            }
            
            if (textField.text.length == 9) {
                
                NSArray *tempArr = [textField.text componentsSeparatedByString:@" "];
                
                if (tempArr.count == 2) {
                    
                    NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                    
                    [tempStr insertString:@" " atIndex:8];
                    
                    textField.text = tempStr.copy;
                }
            }
            
            
            phoneNUmLengh = textField.text.length;
        }else{
            
            //是在删除  需要删除空格
            
            if (textField.text.length == 4 || textField.text.length == 9) {
                
                NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
                
                [tempStr deleteCharactersInRange:NSMakeRange(textField.text.length-1, 1)];
                
                textField.text = tempStr.copy;
            }
            
            phoneNUmLengh = textField.text.length;
        }
        
        if (textField.text.length == 13) {
            
            self.loginButton.canOpration = YES;
        }
        
        if (textField.text.length > 13) {
            
            NSMutableString *tempStr = [NSMutableString stringWithString:textField.text];
            
            [tempStr deleteCharactersInRange:NSMakeRange(13, 1)];
            
            textField.text = tempStr.copy;
        }
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
