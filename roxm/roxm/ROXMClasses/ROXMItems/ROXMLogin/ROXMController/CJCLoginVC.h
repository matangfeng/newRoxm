//
//  CJCLoginVC.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonVC.h"

typedef void(^LoginSuccess)(void);

@interface CJCLoginVC : CommonVC

@property (nonatomic ,copy) NSString *isLoginOrRegiste;
@property (nonatomic , strong) LoginSuccess loginSuccessBlock;
@end
