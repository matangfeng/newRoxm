//
//  CJCLoginAndRegisterVC.m
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCLoginAndRegisterVC.h"
#import "CJCCommon.h"
#import "CJCLoginVC.h"
#import "CJCUploadManager.h"
#import <AVFoundation/AVFoundation.h>
#import "CJCFistAnimationView.h"

#define MARGIN 28

@interface CJCLoginAndRegisterVC (){

    AVPlayer *videoPlayer;
    AVPlayerLayer *videoplayerLayer;
    AVPlayerItem *videoitem;
}

@property (nonatomic ,strong) CJCFistAnimationView *animationView;

@end

@implementation CJCLoginAndRegisterVC

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [videoitem removeObserver:self forKeyPath:@"status"];

}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
//    [videoPlayer play];
    
    if (_isVideoPlay){
    
        [videoPlayer play];
    }

    [self.animationView animationStart];
    
    __weak __typeof(self) weakSelf = self;
    __weak __typeof(videoPlayer) weakVideoPlayer = videoPlayer;
    self.animationView.finishHandle = ^{
        
        weakSelf.isVideoPlay = YES;
        [weakVideoPlayer play];
    };
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [videoPlayer pause];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self setUpUI];
    
    if (!_isVideoPlay) {
        
//        CJCFistAnimationView *view = [[CJCFistAnimationView alloc] init];
//        view.frame = [UIScreen mainScreen].bounds;
//
//        self.animationView = view;
//        [self.view addSubview:view];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification object:nil]; //监听是否触发home键挂起程序.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil]; //监听是否重新进入程序程序.
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"escLogin" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        [self alertLoginStr:@"你的濡沫账号在另一台设备登录。如果这不是你的操作，你的濡沫密码已经泄漏。请尽快登陆濡沫修改濡沫密码。或访问romo110.com冻结濡沫。"];
    }];
}


- (void)applicationWillResignActive:(NSNotification *)noti {
    
    [videoPlayer pause];
}

- (void)applicationDidBecomeActive:(NSNotification *)noti {
    
    UIViewController *currentVC = [CJCTools currentViewController];
    
    if ([currentVC isKindOfClass:[CJCLoginAndRegisterVC class]]) {
        
        if (self.isVideoPlay) {
            
            [videoPlayer play];
        }
        
    }else{
    
        [videoPlayer pause];
    }
}

-(void)setUpUI{
    
    [self setUpVideoView];
    
    CGFloat middleMargin = kAdaptedValue(15);
    CGFloat leftMargin = kAdaptedValue(28);
    CGFloat buttonWidth = SCREEN_WITDH - 2*leftMargin - middleMargin;
    CGFloat buttonHeight = kAdaptedValue(48.5);
    
    UIButton *loginBtn = [self getLoginResterButtonWithTitle:@"登录"];
    
    loginBtn.frame = CGRectMake(leftMargin, SCREEN_HEIGHT - kAdaptedValue(18) - buttonHeight, buttonWidth/2, buttonHeight);
    
    loginBtn.layer.cornerRadius = kAdaptedValue(4);
    loginBtn.layer.masksToBounds = YES;
    
    [loginBtn addTarget:self action:@selector(loginButtonDidClick) forControlEvents:UIControlEventTouchUpInside];

    
    UIButton *registerBtn = [self getLoginResterButtonWithTitle:@"注册"];
    
    registerBtn.frame = CGRectMake(SCREEN_WITDH - kAdaptedValue(MARGIN+buttonWidth), SCREEN_HEIGHT-kAdaptedValue(120), buttonWidth/2, buttonHeight);
    
    registerBtn.left = loginBtn.right+middleMargin;
    registerBtn.centerY = loginBtn.centerY;
    
    registerBtn.layer.cornerRadius = kAdaptedValue(4);
    registerBtn.layer.masksToBounds = YES;
    
    [registerBtn addTarget:self action:@selector(registerButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)setUpVideoView{
    
    NSString *str = [[NSBundle mainBundle] resourcePath];
    NSString *videoStr = [NSString stringWithFormat:@"%@%@",str,@"/loginVidelFullScreen.mp4"];
    
    if (videoitem == nil) {
        
        videoitem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:videoStr]];
        
        [videoitem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    if (videoPlayer == nil) {
        
        videoPlayer = [[AVPlayer alloc] initWithPlayerItem:videoitem];
        
        videoPlayer.volume = 0.0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished) name:AVPlayerItemDidPlayToEndTimeNotification object:videoitem];
    }
    
    
    if (videoplayerLayer == nil) {
        
        videoplayerLayer = [AVPlayerLayer playerLayerWithPlayer:videoPlayer];
        
        videoplayerLayer.frame = CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT);
        
        [self.view.layer addSublayer:videoplayerLayer];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerItemStatus status= [[change objectForKey:@"new"] intValue];
        
        if (status == AVPlayerStatusReadyToPlay) {
            
            [videoPlayer seekToTime:CMTimeMake(0, 1)];
        }
    }
}

-(void)playbackFinished{
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    [videoPlayer play];
}

-(UIButton *)getLoginResterButtonWithTitle:(NSString *)title{

    UIImage *buttonImage = [UIView imageWithColor:[UIColor toUIColorByStr:@"000000"]];
    
    UIButton *loginBtn = [[UIButton alloc] init];
    
    loginBtn.alpha = 0.6;
    
    [loginBtn setTitle:title forState:UIControlStateNormal];
    [loginBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    loginBtn.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    [self.view addSubview:loginBtn];
    
    return loginBtn;
}

-(void)loginButtonDidClick{

    [videoPlayer pause];
    
    CJCLoginVC *loginVC = [[CJCLoginVC alloc] init];
    loginVC.isLoginOrRegiste = @"登录";
    loginVC.loginSuccessBlock = ^{
        if (_loginSuccess) {
            BOOL status = _loginSuccess();
            if (status) {
                [self tapRootCtr];
            }
        }
    };
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)registerButtonDidClick{

    [videoPlayer pause];
    
    CJCLoginVC *loginVC = [[CJCLoginVC alloc] init];
    
    loginVC.isLoginOrRegiste = @"使用手机号注册";
    
    [self.navigationController pushViewController:loginVC animated:YES];
    
}

- (void)tapRootCtr
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
