//
//  CJCInputPassWordVC.m
//  roxm
//
//  Created by lfy on 2017/8/30.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInputPassWordVC.h"
#import "CJCCommon.h"
#import "CJCGetPhoneCodeVC.h"

@interface CJCInputPassWordVC ()<UITextFieldDelegate>

@property (nonatomic ,strong) UITextField *passWordTF;

@property (nonatomic ,strong) CJCOprationButton *loginButton;

@end

@implementation CJCInputPassWordVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.passWordTF becomeFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    [self setUpUI];
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getYYLabelWithStr:@"忘记密码" fontName:kFONTNAMELIGHT size:24 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(88), kAdaptedValue(120), kAdaptedValue(24));
    
    [self.view addSubview:titleLabel];
    
    UILabel *hintLabel = [UIView getYYLabelWithStr:@"长度不少于6位" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    hintLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(130.5), kAdaptedValue(210), kAdaptedValue(14));
    
    [self.view addSubview:hintLabel];
    
    UILabel *passWordLabel = [UIView getYYLabelWithStr:@"密码" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    passWordLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(189.5), kAdaptedValue(28), kAdaptedValue(14));
    
    [self.view addSubview:passWordLabel];
    
    UITextField *passTF = [[UITextField alloc] initWithFrame:CGRectMake(kAdaptedValue(23), kAdaptedValue(220.5), kAdaptedValue(329), kAdaptedValue(31))];
    
    self.passWordTF = passTF;
    
    passTF.delegate = self;
    
    passTF.tag = 10002;
    
    passTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    passTF.textColor = [UIColor toUIColorByStr:@"333333"];
    
    passTF.placeholder = @"请输入密码";
    
    passTF.clearButtonMode = UITextFieldViewModeAlways;
    
    passTF.clearsOnBeginEditing = NO;
    
    //passTF.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    [passTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:passTF];
    
    UIView *BottomLineView = [UIView getLineView];
    
    BottomLineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(251.5), kAdaptedValue(329), OnePXLineHeight);
    
    [self.view addSubview:BottomLineView];
    
    CJCOprationButton *loginBtn = [[CJCOprationButton alloc] init];
    
    self.loginButton = loginBtn;
    
    loginBtn.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(284.5), kAdaptedValue(200), kAdaptedValue(45));
    
    [loginBtn setTitle:@"完成" forState:UIControlStateNormal];
    
    loginBtn.centerX = self.view.centerX;
    
    loginBtn.canOpration = NO;
    
    [loginBtn addTarget:self action:@selector(loginButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:loginBtn];

}

//登陆按钮的点击事件
-(void)loginButtonDidClick{
    
    [self.view endEditing:YES];
    
    [self userRegister];
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{

    //当输入密码 并且输入完手机号  登陆按钮可以点击
    if (textField.text.length>5) {
        
        self.loginButton.canOpration = YES;
    }else{
        
        self.loginButton.canOpration = NO;
    }

}

-(void)userRegister{
    
    [self showWithLabelAnimation];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/sms/send/for_find_password"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneNum;
    
    [CJCHttpTool postWithUrl:urlStr params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            CJCGetPhoneCodeVC *codeVC = [[CJCGetPhoneCodeVC alloc] init];
            
            codeVC.phoneNum = self.phoneNum;
            codeVC.passWord = self.passWordTF.text;
            codeVC.verificationCode = responseObject[@"data"];
            codeVC.codeType = CJCGetCodeTypeForgetPassWord;
            
            [self.navigationController pushViewController:codeVC animated:YES];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
