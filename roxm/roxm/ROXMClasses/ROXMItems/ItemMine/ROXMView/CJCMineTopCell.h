//
//  CJCMineTopCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCMineTopCellDelegate <NSObject>

-(void)iconImageClick;

@end

@class CJCpersonalInfoModel;

@interface CJCMineTopCell : UITableViewCell

@property (nonatomic ,weak) id<CJCMineTopCellDelegate> delegate;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@end
