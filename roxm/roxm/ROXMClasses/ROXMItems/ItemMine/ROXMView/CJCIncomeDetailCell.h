//
//  CJCIncomeDetailCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCIncomeDetailModel;

@interface CJCIncomeDetailCell : UITableViewCell

@property (nonatomic ,strong) CJCIncomeDetailModel *detailModel;

@end
