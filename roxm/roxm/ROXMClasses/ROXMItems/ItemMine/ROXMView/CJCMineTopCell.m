//
//  CJCMineTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMineTopCell.h"
#import "CJCCommon.h"
#import "CJCpersonalInfoModel.h"

@interface CJCMineTopCell ()

@property (nonatomic ,strong) UILabel *nameLabel;

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIButton *titleButton;

@property (nonatomic ,strong) UIImageView *iconImageView;

@end

@implementation CJCMineTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    nameLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(16), 64, 32);
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    NSString *title = @"查看并编辑个人资料";
    CGFloat width = [CJCTools getShortStringLength:title withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    UIButton *titleButton = [UIView getButtonWithStr:title fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"808080"]];
    
    titleButton.frame = CGRectMake(kAdaptedValue(20.75), nameLabel.bottom+kAdaptedValue(8), width, kAdaptedValue(21));
    
    self.titleButton = titleButton;
    [self.contentView addSubview:titleButton];
    
    UIImageView *iconImageView = [[UIImageView alloc] init];
    
    iconImageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconImageView.size = CGSizeMake(kAdaptedValue(74), kAdaptedValue(74));
    iconImageView.y = kAdaptedValue(8);
    iconImageView.right = SCREEN_WITDH - kAdaptedValue(23);
    
    iconImageView.layer.cornerRadius = kAdaptedValue(37);
    iconImageView.layer.masksToBounds = YES;
    
    iconImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *iconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconImageClickHandle)];
    
    [iconImageView addGestureRecognizer:iconTap];
    
    self.iconImageView = iconImageView;
    [self.contentView addSubview:iconImageView];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(17.5), kAdaptedValue(122), SCREEN_WITDH - kAdaptedValue(35), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)iconImageClickHandle{

    if ([self.delegate respondsToSelector:@selector(iconImageClick)]) {
        
        [self.delegate iconImageClick];
    }
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{

    
    CGFloat width = [CJCTools getShortStringLength:[CJCpersonalInfoModel infoModel].loginUserDict[@"nickname"] withFont:[UIFont accodingVersionGetFont_mediumWithSize:32]];
    
    NSLog(@"userName%@",[CJCpersonalInfoModel infoModel].loginUserDict[@"nickname"]);
    NSLog(@"userImg%@",[CJCpersonalInfoModel infoModel].loginUserDict[@"imageUrl"]);

    self.nameLabel.text = infoModel.nickname;
    self.nameLabel.width = width;
    
    [self.iconImageView setImageURL:[NSURL URLWithString:infoModel.imageUrl]];
    
    self.titleLabel.y = self.nameLabel.bottom+kAdaptedValue(8);
}

@end
