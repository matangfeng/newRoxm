//
//  CJCSettingOprationCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCSettingOprationCell.h"
#import "CJCCommon.h"

@interface CJCSettingOprationCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UIView *bottomView;

@property (nonatomic ,strong) UILabel *bottomDetailLabel;

@property (nonatomic ,strong) UILabel *middleDetailLabel;

@end

@implementation CJCSettingOprationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{

    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
    
    [self.contentView addSubview:topLineView];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(22), SCREEN_WITDH - kAdaptedValue(150), kAdaptedValue(17));
    
    self.titleLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
    
    UIView *bottomLineView = [UIView getLineView];
    
    bottomLineView.frame = CGRectMake(0, kAdaptedValue(60), SCREEN_WITDH, OnePXLineHeight);
    
    self.bottomLineView = bottomLineView;
    [self.contentView addSubview:bottomLineView];
}

-(void)setCellType:(CJCSettingOprationCellType)cellType{

    switch (cellType) {
        case CJCSettingOprationCellTypeDefault:{
        
            self.arrowImageView.hidden = NO;
            self.settingSwitch.hidden = YES;
            self.bottomView.hidden = YES;
            self.middleDetailLabel.hidden = YES;
        }
            break;
            
        case CJCSettingOprationCellTypeDefaultDetail:{
            
            self.arrowImageView.hidden = NO;
            self.settingSwitch.hidden = YES;
            self.bottomView.hidden = YES;
            self.middleDetailLabel.hidden = NO;
        }
            break;
            
        case CJCSettingOprationCellTypeDefaultHigher:{
            
            self.arrowImageView.hidden = NO;
            self.settingSwitch.hidden = YES;
            self.bottomView.hidden = NO;
            self.middleDetailLabel.hidden = YES;
        }
            break;
            
        case CJCSettingOprationCellTypeSwitch:{
            
            self.arrowImageView.hidden = YES;
            self.settingSwitch.hidden = NO;
            self.bottomView.hidden = YES;
            self.middleDetailLabel.hidden = YES;
            self.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
            
        case CJCSettingOprationCellTypeSwitchHigher:{
            
            self.arrowImageView.hidden = YES;
            self.settingSwitch.hidden = NO;
            self.bottomView.hidden = NO;
            self.middleDetailLabel.hidden = YES;
            self.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
            
        case CJCSettingOprationCellTypeNoArrow:{
            
            self.arrowImageView.hidden = YES;
            self.settingSwitch.hidden = YES;
            self.bottomView.hidden = YES;
            self.middleDetailLabel.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}

-(void)setTitle:(NSString *)title{

    self.titleLabel.text = title;
}

-(void)setDetail:(NSString *)detail{

    self.bottomDetailLabel.text = detail;
    
    [self.bottomDetailLabel sizeToFit];
}

-(void)setMiddleDetail:(NSString *)middleDetail{

    self.middleDetailLabel.text = middleDetail;
    
    CGFloat width = [CJCTools getShortStringLength:middleDetail withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    self.middleDetailLabel.width = width;
    self.middleDetailLabel.right = SCREEN_WITDH - kAdaptedValue(40);
}

-(UISwitch *)settingSwitch{

    if (_settingSwitch == nil) {
        
        UISwitch *swit = [[UISwitch alloc] init];
        
        swit.size = CGSizeMake(kAdaptedValue(51), kAdaptedValue(31));
        swit.centerY = kAdaptedValue(30);
        swit.right = SCREEN_WITDH - kAdaptedValue(19);
        
        [self.contentView addSubview:swit];
        
        [swit addTarget:self action:@selector(cellSwitchHandle:) forControlEvents:UIControlEventValueChanged];
        
        _settingSwitch = swit;
    }
    
    return _settingSwitch;
}

-(void)cellSwitchHandle:(UISwitch *)sender{

    if ([self.delegate respondsToSelector:@selector(switchChangeHandle:andIndex:)]) {
     
        [self.delegate switchChangeHandle:sender.isOn andIndex:self];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    self.bottomView.backgroundColor = [UIColor whiteColor];
}

-(void)setIsShow:(BOOL)isShow{

    if (isShow) {
        
        [self.contentView removeAllSubviews];
    }
}

-(UIImageView *)arrowImageView{

    if (_arrowImageView == nil) {
        
        UIImageView *arrowView = [[UIImageView alloc] initWithImage:kGetImage(@"profile_icon_arrowright")];
        
        arrowView.frame = CGRectMake(334, 22, kAdaptedValue(22), kAdaptedValue(22));
        arrowView.centerY = kAdaptedValue(30);
        arrowView.right = SCREEN_WITDH - kAdaptedValue(19);
        
        [self.contentView addSubview:arrowView];
        
        _arrowImageView = arrowView;
    }
    return _arrowImageView;
}

-(UILabel *)middleDetailLabel{

    if (_middleDetailLabel == nil) {
        
        UILabel *detailLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"9A9A9A"]];
        
        detailLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(8), kAdaptedValue(150), kAdaptedValue(45));
        detailLabel.centerY = kAdaptedValue(30);
        detailLabel.right = SCREEN_WITDH - kAdaptedValue(40);
        
        detailLabel.textAlignment = NSTextAlignmentRight;
        
        [self.contentView addSubview:detailLabel];
        _middleDetailLabel = detailLabel;
    }
    return _middleDetailLabel;
}

-(UIView *)bottomView{

    if (_bottomView == nil) {
        
        UIView *view = [[UIView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.frame = CGRectMake(0, kAdaptedValue(61), SCREEN_WITDH, kAdaptedValue(61));
        
        [self.contentView addSubview:view];
        
        _bottomView = view;
        
        UILabel *detailLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"8C959F"]];
        
        detailLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(8), SCREEN_WITDH - kAdaptedValue(46), kAdaptedValue(45));
        
        self.bottomDetailLabel = detailLabel;
        [view addSubview:detailLabel];
    }
    return _bottomView;
}

@end
