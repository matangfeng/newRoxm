//
//  CJCBigTopTitleCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCBigTopTitleCell : UITableViewCell

@property (nonatomic ,copy) NSString *title;

@end
