//
//  CJCIncomeDetailCell.m
//  roxm
//
//  Created by 陈建才 on 2017/10/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCIncomeDetailCell.h"
#import "CJCCommon.h"
#import "CJCIncomeDetailModel.h"

@interface CJCIncomeDetailCell ()

@property (nonatomic ,strong) UIImageView *giftImageView;

@property (nonatomic ,strong) UILabel *incomeNameLabel;

@property (nonatomic ,strong) UILabel *incomeTimeLabel;

@property (nonatomic ,strong) UILabel *incomeAmontLabel;

@end

@implementation CJCIncomeDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UIImageView *iconImage = [[UIImageView alloc] init];
    
//    iconImage.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    iconImage.frame = CGRectMake(kAdaptedValue(18), kAdaptedValue(14), kAdaptedValue(64), kAdaptedValue(64));
    
    iconImage.centerY = kAdaptedValue(46);
    
    self.giftImageView = iconImage;
    [self.contentView addSubview:iconImage];
    
    UILabel *nameLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:17 color:[UIColor toUIColorByStr:@"353535"]];
    
    nameLabel.frame = CGRectMake(iconImage.right+kAdaptedValue(14), kAdaptedValue(23), kAdaptedValue(150), kAdaptedValue(19));
    
    self.incomeNameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UILabel *timeLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"8C8C8C"]];
    
    timeLabel.frame = CGRectMake(nameLabel.x, nameLabel.bottom+kAdaptedValue(13), kAdaptedValue(150), kAdaptedValue(14));
    
    self.incomeTimeLabel = timeLabel;
    [self.contentView addSubview:timeLabel];
    
    UILabel *amontLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:17 color:[UIColor toUIColorByStr:@"B39B6B"]];
    
    amontLabel.textAlignment = NSTextAlignmentRight;
    
    amontLabel.frame = CGRectMake(200, 10, kAdaptedValue(150), kAdaptedValue(24));
    amontLabel.centerY = kAdaptedValue(46);
    amontLabel.right = SCREEN_WITDH - kAdaptedValue(18);
    
    self.incomeAmontLabel = amontLabel;
    [self.contentView addSubview:amontLabel];
}

-(void)setDetailModel:(CJCIncomeDetailModel *)detailModel{

    
    [self.giftImageView setImageURL:[NSURL URLWithString:detailModel.imageUrl]];
    
    CGFloat nameWidth = [CJCTools getShortStringLength:detailModel.name withFont:[UIFont accodingVersionGetFont_regularWithSize:17]];
    
    self.incomeNameLabel.text = detailModel.name;
    self.incomeNameLabel.width = nameWidth;
    
    NSString *price = [NSString stringWithFormat:@"%.2f",detailModel.price];
    
    if ([price rangeOfString:@"-"].location !=NSNotFound) {
        
        self.incomeAmontLabel.textColor = [UIColor toUIColorByStr:@"222222"];
        self.incomeAmontLabel.text = [NSString stringWithFormat:@"%@",price];
        
    }else{
    
        self.incomeAmontLabel.textColor = [UIColor toUIColorByStr:@"B39B6B"];
        self.incomeAmontLabel.text = [NSString stringWithFormat:@"+%@",price];
    }
    
    CGFloat amontWidth = [CJCTools getShortStringLength:self.incomeNameLabel.text withFont:[UIFont accodingVersionGetFont_regularWithSize:17]];
    
    self.incomeAmontLabel.width = amontWidth;
    self.incomeAmontLabel.right = SCREEN_WITDH - kAdaptedValue(18);
    
    self.incomeTimeLabel.text = [self shijianchuoToTimeStr:detailModel.createTime];
    
}

-(NSString *)shijianchuoToTimeStr:(long)chuo{

    NSDate *date = [CJCTools getDateTimeFromMilliSeconds:chuo];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    format.dateFormat = @"MM月dd日 HH:mm";
    
    NSString *string = [format stringFromDate:date];
    
    return string;
}

@end
