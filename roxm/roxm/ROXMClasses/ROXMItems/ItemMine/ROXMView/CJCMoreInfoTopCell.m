//
//  CJCMoreInfoTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMoreInfoTopCell.h"
#import "CJCCommon.h"

@implementation CJCMoreInfoTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *label = [UIView getSystemLabelWithStr:@"更多资料" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    label.frame = CGRectMake(kAdaptedValue(23), 26, 200, kAdaptedValue(32));
    label.centerY = kAdaptedValue(43);

    [self.contentView addSubview:label];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(85), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

@end
