//
//  CJCMineGiftCell.h
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCInviteGiftModel;

@interface CJCMineGiftCell : UICollectionViewCell

@property (nonatomic ,strong) CJCInviteGiftModel *giftModel;

@end
