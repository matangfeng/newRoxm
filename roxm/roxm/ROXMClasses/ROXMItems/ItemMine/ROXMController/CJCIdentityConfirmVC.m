//
//  CJCIdentityConfirmVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCIdentityConfirmVC.h"
#import "CJCCommon.h"
#import "CJCInputSecretView.h"
#import "CJCTurnOutSuccessVC.h"
#import "CJCInstallPaySecretVC.h"
#import <ReactiveObjC.h>

@interface CJCIdentityConfirmVC ()<UITextFieldDelegate>

@property (nonatomic ,strong) UIButton *reSendButton;

@property (nonatomic ,strong) UIButton *nextStepButton;

@property (nonatomic ,strong) UITextField *codeTextField;

@property (nonatomic ,copy) NSString *returnCode;

@end

@implementation CJCIdentityConfirmVC

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
//    if (![_tempTimer isValid]){
//        
//        [self timerOpenFire];
//    }

    [self getPhoneCode];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    if ([_tempTimer isValid]) {
        
        [_tempTimer invalidate];
        _tempTimer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNaviView];
    [self setUpUI];
}

-(void)timerOpenFire{
    self.reSendButton.userInteractionEnabled = NO;
    [self.reSendButton setTitleColor:[UIColor toUIColorByStr:@"333333" andAlpha:0.5] forState:UIControlStateNormal];
    [self.reSendButton setTitle:@"重新发送(60s)" forState:UIControlStateNormal];
    self.reSendButton.width = kAdaptedValue(95);
    self.reSendButton.right = SCREEN_WITDH - kAdaptedValue(23);
    __block NSInteger i = 59;
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
        if (i == 0) {
            [timer invalidate];
            timer = nil;
            [self timerCloseFire];
        }else{
            NSString *timeStr = [NSString stringWithFormat:@"(重新发送%lds)",(long)i--];
            
            [self.reSendButton setTitle:timeStr forState:UIControlStateNormal];
        }
    }];
    
    _tempTimer = timer;
    
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
}

-(void)getPhoneCode{
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *urlStr = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/sms/send/for_find_password"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = localInfoModel.mobile;
    
    [CJCHttpTool postWithUrl:urlStr params:params success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            self.returnCode = responseObject[@"data"];
            [self timerOpenFire];
        }else{
            [self showWithLabel:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
    }];
}

-(void)timerCloseFire{
    self.reSendButton.userInteractionEnabled = YES;
    self.reSendButton.width = kAdaptedValue(56);
    self.reSendButton.right = SCREEN_WITDH - kAdaptedValue(23);
    [self.reSendButton setTitleColor:[UIColor toUIColorByStr:@"4198F0"] forState:UIControlStateNormal];
    [self.reSendButton setTitle:@"重新发送" forState:UIControlStateNormal];
    [self.reSendButton sizeToFit];
    
    self.reSendButton.top = kAdaptedValue(18)+kAdaptedValue(80);
    self.reSendButton.right = SCREEN_WITDH - kAdaptedValue(23);
    
    [_tempTimer invalidate];
    _tempTimer = nil;
}

-(void)setUpUI{

    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"roxm濡沫 已将短信验证码发送到你的手机" fontName:kFONTNAMEREGULAR size:13 color:[UIColor toUIColorByStr:@"AEAEAE"]];
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    hintLabel.frame = CGRectMake(0, kAdaptedValue(112), SCREEN_WITDH, kAdaptedValue(18));
    
    [self.view addSubview:hintLabel];
    
    UILabel *phoneLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    phoneLabel.frame = CGRectMake(0, hintLabel.bottom+kAdaptedValue(17), SCREEN_WITDH, kAdaptedValue(18));
    
    phoneLabel.textAlignment = NSTextAlignmentCenter;
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

     NSString *str4 = [localInfoModel.mobile stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    
    phoneLabel.text = str4;
    
    [self.view addSubview:phoneLabel];
    
    UIButton *totalButton = [UIView getButtonWithStr:@"重新发送" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    [totalButton sizeToFit];
    
    totalButton.top = phoneLabel.bottom+kAdaptedValue(80);
    totalButton.right = SCREEN_WITDH - kAdaptedValue(23);
    
    self.reSendButton = totalButton;
    [self.view addSubview:totalButton];
    
    UITextField *phoneTF = [[UITextField alloc] init];
    
    phoneTF.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(177), SCREEN_WITDH-kAdaptedValue(150), kAdaptedValue(31));
    
    phoneTF.centerY = totalButton.centerY;
    
    phoneTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    phoneTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    phoneTF.placeholder = @"输入验证码";
    
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    
    phoneTF.delegate = self;
    //
//    [phoneTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [phoneTF.rac_textSignal subscribeNext:^(NSString * _Nullable x) {
        if (x.length == 4) {
            self.nextStepButton.canOpration = YES;
        }else{
            self.nextStepButton.canOpration = NO;
        }
        
        if (x.length > 4) {
            
            NSMutableString *tempStr = [NSMutableString stringWithString:x];
            
            [tempStr deleteCharactersInRange:NSMakeRange(4, 1)];
            
            phoneTF.text = tempStr.copy;
            
            self.nextStepButton.canOpration = YES;
        }
    }];
    
    
    self.codeTextField = phoneTF;
    [self.view addSubview:phoneTF];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), phoneLabel.bottom+kAdaptedValue(117), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:lineView];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:0];
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.5]] forState:UIControlStateSelected];

    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];

    [buyButton setTitle:@"下一步" forState:UIControlStateNormal];
    buyButton.frame = CGRectMake(kAdaptedValue(23), lineView.bottom+kAdaptedValue(40), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(49));
    buyButton.canOpration = NO;
    buyButton.layer.cornerRadius = kAdaptedValue(3);
    buyButton.layer.masksToBounds = YES;
    [[buyButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        if ([self.codeTextField.text isEqualToString:self.returnCode]) {
            CJCInstallPaySecretVC *nextVC = [[CJCInstallPaySecretVC alloc]init];
            nextVC.phoneCode = self.returnCode;
            [self.navigationController pushViewController:nextVC animated:YES];
        }else{
            [MBManager showBriefAlert:@"请输入正确的验证码"];
        }
    }];
    
    self.nextStepButton = buyButton;
    [self.view addSubview:buyButton];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"身份认证" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"身份认证" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
