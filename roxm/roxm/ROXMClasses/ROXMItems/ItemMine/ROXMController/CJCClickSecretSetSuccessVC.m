//
//  CJCClickSecretSetSuccessVC.m
//  roxm
//
//  Created by 陈建才 on 2017/11/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCClickSecretSetSuccessVC.h"
#import "CJCCommon.h"
#import "CJCPrivacyModelVC.h"

@interface CJCClickSecretSetSuccessVC ()

@end

@implementation CJCClickSecretSetSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)setUpUI{

    UIView *showSecretView = [[UIView alloc] init];
    
    showSecretView.backgroundColor = [UIColor blackColor];
    
    showSecretView.size = CGSizeMake(kAdaptedValue(220), SCREEN_HEIGHT/SCREEN_WITDH*kAdaptedValue(220));
    
    showSecretView.centerX = self.view.centerX;
    showSecretView.y = kAdaptedValue(200);
    
    [self.view addSubview:showSecretView];
    
    UIImageView *showTouchView = [[UIImageView alloc]initWithImage:kGetImage(@"set_pic_finger")];
    
    showTouchView.size = CGSizeMake(kAdaptedValue(43), kAdaptedValue(42));
    showTouchView.center = CGPointMake(kAdaptedValue(220)/SCREEN_WITDH*self.secretPoint.x, kAdaptedValue(220)/SCREEN_WITDH*self.secretPoint.y);
    
    [showSecretView addSubview:showTouchView];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    UILabel *backLabel = [UIView getSystemLabelWithStr:@"完成" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    [backLabel sizeToFit];
    backLabel.top = kAdaptedValue(32);
    backLabel.right = SCREEN_WITDH - kAdaptedValue(16);
    
    [self.view addSubview:backLabel];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backButton.frame = CGRectMake(kAdaptedValue(3), kAdaptedValue(33.5), kAdaptedValue(50), kAdaptedValue(50));
    backButton.center = backLabel.center;
    
    [backButton addTarget:self action:@selector(naviViewBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"设置成功" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    detailLabel.textAlignment = NSTextAlignmentCenter;
    
    detailLabel.frame = CGRectMake(0, kAdaptedValue(96), SCREEN_WITDH, kAdaptedValue(28));
    
    detailLabel.userInteractionEnabled = YES;
    
//    self.hintLabel = detailLabel;
    [self.view addSubview:detailLabel];
    
    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"下次登录需要您长按此处" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"636363" ]];
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    hintLabel.userInteractionEnabled = YES;
    
    hintLabel.frame = CGRectMake(0, detailLabel.bottom+kAdaptedValue(12), SCREEN_WITDH, kAdaptedValue(16));
    
    [self.view addSubview:hintLabel];

}

-(void)naviViewBackBtnDidClick{
    
    UIViewController *vc = self.presentingViewController;
    
    if (!vc.presentingViewController)   return;
    
    while (vc.presentingViewController)  {
        vc = vc.presentingViewController;
    }
    
    [vc dismissViewControllerAnimated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"clickSecretViewShow" object:nil];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
