//
//  CJCInviteRecordVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteRecordVC.h"
#import "CJCCommon.h"
#import "CJCInviteRecordCell.h"
#import "CJCInviteRecordModel.h"
#import <NSObject+YYModel.h>
#import "CJCInviteOrderDetailVC.h"
#import <MJExtension.h>
#import "LYSideslipCell.h"

#define kInviteRecordCell       @"CJCInviteRecordCell"

@interface CJCInviteRecordVC ()<UITableViewDelegate,UITableViewDataSource, LYSideslipCellDelegate>{

    NSMutableArray *dataArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    BOOL isDataReady;
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCInviteRecordVC

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if (!isDataReady) {
        
        [self getInviteRecord];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataArray = [NSMutableArray array];
    pageSize = 1;
    isRefresh = NO;
    isDataReady = NO;
    
    [self setUpNaviView];
    
    [self configTableView];
}

-(void)getInviteRecord{

    [self showWithLabelAnimation];
    
//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *recordURL;
    
    if (infoModel.sex == 0) {
        
        recordURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/list/by_partner"];
    }else{
    
        recordURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/list/by_user"];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    
    [CJCHttpTool postWithUrl:recordURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        isDataReady = YES;
        
        NSNumber *rtNum = responseObject[@"rt"];
        NSLog(@"ppppp \n :%@",responseObject);
        
        
        [CJCInviteRecordModel mj_setupReplacedKeyFromPropertyName:^NSDictionary *{
            return @{
                     @"IDd" : @"id",
                     };
        }];
        
        if (rtNum.integerValue == 0) {
            
            
            
//            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteRecordModel class] json:responseObject[@"data"]];
            
            [dataArray removeAllObjects];

            
            NSArray * array = responseObject[@"data"];
            for (NSInteger i = 0; i < [array count]; i++) {
                NSDictionary * dicts = [array objectAtIndex:i];
                CJCInviteRecordModel * infos = [CJCInviteRecordModel mj_objectWithKeyValues:dicts];
                if (infos.status != 0) {
                    [dataArray addObject:infos];
                }
            }
            
//            [self.tableView.mj_header endRefreshing];
//            [self.tableView.mj_footer endRefreshing];
            
            [self.tableView reloadData];
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64) style:(UITableViewStyleGrouped)];
    
//    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCInviteRecordCell class] forCellReuseIdentifier:kInviteRecordCell];
    
    __weak __typeof(self) weakSelf = self;
    
    [weakSelf loadNewData];

//    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//
//    }];
//
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//
//        [weakSelf loadMoreData];
//    }];
    
    self.tableView.mj_footer.automaticallyHidden = YES;
}

-(void)loadNewData{
    pageSize = 1;
    isRefresh = YES;
    [self getInviteRecord];
}

-(void)loadMoreData{
    pageSize++;
    isRefresh = NO;
    [self getInviteRecord];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataArray.count;
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kAdaptedValue(131) + 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 10;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WITDH, 10)];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WITDH, 10)];
    return view;
}

#pragma mark - LYSideslipCellDelegate
- (NSArray<LYSideslipCellAction *> *)sideslipCell:(LYSideslipCell *)sideslipCell editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    LYSideslipCellAction *action2 = [LYSideslipCellAction rowActionWithStyle:LYSideslipCellActionStyleDestructive title:@"删除" handler:^(LYSideslipCellAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        NSLog(@"删除");
    }];

    NSArray *array = @[];
    array = @[action2];
    return array;
}
    
- (BOOL)sideslipCell:(LYSideslipCell *)sideslipCell canSideslipRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
    
- (void)sideslipCell:(LYSideslipCell *)sideslipCell rowAtIndexPath:(NSIndexPath *)indexPath didSelectedAtIndex:(NSInteger)index
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"确认删除此邀约记录" message:@"删除邀约记录后将无法还原；（删除邀约记录不等于取消邀约，删除后会影响确认到达与开始操作。）" preferredStyle:  UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"lll\n :%ld",(long)indexPath.section);
        [self removeRecoderWith:indexPath.section andIndex:indexPath];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"点错了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCInviteRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteRecordCell];
    CJCInviteRecordModel *model = dataArray[indexPath.section];
    cell.recordModel = model;
    cell.delegate = self;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    CJCInviteRecordModel *model = dataArray[indexPath.section];
    
    CJCInviteOrderDetailVC *nextVC = [[CJCInviteOrderDetailVC alloc]init];
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 0) {
        
        nextVC.otherPersonUID = model.userId;
        
    }else{
        
       nextVC.otherPersonUID = model.partnerId;
    }

    nextVC.orderId = model.IDd;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)removeRecoderWith:(NSInteger)index andIndex:(NSIndexPath *)indexPahth{

//    [self showWithLabelAnimation];
    
    
    CJCInviteRecordModel *model = dataArray[index];
    
    NSString *removeURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/remove"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = model.IDd;
    
    [CJCHttpTool postWithUrl:removeURL params:params success:^(id responseObject) {
      
//        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            // 删除模型

            NSLog(@"daad\n:%ld",(long)index);
//            [dataArray removeObjectAtIndex:index];
            NSLog(@"ss \n%@",dataArray);
            
        [dataArray removeObjectAtIndex:index];
         [self.tableView deleteSection:index withRowAnimation:UITableViewRowAnimationFade];
            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            // 刷新
//            [self.tableView reloadData];
//            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            
        }else{
        
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"邀约记录"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

@end
