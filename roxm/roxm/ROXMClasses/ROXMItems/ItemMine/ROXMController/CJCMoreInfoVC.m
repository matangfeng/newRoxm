//
//  CJCMoreInfoVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMoreInfoVC.h"
#import "CJCCommon.h"
#import "CJCEditInfoBottomCell.h"
#import "CJCBirthHeiWeightView.h"
#import "CJCMoreInfoTopCell.h"
#import "CJCInviteGiftModel.h"
#import "CJCChooseDateGiftVC.h"

#define kEditInfoBottomCell     @"CJCEditInfoBottomCell"
#define kMoreInfoTopCell        @"CJCMoreInfoTopCell"

@interface CJCMoreInfoVC ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSDictionary *changeInfoDict;
    
    CJCInviteGiftModel *threeHourGift;
    CJCInviteGiftModel *tweveHourGift;
    
    NSString *threeHourGiftName;
    NSString *twelveHourGiftName;
}

@property (nonatomic ,strong) UITableView *mineTableView;

@end

@implementation CJCMoreInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    [self getThreeHourGiftDetail];
    [self getTwelveHourGiftDetail];
}

-(void)getThreeHourGiftDetail{

//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = model.giftId1;
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
        
            threeHourGift = [CJCInviteGiftModel modelWithJSON:responseObject[@"data"][@"gift"]];
            
            [self.mineTableView reloadRow:2 inSection:0 withRowAnimation:NO];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)getTwelveHourGiftDetail{
    
//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = model.giftId2;
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            
            tweveHourGift = [CJCInviteGiftModel modelWithJSON:responseObject[@"data"][@"gift"]];
            
            [self.mineTableView reloadRow:3 inSection:0 withRowAnimation:NO];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCSexType sexType;
    if (model.sex == 0) {
        
        sexType = CJCSexTypeWoman;
    }else{
        
        sexType = CJCSexTypeMan;
    }
    
    switch (indexPath.row) {
        case 1:{
            //罩杯
            NSString *newStr = model.cupSize;
            
            [self creatDarePickerWithPickerType:CJCPickerViewTypeBust andSelectStr:newStr];
        }
            break;
            
        case 4:{
            //酒店档次
            
            
        }
            break;
            
        case 2:{
            //3小时礼物
            
//            [self creatDarePickerWithPickerType:CJCPickerViewTypeThreeHour andSelectStr:nil];
            
            CJCChooseDateGiftVC *nextVC = [[CJCChooseDateGiftVC alloc] init];
        
            nextVC.chooseHandle = ^(NSString *returnString, NSString *returnIntId, NSNumber *returnIntPrice) {
                
                CJCEditInfoBottomCell *cell = [self.mineTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                
                cell.detail = returnString;
                
                [self updateInfoWith:CJCPickerViewTypeThreeHour andValue:returnIntId];
            };
            
            [self presentViewController:nextVC animated:YES completion:nil];
        }
            break;
            
        case 3:{
            //12小时礼物
            
//            [self creatDarePickerWithPickerType:CJCPickerViewTypeTwelveHoure andSelectStr:nil];
            
            CJCChooseDateGiftVC *nextVC = [[CJCChooseDateGiftVC alloc] init];
            
            nextVC.chooseHandle = ^(NSString *returnString, NSString *returnIntId, NSNumber *returnIntPrice) {
                
                CJCEditInfoBottomCell *cell = [self.mineTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                
                cell.detail = returnString;
                
                [self updateInfoWith:CJCPickerViewTypeTwelveHoure andValue:returnIntId];
            };
            
            [self presentViewController:nextVC animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
    
}

-(void)creatDarePickerWithPickerType:(CJCPickerViewType)pickType andSelectStr:(NSString *)selectStr{
    
//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCBirthHeiWeightView *pick = [[CJCBirthHeiWeightView alloc] init];
    
    CJCSexType sexType;
    if (model.sex == 0) {
        
        sexType = CJCSexTypeWoman;
    }else{
        
        sexType = CJCSexTypeMan;
    }
    
    pick.sexType = sexType;
    
    pick.selectStr = selectStr;
    
    pick.pickerViewType = pickType;
    
    [pick showPickerView];
    
    pick.returnHandle = ^(NSString *returnString, NSString *returnInt){
        
        switch (pickType) {
            case CJCPickerViewTypeThreeHour:{
            
                CJCEditInfoBottomCell *cell = [self.mineTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
                
                cell.detail = returnString;
                
            }
                break;
                
            case CJCPickerViewTypeTwelveHoure:{
                
                CJCEditInfoBottomCell *cell = [self.mineTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                
                cell.detail = returnString;
            }
                break;
                
            default:
                break;
        };
        
        [self updateInfoWith:pickType andValue:returnInt];
    };
}

-(void)updateInfoWith:(CJCPickerViewType)takeType andValue:(NSString *)value{
    
    [self showWithLabelAnimation];
    
    NSString *updateURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    switch (takeType) {
        case CJCPickerViewTypeBust:{
            
            params[@"cupSize"] = value;
        }
            break;
            
        case CJCPickerViewTypeThreeHour:{
            
            params[@"giftId1"] = value;
        }
            break;
            
        case CJCPickerViewTypeTwelveHoure:{
            
            params[@"giftId2"] = value;
        }
            break;
            
        default:
            break;
    }
    
    [CJCHttpTool postWithUrl:updateURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSDictionary *infoDict = responseObject[@"data"][@"user"];
            
//            CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
            CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

            changeInfoDict = infoDict;
            
            switch (takeType) {
                case CJCPickerViewTypeBirthDay:{
                    
                    model.cupSize = infoDict[@"cupSize"];
                    
                    [self.mineTableView reloadRow:4 inSection:0 withRowAnimation:NO];
                }
                    break;
                    
                case CJCPickerViewTypeThreeHour:{
                    
                    model.giftId1 = infoDict[@"giftId1"];
                }
                    break;
                    
                case CJCPickerViewTypeTwelveHoure:{
                    
                    model.giftId2 = infoDict[@"giftId2"];
                }
                    break;
                    
                default:
                    break;
            }
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(86);
    }else{
        
        return kAdaptedValue(62);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    CJCpersonalInfoModel *model = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (indexPath.row == 0) {
        
        CJCMoreInfoTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kMoreInfoTopCell];
        
        return cell;
    }else{
        
        CJCEditInfoBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:kEditInfoBottomCell];
        
        switch (indexPath.row) {
            case 1:{
                
                cell.title = @"罩杯";
                cell.detail = model.cupSize;
            }
                break;
                
            case 4:{
                
                cell.title = @"酒店档次";
                cell.detail = @"重新拍摄";
            }
                break;
                
            case 2:{
                
                cell.title = @"3小时约会礼物";
                cell.detail = threeHourGift.name;
            }
                break;
                
            case 3:{
                
                cell.title = @"12小时约会礼物";
                cell.detail = tweveHourGift.name;
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64);
    
    [tableView registerClass:[CJCMoreInfoTopCell class] forCellReuseIdentifier:kMoreInfoTopCell];
    [tableView registerClass:[CJCEditInfoBottomCell class] forCellReuseIdentifier:kEditInfoBottomCell];
    
    self.mineTableView = tableView;
    [self.view addSubview:tableView];
    
}

-(void)naviViewBackBtnDidClick{
    
   
    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (self.moreInfoHandle) {
            
            self.moreInfoHandle(changeInfoDict);
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
