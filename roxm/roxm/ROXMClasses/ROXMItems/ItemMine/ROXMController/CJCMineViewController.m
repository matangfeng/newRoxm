//
//  CJCMineViewController.m
//  roxm
//
//  Created by lfy on 2017/9/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMineViewController.h"
#import "CJCCommon.h"
#import "CJCMineTopCell.h"
#import "CJCMineBottomCell.h"
#import "CJCpersonalInfoModel.h"
#import "CJCPersonalInfoVC.h"
#import "CJCEditPersonalInfoVC.h"
#import "CJCInviteRecordVC.h"
#import "CJCSettingVC.h"
#import "CJCMineGiftVC.h"
#import "CJCWalletVC.h"
#import "CJCFollowListVC.h"
#import "LYSideslipCell.h"
#import "ROXMRootController.h"

#define kMineTopCell        @"CJCMineTopCell"
#define kMineBottomCell     @"CJCMineBottomCell"

@interface CJCMineViewController ()<UITableViewDelegate,UITableViewDataSource,CJCMineTopCellDelegate>

@property (nonatomic ,strong) UITableView *mineTableView;

@end

@implementation CJCMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.mineTableView reloadRow:0 inSection:0 withRowAnimation:NO];
}

- (void)iconImageClick{
    CJCPersonalInfoVC *nextVC = [[CJCPersonalInfoVC alloc] init];
    nextVC.otherUID = [kUSERDEFAULT_STAND objectForKey:kUSERUID];
    nextVC.infoVCType = CJCPersonalInfoVCTypeOwn;
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        CJCEditPersonalInfoVC *nextVC = [[CJCEditPersonalInfoVC alloc] init];
        nextVC.iconHandle = ^(NSDictionary *infoDict) {
            [self.mineTableView reloadRow:0 inSection:0 withRowAnimation:NO];
        };
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 1){
        CJCInviteRecordVC *nextVC = [[CJCInviteRecordVC alloc] init];
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 2){
        CJCWalletVC *nextVC = [[CJCWalletVC alloc] init];
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 3){
        CJCMineGiftVC *nextVC = [[CJCMineGiftVC alloc] init];
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 4){
        CJCFollowListVC *nextVC = [[CJCFollowListVC alloc] init];
        nextVC.listVCType = CJCFollowListVCTypeOpration;
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 5){
        CJCSettingVC *nextVC = [[CJCSettingVC alloc] init];
        nextVC.escSuccess = ^BOOL{
            [self.mineTableView reloadRow:0 inSection:0 withRowAnimation:NO];
            [[ROXMRootController shardRootController] TapLogin];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[ROXMRootController shardRootController] pushIndex];
            return YES;
            
        };
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return kAdaptedValue(123);
    }else{
        return kAdaptedValue(62);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        CJCMineTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kMineTopCell];
        cell.delegate = self;
        CJCpersonalInfoModel *model = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];
        cell.infoModel = model;
        return cell;
    }else{
        CJCMineBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:kMineBottomCell];
        if (indexPath.row == 1) {
            cell.title = @"邀约记录";
        }else if (indexPath.row == 2){
            cell.title = @"钱包";
        }else if (indexPath.row == 3){
            cell.title = @"我的礼物";
        }else if (indexPath.row == 4){
            cell.title = @"关注列表";
        }else if (indexPath.row == 5){
            cell.title = @"设置";
        }
        return cell;
    }
}

- (void)setUpUI{
    [self setKeyBoardHid:YES];
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.frame = CGRectMake(0, 0, SCREEN_WITDH, SCREEN_HEIGHT);
    [tableView setContentInset:(UIEdgeInsetsMake(64, 0, 0, 0))];
    [tableView registerClass:[CJCMineTopCell class] forCellReuseIdentifier:kMineTopCell];
    [tableView registerClass:[CJCMineBottomCell class] forCellReuseIdentifier:kMineBottomCell];
    self.mineTableView = tableView;
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
