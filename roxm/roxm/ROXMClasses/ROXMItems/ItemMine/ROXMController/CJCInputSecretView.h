//
//  CJCInputSecretView.h
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^secretConfirmHandle)(void);

@interface CJCInputSecretView : UIView

@property (nonatomic ,copy) secretConfirmHandle confirmHandle;

-(void)showPickerView;

-(void)dismissPickerView;

@end
