//
//  CJCEditPersonalInfoVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^changeIconHandle)(NSDictionary *infoDict);

@interface CJCEditPersonalInfoVC : CommonVC

@property (nonatomic ,copy) changeIconHandle iconHandle;

@end
