//
//  CJCChangeNickNameVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCChangeNickNameVC.h"
#import "CJCCommon.h"

@interface CJCChangeNickNameVC ()

@property (nonatomic ,strong) UITextField *phoneNumTF;

@property (nonatomic ,strong) CJCOprationButton *loginButton;

@end

@implementation CJCChangeNickNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)setUpUI{

   
    NSString *titleStr = [NSString stringWithFormat:@"修改%@",self.typeName];
    NSString *newNameStr = [NSString stringWithFormat:@"输入新%@",self.typeName];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:titleStr fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), 90, 200, kAdaptedValue(32));
    
    [self.view addSubview:titleLabel];
    
    UILabel *newNameLabel = [UIView getSystemLabelWithStr:newNameStr fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"888888"]];
    
    newNameLabel.frame = CGRectMake(kAdaptedValue(23), 154, 200, kAdaptedValue(16));
    
    newNameLabel.y = titleLabel.bottom + 32;
    
    [self.view addSubview:newNameLabel];
    
    UITextField *phoneTF = [[UITextField alloc] init];
    
    self.phoneNumTF = phoneTF;
    
    phoneTF.frame = CGRectMake(kAdaptedValue(23), newNameLabel.bottom+1, kAdaptedValue(329), kAdaptedValue(44));
    phoneTF.font = [UIFont accodingVersionGetFont_mediumWithSize:20];
    
    phoneTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    phoneTF.text = self.nickName;
    
    phoneTF.tag = 10001;
    
    [self.view addSubview:phoneTF];
    
    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(kAdaptedValue(23), phoneTF.bottom+1, kAdaptedValue(329), OnePXLineHeight);
    
    [self.view addSubview:topLineView];
    
    CJCOprationButton *loginBtn = [[CJCOprationButton alloc] init];
    
    self.loginButton = loginBtn;
    
    [loginBtn setTitle:@"确定" forState:UIControlStateNormal];
    
    [phoneTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    loginBtn.frame = CGRectMake(kAdaptedValue(87), topLineView.bottom+kAdaptedValue(30), kAdaptedValue(200), kAdaptedValue(45));
    
    loginBtn.centerX = self.view.centerX;
    
    loginBtn.canOpration = NO;
    
    [loginBtn addTarget:self action:@selector(loginButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:loginBtn];
}

- (void)loginButtonDidClick{

    [self.view endEditing:YES];
    [self showWithLabelAnimation];
    
    NSString *updateURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([self.typeName isEqualToString:@"昵称"]) {
        params[@"nickname"] = self.phoneNumTF.text;
    }else if ([self.typeName isEqualToString:@"个人签名"]){
        params[@"brief"] = self.phoneNumTF.text;
    }else if ([self.typeName isEqualToString:@"职业"]){
        params[@"career"] = self.phoneNumTF.text;
    }
    
    [CJCHttpTool postWithUrl:updateURL params:params success:^(id responseObject) {
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            if (self.changeHandle) {
                [self.navigationController popViewControllerAnimated:YES];
                [CJCpersonalInfoModel infoModel].loginUserDict = responseObject[@"data"][@"user"];
                self.changeHandle(responseObject[@"data"][@"user"]);
            }
//            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self showWithLabel:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)phoneNumTFTextChanged:(UITextField *)field{
    self.loginButton.canOpration = YES;
}

- (void)setUpNaviView{
    self.view.backgroundColor = [UIColor whiteColor];
//    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
}

-(void)naviViewBackBtnDidClick{
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
