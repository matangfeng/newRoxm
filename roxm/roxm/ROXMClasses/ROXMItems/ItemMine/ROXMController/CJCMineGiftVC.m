//
//  CJCMineGiftVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMineGiftVC.h"
#import "CJCCommon.h"
#import "CJCInviteGiftModel.h"
#import "CJCMineGiftCell.h"
#import "CJCBuyInviteGiftVC.h"
#import <ReactiveObjC.h>

#define kInviteGiftCell          @"CJCInviteGiftCell"

@interface CJCMineGiftVC ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    
    NSMutableArray *dataArray;
    
    NSInteger pageSize;
    BOOL isRefresh;
    
    NSInteger selectIndex;
}

@property (nonatomic ,strong) UICollectionView *giftCollectionView;

@property (nonatomic ,strong) UIButton *buyButton;

@property (nonatomic ,strong) UILabel *hintLabel;

@end

@implementation CJCMineGiftVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = [NSMutableArray array];
    selectIndex = -1;
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getGiftList];
}

-(void)getGiftList{
    
    [self showWithLabelAnimation];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/list/my"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"page"] = [NSString stringWithFormat:@"%ld",(long)pageSize];
//    params[@"pageSize"] = [NSString stringWithFormat:@"25"];
    params[@"status"] = @(0);
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteGiftModel class] json:responseObject[@"data"]];
            
            [dataArray removeAllObjects];
            
            [dataArray addObjectsFromArray:tempArr];

            if (dataArray.count>0) {
                
                self.hintLabel.hidden = YES;
            }else{
            
                self.hintLabel.hidden = NO;
            }

            [self.giftCollectionView.mj_header endRefreshing];
            [self.giftCollectionView.mj_footer endRefreshing];
            
            [self.giftCollectionView reloadData];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return dataArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCMineGiftCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kInviteGiftCell forIndexPath:indexPath];
    
    CJCInviteGiftModel *model = dataArray[indexPath.item];
    
    cell.giftModel = model;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    CJCInviteGiftModel *model = dataArray[indexPath.item];
    
    if (model.status == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"是否要折现" preferredStyle:  UIAlertControllerStyleActionSheet];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"折现" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self giftDiscountWithGiftID:model.id];
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self presentViewController:alert animated:true completion:nil];
    }
}

-(void)giftDiscountWithGiftID:(NSString *)giftID{

    [self showWithLabelAnimation];
    
    NSString *discountURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/gift/discount"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = giftID;
    
    [CJCHttpTool postWithUrl:discountURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            [MBManager showBriefAlert:@"礼物折现成功"];
            
            [self loadNewData];
            
        }else{
        
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)setUpUI{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    CGFloat itemWidth = (SCREEN_WITDH)/4;
    
    flowLayout.itemSize = CGSizeMake(itemWidth, kAdaptedValue(116));
    flowLayout.minimumInteritemSpacing = 0;
    
    flowLayout.minimumLineSpacing = 0;
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64) collectionViewLayout:flowLayout];
    
    collectionView.backgroundColor = [UIColor whiteColor];
    
    collectionView.showsVerticalScrollIndicator = NO;
    
    collectionView.alwaysBounceVertical = YES;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    [collectionView registerClass:[CJCMineGiftCell class] forCellWithReuseIdentifier:kInviteGiftCell];
    
//    __weak __typeof(self) weakSelf = self;
    
    // 设置回调（一旦进入刷新状态就会调用这个refreshingBlock）
//    collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        
//        [weakSelf loadNewData];
//    }];
//    
//    collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        
//        [weakSelf loadMoreData];
//    }];
    
    self.giftCollectionView = collectionView;
    [self.view addSubview:collectionView];
    
    UILabel *hintLable = [UIView getYYLabelWithStr:@"您还没有购买礼物" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"818181"]];
    
    hintLable.textAlignment = NSTextAlignmentCenter;
    
    hintLable.frame = CGRectMake(0, 322.5, SCREEN_WITDH, kAdaptedValue(22.5));
    hintLable.centerY = SCREEN_HEIGHT/2;
    
    self.hintLabel = hintLable;
    [self.view addSubview:hintLable];
}

-(void)loadNewData{
    
    pageSize = 1;
    isRefresh = YES;
    
    [self getGiftList];
}

-(void)loadMoreData{
    
    pageSize++;
    
    isRefresh = NO;
    
    [self getGiftList];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"我的礼物" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"我的礼物" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.view addSubview:titleLabel];
    
    UIButton *shopButton = [UIView getButtonWithStr:@"礼物商城" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    CGFloat buttonWidth = [CJCTools getShortStringLength:@"礼物商城" withFont:[UIFont accodingVersionGetFont_mediumWithSize:15]];
    
    shopButton.size = CGSizeMake(buttonWidth,kAdaptedValue(23));
    shopButton.centerY = kNAVIVIEWCENTERY;
    shopButton.right = SCREEN_WITDH - kAdaptedValue(18);
    
    [[shopButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        CJCBuyInviteGiftVC *nextVC = [[CJCBuyInviteGiftVC alloc] init];
        nextVC.buyGiftVCType = CJCBuyInviteGiftVCTypePushType;
        nextVC.popHandle = ^{
            [self getGiftList];
        };
        [self.navigationController pushViewController:nextVC animated:YES];
    }];
    
    [self.view addSubview:shopButton];
}

-(void)cancleButtonClick{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
