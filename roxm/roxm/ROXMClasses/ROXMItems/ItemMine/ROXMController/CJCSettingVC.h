//
//  CJCSettingVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef BOOL(^escLoginSuccess)(void);

@interface CJCSettingVC : CommonVC
@property (nonatomic , strong) escLoginSuccess escSuccess;
@end
