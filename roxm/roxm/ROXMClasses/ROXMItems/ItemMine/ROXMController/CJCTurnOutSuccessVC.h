//
//  CJCTurnOutSuccessVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/23.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@interface CJCTurnOutSuccessVC : CommonVC

@property (nonatomic ,copy) NSString *disCountStr;

@end
