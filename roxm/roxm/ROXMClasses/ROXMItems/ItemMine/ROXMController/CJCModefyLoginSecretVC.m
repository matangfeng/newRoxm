//
//  CJCModefyLoginSecretVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCModefyLoginSecretVC.h"
#import "CJCCommon.h"
#import "CJCForgetPassWordVC.h"

@interface CJCModefyLoginSecretVC ()<UITextFieldDelegate>

@property (nonatomic ,strong) UITextField *oldSecretTF;

@property (nonatomic ,strong) UITextField *freshSecretTF;

@property (nonatomic ,strong) UITextField *ConfirmSecretTF;

@property (nonatomic ,strong) CJCOprationButton *saveButton;

@end

@implementation CJCModefyLoginSecretVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpUI];
}

-(void)setUpUI{

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backButton.frame = CGRectMake(kAdaptedValue(3), kAdaptedValue(33.5), kAdaptedValue(50), kAdaptedValue(50));
    
    backButton.centerY = kNAVIVIEWCENTERY;
    
    [backButton setImage:kGetImage(@"profile_wollet_close") forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(naviViewBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"修改密码" fontName:kFONTNAMEMEDIUM size:32 color:[UIColor toUIColorByStr:@"222222"]];
    
    titleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(80), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(32));
    
    [self.view addSubview:titleLabel];
    
    UIView *lineView1 = [UIView getLineView];
    
    lineView1.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(142), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:lineView1];
    
    CGFloat hintWidth = [CJCTools getShortStringLength:@"重复密码" withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    UILabel *oldLabel = [UIView getSystemLabelWithStr:@"旧密码" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"B8B8B8"]];
    
    oldLabel.frame = CGRectMake(kAdaptedValue(23), 100, hintWidth, kAdaptedValue(17));
    oldLabel.centerY = lineView1.bottom+kAdaptedValue(30);
    
    [self.view addSubview:oldLabel];
    
    UITextField *passTF = [self getSecretTextField];
    
    self.oldSecretTF = passTF;
    
    passTF.frame = CGRectMake(oldLabel.right+kAdaptedValue(10), 200, SCREEN_WITDH - kAdaptedValue(120), kAdaptedValue(31));
    passTF.centerY = oldLabel.centerY;

    
    UIView *lineView2 = [UIView getLineView];
    
    lineView2.frame = CGRectMake(kAdaptedValue(23), lineView1.bottom+kAdaptedValue(61), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:lineView2];
    
    UILabel *freshLabel = [UIView getSystemLabelWithStr:@"新密码" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"B8B8B8"]];
    
    freshLabel.frame = CGRectMake(kAdaptedValue(23), 100, hintWidth, kAdaptedValue(17));
    freshLabel.centerY = lineView2.bottom+kAdaptedValue(30);
    
    [self.view addSubview:freshLabel];
    
    UITextField *pass1TF = [self getSecretTextField];
    
    self.freshSecretTF = pass1TF;
    
    pass1TF.frame = CGRectMake(freshLabel.right+kAdaptedValue(10), 200, SCREEN_WITDH - kAdaptedValue(120), kAdaptedValue(31));
    pass1TF.centerY = freshLabel.centerY;
    
    UIView *lineView3 = [UIView getLineView];
    
    lineView3.frame = CGRectMake(kAdaptedValue(23), lineView2.bottom+kAdaptedValue(61), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:lineView3];
    
    UILabel *confirmLabel = [UIView getSystemLabelWithStr:@"重复密码" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"B8B8B8"]];
    
    confirmLabel.frame = CGRectMake(kAdaptedValue(23), 100, hintWidth, kAdaptedValue(17));
    confirmLabel.centerY = lineView3.bottom+kAdaptedValue(30);
    
    [self.view addSubview:confirmLabel];
    
    UITextField *pass2TF = [self getSecretTextField];
    
    self.ConfirmSecretTF = pass1TF;
    
    pass2TF.frame = CGRectMake(confirmLabel.right+kAdaptedValue(10), 200, SCREEN_WITDH - kAdaptedValue(120), kAdaptedValue(31));
    pass2TF.centerY = confirmLabel.centerY;
    
    UIView *lineView4 = [UIView getLineView];
    
    lineView4.frame = CGRectMake(kAdaptedValue(23), lineView3.bottom+kAdaptedValue(61), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:lineView4];
    
    CJCOprationButton *button = [[CJCOprationButton alloc] init];
    
    button.frame = CGRectMake(100, lineView4.bottom+kAdaptedValue(30), kAdaptedValue(200), kAdaptedValue(45));
    
    [button setTitle:@"保存" forState:UIControlStateNormal];
    
    button.centerX = self.view.centerX;
    
    button.canOpration = NO;
    
    self.saveButton = button;
    [self.view addSubview:button];
    
    UIButton *forgetButton = [UIView getButtonWithStr:@"忘记密码？" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    [forgetButton sizeToFit];
    
    forgetButton.top = button.bottom + kAdaptedValue(22);
    forgetButton.centerX = self.view.centerX;
    
    [forgetButton addTarget:self action:@selector(forgetButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:forgetButton];
}

-(void)forgetButtonDidClick{
    
    CJCForgetPassWordVC *nextVC = [[CJCForgetPassWordVC alloc] init];
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(UITextField *)getSecretTextField{

    UITextField *passTF = [[UITextField alloc] init];
    
    passTF.delegate = self;
    
    passTF.font = [UIFont accodingVersionGetFont_regularWithSize:16];
    
    passTF.textColor = [UIColor toUIColorByStr:@"333333"];
    
    passTF.placeholder = @"";
    
    passTF.secureTextEntry = YES;
    
    passTF.clearButtonMode = UITextFieldViewModeAlways;
    
    passTF.clearsOnBeginEditing = NO;
    
//    [passTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addSubview:passTF];
    
    return passTF;
}

-(void)naviViewBackBtnDidClick{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
