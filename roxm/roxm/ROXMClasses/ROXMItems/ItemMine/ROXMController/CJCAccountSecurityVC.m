//
//  CJCAccountSecurityVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCAccountSecurityVC.h"
#import "CJCCommon.h"
#import "CJCBigTopTitleCell.h"
#import "CJCSettingOprationCell.h"
#import "CJCPrivacySettingVC.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCNavigationController.h"
#import "CJCMessageAlertVC.h"
#import "CJCModefyLoginSecretVC.h"
#import "CJCInstallPaySecretVC.h"
#import "CJCIdentityConfirmVC.h"
#import "CJCBanlanceModel.h"
#import "CJCAlipayUserInfoManger.h"

#define kBigTopTitleCell            @"CJCBigTopTitleCell"
#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCAccountSecurityVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{
    
    CGFloat amont;
    
    CJCBanlanceModel *banlanceModel;
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCAccountSecurityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    [self configTableView];
    [self getWalletBanlance];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCBigTopTitleCell class] forCellReuseIdentifier:kBigTopTitleCell];
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{
    
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    if (index.row == 1) {
        
        if (isOpen) {
            
            [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kSETTINGHID];
        }else{
            
            [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kSETTINGHID];
        }
    }
}

-(void)getWalletBanlance{
    
    [self showWithLabelAnimation];
    
    NSString *banlanceURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/balance/get"];
    
    [CJCHttpTool postWithUrl:banlanceURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            banlanceModel = [CJCBanlanceModel modelWithDictionary:responseObject[@"data"][@"balance"]];
            
            [self.tableView reloadRow:3 inSection:0 withRowAnimation:NO];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(83);
    }else{
        
        return kAdaptedValue(61);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCBigTopTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        
        cell.title = @"账号与安全";
        
        return cell;
    }else{
        
        CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
        
        cell.delegate = self;
        
        switch (indexPath.row) {
            case 1:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"修改登录密码";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 2:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"设置支付密码";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 3:{
                
                cell.cellType = CJCSettingOprationCellTypeDefaultDetail;
                cell.title = @"绑定支付宝";
                
                if (![CJCTools isBlankString:banlanceModel.alipayUid]) {
                    
                    if ([CJCTools isBlankString:banlanceModel.alipay]) {
                        
                        cell.middleDetail = @"支付宝账户";
                        
                    }else{
                    
                        NSString *str4 = [banlanceModel.alipay stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
                        
                        cell.middleDetail = [NSString stringWithFormat:@"支付宝账户(%@)",str4];
                    }
                    
                    cell.arrowImageView.hidden = YES;
                    
                }else{
                
                    cell.middleDetail = @"未绑定";
                }
                
            }
                break;
                
            default:
                break;
        }
        cell.accessoryView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"profile_icon_arrowright"]];

        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 1) {
        
        CJCModefyLoginSecretVC *nextVC = [[CJCModefyLoginSecretVC alloc]init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 2){
    
        CJCIdentityConfirmVC *nextVC = [[CJCIdentityConfirmVC alloc]init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 3){
    
        if ([CJCTools isBlankString:banlanceModel.alipay]){
        
            CJCAlipayUserInfoManger *manger = [[CJCAlipayUserInfoManger alloc] init];
            
            manger.infoHandle = ^(NSString *alipayUid, NSString *alipayName) {
                
                [self hidHUD];
                
                NSString *str4 = [alipayName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
                
                CJCSettingOprationCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                
                cell.middleDetail = [NSString stringWithFormat:@"支付宝账户(%@)",str4];
                
                cell.arrowImageView.hidden = YES;
            };
            
            manger.failHandle = ^(NSString *errorStr) {
                
                [self hidHUD];
            };
        
        }
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    self.lineView.hidden = YES;
}

@end
