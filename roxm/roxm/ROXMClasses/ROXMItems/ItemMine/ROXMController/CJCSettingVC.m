//
//  CJCSettingVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCSettingVC.h"
#import "CJCCommon.h"
#import "CJCBigTopTitleCell.h"
#import "CJCSettingOprationCell.h"
#import "CJCPrivacySettingVC.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCNavigationController.h"
#import "CJCMessageAlertVC.h"
#import "CJCAccountSecurityVC.h"
#import "CJCSingleChatVC.h"

#define kBigTopTitleCell            @"CJCBigTopTitleCell"
#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCSettingVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{
    
    
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    [self configTableView];
//    [self getPersonalInfo];
}

//获取个人信息
-(void)getPersonalInfo{
    
    [MBManager showLoadingInView:self.view];
    
    NSString *infoURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/get/other"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = @"183444841672212480";
    
    [CJCHttpTool postWithUrl:infoURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            
        }else{
            
            [MBManager hideAlert];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCBigTopTitleCell class] forCellReuseIdentifier:kBigTopTitleCell];
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{

    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    if (index.row == 1) {
        
        if (isOpen) {
            
            [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kSETTINGHID];
        }else{
        
            [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kSETTINGHID];
        }
    }
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 7;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(83);
    }else if (indexPath.row == 1){
    
//        CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
        CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

        if (localInfoModel.sex == 0) {
            
            return kAdaptedValue(122);
        }
        
        return kAdaptedValue(0);
    }else if (indexPath.row == 5){
        
        return kAdaptedValue(122);
    }else{
    
        return kAdaptedValue(61);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCBigTopTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        
        cell.title = @"设置";
        
        return cell;
    }else{
    
        CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
        cell.delegate = self;
        
        switch (indexPath.row) {
            case 1:{
            
//                CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
                CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

                if (localInfoModel.sex == 0){
                
                    cell.cellType = CJCSettingOprationCellTypeSwitchHigher;
                    cell.title = @"开启隐身";
                    cell.detail = @"开启后，你将不出现在邀约列表中，别人也无法邀约你";
                    
                    if (![kUSERDEFAULT_STAND objectForKey:kSETTINGHID]) {
                        
                        cell.settingSwitch.on = YES;
                        
                        [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kSETTINGHID];
                    }else if ([[kUSERDEFAULT_STAND objectForKey:kSETTINGHID] isEqualToString:kSWITCHON]){
                        
                        cell.settingSwitch.on = YES;
                    }else if ([[kUSERDEFAULT_STAND objectForKey:kSETTINGHID] isEqualToString:kSWITCHOFF]){
                        
                        cell.settingSwitch.on = NO;
                    }
                }else{
                
                    cell.cellType = CJCSettingOprationCellTypeDefault;
                    
                    cell.isShow = YES;
                }
                
            }
                break;
                
            case 2:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"账号与安全";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 3:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"消息通知";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 4:{
                
                cell.cellType = CJCSettingOprationCellTypeDefault;
                cell.title = @"问题反馈";
                cell.bottomLineView.hidden = YES;
            }
                break;
                
            case 5:{
                
                cell.cellType = CJCSettingOprationCellTypeDefaultHigher;
                cell.title = @"隐私设置";
                cell.detail = @"开启后,你手机通讯录中的联系人将不会看到你,你也看不到他们";
            }
                break;
                
            case 6:{
                
                cell.cellType = CJCSettingOprationCellTypeNoArrow;
                cell.title = @"退出当前账号";
                
            }
                break;
                
            default:
                break;
        }

        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
 
    if (indexPath.row == 2) {
        
        CJCAccountSecurityVC *nextVC = [[CJCAccountSecurityVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 3) {
        
        CJCMessageAlertVC *nextVC = [[CJCMessageAlertVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 4) {
        
        CJCSingleChatVC *chatController = [[CJCSingleChatVC alloc] init];
        
        chatController.conversationId = @"183444841672212480";
//        chatController.location = self.location;
        chatController.qunjuId = @"183444841672212480";
//        chatController.infoModel = infoModel;
        chatController.nickName = @"濡沫小助手";
        chatController.iconImageUrl = @"http://image.hojji.com/u_img/58/13/183444841672212480_1709259106";
        
        [self.navigationController pushViewController:chatController animated:YES];
    }else if (indexPath.row == 5) {
        
        CJCPrivacySettingVC *nextVC = [[CJCPrivacySettingVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else if (indexPath.row == 6){
    
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"确定退出当前账号吗?" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            [CJCpersonalInfoModel infoModel].loginUserDict = nil;
            [CJCpersonalInfoModel infoModel].loginInfoDict = nil;
            [[CJCpersonalInfoModel infoModel] userLogout];
            
            if (_escSuccess) {
               BOOL status =  _escSuccess();
                if (status) {
                    
                }
            }
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [self presentViewController:alert animated:true completion:nil];
    }
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);

    self.lineView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
