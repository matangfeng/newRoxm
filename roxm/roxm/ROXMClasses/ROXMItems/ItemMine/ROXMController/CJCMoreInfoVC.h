//
//  CJCMoreInfoVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^moreInfoHandle)(NSDictionary *infoDict);

@interface CJCMoreInfoVC : CommonVC

@property (nonatomic ,copy) moreInfoHandle moreInfoHandle;

@end
