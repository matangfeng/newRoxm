//
//  CJCClickSecretVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCClickSecretVC.h"
#import "CJCCommon.h"
#import "CJCClickSecretView.h"
#import "CJCClickSecretSetSuccessVC.h"

@interface CJCClickSecretVC ()

@property (nonatomic ,strong) UILabel *backLabel;

@property (nonatomic ,strong) UILabel *owmTitleLable;

@property (nonatomic ,strong) UILabel *hintLabel;

@property (nonatomic ,strong) UIView *secretView;

@property (nonatomic ,strong) UIView *touchView;

@property (nonatomic ,strong) UILongPressGestureRecognizer *setSecretTap;

@end

@implementation CJCClickSecretVC

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setUpNaviView];
    
//    UITapGestureRecognizer *setSecretTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setSecretTapHandle:)];
    
    UILongPressGestureRecognizer *setSecretTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(setSecretTapHandle:)];
    
    setSecretTap.minimumPressDuration = 2.0;
    
    self.setSecretTap = setSecretTap;
    [self.view addGestureRecognizer:setSecretTap];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    self.hintLabel.hidden = YES;
    self.owmTitleLable.hidden = YES;
    self.backLabel.hidden = YES;
    
//    self.secretView.hidden = NO;
    self.touchView.hidden = NO;
    
    UITouch *touch = touches.anyObject;
    
    CGPoint centerPoint = [touch locationInView:self.view];
    
    self.secretView.center = centerPoint;
    self.touchView.center = centerPoint;
    
    NSLog(@"%f-------\n%f",centerPoint.x,centerPoint.y);
    
    [self scaleAnimation];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    self.hintLabel.hidden = NO;
    self.owmTitleLable.hidden = NO;
    self.backLabel.hidden = NO;
    
//    self.secretView.hidden = YES;
    self.touchView.hidden = YES;
    
    [self.secretView.layer removeAllAnimations];
}

-(void)scaleAnimation{
    
    CABasicAnimation *anima = [CABasicAnimation animationWithKeyPath:@"transform.scale"];//同上
    anima.toValue = [NSNumber numberWithFloat:2.3f];
    anima.duration = 2.0f;
    anima.repeatCount = NO;
    [self.touchView.layer addAnimation:anima forKey:@"scaleAnimation"];
}

-(void)setSecretTapHandle:(UILongPressGestureRecognizer *)tap{

    if(tap.state == UIGestureRecognizerStateBegan){
    
        CGPoint centerPoint = [tap locationInView:tap.view];
        
        [self updateInfoWithPoint:centerPoint];
    }
}

-(void)updateInfoWithPoint:(CGPoint)point{
    
    [self showWithLabelAnimation];
    
    NSString *updateUrl = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/lock_screen/set"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"lockScreenX"] = @(point.x);
    
    params[@"lockScreenY"] = @(point.y);

    
    [CJCHttpTool postWithUrl:updateUrl params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [kUSERDEFAULT_STAND setObject:@(point.x) forKey:@"secretAreaX"];
            [kUSERDEFAULT_STAND setObject:@(point.y) forKey:@"secretAreaY"];
            
            [kUSERDEFAULT_STAND synchronize];
            
            [self pushToNextVCWithPoint:point];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToNextVCWithPoint:(CGPoint)point{

    CJCClickSecretSetSuccessVC *nextVC = [[CJCClickSecretSetSuccessVC alloc] init];
    
    nextVC.secretPoint = point;
    
    [self presentViewController:nextVC animated:YES completion:nil];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self setKeyBoardHid:YES];
    
    UILabel *backLabel = [UIView getSystemLabelWithStr:@"取消" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor whiteColor]];
    
    [backLabel sizeToFit];
    backLabel.top = kAdaptedValue(32);
    backLabel.right = SCREEN_WITDH - kAdaptedValue(16);
    
    self.backLabel = backLabel;
    [self.view addSubview:backLabel];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    backButton.frame = CGRectMake(kAdaptedValue(3), kAdaptedValue(33.5), kAdaptedValue(50), kAdaptedValue(50));
    backButton.center = backLabel.center;
    
    [backButton addTarget:self action:@selector(naviViewBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    UILabel *detailLabel = [UIView getSystemLabelWithStr:@"放置手指" fontName:kFONTNAMEREGULAR size:28 color:[UIColor toUIColorByStr:@"8C959F"]];
    
    detailLabel.textAlignment = NSTextAlignmentCenter;
    
    detailLabel.frame = CGRectMake(0, kAdaptedValue(96), SCREEN_WITDH, kAdaptedValue(28));
    
    detailLabel.userInteractionEnabled = YES;
    
    self.owmTitleLable = detailLabel;
    [self.view addSubview:detailLabel];
    
    UILabel *hintLabel = [UIView getSystemLabelWithStr:@"请长按设置解锁位置" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.77]];
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    hintLabel.userInteractionEnabled = YES;
    
    hintLabel.frame = CGRectMake(0, detailLabel.bottom+kAdaptedValue(12), SCREEN_WITDH, kAdaptedValue(16));
    
    self.hintLabel = hintLabel;
    [self.view addSubview:hintLabel];
    
    UIView *secretArea = [[UIView alloc] init];
    
    secretArea.backgroundColor = [UIColor toUIColorByStr:@"429CF0"];
    
    secretArea.size = CGSizeMake(kAdaptedValue(124), kAdaptedValue(124));
    secretArea.centerX = self.view.centerX;
    secretArea.top = hintLabel.bottom + kAdaptedValue(134);
    
    secretArea.layer.cornerRadius = kAdaptedValue(62);
    secretArea.layer.masksToBounds = YES;
    
    self.secretView = secretArea;
    [self.view addSubview:secretArea];
    
    UIView *touchArea = [[UIView alloc] init];
    
    touchArea.backgroundColor = [UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.68];
    
    touchArea.size = CGSizeMake(kAdaptedValue(52), kAdaptedValue(52));
    touchArea.center = secretArea.center;
    
    touchArea.layer.cornerRadius = kAdaptedValue(26);
    touchArea.layer.masksToBounds = YES;
    
    self.touchView = touchArea;
    [self.view addSubview:touchArea];
    
    secretArea.hidden = YES;
    touchArea.hidden = YES;
}

-(void)naviViewBackBtnDidClick{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
