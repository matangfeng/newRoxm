//
//  CJCChangeNickNameVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/26.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^changeInfoHandle)(NSDictionary *infoDict);

@interface CJCChangeNickNameVC : CommonVC

@property (nonatomic ,copy) NSString *typeName;

@property (nonatomic ,copy) NSString *nickName;

@property (nonatomic ,copy) changeInfoHandle changeHandle;

@end
