//
//  CJCPrivacyModelVC.m
//  roxm
//
//  Created by 陈建才 on 2017/11/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCPrivacyModelVC.h"
#import "CJCCommon.h"
#import "CJCBigTopTitleCell.h"
#import "CJCSettingOprationCell.h"
#import "CJCPrivacyModelTopCell.h"
#import "CJCClickSecretVC.h"

#define kBigTopTitleCell            @"CJCPrivacyModelTopCell"
#define kSettingOprationCell        @"CJCSettingOprationCell"

@interface CJCPrivacyModelVC ()<UITableViewDelegate,UITableViewDataSource,CJCSettingOprationCellDelegate>{
    
    
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation CJCPrivacyModelVC

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(secretSetSuccess) name:@"clickSecretViewShow" object:nil];
    
    [self setUpNaviView];
    [self configTableView];
}

-(void)configTableView{
    
    UITableView *tableView = [[UITableView alloc] init];
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.tableView = tableView;
    [self.view addSubview:tableView];
    
    [tableView registerClass:[CJCPrivacyModelTopCell class] forCellReuseIdentifier:kBigTopTitleCell];
    [tableView registerClass:[CJCSettingOprationCell class] forCellReuseIdentifier:kSettingOprationCell];
}

-(void)switchChangeHandle:(BOOL)isOpen andIndex:(CJCSettingOprationCell *)cell{
    
    NSIndexPath *index = [self.tableView indexPathForCell:cell];
    
    if (index.row == 1) {
        
        if (isOpen) {
            
            if ([kUSERDEFAULT_STAND objectForKey:@"secretAreaX"]){
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:@"secretAreaIsSet"];
            }else{
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    
                    CJCClickSecretVC *nextVC = [[CJCClickSecretVC alloc]init];
                    
                    [self presentViewController:nextVC animated:YES completion:^{
                        
                        
                    }];
                });
            }
            
        }else{
            
            [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:@"secretAreaIsSet"];
        }
        
        [self.tableView reloadData];
    }
}

#pragma mark ========tableView的delegate和DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([[kUSERDEFAULT_STAND objectForKey:@"secretAreaIsSet"] isEqualToString:kSWITCHON]){
    
        return 3;
    }
    
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return kAdaptedValue(161);
    }else{
        
        return kAdaptedValue(61);
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCPrivacyModelTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        
//        cell.title = @"隐私模式";
        
        return cell;
    }else{
        
        CJCSettingOprationCell *cell = [tableView dequeueReusableCellWithIdentifier:kSettingOprationCell];
        
        cell.delegate = self;
        
        switch (indexPath.row) {
            case 1:{
                
                cell.cellType = CJCSettingOprationCellTypeSwitch;
                cell.title = @"隐私模式";
                
                if ([[kUSERDEFAULT_STAND objectForKey:@"secretAreaIsSet"] isEqualToString:kSWITCHON]){
                
                    cell.settingSwitch.on = YES;
                    cell.bottomLineView.hidden = YES;
                }else{
                
                    cell.settingSwitch.on = NO;
                    cell.bottomLineView.hidden = NO;
                }
                
            }
                break;
                
            case 2:{
                
                cell.cellType = CJCSettingOprationCellTypeDefaultDetail;
                cell.title = @"长按位置";
                
                if ([kUSERDEFAULT_STAND objectForKey:@"secretAreaX"]){
                    
                    cell.middleDetail = @"修改";
                }else{
                
                    cell.middleDetail = @"去绑定";
                }
            
            }
                break;
                
            default:
                break;
        }
        
        return cell;
    }
}

-(void)secretSetSuccess{

    CJCSettingOprationCell *cell1 = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    cell1.settingSwitch.on = YES;
    
    [self switchChangeHandle:YES andIndex:cell1];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 2) {
        
        CJCClickSecretVC *nextVC = [[CJCClickSecretVC alloc]init];
        
        [self presentViewController:nextVC animated:YES completion:^{
            
            
        }];
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
