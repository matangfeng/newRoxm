//
//  CJCBalanceTurnOutVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBalanceTurnOutVC.h"
#import "CJCCommon.h"
#import "CJCIdentityConfirmVC.h"
#import "CJCBindingAlipayVC.h"
#import "CJCBanlanceModel.h"
#import "CJCAlipayUserInfoManger.h"
#import "CJCInputSecretView.h"
#import "CJCTurnOutSuccessVC.h"
#import <ReactiveObjC.h>

@interface CJCBalanceTurnOutVC ()<UITextFieldDelegate>{

    BOOL isBindingAlipay;
}

    @property (nonatomic , strong) UILabel * saveOntLabel;
@property (nonatomic ,strong) UILabel *alipayNickNameLabel;

@property (nonatomic ,strong) UIButton *bindingButton;

@property (nonatomic ,strong) UIImageView *arrowView;

@property (nonatomic ,strong) UILabel *amontLabel;

@property (nonatomic ,strong) UIButton *totalButton;

@property (nonatomic ,strong) UITextField *discountTF;

@property (nonatomic ,strong) UIButton *nextStepButton;

@property (nonatomic ,assign) BOOL isHaveDian;

@end

@implementation CJCBalanceTurnOutVC

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inputSecretView) name:@"inputSecretViewShow" object:nil];
    
    isBindingAlipay = NO;
    
    [self setUpNaviView];
    
    [self setUpUI];
}

-(void)inputSecretView{

    CJCInputSecretView *secretView = [[CJCInputSecretView alloc] init];
    
    secretView.confirmHandle = ^{
        
        [self banlanceTurnout];
    };
    
    [secretView showPickerView];
}

-(void)setUpUI{
    
    UILabel *nameHintLabel = [UIView getSystemLabelWithStr:@"到账账户" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    CGFloat width1 = [CJCTools getShortStringLength:@"到账账户" withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    nameHintLabel.frame = CGRectMake(kAdaptedValue(23), 64+kAdaptedValue(22), width1, kAdaptedValue(17));
    self.saveOntLabel = nameHintLabel;
    [self.view addSubview:nameHintLabel];
    
    UILabel *bindingLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    bindingLabel.frame = CGRectMake(kAdaptedValue(23), 64+kAdaptedValue(22), SCREEN_WITDH - kAdaptedValue(150), kAdaptedValue(17));
    
    self.alipayNickNameLabel = bindingLabel;
    [self.view addSubview:bindingLabel];
    
    UIView *topLineView = [UIView getLineView];
    
    topLineView.frame = CGRectMake(kAdaptedValue(23), 64+kAdaptedValue(61), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:topLineView];
    
    UIButton *bingdingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    bingdingButton.frame = CGRectMake(0, 64, SCREEN_WITDH, 61);
    
    
    [[bingdingButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self showWithLabelAnimation];
        CJCAlipayUserInfoManger *manger = [[CJCAlipayUserInfoManger alloc] init];
        manger.infoHandle = ^(NSString *alipayUid, NSString *alipayName) {
            [self hidHUD];
            self.saveOntLabel.hidden = NO;
            isBindingAlipay = YES;
            self.bindingButton.hidden = YES;
            NSString *str4 = [alipayName stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
            self.alipayNickNameLabel.text = [NSString stringWithFormat:@"支付宝账户(%@)",str4];
            self.alipayNickNameLabel.x = self.discountTF.x;
            self.arrowView.hidden = YES;
            if (![CJCTools isBlankString:self.discountTF.text]) {
                self.nextStepButton.canOpration = YES;
            }
        };
        manger.failHandle = ^(NSString *errorStr) {
            [self hidHUD];
        };
        [self hidHUD];
    }];

    self.bindingButton = bingdingButton;
    [self.view addSubview:bingdingButton];
    
    NSString *alipayAcount;
    
    if ([CJCTools isBlankString:self.banlanceModel.alipayUid]) {
        alipayAcount = @"绑定支付宝账户";
        bingdingButton.hidden = NO;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"profile_icon_arrowright")];
        
        imageView.size = CGSizeMake(kAdaptedValue(22), kAdaptedValue(22));
        imageView.centerY = bindingLabel.centerY;
        imageView.right = SCREEN_WITDH - kAdaptedValue(15);
        
        self.arrowView = imageView;
        [self.view addSubview:imageView];
        
    }else{
        
        isBindingAlipay = YES;
        bingdingButton.hidden = YES;
        if ([CJCTools isBlankString:self.banlanceModel.alipay]) {
            alipayAcount = @"支付宝账户";
        }else{
            NSString *str4 = [self.banlanceModel.alipay stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
            alipayAcount = [NSString stringWithFormat:@"支付宝账户(%@)",str4];
        }
    }
    
    bindingLabel.text = alipayAcount;
    
    UILabel *moneyLabel = [UIView getSystemLabelWithStr:@"金额(元)" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"222222"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"金额(元)" withFont:[UIFont accodingVersionGetFont_regularWithSize:16]];
    
    moneyLabel.frame = CGRectMake(kAdaptedValue(23), topLineView.bottom+kAdaptedValue(22), width, kAdaptedValue(17));
    
    [self.view addSubview:moneyLabel];
    
    UITextField *phoneTF = [[UITextField alloc] init];
    
    phoneTF.frame = CGRectMake(moneyLabel.right+kAdaptedValue(20), kAdaptedValue(177), SCREEN_WITDH-kAdaptedValue(150), kAdaptedValue(31));
    
    phoneTF.centerY = moneyLabel.centerY;
    
    phoneTF.font = [UIFont accodingVersionGetFont_regularWithSize:18];
    
    phoneTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    phoneTF.placeholder = @"请输入金额";
    
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    
    phoneTF.delegate = self;
//
    [phoneTF addTarget:self action:@selector(phoneNumTFTextChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.discountTF = phoneTF;
    [self.view addSubview:phoneTF];
    
    if ([CJCTools isBlankString:self.banlanceModel.alipayUid]) {
        self.saveOntLabel.hidden = YES;
        bindingLabel.x = kAdaptedValue(23);
    }else{
        self.saveOntLabel.hidden = NO;
        bindingLabel.x = phoneTF.x;
    }
    
    UIView *bottomLineView = [UIView getLineView];
    
    bottomLineView.frame = CGRectMake(kAdaptedValue(23), topLineView.bottom+kAdaptedValue(61), SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.view addSubview:bottomLineView];
    
    NSString *amontStr = [NSString stringWithFormat:@"钱包余额 ¥%.2f",self.banlanceModel.amount];
    
    UILabel *balanceLabel = [UIView getSystemLabelWithStr:amontStr fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"A9A9A9"]];
    
    CGFloat balanceWidth = [CJCTools getShortStringLength:amontStr withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    balanceLabel.frame = CGRectMake(kAdaptedValue(23), bottomLineView.bottom+kAdaptedValue(17), balanceWidth, kAdaptedValue(17));
    
    self.amontLabel = balanceLabel;
    [self.view addSubview:balanceLabel];
    
    UIButton *totalButton = [UIView getButtonWithStr:@"全部转出" fontName:kFONTNAMEMEDIUM size:15 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    [totalButton sizeToFit];
    
    totalButton.centerY = balanceLabel.centerY;
    totalButton.left = balanceLabel.right + kAdaptedValue(20);
    
    [totalButton addTarget:self action:@selector(totalButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    self.totalButton = totalButton;
    [self.view addSubview:totalButton];
    
    UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B"]] forState:0];

    [buyButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"B39B6B" andAlpha:0.5]] forState:UIControlStateSelected];

    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [buyButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];

    [buyButton setTitle:@"转出" forState:UIControlStateNormal];
    
    buyButton.frame = CGRectMake(kAdaptedValue(23), totalButton.bottom+kAdaptedValue(30), SCREEN_WITDH-kAdaptedValue(46), kAdaptedValue(49));

    buyButton.canOpration = NO;
    
    buyButton.layer.cornerRadius = kAdaptedValue(3);
    buyButton.layer.masksToBounds = YES;
    
    [buyButton addTarget:self action:@selector(buyButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    self.nextStepButton = buyButton;
    [self.view addSubview:buyButton];
}

-(void)phoneNumTFTextChanged:(UITextField *)textField{

    if (isBindingAlipay && ![CJCTools isBlankString:textField.text]) {
      self.nextStepButton.canOpration = YES;
    }
    
    CGFloat leftBanlance = self.banlanceModel.amount - textField.text.floatValue;
    
    NSString *banlanceStr = [NSString stringWithFormat:@"钱包余额 ¥%.2f",leftBanlance];
    
    if (leftBanlance<0) {
        
        self.amontLabel.text = [NSString stringWithFormat:@"输入金额超过零钱余额"];
        
        CGFloat balanceWidth = [CJCTools getShortStringLength:@"输入金额超过零钱余额" withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
        
        self.amontLabel.width = balanceWidth;
        
        self.totalButton.hidden = YES;
        self.nextStepButton.canOpration = NO;
    }else{
    
        self.amontLabel.text = banlanceStr;
        
        CGFloat balanceWidth = [CJCTools getShortStringLength:banlanceStr withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
        
        self.amontLabel.width = balanceWidth + 10;
        
        self.totalButton.left = self.amontLabel.right+kAdaptedValue(20);
        
        self.totalButton.hidden = NO;
        
        [self getWalletBanlance];
    }
}

-(void)getWalletBanlance{
    
    NSString *banlanceURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/balance/get"];
    
    [CJCHttpTool postWithUrl:banlanceURL params:nil success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            _banlanceModel = [CJCBanlanceModel modelWithDictionary:responseObject[@"data"][@"balance"]];
            
            if ([CJCTools isBlankString:self.banlanceModel.alipayUid]) {
                self.nextStepButton.canOpration = NO;
            }else{
                if (self.discountTF.text.length == 0){
                    self.nextStepButton.canOpration = NO;
                }else{
                    self.nextStepButton.canOpration = YES;
                }
            }
        }else{
       
        }
        
    } failure:^(NSError *error) {
        
    }];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /*
     * 不能输入.0-9以外的字符。
     * 设置输入框输入的内容格式
     * 只能有一个小数点
     * 小数点后最多能输入两位
     * 如果第一位是.则前面加上0.
     * 如果第一位是0则后面必须输入点，否则不能输入。
     */
    
    // 判断是否有小数点
    if ([textField.text containsString:@"."]) {
        self.isHaveDian = YES;
    }else{
        self.isHaveDian = NO;
    }
    
    if (string.length > 0) {
        
        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        
        // 不能输入.0-9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.'))
        {
            return NO;
        }
        
        // 只能有一个小数点
        if (self.isHaveDian && single == '.') {
            return NO;
        }
        
        // 如果第一位是.则前面加上0.
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }
        
        // 如果第一位是0则后面必须输入点，否则不能输入。
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
    
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    
                    return NO;
                }
            }
        }
        
        // 小数点后最多能输入两位
        if (self.isHaveDian) {
            NSRange ran = [textField.text rangeOfString:@"."];
            // 由于range.location是NSUInteger类型的，所以这里不能通过(range.location - ran.location)>2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    
                    return NO;
                }
            }
        }
        
    }
    
    return YES;
}

-(void)totalButtonClickHandle{

    self.discountTF.text = [NSString stringWithFormat:@"%.2f",self.banlanceModel.amount];
    
    self.amontLabel.text = [NSString stringWithFormat:@"钱包余额 ¥0.00"];
    
    self.totalButton.hidden = NO;
    
    if ([CJCTools isBlankString:self.banlanceModel.alipayUid]) {
        self.nextStepButton.canOpration = NO;
    }else{
        self.nextStepButton.canOpration = YES;
    }
}

-(void)buyButtonClickHandle{
    
//    [self getCodefor_pay];
    
    if ([CJCTools isBlankString:self.discountTF.text]) {
        
        [MBManager showBriefAlert:@"请输入取现金额"];
        return;
    }
    
    [self getPayPassWord];
}

-(void)getPayPassWord{

    NSString *getURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/has/pay_password"];
    
    [CJCHttpTool postWithUrl:getURL params:nil success:^(id responseObject) {
    
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            NSNumber *dataNum = responseObject[@"data"];
            
            if (dataNum.integerValue == 0) {
                
                CJCIdentityConfirmVC *nextVC = [[CJCIdentityConfirmVC alloc] init];
                nextVC.amontStr = self.discountTF.text;
                [self.navigationController pushViewController:nextVC animated:YES];
            }else if(dataNum.integerValue == 1){
            
                CJCInputSecretView *secretView = [[CJCInputSecretView alloc] init];
                secretView.confirmHandle = ^{
                        [self banlanceTurnout];
                };
                [secretView showPickerView];
            }
            
        }else{
        
            [MBManager showBriefAlert:@"获取是否设置支付密码失败"];
        }
        
    } failure:^(NSError *error) {
       
        
    }];
}

-(void)banlanceTurnout{

    [self showWithLabelAnimation];
    
    NSString *turnoutURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/pay/withdraw/apply"];
    
    CGFloat turnoutNum = self.discountTF.text.floatValue*100;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"amount"] = @((NSInteger)turnoutNum);
    
    [CJCHttpTool postWithUrl:turnoutURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            CJCTurnOutSuccessVC *nextVC = [[CJCTurnOutSuccessVC alloc]init];
            
            nextVC.disCountStr = self.discountTF.text;
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }else{
        
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getCodefor_pay{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *codeURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/sms/send/for_pay"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = infoModel.mobile;
    
    [CJCHttpTool postWithUrl:codeURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 1011) {
            
            
        }else if (rtNum.integerValue == 0){
        
        
        }else{
        
            [self showWithLabel:responseObject[@""]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"余额转出" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    CGFloat width = [CJCTools getShortStringLength:@"余额转出" withFont:[UIFont accodingVersionGetFont_mediumWithSize:16]];
    
    titleLabel.size = CGSizeMake(width,kAdaptedValue(23));
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
