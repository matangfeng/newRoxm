//
//  CJCBeforeTakePhotoVC.m
//  roxm
//
//  Created by lfy on 2017/8/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBeforeTakePhotoVC.h"
#import "CJCCommon.h"
#import "CJCBeautyCameraAndVideoVC.h"
#import <AVFoundation/AVFoundation.h>
#import "CJCContentBrowser.h"
#import "CJCAddMorePictureVC.h"
#import "CJCUploadManager.h"
#import "CJCMaillistVC.h"
#import "CJCDownloadManger.h"

@interface CJCBeforeTakePhotoVC (){

    AVPlayer *videoPlayer;
    AVPlayerLayer *videoplayerLayer;
    AVPlayerItem *videoitem;
    
    NSDictionary *returnDict;
}

@property (nonatomic ,strong) UIButton *imageButton;

@property (nonatomic ,strong) UIButton *bigRoundButton;

@property (nonatomic ,strong) UIButton *blueLabel;

@property (nonatomic ,strong) UIButton *iconImageView;

@property (nonatomic ,strong) UIButton *reTakePhotoButton;

@property (nonatomic ,strong) CJCOprationButton *nextButton;

@property (nonatomic ,strong) UIImage *returnImage;

@property (nonatomic ,strong) UIButton *playButton;

@property (nonatomic ,strong) CLLocationManager *manger;

@end

@implementation CJCBeforeTakePhotoVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.takePhotoType == CJCTakePhotoTypeChangeVideo) {
        
        [MBManager showLoading];
        
        self.imageButton.hidden = YES;
        self.blueLabel.hidden = YES;
        self.bigRoundButton.hidden = YES;
        self.reTakePhotoButton.hidden = NO;
        
        if (self.nextButton.canOpration != YES) {
            
            self.nextButton.canOpration = NO;
        }
        
        CJCDownloadManger *manger = [[CJCDownloadManger alloc] init];
        
        [manger downloadVideoWithVideoURL:self.videoPath];
        
//        manger.progressHandle = ^(CGFloat progress) {
//            
//            self.cycleView.hidden = NO;
//            
//            self.playView.hidden = YES;
//            
//            self.showImageView.hidden = YES;
//            
//            self.cycleView.center = self.contentView.center;
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [self.cycleView drawProgress:progress];
//            });
//            
//        };
        
        manger.successHandle = ^(NSString *loadPath, UIImage *returnImage, NSString *videoLength) {
        
            [MBManager hideAlert];
            [self creatPreviewViewWithPath:loadPath];
        };

    }
    
    if (self.takePhotoType == CJCTakePhotoTypeChangeCamera){
    
        if (self.nextButton.canOpration != YES) {
            
            self.imageButton.hidden = YES;
            self.blueLabel.hidden = YES;
            self.bigRoundButton.hidden = YES;
            self.reTakePhotoButton.hidden = NO;
            
            self.nextButton.canOpration = NO;
            
            [self.iconImageView setImageWithURL:[NSURL URLWithString:self.imagePath] forState:UIControlStateNormal options:YYWebImageOptionShowNetworkActivity];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self requestLocation];
}

-(void)requestLocation{

    if ([CJCTools locationStatus] == kCLAuthorizationStatusAuthorizedAlways){
    
        return;
    }else{
    
        CLLocationManager *manger = [[CLLocationManager alloc] init];
        
        self.manger = manger;
        
        [manger requestAlwaysAuthorization];
    }
}

-(void)setUpNaviView{

    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 61);
    
    [self setNaviViewBottom:61];
    
    if (self.sexType == CJCSexTypeWoman) {
        
        if (self.takePhotoType == CJCTakePhotoTypeCamera) {
            
            self.progressViewWidth = kWOMANREGISTERPROGRESSWIDTH;
            
        }else{
        
            self.progressViewWidth = kWOMANREGISTERPROGRESSWIDTH*2;
        }
    }else{
        
        if (self.takePhotoType == CJCTakePhotoTypeCamera) {
            
            self.progressViewWidth = kMANREGISTERPROGRESSWIDTH;
        }else{
        
            self.progressViewWidth = kMANREGISTERPROGRESSWIDTH*3;
        }
        
    }
    
}

-(void)setUpUI{

    NSString *bigTitle,*prompt,*imageName,*blueStr;
    CGRect bigTitleFrame,promptFrame;
    
    if (self.takePhotoType == CJCTakePhotoTypeCamera || self.takePhotoType == CJCTakePhotoTypeChangeCamera) {
        
        bigTitle = @"拍摄真实头像";
        bigTitleFrame = CGRectMake(kAdaptedValue(20), kAdaptedValue(85), kAdaptedValue(168), kAdaptedValue(28));
        
        prompt = @"头像不真实无法进行邀约";
        promptFrame = CGRectMake(kAdaptedValue(20), kAdaptedValue(127), kAdaptedValue(193), kAdaptedValue(21));
        
        imageName = @"login_icon_photo";
        
        blueStr = @"打开前置摄像头拍照";
    }else{
    
        bigTitle = @"拍摄短视频";
        bigTitleFrame = CGRectMake(kAdaptedValue(43), kAdaptedValue(80), kAdaptedValue(140), kAdaptedValue(28));
        
        prompt = @"拍摄一段10秒短视频，介绍一下自己，说说自己的兴趣爱好";
        promptFrame = CGRectMake(kAdaptedValue(43), kAdaptedValue(122), kAdaptedValue(289.5), kAdaptedValue(51));
        
        imageName = @"login_icon_video";
        
        blueStr = @"打开前置摄像头录像";
    }
    
    UILabel *titleLabel = [UIView getYYLabelWithStr:bigTitle fontName:kFONTNAMELIGHT size:28 color:[UIColor toUIColorByStr:@"222222"]];

    titleLabel.frame = bigTitleFrame;
    
    [self.view addSubview:titleLabel];
    
    UILabel *promptLabel = [UIView getYYLabelWithStr:prompt fontName:kFONTNAMELIGHT size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    promptLabel.numberOfLines = 0;
    
    promptLabel.frame = promptFrame;
    
    [self.view addSubview:promptLabel];
    
    UIButton *openCameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.bigRoundButton = openCameraButton;
    
    openCameraButton.frame = CGRectMake(kAdaptedValue(139), kAdaptedValue(285), kAdaptedValue(97), kAdaptedValue(97));
    
    UIImage *backGroudImage = [UIView imageWithColor:[UIColor toUIColorByStr:@"B6B8C2"]];
    
    [openCameraButton setBackgroundImage:backGroudImage forState:UIControlStateNormal];
    
    openCameraButton.layer.cornerRadius = kAdaptedValue(46.5);
    openCameraButton.layer.masksToBounds = YES;
    
    //设置高亮状态不置灰
    //[openCameraButton setAdjustsImageWhenHighlighted:NO];
    
    [openCameraButton addTarget:self action:@selector(takePhotoButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:openCameraButton];
    
    UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.imageButton = imageButton;
    
    imageButton.frame = CGRectMake(kAdaptedValue(171.5), kAdaptedValue(317.5), kAdaptedValue(32), kAdaptedValue(32));
    
    [imageButton setImage:kGetImage(imageName) forState:UIControlStateNormal];
    
    [imageButton addTarget:self action:@selector(takePhotoButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:imageButton];
    
    UIButton *blueButton = [UIView getButtonWithStr:blueStr fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"4198F0"]];
    
    [blueButton setTitleColor:[UIColor toUIColorByStr:@"4198F0" andAlpha:0.6] forState:UIControlStateHighlighted];
    
    self.blueLabel = blueButton;
    
    [blueButton addTarget:self action:@selector(takePhotoButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    blueButton.frame = CGRectMake(kAdaptedValue(124.5), kAdaptedValue(398), kAdaptedValue(126), kAdaptedValue(14));
    blueButton.centerX = self.view.centerX;
    
    [self.view addSubview:blueButton];
    
    CJCOprationButton *nextButton = [[CJCOprationButton alloc] init];
    
    nextButton.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(581.5), kAdaptedValue(200), kAdaptedValue(45));
    
    self.nextButton = nextButton;
    
    if (self.sexType == CJCSexTypeWoman) {
        
        [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    }else{
    
        [nextButton setTitle:@"完成" forState:UIControlStateNormal];
    }
    
    nextButton.canOpration = NO;
    
    [nextButton addTarget:self action:@selector(nextButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextButton];
}

-(void)nextButtonDidClick{
    
    if (self.takePhotoType == CJCTakePhotoTypeCamera || self.takePhotoType == CJCTakePhotoTypeChangeCamera) {
        
        [self uploadImageToQINIU];
        
    }else{
    
        [self upLoadVideoToQINIU];
    }

}

-(void)upLoadVideoToQINIU{

    [self showWithLabelAnimation];
    
    NSLog(@"请求之前 %@",[CJCTools getDateTimeTOMilliSeconds]);
    
    NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
    videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
    
    CJCUploadManager *manger = [[CJCUploadManager alloc] init];
    
    [manger uploadVideo:videoStr successHandle:^(NSString *QiniuKey) {
        
        [self updateInfoWithType:@"videoUrl" andKey:QiniuKey];
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self hidHUD];
        
        CJCAddMorePictureVC *nextVC = [[CJCAddMorePictureVC alloc] init];
        
        nextVC.sexType = self.sexType;
        
        [self.navigationController pushViewController:nextVC animated:YES];
        
    });
}

-(void)uploadImageToQINIU{

    [self showWithLabelAnimation];
    
    NSLog(@"请求之前 %@",[CJCTools getDateTimeTOMilliSeconds]);
    
    CJCUploadManager *manger = [[CJCUploadManager alloc] init];
    
    [manger uploadImage:self.returnImage successHandle:^(NSString *QiniuKey) {
        
        [self updateInfoWithType:@"imageUrl" andKey:QiniuKey];
        
    }];
}

- (void)updateInfoWithType:(NSString*)type andKey:(NSString *)key{

    NSString *updateUrl = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSString *domain = [kUSERDEFAULT_STAND objectForKey:kQINIUDOMAIN];
    
    NSString *returnStr = [NSString stringWithFormat:@"%@/%@",domain,key];
    
    params[type] = returnStr;
    
    [CJCHttpTool postWithUrl:updateUrl params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            returnDict = responseObject[@"data"][@"user"];
            
            if (self.sexType == CJCSexTypeMan) {
                
                if (self.takePhotoType == CJCTakePhotoTypeCamera) {
                    
                    NSLog(@"男人走了这里面");
                    NSLog(@"拍摄头像页面:\n%@",responseObject);
                    [CJCpersonalInfoModel infoModel].loginUserDict = responseObject[@"data"][@"user"];
                    [[CJCpersonalInfoModel infoModel] userLoginWithDict:nil];
                }
            }
        
            [self pushToNextVC];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToNextVC{

    if (self.takePhotoType == CJCTakePhotoTypeCamera) {
        
        if (self.sexType == CJCSexTypeMan) {
            
            CJCMaillistVC *nextVC = [[CJCMaillistVC alloc] init];
            
            nextVC.sexType = CJCSexTypeMan;
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }else{
        
            CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
            
            nextVC.takePhotoType = CJCTakePhotoTypeVideo;
            
            nextVC.sexType = self.sexType;
            
            [self.navigationController pushViewController:nextVC animated:YES];
        }
        
    }else if(self.takePhotoType == CJCTakePhotoTypeVideo){
        
    }else{
    
        if (self.changeHandle) {
            [self.navigationController popViewControllerAnimated:YES];
            [CJCpersonalInfoModel infoModel].loginUserDict = returnDict;

            self.changeHandle(returnDict);
        }
        
    }
}

-(void)takePhotoButtonDidClick{

    if (self.takePhotoType == CJCTakePhotoTypeCamera) {
        
        [self cameraAuthorityIsOpen];
    }else{
    
        [self audioAuthorityIsOpen];
    }
    
}

-(void)pushToCJCBeautyCameraAndVideoVC{

    CJCBeautyCameraAndVideoVC *nextVC = [[CJCBeautyCameraAndVideoVC alloc] init];
    
    nextVC.takePhotoType = self.takePhotoType;
    
    if (self.takePhotoType == CJCTakePhotoTypeChangeVideo) {
        
        nextVC.takePhotoType = CJCTakePhotoTypeVideo;
    }
    
    if (self.takePhotoType == CJCTakePhotoTypeChangeCamera) {
        
        nextVC.takePhotoType = CJCTakePhotoTypeCamera;
    }
    
    nextVC.sexType = self.sexType;
    
    nextVC.photoHandle = ^(UIImage *returnImage) {
        
        self.imageButton.hidden = YES;
        self.blueLabel.hidden = YES;
        self.bigRoundButton.hidden = YES;
        self.reTakePhotoButton.hidden = NO;
        self.nextButton.canOpration = YES;
        
        if (self.takePhotoType == CJCTakePhotoTypeCamera || self.takePhotoType == CJCTakePhotoTypeChangeCamera) {
            
            [self.iconImageView setImage:returnImage forState:UIControlStateNormal];
            
            self.returnImage = returnImage;
            
        }else{
            
            [videoitem removeObserver:self forKeyPath:@"status"];
            [videoplayerLayer removeFromSuperlayer];
            videoitem = nil;
            videoPlayer = nil;
            videoplayerLayer = nil;
            
            NSString *videoStr = [[UIApplication sharedApplication] documentsPath];
            videoStr = [NSString stringWithFormat:@"%@/introduction.mp4",videoStr];
            
            [self creatPreviewViewWithPath:videoStr];
        }
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)creatPreviewViewWithPath:(NSString *)videoStr{
    
    if (videoitem == nil) {
        
        videoitem = [[AVPlayerItem alloc] initWithURL:[NSURL fileURLWithPath:videoStr]];
        
        [videoitem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    if (videoPlayer == nil) {
        
        videoPlayer = [[AVPlayer alloc] initWithPlayerItem:videoitem];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:videoitem];
    }
    
    if (videoplayerLayer == nil) {
        
        videoplayerLayer = [AVPlayerLayer playerLayerWithPlayer:videoPlayer];
        
        videoplayerLayer.frame = CGRectMake(kAdaptedValue(82), kAdaptedValue(228), kAdaptedValue(210.5), kAdaptedValue(210.5));
        
        videoplayerLayer.cornerRadius = kAdaptedValue(10);
        videoplayerLayer.masksToBounds = YES;
        
        [self.view.layer addSublayer:videoplayerLayer];
    }
    
}

-(void)playbackFinished:(NSNotification *)notification{
    NSLog(@"视频播放完成.");
    
    
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    
    [videoPlayer seekToTime:CMTimeMake(0, 1)];
    self.playButton.hidden = NO;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        
        if (status == AVPlayerStatusReadyToPlay) {
            
            [videoPlayer seekToTime:CMTimeMake(0, 1)];
            self.playButton.hidden = NO;
        }
    }
}

-(UIButton *)playButton{

    if (_playButton == nil) {
        
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _playButton.frame = CGRectMake(kAdaptedValue(153.5), kAdaptedValue(299), kAdaptedValue(69), kAdaptedValue(69));
        
        [_playButton setImage:kGetImage(@"login_icon_viedo_play") forState:UIControlStateNormal];
        
        [_playButton addTarget:self action:@selector(videoPlay) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:_playButton];
    }
    
    return _playButton;
}

-(void)videoPlay{

    self.playButton.hidden = YES;
    
    [videoPlayer play];
}

-(UIButton *)iconImageView{

    if (_iconImageView == nil) {
        
        _iconImageView = [[UIButton alloc] init];
        
        _iconImageView.frame = CGRectMake(kAdaptedValue(119), kAdaptedValue(265), kAdaptedValue(136), kAdaptedValue(136));
        
        _iconImageView.layer.cornerRadius = kAdaptedValue(68);
        _iconImageView.layer.masksToBounds = YES;
        
        [_iconImageView setAdjustsImageWhenHighlighted:NO];
        
        _iconImageView.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [_iconImageView addTarget:self action:@selector(iconImageButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:_iconImageView];
    }
    
    return _iconImageView;
}

-(void)iconImageButtonDidClick{

    CJCContentBrowser *browser = [[CJCContentBrowser alloc] init];
    
    browser.OriginalFrame = CGRectMake(kAdaptedValue(119), kAdaptedValue(265), kAdaptedValue(136), kAdaptedValue(136));
    
    browser.photo = self.returnImage;
    
    [browser show];
}

-(void)audioAuthorityIsOpen{

    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
    
        [self pushToCJCBeautyCameraAndVideoVC];
        
    }else{
    
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            
            if (granted) {
                // 用户同意获取麦克风
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self pushToCJCBeautyCameraAndVideoVC];
                });
                
            } else {
                
                // 用户不同意获取麦克风
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"拍摄视频需要开启手机的麦克风权限"];
                });
            }
            
        }];
    }
}

-(void)cameraAuthorityIsOpen{
    
    
    AVAuthorizationStatus authStatus =  [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        
        [self pushToCJCBeautyCameraAndVideoVC];
        
    }else{
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {//相机权限
            if (granted) {
                
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self pushToCJCBeautyCameraAndVideoVC];
                });
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"拍摄照片需要开启手机的相机权限"];
                });
                
            }
        }];
        
    }
    
}



-(UIButton *)reTakePhotoButton{

    if (_reTakePhotoButton == nil) {
        
        _reTakePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _reTakePhotoButton.frame = CGRectMake(kAdaptedValue(159.5), kAdaptedValue(539.5), kAdaptedValue(56), kAdaptedValue(14));
        
        _reTakePhotoButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
        
        [_reTakePhotoButton setTitleColor:[UIColor toUIColorByStr:@"429CF0"] forState:UIControlStateNormal];
        
        [_reTakePhotoButton setTitle:@"重新拍摄" forState:UIControlStateNormal];
        
        [_reTakePhotoButton addTarget:self action:@selector(pushToCJCBeautyCameraAndVideoVC) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:_reTakePhotoButton];
    }
    
    return _reTakePhotoButton;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [videoitem removeObserver:self forKeyPath:@"status"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
