//
//  CJCBlankLocationVC.m
//  roxm
//
//  Created by lfy on 2017/9/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCBlankLocationVC.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CJCCommon.h"
#import "ROXMRootController.h"

@interface CJCBlankLocationVC ()<CLLocationManagerDelegate,AMapLocationManagerDelegate>

@property (nonatomic ,strong) CLLocationManager *manger;

@property (nonatomic ,strong) AMapLocationManager *amapManager;

@end

@implementation CJCBlankLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self configLocationService];
}


-(void)configLocationService{

    [AMapServices sharedServices].apiKey = kAMAPKEY;
    
    CLLocationManager *manger = [[CLLocationManager alloc] init];
    
    self.manger = manger;
    
    //定位相关设置
    manger.delegate = self;
    
    //定位权限状态
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusAuthorizedAlways){
    
        [self changeRootViewController];
        
    }else if (status == kCLAuthorizationStatusDenied){
        
        [self alertVCShowWith:@"APP需要开启定位权限才能使用"];
    }else{
        
        //请求用户授权
        [manger requestAlwaysAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"授权状态改变");
    
    if (status == kCLAuthorizationStatusDenied) {
        
        [self alertVCShowWith:@"APP需要开启定位权限才能使用"];
        [self changeRootViewController];
        
    }else if(status == kCLAuthorizationStatusAuthorizedAlways){
    
        [self changeRootViewController];
    }
}

-(void)changeRootViewController{

    [[ROXMRootController shardRootController] TapTabBar];
}

- (void)configLocationManager
{
    self.amapManager = [[AMapLocationManager alloc] init];
    
    [self.amapManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.amapManager.locationTimeout =2;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.amapManager.reGeocodeTimeout = 2;
    
    [self.amapManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        
        if (error)
        {
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            
            if (error.code == AMapLocationErrorLocateFailed)
            {
                return;
            }
        }
        
        NSLog(@"location:%@", location);
        
        if (regeocode)
        {
            NSLog(@"reGeocode:%@", regeocode);
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
