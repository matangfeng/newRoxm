//
//  CJCChooseDateGiftVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/20.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^CJCChooseDateGiftVCHandle)(NSString * returnString,NSString *returnIntId,NSNumber * returnIntPrice);

@interface CJCChooseDateGiftVC : CommonVC

@property (nonatomic ,copy) CJCChooseDateGiftVCHandle chooseHandle;

@end
