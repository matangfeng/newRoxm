//
//  CJCAddMorePictureVC.m
//  roxm
//
//  Created by lfy on 2017/8/22.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCAddMorePictureVC.h"
#import "CJCCommon.h"
#import "TZImagePickerController.h"
#import "CJCAddImageCell.h"
#import "CJCAddImageModel.h"
#import "TZImageManager.h"
#import "CJCBeautyCameraVC.h"
#import "CJCWomanAdditionaIinformationVC.h"
#import "CJCUploadManager.h"
#import "CJCMaillistVC.h"

@interface CJCAddMorePictureVC ()<TZImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{

    NSMutableArray *selectedPhotos;
    NSMutableArray *selectedAssets;
    
    NSInteger selectCellIndex;
    BOOL isDelegateImage;
}

@property (nonatomic ,strong) UICollectionView *addShowImageCV;

@property (nonatomic ,strong) UILabel *hintLabel;

@property (nonatomic ,strong) CJCOprationButton *nextButton;

@end

@implementation CJCAddMorePictureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpNaviView];
    
    [self setKeyBoardHid:YES];
    
    self.sexType = CJCSexTypeWoman;
    
    selectedPhotos = [NSMutableArray array];
    
    [self setUpUI];
}

-(void)setUpNaviView{
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 61);
    
    [self setNaviViewBottom:61];
    
    if (self.sexType == CJCSexTypeWoman) {
        
        self.progressViewWidth = kWOMANREGISTERPROGRESSWIDTH*3;
    }else{
        
        self.progressViewWidth = kMANREGISTERPROGRESSWIDTH*4;
    }
    
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getYYLabelWithStr:@"添加三张图片" fontName:kFONTNAMEMEDIUM size:28 color:[UIColor toUIColorByStr:@"222222"]];

    titleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(85), kAdaptedValue(224), kAdaptedValue(28));
    
    [self.view addSubview:titleLabel];
    
    UILabel *subtitleLabel = [UIView getYYLabelWithStr:@"展示一下生活中的自己吧" fontName:kFONTNAMELIGHT size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    subtitleLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(127), kAdaptedValue(187), kAdaptedValue(17));
    
    [self.view addSubview:subtitleLabel];
    
    [self configCollectionView];
    
    UILabel *hintLabel = [UIView getYYLabelWithStr:@"还需要添加3张图片" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"429CF0"]];
    
    hintLabel.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(539.5), kAdaptedValue(SCREEN_WITDH-40), kAdaptedValue(14));
    
    hintLabel.centerX = self.view.centerX;
    
    self.hintLabel = hintLabel;
    
    hintLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:hintLabel];
    
    CJCOprationButton *nextButton = [[CJCOprationButton alloc] init];
    
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    
    nextButton.canOpration = NO;
    
    nextButton.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(581.5), kAdaptedValue(200), kAdaptedValue(45));
    
    nextButton.centerX = self.view.centerX;
    
    [nextButton addTarget:self action:@selector(nextButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.nextButton = nextButton;
    [self.view addSubview:nextButton];
}

-(void)nextButtonDidClick{

    [self showWithLabelAnimation];
    
    CJCUploadManager *manger = [[CJCUploadManager alloc] init];
    
    [manger uploadGroupImagesAndVideos:selectedPhotos successHandle:^(NSArray *QiniuKeys) {
        
        [self pushToROXMDataWithKey:[self keysArrayToDictsArray:QiniuKeys]];
    }];
}

-(NSArray *)keysArrayToDictsArray:(NSArray *)keys{

    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (CJCAddImageModel *model in selectedPhotos) {
        
        NSDictionary *tempDict;
        
        NSString *domain = [kUSERDEFAULT_STAND objectForKey:kQINIUDOMAIN];
        
        NSString *key = model.uploadKey;
        
        NSString *returnStr = [NSString stringWithFormat:@"%@/%@",domain,key];
        
        if ([key containsString:@"images"]) {
            
            tempDict = @{ @"url":returnStr,@"key":key,@"type":@"20",@"height":@(model.height),@"width":@(model.width)};
        }else{
        
            tempDict = @{ @"url":returnStr,@"key":key,@"type":@"21",@"height":@(model.height),@"width":@(model.width)};
        }
        
        [tempArr addObject:tempDict];
    }
    
    return tempArr.copy;
}

-(void)pushToNextVC{

    if (self.sexType == CJCSexTypeWoman) {
        
        CJCWomanAdditionaIinformationVC *nextVC = [[CJCWomanAdditionaIinformationVC alloc] init];
        
        [self.navigationController pushViewController:nextVC animated:YES];
    }else{
    
        
    }
}

-(void)pushToROXMDataWithKey:(NSArray *)keys{
    
    NSString *pushURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/upload/file"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"files"] = keys;
    
    [CJCHttpTool postImagesAndVideosWithUrl:pushURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
        
            [self pushToNextVC];
        }else{
    
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)configCollectionView{

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    flowLayout.itemSize = CGSizeMake(kAdaptedValue(93), kAdaptedValue(93));
    flowLayout.minimumInteritemSpacing = (SCREEN_WITDH - kAdaptedValue(93)*3)/5;
    
    flowLayout.sectionInset = UIEdgeInsetsMake(0, (SCREEN_WITDH - kAdaptedValue(93)*3)/5, 0, (SCREEN_WITDH - kAdaptedValue(93)*3)/5);
    
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(272), SCREEN_WITDH, kAdaptedValue(93)) collectionViewLayout:flowLayout];
    
    self.addShowImageCV = collectionView;
    
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.backgroundColor = [UIColor whiteColor];
    
    //给集合视图注册一个cell
    [collectionView registerClass:[CJCAddImageCell class] forCellWithReuseIdentifier:@"CJCAddImageCell"];
    
    [self.view addSubview:collectionView];
}

#pragma mark - SYLifeManagerDelegate

//处于编辑状态
- (void)didChangeEditState:(BOOL)inEditState
{

    for (CJCAddImageCell *cell in self.addShowImageCV.visibleCells) {
        cell.inEditState = inEditState;
    
    }
}

#pragma mark =====CollectionViewDelegate  DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (selectedPhotos.count == 0) {
        
        return 1;
    }else if (selectedPhotos.count == 3){
    
        return 3;
    }
    
    return selectedPhotos.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    CJCAddImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CJCAddImageCell" forIndexPath:indexPath];
    
    cell.button.tag = indexPath.item;
    
    if (indexPath.item <= selectedPhotos.count -1 && selectedPhotos.count!=0) {
        
        CJCAddImageModel *tempModel = selectedPhotos[indexPath.item];
        
        UIImage *selectImage = tempModel.image;
        
        if (tempModel.mediaType == CJCMediaTypeVideo) {
            
            cell.playImageView.hidden = NO;
            cell.timeLengthLabel.text = tempModel.videoLength;
        }else if(tempModel.mediaType == CJCMediaTypePhoto){
        
            cell.playImageView.hidden = YES;
        }else{
        
            if (tempModel.assetMediaType == TZAssetModelMediaTypeVideo) {
                
                cell.playImageView.hidden = NO;
                cell.timeLengthLabel.text = tempModel.videoLength;
            }else{
            
                cell.playImageView.hidden = YES;
            }
        }

        cell.selectImage = selectImage;
    }else{
        
        if (selectedPhotos.count == 0) {
            
            if (indexPath.item == 0) {
                
                cell.addImageView.hidden = NO;
            }else{
            
                cell.addImageView.hidden = YES;
            }
        }else{
        
            if (indexPath.item == selectedPhotos.count) {
                
                cell.addImageView.hidden = NO;
            }else{
                
                cell.addImageView.hidden = YES;
            }
        }
        
        cell.selectImageView.hidden = YES;
    }
    
    [cell.button addTarget:self action:@selector(deleteButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)deleteButtonDidClick:(NSInteger)index{
    
    [selectedPhotos removeObjectAtIndex:index];
    
    [self updateHitLabelText];
    
    [self.addShowImageCV reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    CJCAddImageCell *cell = (CJCAddImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    selectCellIndex = indexPath.item;
    
    if (cell.selectImageView.hidden) {
        
        [self chooseImageWaysWith:NO AndIndex:0];
    }else{
    
        [self chooseImageWaysWith:YES AndIndex:indexPath.item];
    }
}

-(void)chooseImageWaysWith:(BOOL)isDelegate AndIndex:(NSInteger)index{

    isDelegateImage = isDelegate;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"选择操作方式" preferredStyle:  UIAlertControllerStyleActionSheet];
    
    if (isDelegate) {
        
        [alert addAction:[UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            [self deleteButtonDidClick:index];
            
        }]];
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照、录像" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        CJCBeautyCameraVC *cameraVC = [[CJCBeautyCameraVC alloc] init];
        
        cameraVC.takePhotoHandle = ^(UIImage *returnImage) {
            
            CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];
            
            tempModel.image = returnImage;
            tempModel.mediaType = CJCMediaTypePhoto;
            tempModel.width = returnImage.size.width;
            tempModel.height = returnImage.size.height;
            
            if (isDelegate) {
                
                [selectedPhotos replaceObjectAtIndex:selectCellIndex withObject:tempModel];
            }else{
                
                [selectedPhotos addObject:tempModel];
            }
            
            [self updateHitLabelText];
            
            [self.addShowImageCV reloadData];
        };
        
        cameraVC.takeVideoHandle = ^(UIImage *returnImage, NSString *videoPath) {
            
            CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];
            
            tempModel.image = returnImage;
            tempModel.mediaType = CJCMediaTypeVideo;
            tempModel.videoPath = videoPath;
            tempModel.videoLength = [self getLocalVideoLengh:videoPath];
            tempModel.width = returnImage.size.width;
            tempModel.height = returnImage.size.height;
            
            if (isDelegate) {
                
                [selectedPhotos replaceObjectAtIndex:selectCellIndex withObject:tempModel];
            }else{
                
                [selectedPhotos addObject:tempModel];
            }
            
            [self updateHitLabelText];
            
            [self.addShowImageCV reloadData];
            
        };
        
        [self.navigationController pushViewController:cameraVC animated:YES];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self photoLibraryAuthorityIsOpen];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)pushtoPhotoLibryVC{

    TZImagePickerController *nextVC;
    
    if (selectedPhotos.count == 3) {
        
        nextVC = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    }else{
        
        nextVC = [[TZImagePickerController alloc] initWithMaxImagesCount:3-selectedPhotos.count columnNumber:3 delegate:self pushPhotoPickerVc:YES];
    }
    
    nextVC.allowTakePicture = NO;
    nextVC.allowPickingMultipleVideo = YES;
    nextVC.allowPickingOriginalPhoto = NO;
    nextVC.allowPreview = NO;
    nextVC.oKButtonTitleColorNormal = [UIColor toUIColorByStr:@"429CF0"];
    nextVC.oKButtonTitleColorDisabled = [UIColor toUIColorByStr:@"429CF0"];
    
    [self presentViewController:nextVC animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    for (int i=0; i<photos.count; i++) {
        
        CJCAddImageModel *tempModel = [[CJCAddImageModel alloc] init];

        UIImage *image = photos[i];
        
        tempModel.image = photos[i];
        tempModel.mediaType = CJCMediaTypeAsset;
        tempModel.asset = assets[i];
        tempModel.assetMediaType = [[TZImageManager manager] getAssetType:assets[i]];
        NSString *time = [NSString stringWithFormat:@"%0.0f",tempModel.asset.duration];
        tempModel.videoLength = [CJCTools getNewTimeFromDurationSecond:time.integerValue];
        tempModel.width = image.size.width;
        tempModel.height = image.size.height;
        
        if (isDelegateImage) {
            
            [selectedPhotos replaceObjectAtIndex:selectCellIndex withObject:tempModel];
        }else{
            
            [selectedPhotos addObject:tempModel];
        }
    }
    
    [self updateHitLabelText];
    
    [self.addShowImageCV reloadData];
}

-(void)updateHitLabelText{

    if (selectedPhotos.count == 3) {
        
        self.hintLabel.text = @"添加完成";
        
        self.nextButton.canOpration = YES;
    }else{
    
        NSString *leftImageNum = [NSString stringWithFormat:@"还需要添加%ld张图片",3-selectedPhotos.count];
        
        self.hintLabel.text = leftImageNum;
        
        self.nextButton.canOpration = NO;
    }
    
}

-(NSString *)getLocalVideoLengh:(NSString *)path{

    AVURLAsset * asset = [AVURLAsset assetWithURL:[NSURL fileURLWithPath:path]];
    CMTime   time = [asset duration];
    double seconds = ceil(time.value/time.timescale);
    
    NSString *timeStr = [NSString stringWithFormat:@"%0.0f",seconds];
    
    return [CJCTools getNewTimeFromDurationSecond:timeStr.integerValue];
}

-(void)photoLibraryAuthorityIsOpen{

     PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if (authStatus == PHAuthorizationStatusAuthorized)
    {
        
        [self pushtoPhotoLibryVC];
        
    }else{
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized)
            {
                
                // 用户同意获取麦克风
                //系统开启设备权限是在子线程开启  需要调回到主线程去操作
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self pushtoPhotoLibryVC];
                });
                
            } else {
                
                // 用户不同意获取麦克风
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self alertVCShowWith:@"选择相册图片需要开启手机的相册权限"];
                });
            }
        }];
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
