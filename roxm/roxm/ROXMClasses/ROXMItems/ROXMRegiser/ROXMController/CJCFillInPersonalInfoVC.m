//
//  CJCFillInPersonalInfoVC.m
//  roxm
//
//  Created by lfy on 2017/8/5.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCFillInPersonalInfoVC.h"
#import "CJCCommon.h"
#import "CJCBirthHeiWeightView.h"
#import "CJCBeforeTakePhotoVC.h"

@interface CJCFillInPersonalInfoVC ()<UITextFieldDelegate>{

    CJCSexType postSexType;
    
    //每个按钮操作的计数  判断用户信息是否完善
    NSInteger birthdayButtonNum;
    NSInteger heightButtonNum;
    NSInteger weightButtonNum;
    NSInteger nameTFNum;
    NSInteger professionTFNum;
    NSInteger sexButtonNum;
    
    NSInteger totalOprationNum;
    
    NSString *birthDayStr;
    NSString *weightStr;
    NSString *heightStr;
}

@property (nonatomic ,strong) UITextField *nameTF;

@property (nonatomic ,strong) UIButton *manButton;

@property (nonatomic ,strong) UIButton *womanButton;

@property (nonatomic ,strong) UILabel *sexLabel;

@property (nonatomic ,strong) UITextField *professionTF;

@property (nonatomic ,strong) UIButton *brithdayButton;

@property (nonatomic ,strong) UIImageView *brithdayImageView;

@property (nonatomic ,strong) UIButton *heightButton;

@property (nonatomic ,strong) UIImageView *heightImageView;

@property (nonatomic ,strong) UIButton *weightButton;

@property (nonatomic ,strong) UIImageView *weightImageView;

@property (nonatomic ,strong) CJCOprationButton *nextStepButton;

@end

@implementation CJCFillInPersonalInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    postSexType = CJCSexTypeNoSelect;
    birthdayButtonNum = 0;
    heightButtonNum = 0;
    weightButtonNum = 0;
    nameTFNum = 0;
    professionTFNum = 0;
    
    [self setUpUI];
    
    [self setUpNaviView];
}

-(void)setUpNaviView{

    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 61);
    
    self.titleLabel.text = @"创建个人资料";
    
    self.titleLabel.frame = CGRectMake(kAdaptedValue(139.5), 32, kAdaptedValue(100), kAdaptedValue(22.5));
    
    self.titleLabel.centerY = kNAVIVIEWCENTERY;
    
    self.naviViewBottom = 62;
    
    self.progressViewWidth = SCREEN_WITDH/20;
}

-(void)setUpUI{

    [self creatLabelAndTextFieldViewWithY:64 andName:@"昵称" andHeight:57];
    
    [self creatChooseSexViewWithY:64+kAdaptedValue(57)];
    
    [self creatLabelAndTextFieldViewWithY:64+kAdaptedValue(114) andName:@"职业" andHeight:60.5];
    
    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(174.5) andType:@"生日"];
    
    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(231.5) andType:@"身高"];
    
    [self creatLabelAndButtonViewWithY:64+kAdaptedValue(288.5) andType:@"体重"];
    
    CJCOprationButton *nextButton = [[CJCOprationButton alloc] init];
    
    nextButton.frame = CGRectMake(kAdaptedValue(87), kAdaptedValue(582), kAdaptedValue(200), kAdaptedValue(45));
    
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    
    self.nextStepButton = nextButton;
    
    nextButton.canOpration = NO;
    
    [nextButton addTarget:self action:@selector(nextButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:nextButton];
}

-(void)nextButtonCanOpration{

    totalOprationNum = birthdayButtonNum+heightButtonNum+weightButtonNum+nameTFNum+professionTFNum+sexButtonNum;
    
    if (totalOprationNum == 6) {
        
        self.nextStepButton.canOpration = YES;
    }else{
    
        self.nextStepButton.canOpration = NO;
    }
}

-(void)nextButtonDidClick{

    if ([self.nameTF.text isEqualToString:@""] || !self.nameTF.text) {
        
        [MBManager showBriefAlert:@"请输入昵称"];
        
        return;
    }
    
    if ([self.professionTF.text isEqualToString:@""] || !self.professionTF.text) {
        
        [MBManager showBriefAlert:@"请输入职业"];
        
        return;
    }
    
    NSInteger jishu = birthdayButtonNum+heightButtonNum+weightButtonNum;
    
    if (jishu != 3) {
        
        [MBManager showBriefAlert:@"请确保信息完善完整"];
        
        return;
    }
    
    [self updateInfo];
}

-(void)updateInfo{

    [self showWithLabelAnimation];
    
    NSString *updateUrl = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/update"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"nickname"] = self.nameTF.text;
    
    if (postSexType == CJCSexTypeWoman) {
        
        params[@"sex"] = @"0";
    }else{
    
        params[@"sex"] = @"1";
    }
    params[@"career"] = self.professionTF.text;
    params[@"birthDay"] = birthDayStr;
    params[@"weight"] = weightStr;
    params[@"height"] = heightStr;
    
    [CJCHttpTool postWithUrl:updateUrl params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            [self pushToNextVC];
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToNextVC{

    CJCBeforeTakePhotoVC *nextVC = [[CJCBeforeTakePhotoVC alloc] init];
    
    nextVC.takePhotoType = CJCTakePhotoTypeCamera;
    
    nextVC.sexType = postSexType;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark ======== 一个label  一个textField的view
-(void)creatLabelAndTextFieldViewWithY:(CGFloat)y andName:(NSString *)name andHeight:(CGFloat)height{

    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(y), SCREEN_WITDH, kAdaptedValue(height))];
    
    [self.view addSubview:containView];
    
    UIView *lineView = [self creatMarginLine];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(56), kAdaptedValue(329), OnePXLineHeight);
    
    [containView addSubview:lineView];
    
    UILabel *nameLabel = [UIView getYYLabelWithStr:name fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"666666"]];
    
    nameLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(21), kAdaptedValue(30), kAdaptedValue(15));
    
    [containView addSubview:nameLabel];
    
    UITextField *inputTF = [[UITextField alloc] init];
    
    inputTF.frame = CGRectMake(kAdaptedValue(83), kAdaptedValue(21), kAdaptedValue(200), kAdaptedValue(15));
    
    inputTF.font = [UIFont accodingVersionGetFont_lightWithSize:14];
    
    inputTF.textColor = [UIColor toUIColorByStr:@"222222"];
    
    NSString *inputStr = [NSString stringWithFormat:@"输入%@",name];
    
    inputTF.placeholder = inputStr;
    
    inputTF.returnKeyType = UIReturnKeyDone;
    
    inputTF.delegate = self;
    
    [containView addSubview:inputTF];
    
    UIButton *containButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    containButton.frame = containView.bounds;
    
    [containButton addTarget:self action:@selector(bigButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:containButton];
    
    if ([name isEqualToString:@"昵称"]) {
        
        self.nameTF = inputTF;
        
        inputTF.tag = 10001;
        
        containButton.tag = 1111;
    }else{
    
        self.professionTF = inputTF;
        
        inputTF.tag = 10002;
        
        containButton.tag = 1112;
    }
}

#pragma  mark =======textFieldDalegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [self.view endEditing:YES];
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{

    if (textField.tag == 10001) {
        
        if ([CJCTools isBlankString:textField.text]) {
            
            nameTFNum = 0;
        }else{
        
            if (nameTFNum == 0) {
                
                nameTFNum++;
            }
        }
    }else{
    
        if ([CJCTools isBlankString:textField.text]) {
            
            professionTFNum = 0;
        }else{
            
            if (professionTFNum == 0) {
                
                professionTFNum++;
            }
        }
    }
    
    [self nextButtonCanOpration];
}

#pragma mark ======== 选择性别的view
-(void)creatChooseSexViewWithY:(CGFloat)y{

    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(y), SCREEN_WITDH, kAdaptedValue(57))];
    
    [self.view addSubview:containView];
    
    UIView *lineView = [self creatMarginLine];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(56), kAdaptedValue(329), OnePXLineHeight);
    
    [containView addSubview:lineView];
    
    UILabel *nameLabel = [UIView getYYLabelWithStr:@"性别" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"666666"]];
    
    nameLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(21), kAdaptedValue(30), kAdaptedValue(15));
    
    [containView addSubview:nameLabel];
    
    UILabel *sexLabel = [UIView getYYLabelWithStr:@"选择性别" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"BCBCBC"]];
    
    sexLabel.textAlignment = NSTextAlignmentLeft;
    
    self.sexLabel = sexLabel;
    
    sexLabel.frame = CGRectMake(kAdaptedValue(83), kAdaptedValue(21.5), kAdaptedValue(60), kAdaptedValue(14));
    
    [containView addSubview:sexLabel];
    
    UIButton *manButton = [self creatChooseSexButtonWith:@"男"];
    
    manButton.frame = CGRectMake(kAdaptedValue(216), kAdaptedValue(12), kAdaptedValue(61), kAdaptedValue(33));
    
    self.manButton = manButton;
    manButton.tag = 1001;
    
    self.manButton.alpha = 0.5;
    
    [containView addSubview:manButton];
    
    UIButton *womanButton = [self creatChooseSexButtonWith:@"女"];
    
    womanButton.frame = CGRectMake(kAdaptedValue(291), kAdaptedValue(12), kAdaptedValue(61), kAdaptedValue(33));
    
    self.womanButton = womanButton;
    womanButton.tag = 1002;
    
    self.womanButton.alpha = 0.5;
    
    [containView addSubview:womanButton];
    
    [manButton addTarget:self action:@selector(sexButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
     [womanButton addTarget:self action:@selector(sexButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)sexButtonDidClick:(UIButton *)button{

    [self.view endEditing:YES];
    
    if (sexButtonNum == 0) {
        
        sexButtonNum++;
    }
    
    button.selected = !button.selected;
    
    self.sexLabel.textColor = [UIColor toUIColorByStr:@"222222"];
    
    if ([self.sexLabel.text isEqualToString:@"选择性别"]) {
        
        if (button.tag == 1001) {
            
            self.manButton.alpha = 1;
            
            self.sexLabel.text = @"男";
            
            postSexType = CJCSexTypeMan;
        }else{
        
            self.womanButton.alpha = 1;
            
            self.sexLabel.text = @"女";
            
            postSexType = CJCSexTypeWoman;
        }
    }else{
    
        if (button.tag == 1001) {
            
            self.manButton.alpha = 1;
            
            self.womanButton.alpha = 0.5;
            
            self.sexLabel.text = @"男";
            
            postSexType = CJCSexTypeMan;
        }else{
            
            self.manButton.alpha = 0.5;
            
            self.womanButton.alpha = 1;
            
            self.sexLabel.text = @"女";
            
            postSexType = CJCSexTypeWoman;
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"注册成功后，性别不可以修改" message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
     [self presentViewController:alert animated:true completion:nil];
    
    [self nextButtonCanOpration];
}

-(UIButton *)creatChooseSexButtonWith:(NSString *)sex{

    UIButton *sexButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    sexButton.size = CGSizeMake(kAdaptedValue(61), kAdaptedValue(33));
    
    sexButton.layer.cornerRadius = kAdaptedValue(16.5);
    sexButton.layer.masksToBounds = YES;
    
    sexButton.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:14];
    
    [sexButton setTitle:sex forState:UIControlStateNormal];
    
    UIImage *image = [UIView imageWithColor:[UIColor toUIColorByStr:@"222222"]];
    
    [sexButton setBackgroundImage:image forState:UIControlStateNormal];
    
    return sexButton;
}

#pragma mark ======== 一个label 一个button的view
-(void)creatLabelAndButtonViewWithY:(CGFloat)y andType:(NSString *)type{

    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(0, kAdaptedValue(y), SCREEN_WITDH, kAdaptedValue(57))];
    
    [self.view addSubview:containView];
    
    UIView *lineView = [self creatMarginLine];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(56), kAdaptedValue(329), OnePXLineHeight);
    
    [containView addSubview:lineView];
    
    NSString *typeStr = [NSString stringWithFormat:@"选择%@",type];
    
    UILabel *typeLabel = [UIView getYYLabelWithStr:typeStr fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"666666"]];
    
    typeLabel.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(18), kAdaptedValue(60), kAdaptedValue(21));
    
    [containView addSubview:typeLabel];
    
    UIButton *chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    chooseButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:14];
    
    [chooseButton setTitle:@"选择" forState:UIControlStateNormal];
    
    [chooseButton setTitleColor:[UIColor toUIColorByStr:@"BCBCBC"] forState:UIControlStateNormal];
    
    chooseButton.frame = CGRectMake(kAdaptedValue(113), kAdaptedValue(18), kAdaptedValue(28), kAdaptedValue(20));
    
    [containView addSubview:chooseButton];
    
    [chooseButton addTarget:self action:@selector(chooseBirthHeightWeightButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *downImageView = [[UIImageView alloc] initWithImage:kGetImage(@"list_icon_arrowdown")];
    
    downImageView.frame = CGRectMake(chooseButton.right+10, kAdaptedValue(24.5), kAdaptedValue(14.5), kAdaptedValue(8));
    
    [containView addSubview:downImageView];
    
    UIButton *containButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    containButton.frame = containView.bounds;
    
    [containButton addTarget:self action:@selector(bigButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [containView addSubview:containButton];
    
    if ([type isEqualToString:@"生日"]) {
        
        self.brithdayButton = chooseButton;
        chooseButton.tag = 1011;
        
        self.brithdayImageView = downImageView;
        
        containButton.tag = 1113;
    }else if ([type isEqualToString:@"身高"]){
        
        self.heightButton = chooseButton;
        chooseButton.tag = 1012;
        
        self.heightImageView = downImageView;
        
        containButton.tag = 1114;
    }else{
        
        self.weightButton = chooseButton;
        chooseButton.tag = 1013;
        
        self.weightImageView = downImageView;
        
        containButton.tag = 1115;
    }
}

-(void)resetChooseButton{

    birthdayButtonNum = 0;
    heightButtonNum = 0;
    weightButtonNum = 0;
    
    [self nextButtonCanOpration];
    
    [self.brithdayButton setTitle:@"选择" forState:UIControlStateNormal];
    
    [self.brithdayButton setTitleColor:[UIColor toUIColorByStr:@"BCBCBC"] forState:UIControlStateNormal];
    
    self.brithdayButton.frame = CGRectMake(kAdaptedValue(113), kAdaptedValue(18), kAdaptedValue(28), kAdaptedValue(20));
    
    self.brithdayImageView.x = self.brithdayButton.right+10;
    
    
    [self.heightButton setTitle:@"选择" forState:UIControlStateNormal];
    
    [self.heightButton setTitleColor:[UIColor toUIColorByStr:@"BCBCBC"] forState:UIControlStateNormal];
    
    self.heightButton.frame = CGRectMake(kAdaptedValue(113), kAdaptedValue(18), kAdaptedValue(28), kAdaptedValue(20));
    
    self.heightImageView.x = self.heightButton.right+10;
    
    
    [self.weightButton setTitle:@"选择" forState:UIControlStateNormal];
    
    [self.weightButton setTitleColor:[UIColor toUIColorByStr:@"BCBCBC"] forState:UIControlStateNormal];
    
    self.weightButton.frame = CGRectMake(kAdaptedValue(113), kAdaptedValue(18), kAdaptedValue(28), kAdaptedValue(20));
    
    self.weightImageView.x = self.weightButton.right+10;
}

-(void)chooseBirthHeightWeightButtonDidClick:(UIButton *)button{

    [self.view endEditing:YES];
    
    switch (button.tag) {
        case 1011:{
            
            [self creatDarePickerWithWidth:94 andSexType:postSexType andButton:button andPickerType:CJCPickerViewTypeBirthDay];
        }
            break;
            
        case 1012:{
            
            [self creatDarePickerWithWidth:42.5 andSexType:postSexType andButton:button andPickerType:CJCPickerViewTypeHeight];
            
        }
            break;
            
        case 1013:{
            
            [self creatDarePickerWithWidth:32.5 andSexType:postSexType andButton:button andPickerType:CJCPickerViewTypeWeight];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)creatDarePickerWithWidth:(CGFloat)width andSexType:(CJCSexType)sextype andButton:(UIButton *)button andPickerType:(CJCPickerViewType)pickType{

    CJCBirthHeiWeightView *pick = [[CJCBirthHeiWeightView alloc] init];
    
    pick.sexType = sextype;
    
    pick.selectStr = button.titleLabel.text;
    
    pick.pickerViewType = pickType;
    
    [pick showPickerView];
    
    pick.returnHandle = ^(NSString *returnString, NSString *returnInt) {
        
        CGFloat buttonWidth = [CJCTools getShortStringLength:returnString withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
        
        button.width = buttonWidth;
        
        [button setTitleColor:[UIColor toUIColorByStr:@"222222"] forState:UIControlStateNormal];
        
        [button setTitle:returnString forState:UIControlStateNormal];
        
        switch (button.tag) {
            case 1011:{
                
                self.brithdayImageView.x = button.right+10;
                
                birthDayStr = returnInt;
                
                if (birthdayButtonNum == 0) {
                    
                    birthdayButtonNum++;
                }
            }
                break;
                
            case 1012:{
                
                self.heightImageView.x = button.right+10;
                
                heightStr = returnInt;
                
                if (heightButtonNum == 0) {
                    
                    heightButtonNum++;
                }
            }
                break;
                
            case 1013:{
                
                self.weightImageView.x = button.right+10;
                
                weightStr = returnInt;
                
                if (weightButtonNum == 0) {
                    
                    weightButtonNum++;
                }
            }
                break;
                
            default:
                break;
        }
        
        [self nextButtonCanOpration];
    };
    
}

-(void)bigButtonDidClick:(UIButton *)button{

    switch (button.tag) {
        case 1111:{
        
            [self.nameTF becomeFirstResponder];
        }
            break;
            
        case 1112:{
            
            [self.professionTF becomeFirstResponder];
        }
            break;
            
        case 1113:{
            
            [self chooseBirthHeightWeightButtonDidClick:self.brithdayButton];
        }
            break;
            
        case 1114:{
            
            [self chooseBirthHeightWeightButtonDidClick:self.heightButton];
        }
            break;
            
        case 1115:{
            
            [self chooseBirthHeightWeightButtonDidClick:self.weightButton];
        }
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
