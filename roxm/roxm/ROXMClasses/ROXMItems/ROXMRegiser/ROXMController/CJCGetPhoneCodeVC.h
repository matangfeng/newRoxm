//
//  CJCGetPhoneCodeVC.h
//  roxm
//
//  Created by lfy on 2017/8/5.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCGetCodeType){
    
    CJCGetCodeTypeRegist                     = 0,
    CJCGetCodeTypeForgetPassWord             = 1,

};

@interface CJCGetPhoneCodeVC : CommonVC

@property (nonatomic ,copy) NSString *phoneNum;

@property (nonatomic ,copy) NSString *passWord;

@property (nonatomic ,copy) NSString *verificationCode;

@property (nonatomic ,assign) CJCGetCodeType codeType;

@property (nonatomic ,strong) NSTimer *tempTimer;

@end
