//
//  CJCFirstAnimationVC.h
//  roxm
//
//  Created by lfy on 2017/9/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TapStatus)(BOOL currentStatus);

@interface CJCFirstAnimationVC : UIViewController
@property (nonatomic , strong) TapStatus tapStaus;
@end
