//
//  CJCFirstAnimationVC.m
//  roxm
//
//  Created by lfy on 2017/9/7.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCFirstAnimationVC.h"
#import "CJCCommon.h"
#import "CJCNavigationController.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCAddMorePictureVC.h"
#import "CJCClickSecretView.h"

@interface CJCFirstAnimationVC ()<CAAnimationDelegate>

@property (nonatomic ,strong) UIImageView *imageScroll;

@end

@implementation CJCFirstAnimationVC

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        CGPoint newPoint = self.imageScroll.center;
        
        CABasicAnimation *animation =[CABasicAnimation animationWithKeyPath:@"position"];
        [animation setToValue:[NSValue valueWithCGPoint:CGPointMake(newPoint.x - 50, newPoint.y)]];
        
        animation.delegate = self;
        animation.duration = 1.0;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;//removedOnCompletion,fillMode配合使用保持动画完成效果
        animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [self.imageScroll.layer addAnimation:animation forKey:@"position"];
        
    });
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
    [self changeRootViewController];
}

-(void)changeRootViewController{
    
    if ([kUSERDEFAULT_STAND objectForKey:kUSERINFOCOMPLETE]) {
        
        if (_tapStaus) {
            _tapStaus(YES);
        }
//        CJCTabarController *naviVC = [[CJCTabarController alloc] init];
//
//        UIWindow *window = [UIApplication sharedApplication].keyWindow;
//
//        window.rootViewController = naviVC;
//
//        AppDelegate *dele = (AppDelegate *)[UIApplication sharedApplication].delegate;
//
//        [window bringSubviewToFront:dele.secretView];
        
    }else{
        
        if (_tapStaus) {
            _tapStaus(NO);
        }
//        CJCLoginAndRegisterVC *logstrVC = [[CJCLoginAndRegisterVC alloc] init];
//        
//        
////        [self.navigationController pushViewController:logstrVC animated:YES];
//        //CJCBeforeTakePhotoVC *logstrVC = [[CJCBeforeTakePhotoVC alloc] init];
//        //logstrVC.takePhotoType = CJCTakePhotoTypeVideo;
//        
//        //CJCAddMorePictureVC *logstrVC = [[CJCAddMorePictureVC alloc] init];
//        //CJCMaillistVC *logstrVC = [[CJCMaillistVC alloc] init];
//        
//        CJCNavigationController *naviVC = [[CJCNavigationController alloc] initWithRootViewController:logstrVC];
//        
//        UIWindow *window = [UIApplication sharedApplication].keyWindow;
//        
//        window.rootViewController = naviVC;
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    NSNumber *countNum = [kUSERDEFAULT_STAND objectForKey:kAPPSTARTCOUNT];
//    
//    if (countNum != nil) {
//        
//        NSNumber *newNum = [NSNumber numberWithInteger:countNum.integerValue+1];
//        
//        [kUSERDEFAULT_STAND setObject:newNum forKey:kAPPSTARTCOUNT];
//        
//    }else{
//    
//        NSNumber *num = [NSNumber numberWithInteger:1];
//        
//        [kUSERDEFAULT_STAND setObject:num forKey:kAPPSTARTCOUNT];
//    }
    
    [self setUpUI];
}

-(void)setUpUI{
    
    NSNumber *countNum = [kUSERDEFAULT_STAND objectForKey:kAPPSTARTCOUNT];
    
    NSLog(@"%@",countNum);
    
    NSString *fullImageName,*bottomImageName;
    
//    if (countNum.integerValue%2 == 0 ) {
//        
//        fullImageName = @"start_user1";
//        bottomImageName = @"start_ad1";
//    }else{
//    
//        
//    }
    
    fullImageName = @"start_user1.jpg";
    bottomImageName = @"bg4.png";
    
    UIImage *fullImage = [UIImage imageNamed:fullImageName];
    
    CGFloat imageWidth = SCREEN_WITDH+kAdaptedValue(50);
    
    CGFloat imageRatio = (CGFloat)fullImage.size.height/fullImage.size.width;
    
    UIImageView *fullImageView = [[UIImageView alloc] initWithImage:fullImage];
    
    fullImageView.frame = CGRectMake(0, 0, imageWidth, imageWidth*imageRatio);
    
    self.imageScroll = fullImageView;
    [self.view addSubview:fullImageView];
    
    UIImage *bottomImage = kGetImage(bottomImageName);
    
    UIImageView *bottomView = [[UIImageView alloc] initWithImage:bottomImage];
    
    CGFloat ratio = (CGFloat)bottomImage.size.height/bottomImage.size.width;
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-SCREEN_WITDH*ratio, SCREEN_WITDH, SCREEN_WITDH*ratio);
    
    [self.view addSubview:bottomView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
