//
//  AppDelegate.h
//  roxm
//
//  Created by lfy on 2017/8/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,CJCKeyboardType){
    
    CJCKeyboardTypeDefault                 = 0,//
    CJCKeyboardTypeOnlySystem              = 1,//只显示系统的键盘
};

@class CJCpersonalInfoModel,CLLocation;

@class CJCClickSecretView;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic ,assign) CJCKeyboardType keyBoardType;

@property (nonatomic ,strong) CJCpersonalInfoModel *personalInfoModel;

@property (nonatomic ,strong) CJCClickSecretView *secretView;

@property (nonatomic ,strong) CLLocation *userLocation;

-(void)acceptDateStartUploadLocation;

-(void)startDateEndUploadLocation;

@end

