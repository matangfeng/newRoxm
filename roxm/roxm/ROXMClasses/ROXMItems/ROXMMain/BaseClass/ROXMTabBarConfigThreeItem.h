//
//  LCDTabBarConfig.h
//  LCDBorrowed
//
//  Created by 马棠丰 on 2017/9/28.
//  Copyright © 2017年 马棠丰. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CYLTabBarController.h"

@interface ROXMTabBarConfigThreeItem : NSObject
@property (nonatomic, readonly, strong) CYLTabBarController *tabBarController;
@end
