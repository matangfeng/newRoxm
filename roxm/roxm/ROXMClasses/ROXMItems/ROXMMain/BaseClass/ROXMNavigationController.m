//
//  LCDNavigationController.m
//  LCDBorrowed
//
//  Created by 马棠丰 on 2017/9/27.
//  Copyright © 2017年 马棠丰. All rights reserved.
//

#import "ROXMNavigationController.h"

@interface ROXMNavigationController ()

@end

@implementation ROXMNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UIBarButtonItem *)rt_customBackItemWithTarget:(id)target
                                          action:(SEL)action
{
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil)
                                            style:UIBarButtonItemStylePlain
                                           target:target
                                           action:action];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
