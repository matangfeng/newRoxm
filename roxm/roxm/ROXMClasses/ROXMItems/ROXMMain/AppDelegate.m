//
//  AppDelegate.m
//  roxm
//
//  Created by lfy on 2017/8/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "AppDelegate.h"
#import "CJCLoginAndRegisterVC.h"
#import "CJCNavigationController.h"
#import "CJCCommon.h"
#import <LinkedME_iOS/LinkedME.h>
#import <MobClick.h>
#import <CoreLocation/CoreLocation.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <Hyphenate/Hyphenate.h>
#import "CJCWomanAdditionaIinformationVC.h"
#import "CJCFirstAnimationVC.h"
#import "CJCFistAnimationView.h"
#import <UserNotifications/UserNotifications.h>
#import "CJCClickSecretView.h"
#import <EMCDDeviceManager.h>
#import <EaseUI.h>
#import <AlipaySDK/AlipaySDK.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "CJCPersonalInfoVC.h"
#import <JPUSHService.h>
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>


#import "ROXMRootController.h"
#endif

@interface AppDelegate ()<EMChatManagerDelegate,JPUSHRegisterDelegate,CLLocationManagerDelegate,EMClientDelegate>

@property (nonatomic ,strong) AMapLocationManager *amapManager;

@property (nonatomic ,strong) NSTimer *tempTimer;

@end

@implementation AppDelegate

-(void)acceptDateStartUploadLocation{

    if (self.tempTimer == nil) {
        
        [self setUpLocationManger];
        
        NSTimer *timer = [NSTimer timerWithTimeInterval:kLOCATIONFRESHTIME repeats:YES block:^(NSTimer * _Nonnull timer) {
            
            [self setUpLocationManger];
        }];
        
        self.tempTimer = timer;
        
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
}

-(void)startDateEndUploadLocation{

    if (self.tempTimer) {
        [self.tempTimer invalidate];
        self.tempTimer = nil;
    }
}

-(void)setUpLocationManger{

    if (self.amapManager == nil) {
        
        self.amapManager = [[AMapLocationManager alloc] init];
        
        [self.amapManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        //   定位超时时间，最低2s，此处设置为2s
        self.amapManager.locationTimeout = 5;
        //   逆地理请求超时时间，最低2s，此处设置为2s
        self.amapManager.reGeocodeTimeout = 5;
    }
    
    [self.amapManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        
        if (error)
        {
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            
            if (error.code == AMapLocationErrorLocateFailed)
            {
                return;
            }
        }
        
        NSString *uploadURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/location/add"];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"lat"] = @(location.coordinate.latitude);
        params[@"lng"] = @(location.coordinate.longitude);
        
        [CJCHttpTool postWithUrl:uploadURL params:params success:^(id responseObject) {
            
            NSNumber *rtNum = responseObject[@"rt"];
            if (rtNum.integerValue == 0) {
                
//                [MBManager showBriefAlert:@"上传地理位置成功"];
            }else{
            
//                [MBManager showBriefAlert:@"上传地理位置失败"];
            }
            
        } failure:^(NSError *error) {
        
//            [MBManager showBriefAlert:@"上传地理位置失败"];
        }];
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NSThread sleepForTimeInterval:0.001];

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [ROXMRootController shardRootController];
    [self.window makeKeyAndVisible];
    
    [self configThirdFramesWithOptions:launchOptions];//配置第三方友盟、高德、环信、七牛

//    CLLocationManager *manger = [[CLLocationManager alloc] init];
//    manger.delegate = self;
//    [manger requestAlwaysAuthorization];
    
    if ([[kUSERDEFAULT_STAND objectForKey:kACCEPTYAOYUEUPLOADLOCATION]isEqualToString:kSWITCHON]) {
        
        [self acceptDateStartUploadLocation];
    }
    [self linkedMEActipnWithOptions:launchOptions];

    if ([[kUSERDEFAULT_STAND objectForKey:@"secretAreaIsSet"] isEqualToString:kSWITCHON]) {
        if ([kUSERDEFAULT_STAND objectForKey:@"secretAreaX"]) {
            CJCClickSecretView *view = [[CJCClickSecretView alloc] init];
            [view showViewShow];
        }
    }
    return YES;
}

- (void)configThirdFramesWithOptions:(NSDictionary *)launchOptions{
    //友盟统计
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    [MobClick startWithAppkey:kUMENGKey];
    
    //高德地图
    [AMapServices sharedServices].apiKey = kAMAPKEY;
    
    //环信
    EMOptions *options = [EMOptions optionsWithAppkey:@"qunju#roxm"];
    [[EMClient sharedClient] initializeSDKWithOptions:options];
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];

    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [self configLocalPush];
    
    //极光推送
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    [JPUSHService setupWithOption:launchOptions appKey:kJPUSHAPPKEY
                          channel:@"App Store"
                 apsForProduction:@(0)
            advertisingIdentifier:nil];
    
    //获取七牛token
    [self getQINIUToken];
}

- (void)didLoginFromOtherDevice
{
    [CJCpersonalInfoModel infoModel].loginUserDict = nil;
    [CJCpersonalInfoModel infoModel].loginInfoDict = nil;
    [[CJCpersonalInfoModel infoModel] removeLocalInfo];
    [[ROXMRootController shardRootController] TapLogin];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"escLogin" object:nil];
}

- (void)getQINIUToken{
    NSString *QINIUURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"qiniu/common/token"];
    [CJCHttpTool postWithUrl:QINIUURL params:nil success:^(id responseObject) {
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            [kUSERDEFAULT_STAND setObject:responseObject[@"data"][@"token"] forKey:kQINIUTOKEN];
            [kUSERDEFAULT_STAND setObject:responseObject[@"data"][@"domain"] forKey:kQINIUDOMAIN];
            
            [kUSERDEFAULT_STAND synchronize];
        }else{
            [MBManager showBriefAlert:@"七牛Token请求错误"];
        }
    } failure:^(NSError *error) {
    }];
}

-(void)configLocalPush{
    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    if([application respondsToSelector:@selector(registerUserNotificationSettings:)]){
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages {
    for (EMMessage *msg in aMessages) {
        EMMessageBody *messageBody = msg.body;
        if (messageBody.type == EMMessageBodyTypeText) {
            NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                        convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
            if ([didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                NSDictionary *tempDict = msg.ext;
                NSNumber *orderStatusNum = tempDict[kHXYAOYUEORDERSTATUS];
                if (orderStatusNum.integerValue == 20) {
                    [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kACCEPTYAOYUEUPLOADLOCATION];
                    [self acceptDateStartUploadLocation];
                }else if (orderStatusNum.integerValue == 40){
                    [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kACCEPTYAOYUEUPLOADLOCATION];
                    [self startDateEndUploadLocation];
                }
            }
        }
        
        if ([[kUSERDEFAULT_STAND objectForKey:kCHATMESSAGENOTICE] isEqualToString:kSWITCHOFF]&&[[kUSERDEFAULT_STAND objectForKey:kYAOYUEMESSAGENOTICE] isEqualToString:kSWITCHOFF]) {
            return;
        }else if ([[kUSERDEFAULT_STAND objectForKey:kCHATMESSAGENOTICE] isEqualToString:kSWITCHOFF]){
            if (messageBody.type == EMMessageBodyTypeText) {
                NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                            convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                if ([didReceiveText isEqualToString:kHXYAOYUEIDENTIGYCATION]||[didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                }else{
                    return;
                }
                
            }
        }else if ([[kUSERDEFAULT_STAND objectForKey:kYAOYUEMESSAGENOTICE] isEqualToString:kSWITCHOFF]){
        
            if (messageBody.type == EMMessageBodyTypeText) {
                
                NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                            convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                
                if ([didReceiveText isEqualToString:kHXYAOYUEIDENTIGYCATION]||[didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                 
                    return;
                }
                
            }
        }
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        // App在后台
        if (state == UIApplicationStateBackground) {
            
            NSInteger number = [UIApplication sharedApplication].applicationIconBadgeNumber;
            
            number++;
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = number;
            
            //发送本地推送
            if (NSClassFromString(@"UNUserNotificationCenter")) { // ios 10
                // 设置触发时间
                UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:0.01 repeats:NO];
                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                content.sound = [UNNotificationSound defaultSound];
                // 提醒，可以根据需要进行弹出，比如显示消息详情，或者是显示“您有一条新消息”
                
                if ([[kUSERDEFAULT_STAND objectForKey:kMESSAGEISSHOWDETAIL] isEqualToString:kSWITCHOFF]) {
                    
                    content.body = @"您有一条新消息";
                }else{
        
                    content.body = [self _latestMessageTitleForConversationModel:msg];
                }
                
                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:msg.messageId content:content trigger:trigger];
                [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:nil];
            }else {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.fireDate = [NSDate date]; //触发通知的时间
                
                if ([[kUSERDEFAULT_STAND objectForKey:kMESSAGEISSHOWDETAIL] isEqualToString:kSWITCHOFF]) {
                    
                    notification.alertBody = @"您有一条新消息";
                }else{
                    
                    notification.alertBody = [self _latestMessageTitleForConversationModel:msg];
                }

                notification.alertAction = @"Open";
                notification.timeZone = [NSTimeZone defaultTimeZone];
                notification.soundName = UILocalNotificationDefaultSoundName;
                [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            }
        }else if (state == UIApplicationStateActive){
        
            EMCDDeviceManager *manger = [EMCDDeviceManager sharedInstance];
            
            if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEAUDIO] isEqualToString:kSWITCHOFF]) {
                
                
            }else{
                
                [manger playNewMessageSound];
            }
            
            if ([[kUSERDEFAULT_STAND objectForKey:kNOTICEVIBRATION] isEqualToString:kSWITCHOFF]) {
                
                
            }else{
            
                [manger playVibration];
            }
            
        }
    }
}

- (NSString *)_latestMessageTitleForConversationModel:(EMMessage *)conversationModel
{
    
    NSString *latestMessageTitle = @"";
    
    EMMessage *lastMessage = conversationModel;
    if (lastMessage) {
        EMMessageBody *messageBody = lastMessage.body;
        
        switch (messageBody.type) {
            case EMMessageBodyTypeImage:{
                latestMessageTitle = NSEaseLocalizedString(@"message.image1", @"[image]");
                
            } break;
            case EMMessageBodyTypeText:{
                NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                            convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                latestMessageTitle = didReceiveText;
                
                if ([didReceiveText isEqualToString:kHXYAOYUEIDENTIGYCATION]){
                    
                    latestMessageTitle = @"[邀约]";
                }
                if ([didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                    
                    latestMessageTitle = @"[邀约]";
                    
                    NSDictionary *tempDict = lastMessage.ext;
                    
                    NSNumber *orderStatusNum = tempDict[kHXYAOYUEORDERSTATUS];
                    
                    switch (orderStatusNum.integerValue) {
                        case 10:{
                            //已支付 待接收  发送的是邀约的cell
                            
                        }
                            break;
                            
                        case 20:{
                            //已接受  待到达
                            latestMessageTitle = @"已接受邀约";
                        }
                            break;
                            
                        case 30:{
                            //已到达  待开始
                            latestMessageTitle = @"已到达";
                        }
                            break;
                            
                        case 40:{
                            //已开始  待结束
                            latestMessageTitle = @"已开始邀约";
                        }
                            break;
                            
                        case 50:{
                            //已结束
                            latestMessageTitle = @"约会已完成";
                        }
                            break;
                            
                        case 110:{
                            //被婉拒
                            latestMessageTitle = @"已取消邀约";
                        }
                            break;
                            
                        case 120:{
                            //被取消
                            latestMessageTitle = @"已取消邀约";
                        }
                            break;
                            
                        default:
                            break;
                    }
                    
                }
                
            } break;
            case EMMessageBodyTypeVoice:{
                latestMessageTitle = NSEaseLocalizedString(@"message.voice1", @"[voice]");
                
            } break;
            case EMMessageBodyTypeLocation: {
                latestMessageTitle = NSEaseLocalizedString(@"message.location1", @"[location]");
                
            } break;
            case EMMessageBodyTypeVideo: {
                latestMessageTitle = NSEaseLocalizedString(@"message.video1", @"[video]");
            } break;
            case EMMessageBodyTypeFile: {
                latestMessageTitle = NSEaseLocalizedString(@"message.file1", @"[file]");
            } break;
            default: {
            } break;
        }
    }
    
    NSString *nickName;
    
    if (lastMessage.ext){
        
        nickName = lastMessage.ext[kHUANXINNICKNAMESENDER];
    }
    
    return [NSString stringWithFormat:@"%@:%@",nickName,latestMessageTitle];
}

//判断最顶层的控制器  输入密码时 使用系统的数字键盘
//- (BOOL)application:(UIApplication *)application
//
//shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier
//
//{
//
//    if (self.keyBoardType == CJCKeyboardTypeOnlySystem) {
//        
//        if ([extensionPointIdentifier isEqualToString:@"com.apple.keyboard-service"]) {
//            
//            return NO;
//            
//        }else{
//        
//            return YES;
//        }
//    }
//
//    return YES;
//    
//}

-(void)linkedMEActipnWithOptions:(NSDictionary *)launchOptions{

    LinkedME* linkedme = [LinkedME getInstance];
    
    [linkedme disableMatching];
    
    [linkedme initSessionWithLaunchOptions:launchOptions automaticallyDisplayDeepLinkController:NO deepLinkHandler:^(NSDictionary* params, NSError* error) {
        if (!error) {
            //防止传递参数出错取不到数据,导致App崩溃这里一定要用try catch
            @try {
                NSLog(@"LinkedME finished init with params = %@",[params description]);
                //获取标题
                NSArray *title = [params objectForKey:@"source"];
                NSString *source = params[@"$control"][@"source"];
                
                if (source) {
                    
                    [kUSERDEFAULT_STAND setObject:source forKey:kREGISTERSOURCE];
                    
                    [kUSERDEFAULT_STAND synchronize];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:source message:source preferredStyle:  UIAlertControllerStyleAlert];
                    
                    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                    }]];
                    
                    [self.window.rootViewController presentViewController:alert animated:true completion:nil];
                }
            } @catch (NSException *exception) {
                
            } @finally {
            }
        } else {
            NSLog(@"LinkedME failed init: %@", error);
        }
    }];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation{
    //判断是否是通过LinkedME的UrlScheme唤起App
//    if ([[url description] rangeOfString:@"roxm_9DD"].location != NSNotFound) {
//        return [[LinkedME getInstance] handleDeepLink:url];
//    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    
    if ([url.host isEqualToString:@"platformapi"]){
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    
    return YES;
}

//Universal Links 通用链接实现深度链接技术
- (BOOL)application:(UIApplication*)application continueUserActivity:(NSUserActivity*)userActivity restorationHandler:(void (^)(NSArray*))restorationHandler{
    
    //判断是否是通过LinkedME的Universal Links唤起App
    if ([[userActivity.webpageURL description] rangeOfString:@"lkme.cc"].location != NSNotFound) {
        
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:userActivity.title message:nil preferredStyle:  UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        
        [self.window.rootViewController presentViewController:alert animated:true completion:nil];
        
        return  [[LinkedME getInstance] continueUserActivity:userActivity];
    }
    return YES;
}

//URI Scheme 实现深度链接技术
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options{
    NSLog(@"opened app from URL %@", [url description]);
    
    //判断是否是通过LinkedME的UrlScheme唤起App
//    if ([[url description] rangeOfString:@"roxm_9DD"].location != NSNotFound) {
//        return [[LinkedME getInstance] handleDeepLink:url];
//    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    
    if ([url.host isEqualToString:@"platformapi"]){
    
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[EMClient sharedClient] applicationDidEnterBackground:application];

}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [[EMClient sharedClient] applicationWillEnterForeground:application];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if ([[kUSERDEFAULT_STAND objectForKey:@"secretAreaIsSet"] isEqualToString:kSWITCHON]) {
        
        if ([kUSERDEFAULT_STAND objectForKey:@"secretAreaX"]) {
            
            if (self.secretView == nil) {
                
                CJCClickSecretView *view = [[CJCClickSecretView alloc] init];
                
                [view showViewShow];
            }
        }
    }

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
}


- (void)applicationWillTerminate:(UIApplication *)application {
}


@end
