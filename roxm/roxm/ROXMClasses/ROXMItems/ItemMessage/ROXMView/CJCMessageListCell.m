//
//  CJCMessageListCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageListCell.h"
#import "CJCCommon.h"
#import <EaseUI.h>

@interface CJCMessageListCell ()

@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic ,strong) UIButton *imageBtn;

@property (nonatomic ,strong) UILabel *nameLabel;

@property (nonatomic ,strong) UILabel *timeLable;

@property (nonatomic ,strong) UILabel *lastMessageLabel;

@property (nonatomic ,strong) UILabel *statusLabel;

@property (nonatomic ,strong) UIView *lineView;

@property (nonatomic ,strong) UIView *unreadView;

@property (nonatomic ,strong) UILabel *unreadLabel;

@end

@implementation CJCMessageListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    //    UIImageView *iconView = [[UIImageView alloc] init];
    //
    //    iconView.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(16), kAdaptedValue(49), kAdaptedValue(49));
    //
    //    iconView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //
    //    iconView.layer.cornerRadius = kAdaptedValue(49/2);
    //    iconView.layer.masksToBounds = YES;
    //
    //    self.iconImageView = iconView;
    //    [self.contentView addSubview:iconView];
    
    
    UIButton *imageBtn = [[UIButton alloc]initWithFrame:CGRectMake(kAdaptedValue(20), kAdaptedValue(16), kAdaptedValue(49), kAdaptedValue(49))];
    
    imageBtn.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    imageBtn.layer.cornerRadius = kAdaptedValue(49/2);
    imageBtn.layer.masksToBounds = YES;
    
    [imageBtn addTarget:self action:@selector(investBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.imageBtn = imageBtn;
    
    [self.contentView addSubview:imageBtn];
    
    
    UIView *unreadView = [[UIView alloc] init];
    
    unreadView.backgroundColor = [UIColor toUIColorByStr:@"65C4AA"];
    unreadView.frame = CGRectMake(kAdaptedValue(55), kAdaptedValue(51), kAdaptedValue(14), kAdaptedValue(14));
    
    unreadView.layer.cornerRadius = kAdaptedValue(7);
    unreadView.layer.masksToBounds = YES;
    
    self.unreadView = unreadView;
    [self.contentView addSubview:unreadView];
    
    UILabel *unreadNumLabel = [UIView getYYLabelWithStr:@"1" fontName:kFONTNAMELIGHT size:11 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    unreadNumLabel.frame = CGRectMake(5, 5, 5, 12);
    unreadNumLabel.center = CGPointMake(unreadView.width/2, unreadView.height/2);
    
    self.unreadLabel = unreadNumLabel;
    [unreadView addSubview:unreadNumLabel];
    
    UILabel *nameLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"4A4A4A"]];
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UILabel *timeLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:14 color:[UIColor toUIColorByStr:@"999999"]];
    
    self.timeLable = timeLabel;
    [self.contentView addSubview:timeLabel];
    
    UILabel *messageLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:17 color:[UIColor toUIColorByStr:@"707070"]];
    
    self.lastMessageLabel = messageLabel;
    [self.contentView addSubview:messageLabel];
    
    //会根据订单的状态改变字体的颜色
    UILabel *statusLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"65C4AA"]];
    
    self.statusLabel = statusLabel;
    [self.contentView addSubview:statusLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(94), SCREEN_WITDH-kAdaptedValue(40), OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setConversationModel:(EaseConversationModel *)conversationModel{
    
    _conversationModel = conversationModel;
    
    EMMessage *lastMessage = conversationModel.conversation.latestMessage;
    
    if(lastMessage){
        
        if (lastMessage.ext) {
            
            if (lastMessage.direction == EMMessageDirectionSend) {
                
                NSString *iconURL = lastMessage.ext[kHUANXINICONURLRECIVER];
                
                // [self.iconImageView setImageURL:[NSURL URLWithString:iconURL]];
                
                [self.imageBtn setImageWithURL:[NSURL URLWithString:iconURL] forState:UIControlStateNormal options:YYWebImageOptionShowNetworkActivity];
                
                self.nameLabel.text = lastMessage.ext[kHUANXINNICKNAMERECIVER];
            }else{
                
                NSString *iconURL = lastMessage.ext[kHUANXINICONURLSENDER];
                
                // [self.iconImageView setImageURL:[NSURL URLWithString:iconURL]];
                [self.imageBtn setImageWithURL:[NSURL URLWithString:iconURL] forState:UIControlStateNormal options:YYWebImageOptionShowNetworkActivity];
                
                self.nameLabel.text = lastMessage.ext[kHUANXINNICKNAMESENDER];
            }
            
        }else{
            
            UIImage *image = conversationModel.avatarImage;
            
            [self.imageBtn setImage:image forState:UIControlStateNormal];
            
            //  self.iconImageView.image = conversationModel.avatarImage;
            self.nameLabel.text = @"未知聊天者";
        }
        
        self.nameLabel.frame = CGRectMake(self.imageBtn.right+kAdaptedValue(9), kAdaptedValue(19.5), SCREEN_WITDH-kAdaptedValue(180), kAdaptedValue(17));
        
        self.lastMessageLabel.text = [self _latestMessageTitleForConversationModel:conversationModel];
        
        if (lastMessage.direction == EMMessageDirectionSend) {
            
            self.lastMessageLabel.textColor = [UIColor toUIColorByStr:@"222222"];
        }else{
            
            self.lastMessageLabel.textColor = [UIColor toUIColorByStr:@"707070"];
        }
        
        //消息的显示会根据 发送者还是接收者  更改颜色和字体
        self.lastMessageLabel.frame = CGRectMake(self.imageBtn.right+kAdaptedValue(9), self.nameLabel.bottom+kAdaptedValue(5.5), SCREEN_WITDH-kAdaptedValue(100), 22);
        
        self.statusLabel.x = self.lastMessageLabel.x;
        self.statusLabel.y = self.lastMessageLabel.bottom+5;
        self.statusLabel.size = CGSizeMake(self.lastMessageLabel.width, 15);
        
        NSString *timeStr = [self _latestMessageTimeForConversationModel:conversationModel];
        
        self.timeLable.text = timeStr;
        
        CGFloat timeLabelWidth = [CJCTools getShortStringLength:timeStr withFont:[UIFont accodingVersionGetFont_lightWithSize:14]];
        
        self.timeLable.frame = CGRectMake(323, 19, timeLabelWidth, 16);
        
        self.timeLable.right = SCREEN_WITDH - kAdaptedValue(20);
        self.timeLable.centerY = self.nameLabel.centerY;
    }
    
}

#pragma mark - private

/*!
 @method
 @brief 获取会话最近一条消息内容提示
 @discussion
 @param conversationModel  会话model
 @result 返回传入会话model最近一条消息提示
 */
- (NSString *)_latestMessageTitleForConversationModel:(id<IConversationModel>)conversationModel
{
    
    self.lineView.bottom = kAdaptedValue(81);
    
    NSString *latestMessageTitle = @"";
    
    EMConversation *conversation = conversationModel.conversation;
    
    if (conversation.unreadMessagesCount > 0) {
        
        self.unreadView.hidden = NO;
        self.unreadLabel.text = [NSString stringWithFormat:@"%d",conversation.unreadMessagesCount];
    }else{
        
        self.unreadView.hidden = YES;
    }
    
    EMMessage *lastMessage = conversation.latestMessage;
    if (lastMessage) {
        EMMessageBody *messageBody = lastMessage.body;
        switch (messageBody.type) {
            case EMMessageBodyTypeImage:{
                latestMessageTitle = NSEaseLocalizedString(@"message.image1", @"[image]");
                
                self.lineView.bottom = kAdaptedValue(80);
                self.statusLabel.hidden = YES;
                
            } break;
            case EMMessageBodyTypeText:{
                NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                            convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
                latestMessageTitle = didReceiveText;
                
                CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
                
                if ([didReceiveText isEqualToString:kHXYAOYUEIDENTIGYCATION]){
                    
                    latestMessageTitle = @"[邀约]";
                    self.lineView.bottom = kAdaptedValue(94);
                    
                    self.statusLabel.hidden = NO;
                    if (localInfoModel.sex == 1) {
                        
                        self.statusLabel.text = @"已发出邀约";
                    }else{
                        
                        self.statusLabel.text = @"邀约待回应";
                    }
                    self.statusLabel.textColor = [UIColor toUIColorByStr:@"65C4AA"];
                }else if ([didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                    
                    latestMessageTitle = @"[邀约]";
                    self.lineView.bottom = kAdaptedValue(94);
                    
                    NSDictionary *tempDict = lastMessage.ext;
                    
                    NSNumber *orderStatusNum = tempDict[kHXYAOYUEORDERSTATUS];
                    
                    switch (orderStatusNum.integerValue) {
                        case 10:{
                            //已支付 待接收  发送的是邀约的cell
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"65C4AA"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"已发出邀约";
                            }else{
                                
                                self.statusLabel.text = @"邀约待回应";
                            }
                        }
                            break;
                            
                        case 20:{
                            //已接受  待到达
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"65C4AA"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"对方已应邀";
                            }else{
                                
                                self.statusLabel.text = @"已应邀";
                            }
                        }
                            break;
                            
                        case 30:{
                            //已到达  待开始
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"65C4AA"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"对方已到达";
                            }else{
                                
                                self.statusLabel.text = @"已到达";
                            }
                        }
                            break;
                            
                        case 40:{
                            //已开始  待结束
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"65C4AA"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"待评价";
                            }else{
                                
                                self.statusLabel.text = @"待评价";
                            }
                        }
                            break;
                            
                        case 50:{
                            //已结束
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"707070"];
                            if (localInfoModel.sex ==1) {
                                
                                self.statusLabel.text = @"已结束";
                            }else{
                                
                                self.statusLabel.text = @"已结束";
                            }
                        }
                            break;
                            
                        case 110:{
                            //被婉拒
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"707070"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"邀约已取消";
                            }else{
                                
                                self.statusLabel.text = @"已取消";
                            }
                        }
                            break;
                            
                        case 120:{
                            //被取消
                            self.statusLabel.textColor = [UIColor toUIColorByStr:@"707070"];
                            if (localInfoModel.sex == 1) {
                                
                                self.statusLabel.text = @"邀约已取消";
                            }else{
                                
                                self.statusLabel.text = @"已取消";
                            }
                        }
                            break;
                            
                        default:
                            break;
                    }
                    
                }else{
                    
                    self.lineView.bottom = kAdaptedValue(80);
                    self.statusLabel.hidden = YES;
                }
                
            } break;
            case EMMessageBodyTypeVoice:{
                latestMessageTitle = NSEaseLocalizedString(@"message.voice1", @"[voice]");
                
                self.lineView.bottom = kAdaptedValue(80);
                self.statusLabel.hidden = YES;
                
            } break;
            case EMMessageBodyTypeLocation: {
                latestMessageTitle = NSEaseLocalizedString(@"message.location1", @"[location]");
                
                self.lineView.bottom = kAdaptedValue(80);
                self.statusLabel.hidden = YES;
                
            } break;
            case EMMessageBodyTypeVideo: {
                latestMessageTitle = NSEaseLocalizedString(@"message.video1", @"[video]");
                
                self.lineView.bottom = kAdaptedValue(80);
                self.statusLabel.hidden = YES;
                
            } break;
            case EMMessageBodyTypeFile: {
                latestMessageTitle = NSEaseLocalizedString(@"message.file1", @"[file]");
                
                self.lineView.bottom = kAdaptedValue(80);
                self.statusLabel.hidden = YES;
                
            } break;
            default: {
            } break;
        }
    }
    return latestMessageTitle;
}

/*!
 @method
 @brief 获取会话最近一条消息时间
 @discussion
 @param conversationModel  会话model
 @result 返回传入会话model最近一条消息时间
 */
- (NSString *)_latestMessageTimeForConversationModel:(id<IConversationModel>)conversationModel
{
    NSString *latestMessageTime = @"";
    EMMessage *lastMessage = [conversationModel.conversation latestMessage];;
    if (lastMessage) {
        long long timeInterval = lastMessage.timestamp ;
        if(timeInterval > 140000000000) {
            timeInterval = timeInterval/1000.0;
            
            NSString *nowDate = [CJCTools getDateTimeTOMilliSeconds];
            
            //            latestMessageTime = [self compareTwoTime:timeInterval time2:nowDate.longLongValue];
            
            latestMessageTime = [self distanceTimeWithBeforeTime:timeInterval];
        }
        
    }
    return latestMessageTime;
}

- (NSString *)distanceTimeWithBeforeTime:(double)beTime
{
    NSTimeInterval now = [[NSDate date]timeIntervalSince1970];
    double distanceTime = now - beTime;
    NSString * distanceStr;
    
    NSDate * beDate = [NSDate dateWithTimeIntervalSince1970:beTime];
    NSDateFormatter * df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm"];
    NSString * timeStr = [df stringFromDate:beDate];
    
    [df setDateFormat:@"dd"];
    NSString * nowDay = [df stringFromDate:[NSDate date]];
    NSString * lastDay = [df stringFromDate:beDate];
    
    if(distanceTime <24*60*60 && [nowDay integerValue] == [lastDay integerValue]){//时间小于一天
        distanceStr = [NSString stringWithFormat:@"%@",timeStr];
    }else if(distanceTime<24*60*60*2 && [nowDay integerValue] != [lastDay integerValue]){
        
        if ([nowDay integerValue] - [lastDay integerValue] ==1 || ([lastDay integerValue] - [nowDay integerValue] > 10 && [nowDay integerValue] == 1)) {
            distanceStr = [NSString stringWithFormat:@"昨天"];
        }
        else{
            [df setDateFormat:@"MM-dd"];
            distanceStr = [df stringFromDate:beDate];
        }
    }else{
        [df setDateFormat:@"MM-dd"];
        distanceStr = [df stringFromDate:beDate];
    }
    return distanceStr;
}

- (NSString*)compareTwoTime:(long long)time1 time2:(long long)time2{
    
    NSTimeInterval balance = time2 /1000.0- time1 /1000.0;
    
    NSString *timeString = [[NSString alloc]init];
    
    timeString = [NSString stringWithFormat:@"%f",balance /60.0];
    
    timeString = [timeString substringToIndex:timeString.length-7];
    
    NSInteger timeInt = [timeString intValue];
    
    NSInteger day = timeInt/60/24;
    
    if(day == 0) {
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"HH:mm"];
        return  [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time1 /1000.0]];
        
    }else if(day == 1){
        
        return @"昨天";
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM-dd"];
    return  [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:time1 /1000.0]];
}

- (void)investBtnClick
{
    if (self.addToCartsBlock) {
        
        self.addToCartsBlock(self);
        
    }
    
}

@end
