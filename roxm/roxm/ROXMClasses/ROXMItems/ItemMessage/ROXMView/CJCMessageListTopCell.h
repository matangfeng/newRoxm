//
//  CJCMessageListTopCell.h
//  roxm
//
//  Created by 陈建才 on 2017/11/5.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CJCMessageListTopCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *messageCountLabel;



@end
