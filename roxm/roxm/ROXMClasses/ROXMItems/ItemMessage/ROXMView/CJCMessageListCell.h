//
//  CJCMessageListCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYSideslipCell.h"

@class EaseConversationModel;

@interface CJCMessageListCell : LYSideslipCell
typedef void (^AddToCartsBlock) (CJCMessageListCell *);
@property(nonatomic, copy) AddToCartsBlock addToCartsBlock;
@property (nonatomic ,strong) EaseConversationModel *conversationModel;
@end
