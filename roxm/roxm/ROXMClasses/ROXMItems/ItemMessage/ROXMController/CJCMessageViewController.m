//
//  CJCMessageViewController.m
//  roxm
//
//  Created by lfy on 2017/9/1.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageViewController.h"
#import "CJCCommon.h"
#import "CJCMessageListHeader.h"
#import "CJCMessageListCell.h"
#import <Hyphenate/Hyphenate.h>
#import <EaseUI.h>
#import "CJCSingleChatVC.h"
#import <EMCDDeviceManager.h>
#import "CJCBigTopTitleCell.h"
//#import "UITabBar+CJCBadge.h"
#import "CJCFollowListVC.h"
#import "CJCMessageListTopCell.h"
#import "CJCPersonalInfoVC.h"
#import "LYSideslipCell.h"

#define kMessageListCell    @"CJCMessageListCell"
#define kBigTopTitleCell    @"CJCBigTopTitleCell"

@interface CJCMessageViewController ()<UITableViewDelegate,UITableViewDataSource,EMChatManagerDelegate,LYSideslipCellDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *dataArray;
    
    int messageCount;
}

@property (nonatomic ,strong) UITableView *listTableView;

@property (nonatomic ,strong) UILabel *hintLabel;

@property (nonatomic ,strong) UIView *shadowView;

@property (nonatomic ,strong) UILabel *bigTitleLabel;

@end

@implementation CJCMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    messageCount = 0;
    dataArray = [NSMutableArray array];
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [self setUpUI];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y <= kAdaptedValue(83)) {
        
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSLog(@"scrollViewDidEndDragging  -  End of Scrolling.");
    NSLog(@"__________%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y > 64) {
        self.mediumTitleLabel.hidden  = NO;
        [UIView animateWithDuration:0.5 animations:^{
            self.mediumTitleLabel.frame = CGRectMake(kAdaptedValue(139.5), 32, kAdaptedValue(100), kAdaptedValue(22.5));
        } completion:^(BOOL finished) {
            
        }];
    }else{
        self.mediumTitleLabel.hidden  = YES;
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"scrollViewDidEndDecelerating");
    NSLog(@"__________%f",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y > 0) {
        self.mediumTitleLabel.hidden  = NO;
        //        [UIView animateWithDuration:0 animations:^{
        //
        //                       self.mediumTitleLabel.frame = CGRectMake(kAdaptedValue(139.5), 32, kAdaptedValue(100), kAdaptedValue(22.5));
        //
        //                    } completion:^(BOOL finished) {
        //
        //                    }];
        
    }else{
        self.mediumTitleLabel.hidden  = YES;
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages{
    [self tableViewDidTriggerHeaderRefresh];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self tableViewDidTriggerHeaderRefresh];
}

#pragma mark =====tableview  delegate  datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return kAdaptedValue(83);
    }
    
    EaseConversationModel *model = dataArray[indexPath.row-1];
    EMConversation *conversation = model.conversation;
    EMMessage *lastMessage = conversation.latestMessage;
    
    switch (lastMessage.body.type) {
        case EMMessageBodyTypeText:{
            EMMessageBody *messageBody = lastMessage.body;
            NSString *didReceiveText = [EaseConvertToCommonEmoticonsHelper
                                        convertToSystemEmoticons:((EMTextMessageBody *)messageBody).text];
            if ([didReceiveText isEqualToString:kHXYAOYUEIDENTIGYCATION]){
                return kAdaptedValue(95);
            }
            if ([didReceiveText isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                return kAdaptedValue(95);
            }
            return kAdaptedValue(81);
            break;
        }
        case EMMessageBodyTypeImage: {
            
            break;
        }
        case EMMessageBodyTypeVideo: {
            break;
        }
        case EMMessageBodyTypeLocation: {
            break;
        }
        case EMMessageBodyTypeVoice: {
            break;
        }
        case EMMessageBodyTypeFile: {
            break;
        }
        case EMMessageBodyTypeCmd: {
            break;
        }
    };
    return kAdaptedValue(81);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        CJCMessageListTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kBigTopTitleCell];
        [UIView animateWithDuration:1.0f animations:^{
            cell.frame = CGRectMake(-SCREEN_WITDH/2+25, 80, 0, 0);
        } completion:^(BOOL finished) {
        }];
        return cell;
    }
    
    CJCMessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:kMessageListCell];
    __weak typeof(self) weakSelf = self;
    cell.addToCartsBlock = ^(CJCMessageListCell *cell) {
        
        [weakSelf myFavoriteCellAdd:cell];
        
    };
    cell.delegate = self;
    cell.conversationModel = dataArray[indexPath.row-1];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.row == 0) {
    //
    //         CJCMessageListTopCell *cell1 = (CJCMessageListTopCell *)cell;
    //
    //        [UIView animateWithDuration:1.0f animations:^{
    //
    //
    //            cell1.frame = CGRectMake(SCREEN_WITDH/2-25, -80, 0, 0);
    //
    //        } completion:^(BOOL finished) {
    //
    //        }];
    //    }
    
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    if (indexPath.row == 0) {
    //
    //        [UIView animateWithDuration:1.0f animations:^{
    //
    //            CJCMessageListTopCell *cell1 = (CJCMessageListTopCell *)cell;
    //            cell1.frame = CGRectMake(SCREEN_WITDH/2-25, -80, 0, 0);
    //
    //        } completion:^(BOOL finished) {
    //
    //        }];
    //    }
    
}
    
- (NSArray<LYSideslipCellAction *> *)sideslipCell:(LYSideslipCell *)sideslipCell editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    LYSideslipCellAction *action2 = [LYSideslipCellAction rowActionWithStyle:LYSideslipCellActionStyleDestructive title:@"删除" handler:^(LYSideslipCellAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
    }];
    NSArray *array = @[];
    array = @[action2];
    return array;
}

- (void)sideslipCell:(LYSideslipCell *)sideslipCell rowAtIndexPath:(NSIndexPath *)indexPath didSelectedAtIndex:(NSInteger)index
    {
        EaseConversationModel *model = dataArray[indexPath.row-1];
        NSString *conversationId = model.conversation.conversationId;
        
        [[EMClient sharedClient].chatManager deleteConversation:conversationId isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError){
            //code
        }];
        
        if (dataArray.count && indexPath.row < dataArray.count){
            [dataArray removeObjectAtIndex:indexPath.row];
            [self.listTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }

- (BOOL)sideslipCell:(LYSideslipCell *)sideslipCell canSideslipRowAtIndexPath:(NSIndexPath *)indexPath
    {
        return YES;
    }
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EaseConversationModel *model = dataArray[indexPath.row-1];
    EMMessage *lastMessage = model.conversation.latestMessage;
    CJCSingleChatVC *nextVC = [[CJCSingleChatVC alloc] init];
    NSString *conversationId = model.conversation.conversationId;
    nextVC.qunjuId = model.conversation.conversationId;
    
    if(lastMessage){
        if (lastMessage.ext) {
            if (lastMessage.direction == EMMessageDirectionSend) {
                nextVC.iconImageUrl = lastMessage.ext[kHUANXINICONURLRECIVER];
                nextVC.nickName = lastMessage.ext[kHUANXINNICKNAMERECIVER];
            }else{
                nextVC.iconImageUrl = lastMessage.ext[kHUANXINICONURLSENDER];
                nextVC.nickName = lastMessage.ext[kHUANXINNICKNAMESENDER];
            }
        }
    }
    nextVC.conversationId = conversationId;
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    EaseConversationModel *model = dataArray[indexPath.row-1];
//    NSString *conversationId = model.conversation.conversationId;
//
//    [[EMClient sharedClient].chatManager deleteConversation:conversationId isDeleteMessages:YES completion:^(NSString *aConversationId, EMError *aError){
//        //code
//    }];
//
//    [dataArray removeObjectAtIndex:indexPath.row];
//    // 刷新
//    [self.listTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//}
//
///**
// *  修改Delete按钮文字为“删除”
// */
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return @"删除";
//}

-(void)setUpUI{
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    self.backButton.hidden = YES;
    self.mediumTitleLabel.hidden  = YES;
    self.mediumTitleLabel.text = @"消息";
    self.mediumTitleLabel.frame = CGRectMake(kAdaptedValue(139.5), 32, kAdaptedValue(100), kAdaptedValue(22.5));
    self.titleLabel.centerY = kNAVIVIEWCENTERY;
    self.titleLabel.centerX = self.view.centerX;
    
    self.lineView.hidden = YES;
    [self setKeyBoardHid:YES];
    
    UIButton *titleImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    [titleImageView setImage:kGetImage(@"messagelist_icon_friend") forState:UIControlStateNormal];
    [titleImageView setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"messagelist_icon_friend")] forState:1];

    titleImageView.frame = CGRectMake(321, 20, kAdaptedValue(44), kAdaptedValue(44));
    titleImageView.right = SCREEN_WITDH - kAdaptedValue(10);
    
    [[titleImageView rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        CJCFollowListVC *nextVC = [[CJCFollowListVC alloc] init];
        nextVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:nextVC animated:YES];
    }];
    
    [self.view addSubview:titleImageView];
    
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    
    tableView.frame = CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64-49);
    
    [tableView registerClass:[CJCMessageListCell class] forCellReuseIdentifier:kMessageListCell];
    [tableView registerClass:[CJCMessageListTopCell class] forCellReuseIdentifier:kBigTopTitleCell];
    
    __weak __typeof(self) weakSelf = self;
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf tableViewDidTriggerHeaderRefresh];
    }];
    
    self.listTableView = tableView;
    [self.view addSubview:tableView];
    UILabel *hintLable = [UIView getYYLabelWithStr:@"与TA的聊天消息会出现在这里" fontName:kFONTNAMEREGULAR size:16 color:[UIColor toUIColorByStr:@"818181"]];
    hintLable.textAlignment = NSTextAlignmentCenter;
    hintLable.frame = CGRectMake(0, 322.5, SCREEN_WITDH, kAdaptedValue(22.5));
    hintLable.centerY = SCREEN_HEIGHT/2;
    self.hintLabel = hintLable;
    [self.view addSubview:hintLable];
}

#pragma mark - data

-(void)refreshAndSortView
{
    if ([dataArray count] > 1) {
        if ([[dataArray objectAtIndex:0] isKindOfClass:[EaseConversationModel class]]) {
            NSArray* sorted = [dataArray sortedArrayUsingComparator:
                               ^(EaseConversationModel *obj1, EaseConversationModel* obj2){
                                   EMMessage *message1 = [obj1.conversation latestMessage];
                                   EMMessage *message2 = [obj2.conversation latestMessage];
                                   if(message1.timestamp > message2.timestamp) {
                                       return(NSComparisonResult)NSOrderedAscending;
                                   }else {
                                       return(NSComparisonResult)NSOrderedDescending;
                                   }
                               }];
            [dataArray removeAllObjects];
            [dataArray addObjectsFromArray:sorted];
        }
    }
    [self.listTableView reloadData];
}

/*!
 @method
 @brief 加载会话列表
 @discussion
 @result
 */
- (void)tableViewDidTriggerHeaderRefresh
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSArray* sorted = [conversations sortedArrayUsingComparator:
                       ^(EMConversation *obj1, EMConversation* obj2){
                           EMMessage *message1 = [obj1 latestMessage];
                           EMMessage *message2 = [obj2 latestMessage];
                           if(message1.timestamp > message2.timestamp) {
                               return(NSComparisonResult)NSOrderedAscending;
                           }else {
                               return(NSComparisonResult)NSOrderedDescending;
                           }
                       }];
    
    [dataArray removeAllObjects];
    messageCount = 0;
    
    for (EMConversation *converstion in sorted) {
        EaseConversationModel *model = nil;
        model = [[EaseConversationModel alloc] initWithConversation:converstion];
        if (model) {
            messageCount = messageCount + model.conversation.unreadMessagesCount;
            [dataArray addObject:model];
        }
    }
    
    if (dataArray.count>0) {
        self.hintLabel.hidden = YES;
        if (messageCount>0) {
            self.bigTitleLabel.text = [NSString stringWithFormat:@"消息 (%d)",messageCount];
//            [self.tabBarController.tabBar showBadgeOnItemIndex:1];
        }else{
            self.bigTitleLabel.text = @"消息";
//            [self.tabBarController.tabBar hideBadgeOnItemIndex:1];
        }
        [kUSERDEFAULT_STAND setObject:@(messageCount) forKey:kUNREADMESSAGECOUNT];
        self.titleLabel.centerY = kNAVIVIEWCENTERY;
        self.titleLabel.centerX = self.view.centerX;
    }
    [self.listTableView.mj_header endRefreshing];
    [self.listTableView reloadData];
}


- (void)myFavoriteCellAdd:(CJCMessageListCell *)cell
{
    EaseConversationModel *model = dataArray[[_listTableView indexPathForCell:cell].row-1];
    CJCPersonalInfoVC *nextVC = [[CJCPersonalInfoVC alloc] init];
    nextVC.sexType = CJCSexTypeWoman;
    nextVC.otherUID = model.conversation.conversationId;
    nextVC.infoVCType = CJCPersonalInfoVCTypeOther;
    //nextVC.location = self.location;
    nextVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:nextVC animated:YES];
}

#pragma mark - EMGroupManagerDelegate
- (void)didUpdateGroupList:(NSArray *)groupList
{
    [self tableViewDidTriggerHeaderRefresh];
}

#pragma mark - registerNotifications
-(void)registerNotifications{
    [self unregisterNotifications];
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].groupManager addDelegate:self delegateQueue:nil];
}

-(void)unregisterNotifications{
    [[EMClient sharedClient].chatManager removeDelegate:self];
    [[EMClient sharedClient].groupManager removeDelegate:self];
}

- (void)dealloc{
    [self unregisterNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
