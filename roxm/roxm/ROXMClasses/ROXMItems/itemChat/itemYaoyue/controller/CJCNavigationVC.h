//
//  CJCNavigationVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import <EaseUI.h>

@class CJCInviteOrderModel;

@interface CJCNavigationVC : CommonVC

@property (nonatomic ,strong) CJCInviteOrderModel *orderModel;

@property (nonatomic ,copy) NSString *partnerIcon;

@property (nonatomic ,copy) NSString *partnerID;;

@property (nonatomic ,copy) NSString *hotelName;

@property (nonatomic ,copy) NSString *hotelAddress;

@property (nonatomic ,assign) CGFloat lat;

@property (nonatomic ,assign) CGFloat lng;

@end
