//
//  CJCInviteOrderDetailVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteOrderDetailVC.h"
#import "CJCCommon.h"
#import "CJCInviteDetailTopCell.h"
#import "CJCInviteOrderModel.h"
#import "CJCInviteOrderListCell.h"
#import "CJCCancleOrderVC.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import "CJCChatLocationVC.h"
#import "CJCMessageHandle.h"
#import "CJCInviteDetailPhoneCell.h"
#import "CJCCommentDateVC.h"
#import "CJCNavigationVC.h"
#import <MAMapKit/MAMapKit.h>

#define kInviteDetailTopCell        @"CJCInviteDetailTopCell"
#define kInviteOrderListCell        @"CJCInviteOrderListCell"
#define kInviteDetailPhoneCell      @"CJCInviteDetailPhoneCell"
#define kTwoButtonWidth         (SCREEN_WITDH - kAdaptedValue(46)-kAdaptedValue(19))/2

@interface CJCInviteOrderDetailVC ()<UITableViewDelegate,UITableViewDataSource,CJCInviteDetailPhoneCellDelegate>{

    CJCpersonalInfoModel *infoModel;
    
    CJCInviteOrderModel *orderModel;
}

@property (nonatomic ,strong) UILabel *orderStatusLabel;

@property (nonatomic ,strong) UITableView *inviteDetailTB;

@property (nonatomic ,strong) UIView *bottomView;

@property (nonatomic ,strong) AMapLocationManager *amapManager;

//取消邀约
@property (nonatomic ,strong) UIButton *cancleButton;

//左侧的按钮  功能:取消邀约
@property (nonatomic ,strong) UIButton *leftButton;

//右侧按钮 功能:接受 到达 开始
@property (nonatomic ,strong) UIButton *rightButton;

//评价对方的按钮
@property (nonatomic ,strong) UIButton *evaluateButton;

//邀约完成
@property (nonatomic ,strong) UIButton *dateCompleteButton;

@end

@implementation CJCInviteOrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNaviView];
    
    [self setUpUI];
    
    [self getPersonalInfo];
    
    [self getOrderDetail];

}

//获取个人信息
-(void)getPersonalInfo{
    
    [MBManager showLoadingInView:self.view];
    
    NSString *infoURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/user/get/other"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"otherUserId"] = self.otherPersonUID;
    
    [CJCHttpTool postWithUrl:infoURL params:params success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            infoModel = [CJCpersonalInfoModel modelWithDictionary:responseObject[@"data"][@"user"]];

            CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
            handle.nickName = infoModel.nickname;
            handle.iconImageUrl = infoModel.imageUrl;
            handle.infoModel = infoModel;
            
            [self.inviteDetailTB reloadData];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)getOrderDetail{

    [MBManager showLoading];
    
    NSString *orderURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.orderId;
    
    [CJCHttpTool postWithUrl:orderURL params:params success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            orderModel = [CJCInviteOrderModel modelWithDictionary:responseObject[@"data"][@"order"]];
            
            [self setUpTitleAndBottomView];
            
            [self.inviteDetailTB reloadData];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpTitleAndBottomView{
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *nameStr ;
    
    switch (orderModel.status) {
            
//        case 0:{
//            
//            nameStr = @"待回应";
//            
//            if (localInfoModel.sex == 0) {
//                
//                [self.leftButton setTitle:@"婉拒" forState:UIControlStateNormal];
//                [self.leftButton addTarget:self action:@selector(declineButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
//                
//                [self.rightButton setTitle:@"应邀" forState:UIControlStateNormal];
//                [self.rightButton addTarget:self action:@selector(acceptButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
//                
//            }else{
//                
//                self.cancleButton.hidden = NO;
//                
//            }
//        }
//            break;
            
        case 10:{
        
            nameStr = @"待回应";
            
            if (localInfoModel.sex == 0) {
                
                [self.leftButton setTitle:@"婉拒" forState:UIControlStateNormal];
                [self.leftButton addTarget:self action:@selector(declineButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
                [self.rightButton setTitle:@"应邀" forState:UIControlStateNormal];
                [self.rightButton addTarget:self action:@selector(acceptButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
            
                self.cancleButton.hidden = NO;
                
            }
        }
            break;
            
        case 20:{
            
            nameStr = @"已应邀";
            
            if (localInfoModel.sex == 0){
            
                [self.leftButton removeAllTargets];
                [self.leftButton setTitle:@"取消邀约" forState:UIControlStateNormal];
                [self.leftButton addTarget:self action:@selector(cancleButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
                [self.rightButton removeAllTargets];
                [self.rightButton setTitle:@"确认到达" forState:UIControlStateNormal];
                [self.rightButton addTarget:self action:@selector(arriveButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
            
                [self.leftButton removeAllTargets];
                [self.leftButton setTitle:@"取消邀约" forState:UIControlStateNormal];
                [self.leftButton addTarget:self action:@selector(cancleButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
                [self.rightButton removeAllTargets];
                [self.rightButton setTitle:@"确认开始" forState:UIControlStateNormal];
                [self.rightButton addTarget:self action:@selector(startDateButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
            }

        }
            break;
            
        case 30:{
            
            nameStr = @"已到达";
            
            if (localInfoModel.sex == 0){
            
                self.cancleButton.hidden = NO;
                
            }else{
            
                [self.leftButton removeAllTargets];
                [self.leftButton setTitle:@"取消邀约" forState:UIControlStateNormal];
                [self.leftButton addTarget:self action:@selector(cancleButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
                
                [self.rightButton removeAllTargets];
                [self.rightButton setTitle:@"确认开始" forState:UIControlStateNormal];
                [self.rightButton addTarget:self action:@selector(startDateButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
            break;
            
        case 40:{
            
            if (localInfoModel.sex == 0) {
                
                if (orderModel.partnerCommentId==0) {
                    
                    nameStr = @"待评价";
                    
                    self.evaluateButton.hidden = NO;
                    
                }else{
                
                    nameStr = @"已结束";
                    
                    [self.bottomView removeAllSubviews];
                }
                
            }else{
            
                if (orderModel.userCommentId==0) {
                    
                    nameStr = @"待评价";
                    
                    self.evaluateButton.hidden = NO;
                    
                }else{
                
                    nameStr = @"已结束";
                    
                    [self.bottomView removeAllSubviews];
                }
            
            }
            
            
        }
            break;
            
        case 50:{
            
            nameStr = @"已完成";
            
            self.evaluateButton.hidden = NO;
        }
            break;
            
        case 110:{
            
            nameStr = @"已取消";
            [self.bottomView removeAllSubviews];
        }
            break;
            
        case 120:{
            
            nameStr = @"已取消";
            [self.bottomView removeAllSubviews];
        }
            break;
            
        default:
            break;
    }
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    self.orderStatusLabel.text = nameStr;
    self.orderStatusLabel.width = width;
    self.orderStatusLabel.centerX = self.view.centerX;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 2) {
        
        CJCChatLocationVC *nextVC = [[CJCChatLocationVC alloc] init];
        
        nextVC.locationVCType = CJCChatLocationVCTypeYaoyue;
        nextVC.hotelName = orderModel.hotelName;
        nextVC.lat = orderModel.lat;
        nextVC.lng = orderModel.lng;
        nextVC.hotelAddress = orderModel.hotelAddress;
        
        [self.navigationController pushViewController:nextVC animated:YES];

    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (infoModel == nil||orderModel== nil) {
        
        return 0;
    }
    
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return kAdaptedValue(92);
    }else if (indexPath.row == 1){
    
        if (orderModel.status == 20||orderModel.status == 30){
        
            return 110;
        }else{
        
            return 0;
        }
    }else if (indexPath.row == 2){
    
        if (orderModel.status == 20||orderModel.status == 30){
            
            return 0;
        }else{
            
            return 64;
        }
        
    }
    
    return 64;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        CJCInviteDetailTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteDetailTopCell];
        
        cell.infoModel = infoModel;
        
        return cell;
    }else if (indexPath.row == 1){
    
        CJCInviteDetailPhoneCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteDetailPhoneCell];
        
        cell.delegate = self;
        
        if (orderModel.status == 20||orderModel.status == 30){
        
            cell.isShow = YES;
            
            CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
            
            if (localInfoModel.sex == 0){
                
                if ([CJCTools isBlankString:orderModel.userVirtualPhone]) {
                    
                    cell.canOpration = 0.3;
                }
            }else{
                
                if ([CJCTools isBlankString:orderModel.partnerVirtualPhone]) {
                    
                    cell.canOpration = 0.3;
                }
            }
            
        }else{
        
            cell.isShow = NO;
        }
        
        return cell;
    }
    
    CJCInviteOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteOrderListCell];
    
    if (indexPath.row == 2) {
        
        if (orderModel.status == 20||orderModel.status == 30){
            
            cell.isShow = NO;
        }else{
            
            cell.isShow = YES;
            cell.cellType = CJCInviteOrderListCellTypeDefault;
            cell.title = @"酒店位置";
        }
        
    }else if(indexPath.row == 3){
    
        cell.cellType = CJCInviteOrderListCellTypeNoArrow;
        cell.title = @"约会时长";
        
        if (orderModel.timeType == 1) {
            
            cell.detail = @"3小时";
        }else if (orderModel.timeType == 2){
        
            cell.detail = @"12小时";
        }else{
        
            cell.detail = @"未知";
        }
        
    }else{
    
        cell.cellType = CJCInviteOrderListCellTypeGift;
        cell.title = @"邀约礼物";
        cell.detail = orderModel.giftName;
        cell.imagePath = orderModel.giftUrl;
    }
    
    return cell;
}

-(void)setUpUI{

    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64-69) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[CJCInviteDetailTopCell class] forCellReuseIdentifier:kInviteDetailTopCell];
    [tableView registerClass:[CJCInviteOrderListCell class] forCellReuseIdentifier:kInviteOrderListCell];
    [tableView registerClass:[CJCInviteDetailPhoneCell class] forCellReuseIdentifier:kInviteDetailPhoneCell];
    
    self.inviteDetailTB = tableView;
    [self.view addSubview:tableView];
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    titleLabel.size = CGSizeMake(30, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    self.orderStatusLabel = titleLabel;
    [self.naviView addSubview:titleLabel];
    
    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.backgroundColor = [UIColor whiteColor];
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-69, SCREEN_WITDH, 69);
    
    self.bottomView = bottomView;
    [self.view addSubview:bottomView];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
    
    [bottomView addSubview:lineView];
}

-(UIButton *)leftButton{

    if (_leftButton == nil) {
        
        UIButton *button = [UIView getButtonWithStr:@"" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"282828"]];
        
        button.frame = CGRectMake(kAdaptedValue(23), 12, kTwoButtonWidth, 44);
        
        button.centerY = 69/2;
        
        button.layer.cornerRadius = kAdaptedValue(4);
        [button.layer setBorderColor:[UIColor toUIColorByStr:@"979797"].CGColor];
        [button.layer setBorderWidth:1];
        
        [button.layer setMasksToBounds:YES];
        
        [self.bottomView addSubview:button];
        _leftButton = button;
    }
    return _leftButton;
}

-(UIButton *)rightButton{

    if (_rightButton == nil) {
        
        UIButton *button = [UIView getButtonWithStr:@"" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
        
        button.frame = CGRectMake(kAdaptedValue(23), 12, kTwoButtonWidth, 44);
        
        button.centerY = 69/2;
        button.right = SCREEN_WITDH - kAdaptedValue(23);
        
        button.layer.cornerRadius = kAdaptedValue(4);
        [button.layer setMasksToBounds:YES];
        
        [button setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
        
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828"]] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828" andAlpha:0.5]] forState:UIControlStateSelected];

        [self.bottomView addSubview:button];
        _rightButton = button;
    }
    return _rightButton;
}

-(UIButton *)cancleButton{

    if (_cancleButton == nil) {
        
        UIButton *button = [UIView getButtonWithStr:@"取消邀约" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"282828"]];
        
        button.frame = CGRectMake(kAdaptedValue(23), 12, SCREEN_WITDH-kAdaptedValue(46), 44);
        
        button.centerY = 69/2;
        
        button.layer.cornerRadius = kAdaptedValue(4);
        [button.layer setBorderColor:[UIColor toUIColorByStr:@"979797"].CGColor];
        [button.layer setBorderWidth:1];
        [button.layer setMasksToBounds:YES];
        
        [button addTarget:self action:@selector(cancleButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:button];
        _cancleButton = button;
    }
    return _cancleButton;
}

-(UIButton *)dateCompleteButton{

    if (_dateCompleteButton == nil) {
        
        UIButton *button = [UIView getButtonWithStr:@"结束邀约" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
        
        button.frame = CGRectMake(kAdaptedValue(23), 12, SCREEN_WITDH-kAdaptedValue(46), 44);
        
        button.centerY = 69/2;
        
        button.layer.cornerRadius = kAdaptedValue(4);
        [button.layer setMasksToBounds:YES];
        button.backgroundColor = [UIColor toUIColorByStr:@"282828"];
        
        [button addTarget:self action:@selector(dateCompleteButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:button];
        _dateCompleteButton = button;
    }
    return _dateCompleteButton;
}

-(UIButton *)evaluateButton{

    if (_evaluateButton == nil) {
        
        UIButton *button = [UIView getButtonWithStr:@"评价对方" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
        
        button.frame = CGRectMake(kAdaptedValue(23), 12, SCREEN_WITDH-kAdaptedValue(46), 44);
        
        button.centerY = 69/2;
        
        button.layer.cornerRadius = kAdaptedValue(4);
        [button.layer setMasksToBounds:YES];
        
        [button setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828"]] forState:0];

        [button setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828" andAlpha:0.5]] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(evaluateButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
        
        [self.bottomView addSubview:button];
        _evaluateButton = button;
    }
    return _evaluateButton;
}

#pragma 打电话
-(void)callHandle{

//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 0){
    
        if (![CJCTools isBlankString:orderModel.userVirtualPhone]) {
            
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",orderModel.userVirtualPhone];
            // NSLog(@"str======%@",str);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }else{
    
        if (![CJCTools isBlankString:orderModel.partnerVirtualPhone]) {
            
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",orderModel.partnerVirtualPhone];
            // NSLog(@"str======%@",str);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
}

#pragma 位置点击了
-(void)locationHandle{

    CJCNavigationVC *nextVC = [[CJCNavigationVC alloc] init];
    
    nextVC.orderModel = orderModel;
    nextVC.hotelName = orderModel.hotelName;
    nextVC.lat = orderModel.lat;
    nextVC.lng = orderModel.lng;
    nextVC.hotelAddress = orderModel.hotelAddress;
    nextVC.partnerID = infoModel.uid;
    nextVC.partnerIcon = infoModel.imageUrl;
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)dateCompleteButtonClickHandle{

    [self oprationOrderWithURL:@"/order/end"];
}

-(void)evaluateButtonClickHandle{

    CJCCommentDateVC *nextVC = [[CJCCommentDateVC alloc]init];
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 0) {
        
        nextVC.parnterID = orderModel.userId;
        
    }else{
        
        nextVC.parnterID = orderModel.partnerId;
    }
    
    nextVC.orderID = self.orderId;
    nextVC.commentHandle = ^{
      
        [self getOrderDetail];
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)startDateButtonClickHandle{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否确认开始约会" message:@"确认后女方将收到您赠送的礼物" preferredStyle:  UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self oprationOrderWithURL:@"/order/start"];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)arriveButtonClickHandle{

    [self configLocationManager];
}

-(void)declineButtonClickHandle{

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"真的要拒绝吗?" message:@"他是真的喜欢你才邀请你的T_T~" preferredStyle:  UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self oprationOrderWithURL:@"/order/decline"];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];

}

-(void)acceptButtonClickHandle{

    [self oprationOrderWithURL:@"/order/accept"];
}

-(void)oprationOrderWithURL:(NSString *)typeURL{

    [self showWithLabelAnimation];
    
    NSString *orderURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,typeURL];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.orderId;
    
    [CJCHttpTool postWithUrl:orderURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            orderModel = [CJCInviteOrderModel modelWithDictionary:responseObject[@"data"][@"order"]];
            
            [self clearBottomView];
            
            [self setUpTitleAndBottomView];
            
            [self.inviteDetailTB reloadData];
            
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            
            if (orderModel.status == 20) {
                
                [kUSERDEFAULT_STAND setObject:kSWITCHON forKey:kACCEPTYAOYUEUPLOADLOCATION];
                [delegate acceptDateStartUploadLocation];
            }else if (orderModel.status == 40){
            
                [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kACCEPTYAOYUEUPLOADLOCATION];
                
                [delegate startDateEndUploadLocation];
            }
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinYaoyueStatusMessageWithAmapPOI:orderModel andTo:self.otherPersonUID];
            
            [self useHuanxinVCSendMessage:message];
            
        }else{
        
            [MBManager showGloomy:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)clearBottomView{

    [self.leftButton removeFromSuperview];
    self.leftButton = nil;
    
    [self.rightButton removeFromSuperview];
    self.rightButton = nil;
}

-(void)cancleButtonClickHandle{

    NSString *message;
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 1) {
        
        if (orderModel.status == 30) {
            
            NSInteger compensate;
            
            if (orderModel.price<=1000) {
                
                compensate = 100;
            }else if (orderModel.price >1000 && orderModel.price<2000){
            
                compensate = 150;
            }else{
            
                compensate = 200;
            }
            
            message = [NSString stringWithFormat:@"取消需要支付对方%ld元路费补偿",(long)compensate];
        }
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否确认取消约会" message:message preferredStyle:  UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self commitButtonClickHandle];
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
}

-(void)commitButtonClickHandle{
    
    [self showWithLabelAnimation];
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    NSString *commitURL;
    
    if (localInfoModel.sex == 0) {
        
        commitURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/cancel/by_partner"];
    }else{
        
        commitURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/cancel/by_user"];
    }
    
//    NSString *commitURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/cancel/by_user"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.orderId;
    
    [CJCHttpTool postWithUrl:commitURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            CJCInviteOrderModel *orderModel1 = [CJCInviteOrderModel modelWithDictionary:responseObject[@"data"][@"order"]];
            
            AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            
            [kUSERDEFAULT_STAND setObject:kSWITCHOFF forKey:kACCEPTYAOYUEUPLOADLOCATION];
            
            [delegate startDateEndUploadLocation];
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinYaoyueStatusMessageWithAmapPOI:orderModel1 andTo:self.otherPersonUID];
            
            [self useHuanxinVCSendMessage:message];
            
            [self pushToCancleVC];
            
        }else{
            
            [MBManager showGloomy:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)pushToCancleVC{

    CJCCancleOrderVC *nextVC = [[CJCCancleOrderVC alloc] init];
    
    nextVC.orderId = self.orderId;
    
    nextVC.otherUID = self.otherPersonUID;
    
    nextVC.messageVC = self.messageVC;
    
    nextVC.cancleHandle = ^{
        
        [self getOrderDetail];
    };
    
    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)arriveRequestWith:(CLLocation *)location{

    NSString *orderURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/arrive"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.orderId;
    params[@"lng"] = @(location.coordinate.longitude);
    params[@"lat"] = @(location.coordinate.latitude);
    
    [CJCHttpTool postWithUrl:orderURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            orderModel = [CJCInviteOrderModel modelWithDictionary:responseObject[@"data"][@"order"]];
            
            [self clearBottomView];
            
            [self setUpTitleAndBottomView];
            
            [self.inviteDetailTB reloadData];
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinYaoyueStatusMessageWithAmapPOI:orderModel andTo:self.otherPersonUID];
            
            [self useHuanxinVCSendMessage:message];
            
        }else{
            
            [MBManager showGloomy:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

- (void)configLocationManager
{
    [self showWithLabelAnimation];
    
    self.amapManager = [[AMapLocationManager alloc] init];
    
    [self.amapManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.amapManager.locationTimeout =2;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.amapManager.reGeocodeTimeout = 2;
    
    [self.amapManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        
        if (error)
        {
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            
            if (error.code == AMapLocationErrorLocateFailed)
            {
                return;
            }
            
        }
        
        CLLocationCoordinate2D coor;
        coor.latitude = orderModel.lat;
        coor.longitude = orderModel.lng;
        
        MAMapPoint point1 = MAMapPointForCoordinate(coor);
        
        MAMapPoint point2 = MAMapPointForCoordinate(location.coordinate);
        
        CLLocationDistance distance = MAMetersBetweenMapPoints(point1,point2);
        
        if (distance<=300) {
            
            [self arriveRequestWith:location];
        }else{
        
            [self hidHUD];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"请到达约定酒店再点击到达" message:nil preferredStyle:  UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            
            [self presentViewController:alert animated:true completion:nil];
        }
        
    }];
}

-(void)useHuanxinVCSendMessage:(EMMessage *)message{
    
    if (self.messageVC) {
        
        SEL testFunc = NSSelectorFromString(@"_sendMessage:");
        
        ((void(*)(id,SEL, id,id))objc_msgSend)(self.messageVC, testFunc, message, nil);
    }else{
    
        [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *message, EMError *error) {
            
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
