//
//  CJCNavigationVC.m
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCNavigationVC.h"
#import "CJCCommon.h"
#import <MAMapKit/MAMapKit.h>
#import "CJCInviteOrderModel.h"
#import "CJCDateLocationModel.h"
#import "CJCWebVC.h"
#import "CJCMAPointAnnotation.h"

#define kTwoButtonWidth         (SCREEN_WITDH - kAdaptedValue(32)-kAdaptedValue(11))/2

@interface CJCNavigationVC ()<MAMapViewDelegate>{

    CJCMAPointAnnotation *partnerAnnotation;
    
    BOOL locationReady;
    
    CLLocationCoordinate2D partnerCoor;
}

@property (nonatomic ,strong) MAMapView *aMapView;

@property (nonatomic ,strong) NSTimer *tempTimer;

@end

@implementation CJCNavigationVC

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if (self.tempTimer.isValid) {
        
        [self.tempTimer invalidate];
        self.tempTimer = nil;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    if (self.tempTimer == nil) {
        
        [self getPersonalInfo];
        
        NSTimer *timer = [NSTimer timerWithTimeInterval:kLOCATIONFRESHTIME repeats:YES block:^(NSTimer * _Nonnull timer) {
            
            [self getPersonalInfo];
        }];
        
        _tempTimer = timer;
        
        [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locationReady = NO;
    
    [self setUpNaviView];
    [self setUpUI];
    [self setUpBottomView];
}

-(void)getPersonalInfo{
    
    NSString *infoURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/location/get/last"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"userId"] = self.partnerID;
    
    [CJCHttpTool postWithUrl:infoURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        if (rtNum.integerValue == 0){
            
            locationReady = YES;
            
            CJCDateLocationModel *locationModel = [CJCDateLocationModel modelWithDictionary:responseObject[@"data"]];
            
            CLLocationCoordinate2D coor;
            coor.latitude = locationModel.lat;
            coor.longitude = locationModel.lng;
            
            partnerCoor = coor;
            
            if (partnerAnnotation == nil) {
                
                CJCMAPointAnnotation *pointAnnotation = [[CJCMAPointAnnotation alloc] init];
                
                partnerAnnotation = pointAnnotation;
                
//                CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
                CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

                if (localInfoModel.sex == 0){
                
                    partnerAnnotation.imageName = self.orderModel.userImageUrl;
                    partnerAnnotation.bottomImageName = @"locate_female";
                    
                }else{
                
                    partnerAnnotation.imageName = self.orderModel.partnerImageUrl;
                    partnerAnnotation.bottomImageName = @"message_place2";
                }
                
                partnerAnnotation.coordinate = coor;
                
                [self.aMapView addAnnotation:partnerAnnotation];
            }
            
            partnerAnnotation.coordinate = coor;
            //设置地图的定位中心点坐标
            //将点添加到地图上，即所谓的大头针
            
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)setUpBottomView{
    
    UIView *bottomView = [[UIView alloc] init];
    
    bottomView.frame = CGRectMake(0, SCREEN_HEIGHT-kAdaptedValue(66+69), SCREEN_WITDH, kAdaptedValue(66+69));
    
    [self.view addSubview:bottomView];
    
    UILabel *nameLable = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:16 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *name = self.hotelName;
    
    nameLable.text = name;
    
    nameLable.frame = CGRectMake(kAdaptedValue(16), kAdaptedValue(14), SCREEN_WITDH-kAdaptedValue(32), kAdaptedValue(16));
    
    [bottomView addSubview:nameLable];
    
    UILabel *addressLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:12 color:[UIColor toUIColorByStr:@"333333"]];
    
    addressLabel.text = self.hotelAddress;
    
    addressLabel.frame = CGRectMake(kAdaptedValue(16), kAdaptedValue(38), SCREEN_WITDH-kAdaptedValue(32), kAdaptedValue(16));
    
    [bottomView addSubview:addressLabel];
    
    UIButton *leftButton = [UIView getButtonWithStr:@"打车" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    leftButton.frame = CGRectMake(kAdaptedValue(16), addressLabel.bottom+20, kTwoButtonWidth, 44);
    
    leftButton.layer.cornerRadius = kAdaptedValue(6);
    [leftButton.layer setMasksToBounds:YES];
    
    [leftButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
    
    [leftButton setImage:kGetImage(@"invite_car") forState:UIControlStateNormal];
    [leftButton setImage:[self imageByApplyingAlpha:0.5 image:kGetImage(@"invite_car")] forState:1];

    [leftButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    [leftButton setImageEdgeInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
    
    [leftButton addTarget:self action:@selector(gotoDIDIdache) forControlEvents:UIControlEventTouchUpInside];
    
    [bottomView addSubview:leftButton];
    
    UIButton *rightButton = [UIView getButtonWithStr:@"导航" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    rightButton.frame = CGRectMake(kAdaptedValue(23), addressLabel.bottom+20, kTwoButtonWidth, 44);

    rightButton.right = SCREEN_WITDH - kAdaptedValue(16);
    
    rightButton.layer.cornerRadius = kAdaptedValue(6);
    [rightButton.layer setMasksToBounds:YES];
    
    [rightButton setImage:kGetImage(@"invite_go") forState:UIControlStateNormal];
    
    [rightButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
    [rightButton setImageEdgeInsets:UIEdgeInsetsMake(0, -6, 0, 0)];
    
    [rightButton addTarget:self action:@selector(followButtonDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [bottomView addSubview:rightButton];
    
//    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *localInfoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    if (localInfoModel.sex == 0) {
        
        [leftButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"4BD3C5"]] forState:UIControlStateNormal];
        [leftButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"4BD3C5" andAlpha:0.5]] forState:UIControlStateSelected];
        
        [rightButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"429CF0"]] forState:UIControlStateNormal];
        [rightButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"429CF0" andAlpha:0.5]] forState:UIControlStateSelected];
    }else{
        [leftButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"1F1C1B"]] forState:UIControlStateNormal];
        [leftButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"1F1C1B" andAlpha:0.5]] forState:UIControlStateSelected];
        
        [rightButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"429CF0"]] forState:UIControlStateNormal];
        [rightButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"429CF0" andAlpha:0.5]] forState:UIControlStateSelected];
    }
}

-(void)gotoDIDIdache{

//    CJCWebVC *nextVC = [[CJCWebVC alloc] init];
//    nextVC.requestURL = @"https://common.diditaxi.com.cn/general/webEntry?maptype=wgs";
//    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)followButtonDidClick{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"跳转苹果地图导航" message:nil preferredStyle:  UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"跳转" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gotoMap];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [self presentViewController:alert animated:true completion:nil];
    
}

-(void)gotoMap{
    
    CLLocationCoordinate2D coords2 = CLLocationCoordinate2DMake(self.lat,self.lng);
    
    //起点
    
    NSString *name = self.hotelName;
    
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    
    //目的地的位置
    
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coords2 addressDictionary:nil]];
    
    toLocation.name = name;
    
    NSArray *items = [NSArray arrayWithObjects:currentLocation, toLocation, nil];
    
    NSDictionary *options = @{ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES }; //打开苹果自身地图应用，并呈现特定的item
    
    [MKMapItem openMapsWithItems:items launchOptions:options];
    
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay{
    
    return nil;
}

-(void)setUpUI{
    
    MAMapView *_mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT-64-kAdaptedValue(66+69))];
    
    ///把地图添加至view
    self.aMapView = _mapView;
    [self.view addSubview:_mapView];
    
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
    [_mapView setZoomLevel:16.1 animated:YES];
    
    _mapView.showsCompass= NO;
    _mapView.showsScale= NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    _mapView.rotateCameraEnabled = NO;
    _mapView.skyModelEnable = NO;
    _mapView.showsBuildings = NO;
    _mapView.showsLabels = YES;
    
    _mapView.customizeUserLocationAccuracyCircleRepresentation = YES;
    
    CLLocationCoordinate2D coor;
    coor.latitude = self.lat;
    coor.longitude = self.lng;
    
    _mapView.centerCoordinate = coor;
    
    CJCMAPointAnnotation *pointAnnotation = [[CJCMAPointAnnotation alloc] init];
    
    pointAnnotation.coordinate = coor;
    
    pointAnnotation.bottomImageName = @"locate_hotel";
    
    //设置地图的定位中心点坐标
    _mapView.centerCoordinate = coor;
    //将点添加到地图上，即所谓的大头针
    [_mapView addAnnotation:pointAnnotation];
    
//    UIButton *partnerOriginButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    
//    [partnerOriginButton setImage:kGetImage(@"message_place1") forState:UIControlStateNormal];
//    
//    partnerOriginButton.frame = CGRectMake(14, 14, kAdaptedValue(44), kAdaptedValue(44));
//    
//    partnerOriginButton.bottom = _mapView.height - 14;
//    
//    [partnerOriginButton addTarget:self action:@selector(mapBackToPartnerLocation) forControlEvents:UIControlEventTouchUpInside];
//    
//    [_mapView addSubview:partnerOriginButton];
    
    UIImageView *iconView = [[UIImageView alloc] init];
    
    iconView.frame = CGRectMake(14, 14, kAdaptedValue(44), kAdaptedValue(44));;
    iconView.bottom = _mapView.height - 14;
    
    iconView.layer.cornerRadius = kAdaptedValue(22);
    iconView.layer.masksToBounds = YES;
    
    iconView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapBackToPartnerLocation)];
    
    [iconView addGestureRecognizer:tapGes];
    
    [iconView setImageURL:[NSURL URLWithString:self.partnerIcon]];
    
    [_mapView addSubview:iconView];
    
    UIButton *originCenterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [originCenterButton setImage:kGetImage(@"message_place1") forState:UIControlStateNormal];
    
    originCenterButton.frame = CGRectMake(iconView.right+8, 14, kAdaptedValue(44), kAdaptedValue(44));
    
    originCenterButton.bottom = _mapView.height - 14;
    
    [originCenterButton addTarget:self action:@selector(mapBackToOrginLocation) forControlEvents:UIControlEventTouchUpInside];
    
    [_mapView addSubview:originCenterButton];
}



- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation{
    
    if (![annotation isKindOfClass:[MAUserLocation class]])
    {
        static NSString *reuseIndetifier = @"annotationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (annotationView == nil)
        {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:reuseIndetifier];
        }
        
        CJCMAPointAnnotation *newAnnotation = (CJCMAPointAnnotation *)annotation;
        
        annotationView.image = [UIImage imageNamed:newAnnotation.bottomImageName];
        //设置中心点偏移，使得标注底部中间点成为经纬度对应点
        annotationView.centerOffset = CGPointMake(0, -kAdaptedValue(37/2));
        
        if (newAnnotation.imageName) {
            
            UIImageView *swit = [[UIImageView alloc] init];
            
            swit.size = CGSizeMake(26, 26);
            swit.center = CGPointMake(annotationView.width/2, annotationView.height/2-4);
            
            swit.backgroundColor = [UIColor redColor];
            
            swit.layer.cornerRadius = 13;
            swit.layer.masksToBounds = YES;
            
            [swit setImageURL:[NSURL URLWithString:newAnnotation.imageName]];
            
            [annotationView addSubview:swit];
        }
        
        return annotationView;
    }
    return nil;
}

-(void)mapBackToPartnerLocation{

    if (locationReady) {
        
        self.aMapView.centerCoordinate = partnerCoor;
    }
}

-(void)mapBackToOrginLocation{
    
    self.aMapView.centerCoordinate = self.aMapView.userLocation.location.coordinate;
    
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:17 color:[UIColor toUIColorByStr:@"333333"]];
    
    NSString *nameStr = [NSString stringWithFormat:@"实时位置"];
    
    CGFloat width = [CJCTools getShortStringLength:nameStr withFont:[UIFont accodingVersionGetFont_mediumWithSize:17]];
    
    titleLabel.text = nameStr;
    
    titleLabel.size = CGSizeMake(width, 24);
    
    titleLabel.centerY = kNAVIVIEWCENTERY;
    titleLabel.centerX = self.view.centerX;
    
    [self.naviView addSubview:titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
