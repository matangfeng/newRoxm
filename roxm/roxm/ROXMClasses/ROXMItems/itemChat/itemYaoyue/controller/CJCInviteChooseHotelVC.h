//
//  CJCInviteChooseHotelVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/6.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@class CLLocation,AMapPOI;

typedef void(^chooseHotelHandle)(AMapPOI *hotelPOI);

@interface CJCInviteChooseHotelVC : CommonVC

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,copy) chooseHotelHandle chooseHandle;

@end
