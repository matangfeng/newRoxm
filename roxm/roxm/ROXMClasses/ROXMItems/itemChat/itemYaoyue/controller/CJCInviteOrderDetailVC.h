//
//  CJCInviteOrderDetailVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

@class CJCpersonalInfoModel,EaseMessageViewController;

@interface CJCInviteOrderDetailVC : CommonVC

@property (nonatomic ,copy) NSString *orderId;

@property (nonatomic ,strong) EaseMessageViewController *messageVC;

@property (nonatomic ,copy) NSString *otherPersonUID;

@end
