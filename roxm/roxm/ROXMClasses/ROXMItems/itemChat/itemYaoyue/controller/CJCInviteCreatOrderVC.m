//
//  CJCInviteCreatOrderVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteCreatOrderVC.h"
#import "CJCCommon.h"
#import "UINavigationController+CJCStatusBar.h"
#import "CJCinviteOrderTopCell.h"
#import "CJCpersonalInfoModel.h"
#import "CJCInviteOrderListCell.h"
#import "CJCBirthHeiWeightView.h"
#import "CJCChooseLocationVC.h"
#import "CJCBuyInviteGiftVC.h"
#import "CJCInviteGiftModel.h"
#import <Hyphenate/Hyphenate.h>
#import "CJCMessageHandle.h"
#import "CJCInviteChooseHotelVC.h"
#import "CJCPhotoShowModel.h"
#import "CJCPersonalInfoVC.h"
#import "CJCSingleChatVC.h"
#import "CJCInviteOrderModel.h"
#import <AFNetworking.h>


#define kinviteOrderTopCell         @"CJCinviteOrderTopCell"
#define kInviteOrderListCell        @"CJCInviteOrderListCell"

@interface CJCInviteCreatOrderVC ()<UITableViewDelegate,UITableViewDataSource>{

    NSInteger hotelIndex;
    NSInteger giftIndex;
    
    NSString *dateTimeStr;
    NSString *hotelName;
    NSString *hotelStarStr;
    CLLocation *hotelLocation;
    NSString *giftId;
    
    NSString *sendOrderId;
    
    CJCInviteGiftModel *giftModel;
    CJCInviteOrderModel *orderModel;
    
    AMapPOI *selectPOI;
}

@property (nonatomic ,strong) UITableView *invitTableView;

@property (nonatomic ,strong) UIButton *sureButton;

@end

@implementation CJCInviteCreatOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    handle.nickName = self.infoModel.nickname;
    handle.iconImageUrl = self.infoModel.imageUrl;
    handle.infoModel = self.infoModel;
    
    giftIndex = 0;
    hotelIndex = 0;
    
    [self setUpUI];
    [self getThreeHourGiftDetail];
}

-(void)getThreeHourGiftDetail{
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.infoModel.giftId1;
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            
            if (giftIndex == 0) {
                
                giftIndex++;
            }
            
            giftModel = [CJCInviteGiftModel modelWithJSON:responseObject[@"data"][@"gift"]];
            
            [self.invitTableView reloadRow:3 inSection:0 withRowAnimation:NO];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)getTwelveHourGiftDetail{
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.infoModel.giftId2;
    
    [CJCHttpTool postWithUrl:giftURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0){
            
            giftModel = [CJCInviteGiftModel modelWithJSON:responseObject[@"data"][@"gift"]];
            
            [self.invitTableView reloadRow:3 inSection:0 withRowAnimation:NO];
            
        }else{
            
            [self showWithLabel:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 1) {
        
        CJCBirthHeiWeightView *pick = [[CJCBirthHeiWeightView alloc] init];
        
        pick.pickerViewType = CJCPickerViewTypeDateTime;
        
        [pick showPickerView];
        
        pick.returnHandle = ^(NSString *returnString, NSString *returnInt){
            
            CJCInviteOrderListCell *cell = [self.invitTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            
            cell.detail = returnString;
            dateTimeStr = returnInt;
            
            if ([returnInt isEqualToString:@"1"]) {
                
                [self getThreeHourGiftDetail];
            }else{
            
                [self getTwelveHourGiftDetail];
            }
            
            [self nextStepButtonCanClick];
        };
    }else if (indexPath.row == 2){
    
//        CJCChooseLocationVC *nextVC = [[CJCChooseLocationVC alloc] init];
//        
//        nextVC.location = self.location;
//        nextVC.locationType = CJCChooseLocationTypeInvite;
//        
//        nextVC.locationHandle = ^(AMapPOI *POI, NSString *imagePath, double distance) {
//            
//            if (hotelIndex == 0) {
//                
//                hotelIndex++;
//            }
//            
//            CJCInviteOrderListCell *cell = [self.invitTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//            
//            cell.detail = POI.name;
//            hotelStarStr = [self hotelStar:POI.type];
//            hotelLocation = [[CLLocation alloc] initWithLatitude:POI.location.latitude longitude:POI.location.longitude];
//            selectPOI = POI;
//            hotelName = POI.name;
//            
//            [self nextStepButtonCanClick];
//        };
        
        CJCInviteChooseHotelVC *nextVC = [[CJCInviteChooseHotelVC alloc] init];
        
        nextVC.location = self.location;
        nextVC.chooseHandle = ^(AMapPOI *hotelPOI) {
            
            if (hotelIndex == 0) {
                
                hotelIndex++;
            }
            
            CJCInviteOrderListCell *cell = [self.invitTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            
            cell.detail = hotelPOI.name;
            hotelStarStr = [self hotelStar:hotelPOI.type];
            hotelLocation = [[CLLocation alloc] initWithLatitude:hotelPOI.location.latitude longitude:hotelPOI.location.longitude];
            selectPOI = hotelPOI;
            hotelName = hotelPOI.name;
            
            [self nextStepButtonCanClick];
        };
        
        [self presentViewController:nextVC animated:YES completion:nil];
    }else if(indexPath.row == 3){
    
        
    }
}

-(void)nextStepButtonCanClick{
    
    if (hotelIndex+giftIndex == 2) {
        
        [self.sureButton setTitleColor:[UIColor toUIColorByStr:@"EBC57A"] forState:UIControlStateNormal];
        [self.sureButton setTitleColor:[UIColor toUIColorByStr:@"EBC57A" andAlpha:0.5] forState:UIControlStateSelected];

        self.sureButton.userInteractionEnabled = YES;
    }
}

-(NSString *)hotelStar:(NSString *)POIType{

    if ([POIType containsString:@"三"]) {
        
        return @"3";
    }else if ([POIType containsString:@"四"]){
    
        return @"4";
    }else if ([POIType containsString:@"五"]){
    
        return @"5";
    }
    return @"0";
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        return SCREEN_WITDH;
    }else if (indexPath.row == 1){
    
        return 80;
    }else{
    
        return 64;
    }
}

-(void)setImages:(NSArray *)images{

    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:images];
    
    for (CJCPhotoShowModel *model in images) {
        
        if (model.type == 21) {
            
            [tempArr removeObject:model];
        }
    }
    
    _images = tempArr.copy;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        CJCinviteOrderTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kinviteOrderTopCell];
        
        cell.imagesArray = self.images;
        cell.infoModel = self.infoModel;
        
        return cell;
    }else if (indexPath.row == 1){
    
        CJCInviteOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteOrderListCell];
        
        cell.cellType = CJCInviteOrderListCellTypeHigher;
        cell.title = @"约会时长";
        cell.detail = @"3小时";
        dateTimeStr = @"1";
        
        return cell;
    }else if (indexPath.row == 2){
    
        CJCInviteOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteOrderListCell];
        
        cell.cellType = CJCInviteOrderListCellTypeDefault;
        cell.title = @"选择酒店";
        
        return cell;
    }else if (indexPath.row == 3){
        
        CJCInviteOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:kInviteOrderListCell];
        
        cell.cellType = CJCInviteOrderListCellTypeGift;
        cell.title = @"邀约礼物";
        
        cell.detail = giftModel.name;
        cell.imagePath = giftModel.url;
        giftId = giftModel.giftId;
        
        return cell;
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    cell.backgroundColor = [CJCTools randomColor];
    
    return cell;
}

-(void)setUpUI{

    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:kGetImage(@"nav_icon_back_black") forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(kAdaptedValue(3), kAdaptedValue(23), kAdaptedValue(40), kAdaptedValue(40));
    
    [backButton addTarget:self action:@selector(backButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:backButton];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, -20, SCREEN_WITDH, SCREEN_HEIGHT) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[CJCinviteOrderTopCell class] forCellReuseIdentifier:kinviteOrderTopCell];
    [tableView registerClass:[CJCInviteOrderListCell class] forCellReuseIdentifier:kInviteOrderListCell];
    
    self.invitTableView = tableView;
    [self.view addSubview:tableView];

    [self.view bringSubviewToFront:backButton];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    nextButton.frame = CGRectMake(kAdaptedValue(23), SCREEN_HEIGHT - 57, SCREEN_WITDH-kAdaptedValue(46), 44);
    
    nextButton.layer.cornerRadius = 4;
    nextButton.layer.masksToBounds = YES;
    
    [nextButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828"]] forState:0];
    [nextButton setBackgroundImage:[UIImage imageWithColor:[UIColor toUIColorByStr:@"282828" andAlpha:0.5]] forState:UIControlStateSelected];
    [nextButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF"] forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor toUIColorByStr:@"FFFFFF" andAlpha:0.5] forState:1];
    [nextButton setTitle:@"确定" forState:UIControlStateNormal];

    nextButton.userInteractionEnabled = NO;
    
    [[nextButton rac_signalForControlEvents:(UIControlEventTouchUpInside)] subscribeNext:^(__kindof UIControl * _Nullable x) {
        [self showWithLabelAnimation];
        NSString *sureURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/create"];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"giftId"] = giftModel.giftId;
        params[@"timeType"] = dateTimeStr;
        params[@"partnerId"] = self.infoModel.uid;
        params[@"lng"] = @(hotelLocation.coordinate.longitude);
        params[@"lat"] = @(hotelLocation.coordinate.latitude);
        params[@"hotelName"] = hotelName;
        params[@"hotelLevel"] = hotelStarStr;
        params[@"hotelAddress"] = selectPOI.address;
        
        [CJCHttpTool postWithUrl:sureURL params:params success:^(id responseObject) {
            NSNumber *rtNum = responseObject[@"rt"];
            NSLog(@"kkkkkk detail \n:%@",responseObject);
            if (rtNum.integerValue == 0) {
                NSDictionary *orderDict = responseObject[@"data"];
                sendOrderId = orderDict[@"orderId"];
                //            [self getGiftList];
                [self getGift];
            }else{
                [self hidHUD];
                [MBManager showBriefAlert:responseObject[@"msg"]];
            }
        } failure:^(NSError *error) {
        }];
    }];
    
    self.sureButton = nextButton;
    [self.view addSubview:nextButton];
    [self.view bringSubviewToFront:nextButton];
}

-(void)getGift{

     [self showWithLabelAnimation];
    
    NSString *giftURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/gift/list/my"];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:giftURL parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFormData:[giftId dataUsingEncoding:NSUTF8StringEncoding] name:@"giftId"];
        
        [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kUSERTOKEN] dataUsingEncoding:NSUTF8StringEncoding] name:@"token"];
        
        [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kUSERUID] dataUsingEncoding:NSUTF8StringEncoding] name:@"uid"];
        
        [formData appendPartWithFormData:[@"0.1" dataUsingEncoding:NSUTF8StringEncoding] name:@"v"];
        
        [formData appendPartWithFormData:[[CJCTools getDateTimeTOMilliSeconds] dataUsingEncoding:NSUTF8StringEncoding] name:@"t"];
        
        if ([kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE]) {
            
            [formData appendPartWithFormData:[[kUSERDEFAULT_STAND objectForKey:kREGISTERSOURCE] dataUsingEncoding:NSUTF8StringEncoding] name:@"f"];
        }else{
            
            [formData appendPartWithFormData:[@"测试" dataUsingEncoding:NSUTF8StringEncoding] name:@"f"];
        }
        
        [formData appendPartWithFormData:[[[UIDevice currentDevice] systemName] dataUsingEncoding:NSUTF8StringEncoding] name:@"osv"];
        
        [formData appendPartWithFormData:[[CJCTools iphoneType] dataUsingEncoding:NSUTF8StringEncoding] name:@"pinfo"];

    } error:nil];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
        
            NSLog(@"mmmm\n %@",responseObject);
            
            NSNumber *rtNum = responseObject[@"rt"];
            if (rtNum.integerValue == 0) {
                
                NSArray *tempArr = [NSArray modelArrayWithClass:[CJCInviteGiftModel class] json:responseObject[@"data"]];
                
                NSMutableArray *tempArrStore = [NSMutableArray array];
                
                for (CJCInviteGiftModel *mode in tempArr) {
                    
                    if (mode.status == 0) {
                        
                        [tempArrStore addObject:mode];
                    }
                }
                
                if (tempArrStore.count>0) {
                    
                    [self payOrderWithOrderID:sendOrderId];
                }else{
                    
                    [self hidHUD];
                    
                    [self getOrderDetail];
                }
                
            }else{
                
                [MBManager showBriefAlert:responseObject[@"msg"]];
            }
        }
        
    }];
    
    [uploadTask resume];
    
}



#pragma mark  用已有的礼物支付订单
-(void)payOrderWithOrderID:(NSString *)orderId{
    
    NSString *buyURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/pay/by_gift"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = orderId;
    
    [CJCHttpTool postWithUrl:buyURL params:params success:^(id responseObject) {
        
        NSNumber *rtNum = responseObject[@"rt"];
        
        [self hidHUD];
        
        if (rtNum.integerValue == 0) {
            
            EMMessage *message = [CJCMessageHandle CJC_HuanxinYaoyueMessageWithAmapPOI:selectPOI andOrderId:orderId andTo:self.infoModel.uid];
            
            [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *aMessage, EMError *aError) {
                
                if (aError == nil) {
                    
                    [MBManager showBriefAlert:@"邀约成功"];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
//                        [self.navigationController popViewControllerAnimated:YES];
                        
                        CJCSingleChatVC *chatController = [[CJCSingleChatVC alloc] init];
                        
                        chatController.conversationId = self.infoModel.emId;
                        chatController.location = self.location;
                        chatController.qunjuId = self.infoModel.uid;
                        chatController.infoModel = self.infoModel;
                        chatController.nickName = self.infoModel.nickname;
                        chatController.iconImageUrl = self.infoModel.imageUrl;
                        chatController.popType = CJCSingleChatPopTypePopToInfoVC;
                        
                        [self.navigationController pushViewController:chatController animated:YES];
                        
                    });
                    
                }else{
                    
                    [MBManager showBriefAlert:aError.errorDescription];
                }
                
            }];
            
        }else if (rtNum.integerValue == 1055){
            
            [self getOrderDetail];
            
        }else{
            
            [self hidHUD];
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
       
        
    }];
}

-(void)getOrderDetail{
    
    [MBManager showLoading];
    
    NSString *orderURL = [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/get"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = sendOrderId;
    
    [CJCHttpTool postWithUrl:orderURL params:params success:^(id responseObject) {
        
        [MBManager hideAlert];
        
        NSNumber *rtNum = responseObject[@"rt"];
        NSLog(@"nnnnn\n %@", responseObject);
        if (rtNum.integerValue == 0){
            
            orderModel = [CJCInviteOrderModel modelWithDictionary:responseObject[@"data"][@"order"]];
            [self gotoGiftBuyVC];
        }else{
            
            [MBManager showBriefAlert:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
        
    }];
}

-(void)gotoGiftBuyVC{

    CJCBuyInviteGiftVC *nextVC = [[CJCBuyInviteGiftVC alloc] init];
    
    nextVC.giftId = giftModel.giftId;
    nextVC.orderId = sendOrderId;
    nextVC.infoModel = self.infoModel;
    nextVC.selectPOI = selectPOI;
    nextVC.buyGiftVCType = CJCBuyInviteGiftVCTypePayType;
    nextVC.infoVC = self.infoVC;
    nextVC.location = self.location;
    nextVC.orderModel = orderModel;

    [self.navigationController pushViewController:nextVC animated:YES];
}

-(void)backButtonClickHandle{

    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
