//
//  CJCCancleOrderVC.m
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCancleOrderVC.h"
#import "CJCCommon.h"
#import "CJCCancleOrderTopCell.h"
#import "CJCCancleOrderSelectCell.h"
#import "CJCOprationButton.h"
#import "CJCMessageHandle.h"
#import "CJCInviteOrderModel.h"
#import <EaseUI.h>

#define kCancleOrderTopCell         @"CJCCancleOrderTopCell"
#define kCancleOrderSelectCell      @"CJCCancleOrderSelectCell"

@interface CJCCancleOrderVC ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSArray *dataArray;
    
    NSInteger selectIndex;
}

@property (nonatomic ,strong) CJCOprationButton *commitButton;

@end

@implementation CJCCancleOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataArray = @[@"请告诉我们取消原因",@"对方未出现",@"对方不是本人",@"照片与本人差距太大",@"体验与预期不符",@"我临时有事",@"酒托、饭托或其它欺诈行为",@"暴力、敲诈勒索等违法行为"];
    
    selectIndex = 0;
    
    [self setUpNaviView];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        
    }else{
    
        self.commitButton.canOpration = YES;
        
        if (selectIndex == 0) {
            
            CJCCancleOrderSelectCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            cell.isSelect = YES;
            
            selectIndex = indexPath.row;
        }else{
        
            if (selectIndex != indexPath.row) {
                
                CJCCancleOrderSelectCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                cell.isSelect = YES;
                
                CJCCancleOrderSelectCell *oldCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectIndex inSection:0]];
                
                oldCell.isSelect = NO;
                
                selectIndex = indexPath.row;
            }
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        return 83;
    }
    
    return 61;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        CJCCancleOrderTopCell *cell = [tableView dequeueReusableCellWithIdentifier:kCancleOrderTopCell];
        
        cell.title = dataArray[indexPath.row];
        
        return cell;
    }else{
        
        CJCCancleOrderSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:kCancleOrderSelectCell];
        
        cell.title = dataArray[indexPath.row];
        cell.isSelect = NO;
        
        return cell;
    }
}

-(void)setUpNaviView{
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setKeyBoardHid:YES];
    
    self.naviView.frame = CGRectMake(0, 0, SCREEN_WITDH, 64);
    
    self.backButton.size = CGSizeMake(kAdaptedValue(44), kAdaptedValue(44));
    self.backButton.x = kAdaptedValue(4);
    self.backButton.centerY = kNAVIVIEWCENTERY;
    [self.backButton setImage:kGetImage(@"profile_wollet_close") forState:UIControlStateNormal];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, SCREEN_WITDH, SCREEN_HEIGHT - 64 - 76) style:UITableViewStylePlain];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerClass:[CJCCancleOrderTopCell class] forCellReuseIdentifier:kCancleOrderTopCell];
    [tableView registerClass:[CJCCancleOrderSelectCell class] forCellReuseIdentifier:kCancleOrderSelectCell];
    
    [self.view addSubview:tableView];
    
    CJCOprationButton *button = [[CJCOprationButton alloc] init];
    
    [button setTitle:@"提交" forState:UIControlStateNormal];
    
    button.canOpration = NO;
    
    button.centerX = self.view.centerX;
    button.bottom = SCREEN_HEIGHT - 30;
    
    [button addTarget:self action:@selector(commitButtonClickHandle) forControlEvents:UIControlEventTouchUpInside];
    
    self.commitButton = button;
    [self.view addSubview:button];
}

//-(void)commitButtonClickHandle{
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(void)commitButtonClickHandle{

    [self showWithLabelAnimation];
    
    NSString *reason = dataArray[selectIndex];
    
    NSString *commitURL= [NSString stringWithFormat:@"%@%@",kROXMBaseURL,@"/order/save/cancel_reason"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"id"] = self.orderId;
    params[@"note"] = reason;
    
    [CJCHttpTool postWithUrl:commitURL params:params success:^(id responseObject) {
        
        [self hidHUD];
        
        NSNumber *rtNum = responseObject[@"rt"];
        if (rtNum.integerValue == 0) {
            
            [MBManager showGloomy:@"取消成功"];
            
            if (self.cancleHandle) {
                
                self.cancleHandle();
            }
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
        
            [MBManager showGloomy:responseObject[@"msg"]];
        }
        
    } failure:^(NSError *error) {
       
        
    }];
}

-(void)naviViewBackBtnDidClick{

//    [self commitButtonClickHandle];
    
    if (self.cancleHandle) {
        
        self.cancleHandle();
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
