//
//  CJCBuyInviteGiftVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/28.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

/*
  邀约时购买礼物的逻辑:先调用后台生成支付宝订单的接口  成功之后调用支付宝支付  支付成功之后调用购买礼物的接口  最后调用支付订单的接口
 */

#import "CommonVC.h"

typedef NS_ENUM(NSInteger,CJCBuyInviteGiftVCType){
    
    CJCBuyInviteGiftVCTypedefault               = 0,//present出来的 可以选择礼物
    CJCBuyInviteGiftVCTypePayType               = 1,//present出来的 不可以选择礼物
    CJCBuyInviteGiftVCTypePushType              = 2,//push出来的  可以选择礼物
};

typedef void(^popVCHandle)(void);

@class CJCInviteGiftModel,CJCpersonalInfoModel,AMapPOI,CJCPersonalInfoVC,CLLocation,CJCInviteOrderModel;

@interface CJCBuyInviteGiftVC : CommonVC

@property (nonatomic ,copy) popVCHandle popHandle;

@property (nonatomic ,assign) CJCBuyInviteGiftVCType buyGiftVCType;

@property (nonatomic ,copy) NSString *giftId;

@property (nonatomic ,copy) NSString *orderId;

@property (nonatomic ,strong) CJCpersonalInfoModel *infoModel;

@property (nonatomic ,strong) CJCInviteOrderModel *orderModel;

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,strong) AMapPOI *selectPOI;

@property (nonatomic ,strong) CJCPersonalInfoVC *infoVC;

@end
