//
//  CJCCommentDateVC.h
//  roxm
//
//  Created by 陈建才 on 2017/10/25.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"

typedef void(^commentSuccessHandle)(void);

@interface CJCCommentDateVC : CommonVC

@property (nonatomic ,copy) NSString *parnterID;

@property (nonatomic ,copy) NSString *orderID;

@property (nonatomic ,copy) commentSuccessHandle commentHandle;

-(void)reportSuccess;

@end
