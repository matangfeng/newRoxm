//
//  CJCinviteOrderImageCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCinviteOrderImageCell.h"
#import "CJCPhotoShowModel.h"
#import "CJCCommon.h"
#import "CJCSectorCycleView.h"

@interface CJCinviteOrderImageCell ()

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,strong) UIImageView *playView;

@property (nonatomic ,strong) CJCSectorCycleView *cycleView;

@end

@implementation CJCinviteOrderImageCell

-(instancetype)initWithFrame:(CGRect)frame{

    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UIImageView *showView = [[UIImageView alloc] init];
    
    showView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    showView.contentMode = UIViewContentModeScaleAspectFill;
    showView.layer.masksToBounds = YES;
    
    self.showImageView = showView;
    [self.contentView addSubview:showView];
}

-(void)setShowModel:(CJCPhotoShowModel *)showModel{

    if (showModel.type == 20) {
        
        self.playView.hidden = YES;
        
        __weak __typeof(self) weakSelf = self;
        [self.showImageView setImageWithURL:[NSURL URLWithString:showModel.url] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            CGFloat imageWidth = SCREEN_WITDH;
            CGFloat imageHeight = image.size.height/image.size.width*imageWidth;
            
            weakSelf.showImageView.size = CGSizeMake(imageWidth, imageHeight);
            weakSelf.showImageView.center = weakSelf.contentView.center;
            
        }];
    
    }else{
    
        NSString *previewImage = [NSString stringWithFormat:@"%@%@",showModel.url,@"?vframe/jpg/offset/1/w/480/h/360"];
        
        self.playView.hidden = NO;
        
        __weak __typeof(self) weakSelf = self;
        [self.showImageView setImageWithURL:[NSURL URLWithString:previewImage] placeholder:nil options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
            
            CGFloat imageWidth = SCREEN_WITDH;
            CGFloat imageHeight = image.size.height/image.size.width*imageWidth;
            
            weakSelf.showImageView.size = CGSizeMake(imageWidth, imageHeight);
            weakSelf.showImageView.center = weakSelf.contentView.center;
            
        }];
    }
    
    self.showImageView.frame = self.contentView.bounds;
}

-(CJCSectorCycleView *)cycleView{
    
    if (_cycleView == nil) {
        
        CJCSectorCycleView *view = [[CJCSectorCycleView alloc] init];
        
        view.backgroundColor = [UIColor whiteColor];
        
        view.size = CGSizeMake(kAdaptedValue(100), kAdaptedValue(100));
        view.center = self.contentView.center;
        
        [self.contentView addSubview:view];
        
        _cycleView = view;
    }
    return _cycleView;
}

-(UIImageView *)playView{
    
    if (_playView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_videoplay")];
        
        imageView.size = CGSizeMake(100, 100);
        imageView.center = self.contentView.center;
        
        [self.contentView addSubview:imageView];
        
        _playView = imageView;
    }
    return _playView;
}

@end
