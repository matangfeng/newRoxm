//
//  CJCCancleOrderTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/29.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCCancleOrderTopCell.h"
#import "CJCCommon.h"

@interface CJCCancleOrderTopCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@end

@implementation CJCCancleOrderTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{
    
    UILabel *label = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:28 color:[UIColor toUIColorByStr:@"222222"]];
    
    label.frame = CGRectMake(kAdaptedValue(20), 31, 300, kAdaptedValue(28));
    
    self.titleLabel = label;
    
    [self.contentView addSubview:label];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(kAdaptedValue(23), 82, SCREEN_WITDH-kAdaptedValue(46), OnePXLineHeight);
    
    [self.contentView addSubview:lineView];
}

-(void)setTitle:(NSString *)title{

    self.titleLabel.text = title;
}

@end
