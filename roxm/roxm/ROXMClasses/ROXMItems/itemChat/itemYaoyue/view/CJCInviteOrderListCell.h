//
//  CJCInviteOrderListCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,CJCInviteOrderListCellType){
    
    CJCInviteOrderListCellTypeDefault              = 0,//默认样式:箭头 title detaile
    CJCInviteOrderListCellTypeHigher               = 1,//行高为80的样式
    CJCInviteOrderListCellTypeGift                 = 2,//带礼物的样式
    CJCInviteOrderListCellTypeNoArrow              = 3,//没有箭头的样式
};

@interface CJCInviteOrderListCell : UITableViewCell

@property (nonatomic ,assign) CJCInviteOrderListCellType cellType;

@property (nonatomic ,copy) NSString *title;

@property (nonatomic ,copy) NSString *detail;

@property (nonatomic ,copy) NSString *imagePath;

@property (nonatomic ,assign) BOOL isShow;

@end
