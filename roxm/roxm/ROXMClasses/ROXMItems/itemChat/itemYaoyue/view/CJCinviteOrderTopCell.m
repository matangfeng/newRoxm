//
//  CJCinviteOrderTopCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCinviteOrderTopCell.h"
#import "CJCCommon.h"
#import "CJCinviteOrderImageCell.h"
#import "CJCPhotoShowModel.h"
#import "CJCpersonalInfoModel.h"
#import "SDCycleScrollView.h"

#define kinviteOrderImageCell   @"CJCinviteOrderImageCell"

@interface CJCinviteOrderTopCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic ,strong) UICollectionView *imagesView;

@property (nonatomic ,strong) UIView *cycleView;

@property (nonatomic ,strong)  UILabel *nameLabel;

@property (nonatomic ,strong) UIView *sexAndAgeView;

@property (nonatomic ,strong) UIImageView *sexImageView;

@property (nonatomic ,strong) UILabel *ageLabel;

@property (nonatomic ,strong) UILabel *professionLabel;

@property (nonatomic ,strong) UILabel *distanceTimeLabel;

@property (nonatomic ,strong) UIImageView *DiamondImageView;

@property (nonatomic ,strong) UILabel *ObservantRatio;

@end

@implementation CJCinviteOrderTopCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
//        [self setUpUI];
        [self setUpCycleView];
    }
    return self;
}

-(void)setUpCycleView{

    UIView *containView = [[UIView alloc] init];
    
    containView.frame = CGRectMake(0, 0, SCREEN_WITDH, SCREEN_WITDH);
    
    self.cycleView = containView;
    [self.contentView addSubview:containView];
    
    [self setUpPersonInfoView];
}

-(void)setUpUI{

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    flowLayout.itemSize = CGSizeMake(SCREEN_WITDH, SCREEN_WITDH);
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *imageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WITDH, SCREEN_WITDH) collectionViewLayout:flowLayout];
    
    imageCollectionView.backgroundColor = [UIColor whiteColor];
    
    imageCollectionView.dataSource = self;
    imageCollectionView.delegate = self;
    
    imageCollectionView.alwaysBounceHorizontal = YES;
    
    imageCollectionView.pagingEnabled = YES;
    
    imageCollectionView.showsHorizontalScrollIndicator = NO;
    
    [imageCollectionView registerClass:[CJCinviteOrderImageCell class] forCellWithReuseIdentifier:kinviteOrderImageCell];
    
    self.imagesView = imageCollectionView;
    [self.contentView addSubview:imageCollectionView];
    
    [self setUpPersonInfoView];
}

-(void)setUpPersonInfoView{

    UILabel *nameLabel = [UIView getYYLabelWithStr:@"名字" fontName:kFONTNAMEREGULAR size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    nameLabel.frame = CGRectMake(15, 19, 30, kAdaptedValue(16));
    
    self.nameLabel = nameLabel;
    [self.contentView addSubview:nameLabel];
    
    UIView *sexAndAgeView = [[UIView alloc] init];
    
    sexAndAgeView.frame = CGRectMake(nameLabel.right+kAdaptedValue(6), kAdaptedValue(19.5), kAdaptedValue(32), kAdaptedValue(14));
    
    sexAndAgeView.backgroundColor = [UIColor toUIColorByStr:@"EA78AA"];
    
    sexAndAgeView.layer.cornerRadius = kAdaptedValue(2);
    sexAndAgeView.layer.masksToBounds = YES;
    
    self.sexAndAgeView = sexAndAgeView;
    [self.contentView addSubview:sexAndAgeView];
    
    UIImageView *sexImageView = [[UIImageView alloc] initWithImage:kGetImage(@"index_female")];
    
    sexImageView.frame = CGRectMake(kAdaptedValue(3), 0, kAdaptedValue(9), kAdaptedValue(9));
    
    sexImageView.centerY = kAdaptedValue(7);
    
    self.sexImageView = sexImageView;
    [sexAndAgeView addSubview:sexImageView];
    
    UILabel *ageLabel = [UIView getYYLabelWithStr:@"20" fontName:kFONTNAMEREGULAR size:9.5 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    ageLabel.textAlignment = NSTextAlignmentLeft;
    
    ageLabel.frame = CGRectMake(sexImageView.right+kAdaptedValue(2), 0, sexAndAgeView.width-sexImageView.right-kAdaptedValue(2), 12);
    
    ageLabel.centerY = kAdaptedValue(7);
    
    self.ageLabel = ageLabel;
    [sexAndAgeView addSubview:ageLabel];
    
    UILabel *professionLabel = [UIView getYYLabelWithStr:@"职业" fontName:kFONTNAMEREGULAR size:12 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    professionLabel.frame = CGRectMake(kAdaptedValue(15), nameLabel.bottom+kAdaptedValue(6), kAdaptedValue(48), kAdaptedValue(13));
    
    self.professionLabel = professionLabel;
    [self.contentView addSubview:professionLabel];
    
    UILabel *distanceTimeLabel = [UIView getYYLabelWithStr:@"1.1km·15分钟前" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    distanceTimeLabel.frame = CGRectMake(kAdaptedValue(15), kAdaptedValue(84.5), 76, kAdaptedValue(11));
    
    self.distanceTimeLabel = distanceTimeLabel;
    [self.contentView addSubview:distanceTimeLabel];
    
    UIImageView *zuanshiView = [[UIImageView alloc] initWithImage:kGetImage(@"index_zuanshi_tanchuang")];
    
    zuanshiView.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(54), kAdaptedValue(29), kAdaptedValue(16), kAdaptedValue(14.5));
    
    self.DiamondImageView = zuanshiView;
    [self.contentView addSubview:zuanshiView];
    
    UILabel *observantLabel = [UIView getYYLabelWithStr:@"50%" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    observantLabel.textAlignment = NSTextAlignmentRight;
    
    observantLabel.frame = CGRectMake(zuanshiView.right, kAdaptedValue(29), SCREEN_WITDH-zuanshiView.right-kAdaptedValue(11), kAdaptedValue(11));
    
    observantLabel.centerY = zuanshiView.centerY;
    
    self.ObservantRatio = observantLabel;
    [self.contentView addSubview:observantLabel];
}

-(void)setInfoModel:(CJCpersonalInfoModel *)infoModel{
    
    _infoModel = infoModel;
    
    CGFloat nameLabelWidth = [CJCTools getShortStringLength:infoModel.nickname withFont:[UIFont accodingVersionGetFont_regularWithSize:15]];
    
    self.nameLabel.width = nameLabelWidth;
    
    self.nameLabel.text = infoModel.nickname;
    
    self.sexAndAgeView.left = self.nameLabel.right+kAdaptedValue(6);
    
    self.ageLabel.text = [self getAgeStr];
    
    NSString *career;
    if([CJCTools isBlankString:infoModel.career]){
        
        career = @"待业";
    }else{
        
        career = infoModel.career;
    }
    
    NSInteger height,weight;
    
    if (infoModel.height == 0) {
        
        height = 170;
    }else{
        
        height = infoModel.height;
    }
    
    if (infoModel.weight == 0) {
        
        weight = 65;
    }else{
        
        weight = infoModel.weight;
    }
    
    NSString *heightStr = [NSString stringWithFormat:@"%ldcm·%ldkg",height,weight];
    
    CGFloat distanceWidth = [CJCTools getShortStringLength:@"1.1km·15分钟前" withFont:[UIFont accodingVersionGetFont_regularWithSize:11]];
    
    self.distanceTimeLabel.width = distanceWidth;
    self.distanceTimeLabel.bottom = SCREEN_WITDH - 12;
    self.distanceTimeLabel.centerX = SCREEN_WITDH/2;
    
    NSString *newStr = [NSString stringWithFormat:@"%@  %@",career,heightStr];
    
    CGFloat careerLabelWidth = [CJCTools getShortStringLength:newStr withFont:[UIFont accodingVersionGetFont_regularWithSize:12]];
    
    self.professionLabel.text = newStr;
    self.professionLabel.width = careerLabelWidth;
    
    self.professionLabel.bottom = self.distanceTimeLabel.top - 9;
    self.professionLabel.centerX = SCREEN_WITDH/2;
    
    self.DiamondImageView.bottom = SCREEN_WITDH - 15;
    self.ObservantRatio.centerY = self.DiamondImageView.centerY;
    
    self.nameLabel.bottom = self.professionLabel.top - 8;
    self.nameLabel.centerX = SCREEN_WITDH/2-kAdaptedValue(19);
    self.sexAndAgeView.centerY = self.nameLabel.centerY;
    self.sexAndAgeView.left = self.nameLabel.right+kAdaptedValue(6);
}

-(NSString *)getAgeStr{
    
    NSString *birthStr;
    
    if ([CJCTools isBlankString:self.infoModel.birthDay]) {
        
        birthStr = @"1997";
    }else{
        
        birthStr = [self.infoModel.birthDay substringToIndex:4];
    }
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY"];
    NSString *DateTime = [formatter stringFromDate:date];
    
    NSString *nameAgeStr = [NSString stringWithFormat:@"%ld",DateTime.integerValue-birthStr.integerValue];
    
    return nameAgeStr;
}

-(void)setImagesArray:(NSArray *)imagesArray{

//    _imagesArray = imagesArray;
//    
//    [self.imagesView reloadData];
    
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WITDH, SCREEN_WITDH) shouldInfiniteLoop:YES imageNamesGroup:imagesArray];
    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    [self.cycleView addSubview:cycleScrollView];
    cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    //         --- 轮播时间间隔，默认1.0秒，可自定义
    cycleScrollView.autoScrollTimeInterval = 3.0;
}

#pragma  mark ======collectionView 的delegate 和DataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.imagesArray.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CJCinviteOrderImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kinviteOrderImageCell forIndexPath:indexPath];
    
    CJCPhotoShowModel *model = self.imagesArray[indexPath.row];
    
    cell.showModel = model;
    
    return cell;
}

@end
