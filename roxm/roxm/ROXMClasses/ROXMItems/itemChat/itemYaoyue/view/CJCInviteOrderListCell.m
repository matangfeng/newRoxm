//
//  CJCInviteOrderListCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCInviteOrderListCell.h"
#import "CJCCommon.h"

@interface CJCInviteOrderListCell ()

@property (nonatomic ,strong) UILabel *titleLabel;

@property (nonatomic ,strong) UILabel *detailLabel;

@property (nonatomic ,strong) UIImageView *arrowView;

@property (nonatomic ,strong) UIImageView *giftImageView;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation CJCInviteOrderListCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    return self;
}

-(void)setUpUI{

    UILabel *titleLabel = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMELIGHT size:17 color:[UIColor toUIColorByStr:@"484848"]];
    
    titleLabel.frame = CGRectMake(23, 25, 68, 15);
    
    self.titleLabel = titleLabel;
    [self.contentView addSubview:titleLabel];
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(23, 63, SCREEN_WITDH-46, OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setTitle:(NSString *)title{

    self.titleLabel.text = title;
    
    CGFloat width = [CJCTools getShortStringLength:title withFont:[UIFont accodingVersionGetFont_lightWithSize:17]];
    
    self.titleLabel.width = width;
    
    switch (self.cellType) {
        case CJCInviteOrderListCellTypeDefault:{
            
            self.titleLabel.centerY = 32;
            self.arrowView.centerY = self.titleLabel.centerY;
            self.arrowView.hidden = NO;
            self.detailLabel.hidden = NO;
            self.giftImageView.hidden = YES;
            self.lineView.y = 63;
        }
            break;
            
        case CJCInviteOrderListCellTypeHigher:{
            
            self.titleLabel.bottom = 55;
            self.arrowView.centerY = self.titleLabel.centerY;
            self.arrowView.hidden = NO;
            self.detailLabel.hidden = NO;
            self.giftImageView.hidden = YES;
            self.lineView.y = 79;
        }
            break;
            
        case CJCInviteOrderListCellTypeGift:{
            
            self.titleLabel.centerY = 32;
            self.arrowView.centerY = self.titleLabel.centerY;
            self.arrowView.hidden = YES;
            self.detailLabel.hidden = NO;
            self.giftImageView.hidden = NO;
            
            self.giftImageView.centerY = self.titleLabel.centerY;
            self.giftImageView.right = SCREEN_WITDH - 106;
            
            self.lineView.y = 63;
        }
            break;
            
        case CJCInviteOrderListCellTypeNoArrow:{
            
            self.titleLabel.centerY = 32;
            self.arrowView.hidden = YES;
            self.detailLabel.hidden = NO;
            self.giftImageView.hidden = YES;
            self.lineView.y = 63;
        }
            break;
            
        default:
            break;
    }
}

-(void)setDetail:(NSString *)detail{

    self.detailLabel.text = detail;
    
    CGFloat width = [CJCTools getShortStringLength:detail withFont:[UIFont accodingVersionGetFont_regularWithSize:17]];
    
    if (width > SCREEN_WITDH-150) {
        width = SCREEN_WITDH - 150;
    }
    
    self.detailLabel.size = CGSizeMake(width, 19);
    
    switch (self.cellType) {
        case CJCInviteOrderListCellTypeDefault:{
            
            self.detailLabel.right = self.arrowView.left - 5;
            
        }
            break;
            
        case CJCInviteOrderListCellTypeHigher:{
            
            self.detailLabel.right = self.arrowView.left - 5;
            
        }
            break;
            
        case CJCInviteOrderListCellTypeGift:{
            
            self.detailLabel.right = SCREEN_WITDH - 23;
            
            
        }
            break;
            
        case CJCInviteOrderListCellTypeNoArrow:{
            
            self.detailLabel.right = SCREEN_WITDH - 23;
        }
            break;
            
        default:
            break;
    }
    
    self.detailLabel.centerY = self.titleLabel.centerY;
}

-(void)setImagePath:(NSString *)imagePath{

    self.giftImageView.right = self.detailLabel.left - 15;
    
    [self.giftImageView setImageURL:[NSURL URLWithString:imagePath]];
}

-(void)setIsShow:(BOOL)isShow{
    
    self.titleLabel.hidden = !isShow;
    self.arrowView.hidden = !isShow;
    self.lineView.hidden = !isShow;
}

-(UIImageView *)arrowView{

    if (_arrowView == nil) {
        
        UIImageView *arrowView = [[UIImageView alloc] initWithImage:kGetImage(@"profile_icon_arrowright")];
        
        arrowView.frame = CGRectMake(334, 22, 22, 22);
        arrowView.centerY = 32;
        arrowView.right = SCREEN_WITDH - 19;
        
        [self.contentView addSubview:arrowView];
        
        _arrowView = arrowView;
    }
    return _arrowView;
}

-(UILabel *)detailLabel{

    if (_detailLabel == nil) {
        
        UILabel *label = [UIView getSystemLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:17 color:[UIColor toUIColorByStr:@"565656"]];
        
        label.frame = CGRectZero;
        
        [self.contentView addSubview:label];
        
        _detailLabel = label;
    }
    return _detailLabel;
}

-(UIImageView *)giftImageView{

    if (_giftImageView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] init];
        
        imageView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        imageView.size = CGSizeMake(42, 42);
        
        [self.contentView addSubview:imageView];
        
        _giftImageView = imageView;
    }
    return _giftImageView;
}

@end
