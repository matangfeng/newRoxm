//
//  CJCinviteOrderImageCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCPhotoShowModel;

@interface CJCinviteOrderImageCell : UICollectionViewCell

@property (nonatomic ,strong) CJCPhotoShowModel *showModel;

@end
