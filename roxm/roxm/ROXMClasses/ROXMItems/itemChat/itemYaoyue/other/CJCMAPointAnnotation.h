//
//  CJCMAPointAnnotation.h
//  roxm
//
//  Created by 陈建才 on 2017/10/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface CJCMAPointAnnotation : MAPointAnnotation

@property (nonatomic ,copy) NSString *imageName;

@property (nonatomic ,copy) NSString *bottomImageName;

@end
