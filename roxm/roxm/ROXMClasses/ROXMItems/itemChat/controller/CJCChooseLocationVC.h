//
//  CJCChooseLocationVC.h
//  roxm
//
//  Created by 陈建才 on 2017/9/9.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CommonVC.h"
#import <AMapSearchKit/AMapSearchKit.h>

typedef NS_ENUM(NSInteger,CJCChooseLocationType){
    
    CJCChooseLocationTypeChat                 = 0,//聊天类型的地图  搜索无限制 选择位置无限制
    CJCChooseLocationTypeInvite               = 1,//邀约类型的地图  搜索只能是酒店 选择位置只能选择酒店
};

@class AMapPOI,CLLocation;

typedef void(^CJCChooseLocationHandle)(AMapPOI *POI,NSString *imagePath,double distance);

typedef void(^CJCLocationHandle)(CLLocationCoordinate2D returnLocation,NSString *address,NSString *imagePath,double   distance);

@interface CJCChooseLocationVC : CommonVC

@property (nonatomic ,strong) CLLocation *location;

@property (nonatomic ,assign) CJCChooseLocationType locationType;

@property (nonatomic ,copy) CJCChooseLocationHandle locationHandle;

@property (nonatomic ,copy) CJCLocationHandle addressLocationHandle;

@end
