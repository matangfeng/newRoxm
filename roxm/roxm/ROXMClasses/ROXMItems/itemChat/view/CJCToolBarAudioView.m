//
//  CJCToolBarAudioView.m
//  roxm
//
//  Created by 陈建才 on 2017/9/8.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCToolBarAudioView.h"
#import "CJCCommon.h"

@interface CJCToolBarAudioView ()

@end

@implementation CJCToolBarAudioView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setUpUI];
    
    }
    return self;
}

-(void)setUpUI{

    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, 0, SCREEN_WITDH, OnePXLineHeight);
    
    [self addSubview:lineView];
    
    UIImageView *bottomImageView = [[UIImageView alloc] initWithImage:kGetImage(@"message_speak_outside")];
    
    bottomImageView.frame = CGRectMake(147.5, 40.5, 80, 80);
    bottomImageView.centerX = SCREEN_WITDH/2;
    
    bottomImageView.userInteractionEnabled = YES;
    
    [self addSubview:bottomImageView];
    
    UIButton *audioButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [audioButton setImage:kGetImage(@"message_speak_inside") forState:UIControlStateNormal];
    
    
    audioButton.frame = CGRectMake(154, 47, 67, 67);
    audioButton.center = bottomImageView.center;
    
    self.MicrophoneButton = audioButton;
    [self addSubview:audioButton];
}

@end
