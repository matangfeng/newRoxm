//
//  CJCLocationCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCLocationCell.h"
#import "CJCCommon.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "CJCCustomAmapPOI.h"

@interface CJCLocationCell ()

@property (nonatomic ,strong) UILabel *hotelNameLable;

@property (nonatomic ,strong) UIButton *selectButton;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation CJCLocationCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setUpUI];
        
    }
    return self;
}

-(void)setUpUI{

    UILabel *nameLable = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEMEDIUM size:14 color:[UIColor toUIColorByStr:@"333333"]];
    
    self.hotelNameLable = nameLable;
    [self.contentView addSubview:nameLable];
    
    UIButton *selectView = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [selectView setImage:kGetImage(@"all_check_smallblue") forState:UIControlStateSelected];
    
    selectView.frame = CGRectMake(337, 27, kAdaptedValue(10), kAdaptedValue(8));
    
    self.selectButton = selectView;
    [self.contentView addSubview:selectView];
    
    self.selectButton.centerY = kAdaptedValue(27);
    
    UIView *lineView = [UIView getLineView];
    
    lineView.frame = CGRectMake(0, kAdaptedValue(53), SCREEN_WITDH, OnePXLineHeight);
    
    self.lineView = lineView;
    [self.contentView addSubview:lineView];
}

-(void)setCustomAmapPOI:(CJCCustomAmapPOI *)customAmapPOI{

    AMapReGeocode *reGeocode = customAmapPOI.reGeocode;
    
    AMapPOI *poi = reGeocode.pois[0];
    
    NSString *locationName = [NSString stringWithFormat:@"%@%@%@",reGeocode.addressComponent.district,reGeocode.addressComponent.streetNumber.street,poi.name];
    
    self.hotelNameLable.text = locationName;
    
    self.hotelNameLable.frame = CGRectMake(kAdaptedValue(23), kAdaptedValue(10.5), SCREEN_WITDH-kAdaptedValue(80) , 15);
    
    self.hotelNameLable.centerY = kAdaptedValue(27);
    
    if (customAmapPOI.isSelect) {
        
        self.selectButton.selected = YES;
    }else{
        
        self.selectButton.selected = NO;
    }
}

@end
