//
//  CJCEmojiView.h
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCEmojiViewDelegate <NSObject>

-(void)emojiItemDidClick:(NSString *)emojiStr;

-(void)deleteItemDidClick;

-(void)sendItemDidClick;

@end

@interface CJCEmojiView : UIView

@property (nonatomic ,weak) id<CJCEmojiViewDelegate> delegate;

@end
