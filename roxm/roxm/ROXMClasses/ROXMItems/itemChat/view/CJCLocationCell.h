//
//  CJCLocationCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/21.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CJCCustomAmapPOI;

@interface CJCLocationCell : UITableViewCell

@property (nonatomic ,strong) CJCCustomAmapPOI *customAmapPOI;

@end
