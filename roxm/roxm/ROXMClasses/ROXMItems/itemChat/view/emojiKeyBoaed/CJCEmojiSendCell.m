//
//  CJCEmojiSendCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCEmojiSendCell.h"
#import "CJCCommon.h"

@interface CJCEmojiSendCell ()

@property (nonatomic ,strong) UIButton *itemButton;

@end

@implementation CJCEmojiSendCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self setUpUI];
    }
    return self;
}

-(void)emojiButtonClick{
    
    if ([self.delegate respondsToSelector:@selector(sendButtonClick)]) {
        
        [self.delegate sendButtonClick];
    }
}

-(void)setUpUI{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    self.itemButton = button;
    [self.contentView addSubview:button];
    
    [button addTarget:self action:@selector(emojiButtonClick) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setImageName:(NSString *)imageName{
    
    self.itemButton.frame = self.contentView.bounds;
    [self.itemButton setImage:kGetImage(imageName) forState:UIControlStateNormal];
}

@end
