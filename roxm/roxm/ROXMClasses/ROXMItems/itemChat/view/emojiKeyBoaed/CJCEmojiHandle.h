//
//  CJCEmojiHandle.h
//  roxm
//
//  Created by 陈建才 on 2017/9/16.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJCEmojiHandle : NSObject

+(BOOL)stringContainsEmoji:(NSString *)string;

@end
