//
//  CJCEmojiCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/15.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CJCEmojiCellDelegate <NSObject>

-(void)emojiButtonClick:(NSString *)emojiStr;

@end

@interface CJCEmojiCell : UICollectionViewCell

@property (nonatomic ,copy) NSString *emojiStr;

@property (nonatomic ,copy) NSString *imageName;

@property (nonatomic ,weak) id<CJCEmojiCellDelegate> delegate;

@end
