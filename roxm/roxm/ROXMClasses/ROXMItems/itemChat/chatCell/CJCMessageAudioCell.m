//
//  CJCMessageAudioCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageAudioCell.h"
#import "CJCCommon.h"

@interface CJCMessageAudioCell ()

@property (nonatomic ,strong) UIImageView *voiceImageView;

@property (nonatomic ,strong) UILabel *voiceLenghLabel;

@property (nonatomic ,strong) UIView *clickView;

@end

@implementation CJCMessageAudioCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UIImageView *showView = [[UIImageView alloc] init];
    
    showView.image = kGetImage(@"message_speak_qipao");
    
    self.voiceImageView = showView;
    [self.messageContainView insertSubview:showView aboveSubview:self.bubbleImageView];
    
    UILabel *lengthLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:14 color:[UIColor toUIColorByStr:@"FFFFFF"]];
    
    self.voiceLenghLabel = lengthLabel;
    [self.messageContainView insertSubview:lengthLabel aboveSubview:self.bubbleImageView];
    
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];
    
    self.voiceImageView.frame = CGRectMake(kAdaptedValue(12), kAdaptedValue(12), kAdaptedValue(19.5), kAdaptedValue(15));
    
    self.voiceImageView.centerY = 22.5;
    
    NSString *voiceLength = [NSString stringWithFormat:@"%.0f\"",messageModel.mediaDuration];
    
    CGFloat labelWidth = [CJCTools getShortStringLength:voiceLength withFont:[UIFont accodingVersionGetFont_regularWithSize:14]];
    
    self.voiceLenghLabel.size = CGSizeMake(labelWidth, 20);
    self.voiceLenghLabel.x = self.voiceImageView.right+kAdaptedValue(18);
    self.voiceLenghLabel.centerY = 22.5;
    self.voiceLenghLabel.text = voiceLength;
    
}


@end
