//
//  CJCMessageTextCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/13.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageTextCell.h"
#import "CJCCommon.h"
#import "CJCChatMessageHandle.h"
#import "UIFont+AccodingVersion.h"

@interface CJCMessageTextCell ()

@property (nonatomic ,strong) UILabel *messageTextLabel;

@property (nonatomic ,strong) id<IMessageModel> messageModel;

@end

@implementation CJCMessageTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    UILabel *messageLabel = [[UILabel alloc] init];
    
    messageLabel.font = [UIFont accodingVersionGetFont_lightWithSize:15];
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.numberOfLines = 0;
    
    self.messageTextLabel = messageLabel;
    [self.messageContainView insertSubview:messageLabel aboveSubview:self.bubbleImageView];
    
//    YYLabel *messageTextLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:15 color:[UIColor toUIColorByStr:@"FFFFFF"]];
//    
//    messageTextLabel.numberOfLines = 0;
//    
//    messageTextLabel.preferredMaxLayoutWidth = SCREEN_WITDH-kAdaptedValue(130);
//    
//    self.messageTextLabel = messageTextLabel;
//    [self.messageContainView insertSubview:messageTextLabel aboveSubview:self.bubbleImageView];
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{

    _messageModel = messageModel;
    
    [super updateUIWithModel:messageModel];
    
    CGSize size = [CJCChatMessageHandle sizeWithString:messageModel.text font:[UIFont accodingVersionGetFont_lightWithSize:15]];
    
    self.messageTextLabel.size = CGSizeMake(size.width, size.height+16);
    self.messageTextLabel.origin = CGPointMake(8, 0);
    
    if (messageModel.isSender) {
        
        self.messageTextLabel.textColor = [UIColor toUIColorByStr:@"FFFFFF"];
        
    }else{
        
        self.messageTextLabel.textColor = [UIColor toUIColorByStr:@"333333"];
    }
    
    self.messageTextLabel.text = messageModel.text;
//    self.messageTextLabel.attributedText = str;
}

@end
