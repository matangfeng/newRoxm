//
//  CJCMessageBaseCell.h
//  roxm
//
//  Created by 陈建才 on 2017/9/18.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EaseUI.h>

@protocol CJCMessageBaseCellDelegate <NSObject>

-(void)iconImageViewClickHandle:(id<IMessageModel>)messageModel;

-(void)resendMessageHandle:(id<IMessageModel>)messageModel;

-(void)messageClickHandle:(id<IMessageModel>)messageModel;

@end

@interface CJCMessageBaseCell : UITableViewCell

@property (nonatomic ,strong) UIView *messageContainView;

@property (nonatomic ,strong) UIImageView *bubbleImageView;

@property (nonatomic ,weak) id<CJCMessageBaseCellDelegate> delegate;

-(void)updateUIWithModel:(id<IMessageModel>) messageModel;

@end
