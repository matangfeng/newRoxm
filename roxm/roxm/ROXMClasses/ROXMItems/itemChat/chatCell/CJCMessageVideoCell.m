//
//  CJCMessageVideoCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageVideoCell.h"
#import "CJCCommon.h"

@interface CJCMessageVideoCell ()

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,strong) UIView *videoPlayView;

@property (nonatomic ,strong) UILabel *videoLengthLabel;

@end

@implementation CJCMessageVideoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}

-(void)setUpUI{
    
    self.bubbleImageView.hidden = YES;
    
    UIImageView *showView = [[UIImageView alloc] init];
    
    self.showImageView = showView;
    [self.messageContainView insertSubview:showView aboveSubview:self.bubbleImageView];
    
    UIView *playView = [[UIView alloc] init];
    
    playView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.64];
    
    self.videoPlayView = playView;
    [self.messageContainView insertSubview:playView aboveSubview:self.bubbleImageView];
    
    UILabel *timeLengthLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:15 color:[UIColor whiteColor]];
    
    self.videoLengthLabel = timeLengthLabel;
    [playView addSubview:timeLengthLabel];
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];
    
    UIImage *image = messageModel.isSender ? messageModel.image : messageModel.thumbnailImage;
    if (!image) {
        image = messageModel.image;
        if (!image) {
            [self.showImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.fileURLPath] placeholderImage:[UIImage imageNamed:messageModel.failImageName]];
        } else {
            self.showImageView.image = image;
        }
    } else {
        self.showImageView.image = image;
    }
    
    self.showImageView.size = CGSizeMake(100, 100);
    self.showImageView.origin = CGPointMake(0, 0);
    
    self.videoPlayView.frame = self.showImageView.frame;
    
    self.videoLengthLabel.text = [CJCTools getNewTimeFromDurationSecond:messageModel.mediaDuration];
    
    self.videoLengthLabel.frame = CGRectMake(6, self.videoPlayView.height - 20, self.videoPlayView.width, 17);
}

@end
