//
//  CJCChatMessageHandle.m
//  roxm
//
//  Created by 陈建才 on 2017/9/13.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCChatMessageHandle.h"
#import <UIKit/UIKit.h>
#import "CJCCommon.h"
#import "EaseEmoji.h"
#import "CJCInviteOrderModel.h"

@implementation CJCChatMessageHandle

+(CGSize)CJC_getScaleImageSize:(CGSize)imageSize{

    if (imageSize.height == 0) {
        
        return CGSizeMake(100, 100);
    }else if (imageSize.width == 0){
    
        return CGSizeMake(100, 100);
    }
    
    CGFloat cjc_imageHeight = 100.0;
    
    CGFloat cjc_imageWidth = cjc_imageHeight*imageSize.width/imageSize.height;
    
    return CGSizeMake(cjc_imageWidth, cjc_imageHeight);
}

+(CGSize)sizeWithString:(NSString *)string font:(UIFont *)font {
    
    CGSize stringSize = CGSizeZero;
    CGFloat maxTextWidth = SCREEN_WITDH - kAdaptedValue(130);
    
    NSDictionary *attributes = @{NSFontAttributeName:font};
    NSInteger options = NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin;
    CGRect stringRect = [string boundingRectWithSize:CGSizeMake(maxTextWidth, CGFLOAT_MAX) options:options attributes:attributes context:NULL];
    stringSize = CGSizeMake(stringRect.size.width, stringRect.size.height);
    
    return stringSize;
}

+(UIImage *)getLashenImage:(NSString *)imageName{
    
    UIImage *image = kGetImage(imageName);
    
    // 左端盖宽度
    CGFloat leftCapWidth = image.size.width * 0.5f;
    // 顶端盖高度
    CGFloat topCapHeight = image.size.height * 0.5f;
    // 重新赋值
    image = [image stretchableImageWithLeftCapWidth:leftCapWidth topCapHeight:topCapHeight];
    
    return image;
}

+(NSString *)getTitleFromOrderStatus:(id<IMessageModel>)messageModel{

    NSDictionary *tempDict = messageModel.message.ext;
    
    NSNumber *orderStatusNum = tempDict[kHXYAOYUEORDERSTATUS];
    
    switch (orderStatusNum.integerValue) {
        case 10:{
            //已支付 待接收  发送的是邀约的cell
        
        }
            break;
            
        case 20:{
            //已接受  待到达
            return @"我接受了您的邀约";
        }
            break;
            
        case 30:{
            //已到达  待开始
            return @"我已经到达";
        }
            break;
            
        case 40:{
            //已开始  待结束
            return @"约会已经开始";
        }
            break;
            
        case 50:{
            //已结束
            return @"约会已结束,咱们下次再约";
        }
            break;
            
        case 110:{
            //被婉拒
            return @"抱歉,我已取消了邀约";
        }
            break;
            
        case 120:{
            //被取消
            return @"抱歉,我已取消了邀约";
        }
            break;
            
        default:
            break;
    }
    
    return nil;
}

+(NSString *)getHintFromOrderStatus:(id<IMessageModel>)messageModel{

    NSDictionary *tempDict = messageModel.message.ext;
    
    NSNumber *orderStatusNum = tempDict[kHXYAOYUEORDERSTATUS];
    
    switch (orderStatusNum.integerValue) {
        case 10:{
            //已支付 待接收  发送的是邀约的cell
            
        }
            break;
            
        case 20:{
            //已接受  待到达
            return @"点击查看对方实时位置";
        }
            break;
            
        case 30:{
            //已到达  待开始
            return @"点击开始约会";
        }
            break;
            
        case 40:{
            //已开始  待结束
            return @"点击评价对方";
        }
            break;
            
        case 50:{
            //已结束
            return @"点击查看邀约详情";
        }
            break;
            
        case 110:{
            //被婉拒
            return @"点击查看邀约详情";
        }
            break;
            
        case 120:{
            //被取消
            return @"点击查看邀约详情";
        }
            break;
            
        default:
            break;
    }
    
    return nil;
}

@end
