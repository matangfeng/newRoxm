//
//  CJCMessageBaseCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/18.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageBaseCell.h"
#import "CJCChatMessageHandle.h"
#import "CJCCommon.h"

@interface CJCMessageBaseCell ()

@property (nonatomic ,strong) id<IMessageModel> baseMessageModel;

@property (nonatomic ,strong) UIImageView *iconImageView;

@property (nonatomic ,strong) UIButton *iconButton;

@property (nonatomic ,strong) UILabel *messageStatusLabel;

@property (nonatomic ,strong) UIButton *messageButton;

@property (nonatomic ,strong) UIImageView *statusImageView;

@property (nonatomic ,strong) UIButton *failButton;

@end

@implementation CJCMessageBaseCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self baseCellSetUpUI];
    }
    return self;
}

-(void)baseCellSetUpUI{

    UIImageView *iconView = [[UIImageView alloc] init];
    
    iconView.userInteractionEnabled = YES;
    
    self.iconImageView = iconView;
    [self.contentView addSubview:iconView];
    
    UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [iconButton addTarget:self action:@selector(iconImageClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.iconButton = iconButton;
    [self.contentView addSubview:iconButton];
    
    UIView *containView = [[UIView alloc] init];
    
    self.messageContainView = containView;
    [self.contentView addSubview:containView];
    
    UIImageView *bubbleView = [[UIImageView alloc] init];
    
    self.bubbleImageView = bubbleView;
    [containView addSubview:bubbleView];
    
    UIButton *messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [messageButton addTarget:self action:@selector(messageButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.messageButton = messageButton;
    [containView addSubview:messageButton];
    
    UILabel *statusLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMELIGHT size:11 color:[UIColor toUIColorByStr:@"B9B9B9"]];
    
    self.messageStatusLabel = statusLabel;
    [self.contentView addSubview:statusLabel];
}

-(void)iconImageClick{

    if ([self.delegate respondsToSelector:@selector(iconImageViewClickHandle:)]) {
        
        [self.delegate iconImageViewClickHandle:self.baseMessageModel];
    }
}

-(void)messageButtonClick{

    if ([self.delegate respondsToSelector:@selector(messageClickHandle:)]) {
        
        [self.delegate messageClickHandle:self.baseMessageModel];
    }
}

-(void)failButtonClick{

    [self changeCellStatus];
    
    if ([self.delegate respondsToSelector:@selector(resendMessageHandle:)]) {
        
        [self.delegate resendMessageHandle:self.baseMessageModel];
    }
}

-(void)updateUIWithModel:(id<IMessageModel>) messageModel{
    
    self.baseMessageModel = messageModel;
    
    [self.iconImageView setImageWithURL:[NSURL URLWithString:messageModel.avatarURLPath] placeholder:messageModel.avatarImage];
    
    self.messageContainView.size = [self getMessageContenViewSize:messageModel];
    
    NSString *statusStr;
    
    if (messageModel.isSender){
        
        self.iconImageView.frame = CGRectMake(SCREEN_WITDH-kAdaptedValue(60), kAdaptedValue(10), kAdaptedValue(39), kAdaptedValue(39));
        
        self.messageContainView.right = self.iconImageView.left-kAdaptedValue(10);
        self.messageContainView.y = kAdaptedValue(10);
        
        self.bubbleImageView.image = [CJCChatMessageHandle getLashenImage:@"message_green"];
        
        if (messageModel.messageStatus == EMMessageStatusDelivering) {
            
            self.messageStatusLabel.hidden = YES;
            self.failButton.hidden = YES;
            self.statusImageView.hidden = NO;
            self.statusImageView.size = CGSizeMake(17, 17);
            self.statusImageView.centerY = self.messageContainView.height/2;
            self.statusImageView.right = self.messageStatusLabel.right = self.messageContainView.left-kAdaptedValue(9);
            
            [self deliveringImageViewAnimation];
            
        }else if (messageModel.messageStatus == EMMessageStatusSucceed){
            
            self.statusImageView.hidden = YES;
            self.failButton.hidden = YES;
            self.messageStatusLabel.hidden = NO;
            
            if (messageModel.isMessageRead) {
                
                statusStr = @"已读";
                self.messageStatusLabel.textColor = [UIColor toUIColorByStr:@"B9B9B9"];
            }else{
                
                statusStr = @"送达";
                self.messageStatusLabel.textColor = [UIColor toUIColorByStr:@"429CF0"];
            }
            
            self.messageStatusLabel.text = statusStr;
            self.messageStatusLabel.size = CGSizeMake(22, 11);
            self.messageStatusLabel.top = self.messageContainView.top;
            self.messageStatusLabel.right = self.messageContainView.left-kAdaptedValue(9);
            
        }else{
            
            self.messageStatusLabel.hidden = YES;
            self.statusImageView.hidden = YES;
            self.failButton.hidden = NO;
            self.failButton.size = CGSizeMake(16, 16);
            self.failButton.centerY = self.messageContainView.height/2;
            self.failButton.right = self.messageStatusLabel.right = self.messageContainView.left-kAdaptedValue(9);
        }


    }else{
    
        self.statusImageView.hidden = YES;
        self.failButton.hidden = YES;
        self.messageStatusLabel.hidden = YES;
        
        self.iconImageView.frame = CGRectMake(kAdaptedValue(20), kAdaptedValue(10), kAdaptedValue(39), kAdaptedValue(39));
        
        self.messageContainView.left = self.iconImageView.right+kAdaptedValue(10);
        self.messageContainView.y = kAdaptedValue(10);
        
        self.bubbleImageView.image = [CJCChatMessageHandle getLashenImage:@"message_grey"];
    }
    
    self.bubbleImageView.frame = self.messageContainView.bounds;
    
    self.messageButton.frame = self.messageContainView.bounds;
    [self.messageContainView bringSubviewToFront:self.messageButton];
    
    self.iconImageView.layer.cornerRadius = kAdaptedValue(39/2);
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.bottom = self.messageContainView.bottom;
    self.iconButton.frame = self.iconImageView.frame;
}

-(void)deliveringImageViewAnimation{
    
    CABasicAnimation *anima = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];//绕着z轴为矢量，进行旋转(@"transform.rotation.z"==@@"transform.rotation")
    anima.toValue = [NSNumber numberWithFloat:M_PI*2.0];
    anima.duration = 2.0f;
    anima.repeatCount = MAXFLOAT;
    anima.removedOnCompletion = NO;
    [self.statusImageView.layer addAnimation:anima forKey:@"rotateAnimation"];
}

-(void)changeCellStatus{
    
    self.messageStatusLabel.hidden = YES;
    self.failButton.hidden = YES;
    self.statusImageView.hidden = NO;
    self.statusImageView.size = CGSizeMake(17, 17);
    self.statusImageView.centerY = self.messageContainView.height/2;
    self.statusImageView.right = self.messageStatusLabel.right = self.messageContainView.left-kAdaptedValue(9);
    
    [self deliveringImageViewAnimation];
}

-(CGSize)getMessageContenViewSize:(id<IMessageModel>)messageModel{

    CGSize returnSize;
    
    switch (messageModel.bodyType) {
        case EMMessageBodyTypeText:{
            
            if ([messageModel.text isEqualToString:kHXYAOYUEIDENTIGYCATION]){
            
                return CGSizeMake(SCREEN_WITDH-120, 110);
            }
            
            if ([messageModel.text isEqualToString:kHXYAOYUEORDERIDENTIGYCATION]){
                
                NSString *title = [CJCChatMessageHandle getTitleFromOrderStatus:messageModel];
                
                CGSize size = [CJCChatMessageHandle sizeWithString:title font:[UIFont accodingVersionGetFont_lightWithSize:15]];
                
                NSString *hint = [CJCChatMessageHandle getHintFromOrderStatus:messageModel];
                
                CGSize size1 = [CJCChatMessageHandle sizeWithString:hint font:[UIFont accodingVersionGetFont_lightWithSize:13]];
                
                if (size.width > size1.width) {
                    
                    return CGSizeMake(size.width+16, 70);
                }
                
                return CGSizeMake(size1.width+16, 70);
            }
            
            CGSize size = [CJCChatMessageHandle sizeWithString:messageModel.text font:[UIFont accodingVersionGetFont_lightWithSize:15]];
            
            returnSize = CGSizeMake(size.width+16, size.height+22);

        }
            break;
            
        case EMMessageBodyTypeImage:{
            
            NSDictionary *ext = messageModel.message.ext;
            
            if (ext) {
                
                NSNumber *widthNum = ext[kHUANXINIMAGEWIDTH];
                NSNumber *heightNum = ext[kHUANXINIMAGEHEIGHT];
                
                CGSize imageSize1 = CGSizeMake(widthNum.floatValue, heightNum.floatValue);
                
                CGSize imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:imageSize1];
                
                returnSize = CGSizeMake(imageSize.width, imageSize.height);
            }else{
            
                CGSize imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:messageModel.imageSize];
                
                returnSize = CGSizeMake(imageSize.width, imageSize.height);
            }

        }
            break;
            
        case EMMessageBodyTypeVoice:{
            
            returnSize = CGSizeMake(100,45);

        }
            break;
            
        case EMMessageBodyTypeLocation:{
            
            returnSize = CGSizeMake(200,176);
//            returnSize = CGSizeMake(SCREEN_WITDH-kAdaptedValue(130),kAdaptedValue(110));

        }
            break;
            
        case EMMessageBodyTypeVideo:{
            
            returnSize = CGSizeMake(100,100);

        }
            break;
            
        default:
            returnSize = CGSizeMake(100,100);
            break;
    }
    
    return returnSize;
}

-(UIButton *)failButton{
    
    if (_failButton == nil) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setImage:kGetImage(@"message_defaut") forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(failButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:button];
        _failButton = button;
    }
    return _failButton;
}

-(UIImageView *)statusImageView{
    
    if (_statusImageView == nil) {
        
        UIImageView *imageView = [[UIImageView alloc] init];
        
        imageView.image = kGetImage(@"message_sending");
        
        [self.contentView addSubview:imageView];
        _statusImageView = imageView;
    }
    return _statusImageView;
}

@end
