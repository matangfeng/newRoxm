//
//  CJCMessageImageCell.m
//  roxm
//
//  Created by 陈建才 on 2017/9/14.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageImageCell.h"
#import "CJCCommon.h"
#import "CJCChatMessageHandle.h"

@interface CJCMessageImageCell ()

@property (nonatomic ,strong) UIImageView *showImageView;

@property (nonatomic ,strong) UILabel *typeLabel;

@end

@implementation CJCMessageImageCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setUpUI];
    }
    
    return self;
}


-(void)setUpUI{
    
    UIImageView *showView = [[UIImageView alloc] init];
    
    showView.userInteractionEnabled = YES;
    
    self.showImageView = showView;
    [self.messageContainView insertSubview:showView aboveSubview:self.bubbleImageView];
    
    UILabel *typeLabel = [UIView getYYLabelWithStr:@"" fontName:kFONTNAMEREGULAR size:11 color:[UIColor toUIColorByStr:@"8C8C8C"]];
    
    self.typeLabel = typeLabel;
    [self.contentView addSubview:typeLabel];
    
    self.bubbleImageView.hidden = YES;
}

-(void)updateUIWithModel:(id<IMessageModel>)messageModel{
    
    [super updateUIWithModel:messageModel];

    CGSize imageSize;
    
    NSDictionary *ext = messageModel.message.ext;
    
    if (ext) {
        
        NSNumber *widthNum = ext[kHUANXINIMAGEWIDTH];
        NSNumber *heightNum = ext[kHUANXINIMAGEHEIGHT];
        
        CGSize imageSize1 = CGSizeMake(widthNum.floatValue, heightNum.floatValue);
        
        imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:imageSize1];
        
    }else{
    
        imageSize = [CJCChatMessageHandle CJC_getScaleImageSize:messageModel.imageSize];
    }
    
    UIImage *image = messageModel.isSender ? messageModel.image : messageModel.thumbnailImage;
    if (!image) {
        image = messageModel.image;
        if (!image) {
            [self.showImageView sd_setImageWithURL:[NSURL URLWithString:messageModel.fileURLPath] placeholderImage:[UIImage imageNamed:messageModel.failImageName]];
        } else {
            self.showImageView.image = image;
        }
    } else {
        self.showImageView.image = image;
    }
    
    self.showImageView.size = imageSize;
    self.showImageView.origin = CGPointMake(0, 0);
    
    NSDictionary *typeDict = messageModel.message.ext;
    
    if (typeDict) {
        
        NSString *typeStr = typeDict[kHUANXINIMAGESOURCE];
        
        if ([typeStr isEqualToString:kHUANXINIMAGECAMERA]) {
            
            self.typeLabel.text = @"手机实时拍摄";
        }else{
        
            self.typeLabel.text = @"";
        }
    }else{
    
        self.typeLabel.text = @"";
    }
    
    CGFloat strWidth = [CJCTools getShortStringLength:@"手机实时拍摄" withFont:[UIFont accodingVersionGetFont_regularWithSize:11]];
    
    self.typeLabel.y = self.messageContainView.bottom+6;
    self.typeLabel.size = CGSizeMake(strWidth, 12);
    
    if (messageModel.isSender) {
        
        self.typeLabel.right = self.messageContainView.right;
    }else{
    
        self.typeLabel.left = self.messageContainView.left;
    }
}

@end
