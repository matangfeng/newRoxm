//
//  CJCCustomAmapPOI.h
//  roxm
//
//  Created by 陈建才 on 2017/9/10.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AMapPOI,AMapReGeocode;

@interface CJCCustomAmapPOI : NSObject

@property (nonatomic ,strong) AMapPOI *gaodePOI;

@property (nonatomic ,strong) AMapReGeocode *reGeocode;

@property (nonatomic ,assign) BOOL isSelect;

@end
