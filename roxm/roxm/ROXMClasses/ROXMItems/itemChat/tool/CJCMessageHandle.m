//
//  CJCMessageHandle.m
//  roxm
//
//  Created by 陈建才 on 2017/9/17.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "CJCMessageHandle.h"
#import "CJCCommon.h"
#import <EaseUI.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "CJCInviteOrderModel.h"

@implementation CJCMessageHandle

static CJCMessageHandle *manager;
static dispatch_once_t onceToken;

+(instancetype)standMessageHandle{

    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
        
    });
    
    return manager;
}

+(EMMessage *)CJC_HuanxinYaoyueStatusMessageWithAmapPOI:(CJCInviteOrderModel *)orderModel andTo:(NSString *)toWho{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSDictionary *locationDict;
    
    locationDict =  @{kHXYAOYUEORDERSTATUS:@(orderModel.status),kHXYAOYUEORDERID:orderModel.id,kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl};
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:kHXYAOYUEORDERIDENTIGYCATION];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:locationDict];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinYaoyueMessageWithAmapPOI:(AMapPOI *)poi andOrderId:(NSString *)OrderId andTo:(NSString *)toWho{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSDictionary *locationDict;
    
    NSString *hotelImage;
    
    if (poi.images.count>0) {
        
        AMapImage *image = poi.images[0];
        hotelImage = image.url;
    }else{
    
        hotelImage = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1508474734398&di=648cb25ffa9fc52ec4b5e9691c39eb7d&imgtype=0&src=http%3A%2F%2Fimg1.qunarzz.com%2Fvc%2F5c%2Fc5%2Fa2%2Ff49aa22d5df82b105e41664d04.jpg_92.jpg";
    }
    
    NSLog(@"\n\n\n\n llls %@ - %@ - %@ - %@ - %@  - %@ - %@ - %@ - %@ - %@" , poi.name,poi.address,
          @(poi.location.latitude),@(poi.location.longitude),hotelImage,OrderId,infoModel.nickname,infoModel.imageUrl,handle.nickName,handle.iconImageUrl);
    locationDict =  @{kHXYAOYUEHOTELNAME:poi.name,kHXYAOYUEHOTELADDRESS:poi.address,kHXYAOYUEHOTELLOCATIONLAT:@(poi.location.latitude),kHXYAOYUEHOTELLOCATIONLNG:@(poi.location.longitude),kHXYAOYUEHOTELIMAGE:hotelImage,kHXYAOYUEORDERID:OrderId,kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl};
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:kHXYAOYUEIDENTIGYCATION];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:locationDict];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(NSDictionary *)getIconAndNameMessageExpand{

    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];
    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSLog(@"ccccc\n%@,%@,%@",infoModel.imageUrl,infoModel.nickname,infoModel.uid);
    return @{kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl,kHUANXINUIDRECIVER:infoModel.uid};
}

+(EMMessage *)CJC_HuanxinTextMessage:(NSString *)text andTo:(NSString *)toWho{

    NSDictionary *messageExt = [self getIconAndNameMessageExpand];
    
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:text];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:messageExt];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinImageMessage:(UIImage *)image andType:(CJCImageSourceType)sourceType andTo:(NSString *)toWho{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSDictionary *imageDict;
    
    if (sourceType==CJCImageSourceTypeCamera) {
        
        imageDict =  @{kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl,kHUANXINIMAGESOURCE:kHUANXINIMAGECAMERA,kHUANXINIMAGEWIDTH:@(image.size.width),kHUANXINIMAGEHEIGHT:@(image.size.height)};
    }else{
        
        imageDict =  @{kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl,kHUANXINIMAGESOURCE:kHUANXINIMAGELIBERARY,kHUANXINIMAGEWIDTH:@(image.size.width),kHUANXINIMAGEHEIGHT:@(image.size.height)};
    }
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    EMImageMessageBody *body = [[EMImageMessageBody alloc] initWithData:imageData displayName:@"image.png"];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    //生成Message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:imageDict];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinVoiceMessage:(NSString *)voicePath andDurio:(CGFloat)duration andTo:(NSString *)toWho{

    NSDictionary *messageExt = [self getIconAndNameMessageExpand];
    
    EMVoiceMessageBody *body = [[EMVoiceMessageBody alloc] initWithLocalPath:voicePath displayName:@"audio"];
    body.duration = duration;
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:messageExt];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinVideoMessage:(NSString *)voicePath andTo:(NSString *)toWho{

    NSDictionary *messageExt = [self getIconAndNameMessageExpand];
    
    EMVideoMessageBody *body = [[EMVideoMessageBody alloc] initWithLocalPath:voicePath displayName:@"video.mp4"];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:messageExt];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinLocationMessage:(AMapPOI *)poi andImage:(NSString *)imagePath andDistance:(double)distance andTo:(NSString *)toWho{

//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSDictionary *locationDict;
    
    locationDict =  @{kHUANXINLOCATIONDISTANCE:@(distance),kHUANXINLOCATIONIMAGE:imagePath,kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl,kHUANXINLOCATIONNAME:poi.name};
    
    EMLocationMessageBody *body = [[EMLocationMessageBody alloc] initWithLatitude:poi.location.latitude longitude:poi.location.longitude address:poi.address];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:locationDict];
    message.chatType = EMChatTypeChat;
    
    return message;
}

+(EMMessage *)CJC_HuanxinLocationMessage:(CLLocationCoordinate2D)location andAddress:(NSString *)address andImage:(NSString *)imagePath andDistance:(double)distance andTo:(NSString *)toWho{
    
//    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel infoModel];
    CJCpersonalInfoModel *infoModel = [CJCpersonalInfoModel modelWithDictionary:[CJCpersonalInfoModel infoModel].loginUserDict];

    CJCMessageHandle *handle = [CJCMessageHandle standMessageHandle];
    
    NSDictionary *locationDict;
    
    locationDict =  @{kHUANXINLOCATIONDISTANCE:@(distance),kHUANXINLOCATIONIMAGE:imagePath,kHUANXINNICKNAMESENDER:infoModel.nickname,kHUANXINICONURLSENDER:infoModel.imageUrl,kHUANXINNICKNAMERECIVER:handle.nickName,kHUANXINICONURLRECIVER:handle.iconImageUrl,kHUANXINLOCATIONNAME:address};
    
    EMLocationMessageBody *body = [[EMLocationMessageBody alloc] initWithLatitude:location.latitude longitude:location.longitude address:address];
    NSString *from = [[EMClient sharedClient] currentUsername];
    
    // 生成message
    EMMessage *message = [[EMMessage alloc] initWithConversationID:toWho from:from to:toWho body:body ext:locationDict];
    message.chatType = EMChatTypeChat;
    
    return message;
}

@end
