//
//  UIView+CJCEasyGetSet.m
//  roxm
//
//  Created by lfy on 2017/8/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "UIView+CJCEasyGetSet.h"
#import "CJCCommon.h"

@implementation UIView (CJCEasyGetSet)

@dynamic x;
@dynamic y;
@dynamic width;
@dynamic height;
@dynamic origin;
@dynamic size;

#pragma mark ---------------- Setters-----------------
-(void)setX:(CGFloat)x{
    CGRect r        = self.frame;
    r.origin.x      = x;
    self.frame      = r;
}

-(void)setY:(CGFloat)y{
    CGRect r        = self.frame;
    r.origin.y      = y;
    self.frame      = r;
}

-(void)setWidth:(CGFloat)width{
    CGRect r        = self.frame;
    r.size.width    = width;
    self.frame      = r;
}

-(void)setHeight:(CGFloat)height{
    CGRect r        = self.frame;
    r.size.height   = height;
    self.frame      = r;
}

-(void)setOrigin:(CGPoint)origin{
    self.x          = origin.x;
    self.y          = origin.y;
}

-(void)setSize:(CGSize)size{
    self.width      = size.width;
    self.height     = size.height;
}

-(void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}

-(void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}

-(void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}

-(void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}

#pragma mark ---------------- Getters-----------------
-(CGFloat)x{
    return self.frame.origin.x;
}

-(CGFloat)y{
    return self.frame.origin.y;
}

-(CGFloat)width{
    return self.frame.size.width;
}

-(CGFloat)height{
    return self.frame.size.height;
}

-(CGPoint)origin{
    return CGPointMake(self.x, self.y);
}

-(CGSize)size{
    return CGSizeMake(self.width, self.height);
}

-(CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}

-(CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}

-(CGFloat)centerX {
    return self.center.x;
}

-(CGFloat)centerY {
    return self.center.y;
}


+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f); //宽高 1.0只要有值就够了
    UIGraphicsBeginImageContext(rect.size); //在这个范围内开启一段上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);//在这段上下文中获取到颜色UIColor
    CGContextFillRect(context, rect);//用这个颜色填充这个上下文
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();//从这段上下文中获取Image属性,,,结束
    UIGraphicsEndImageContext();
    
    return image;
}

+(UILabel *)getSystemLabelWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color{

    UILabel *loginLabel = [[UILabel alloc] init];
    
    loginLabel.numberOfLines = 0;
    
    loginLabel.text = title;
    
    loginLabel.textColor = color;
    
    if ([name isEqualToString:kFONTNAMELIGHT]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_lightWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEREGULAR]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_regularWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEMEDIUM]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_mediumWithSize:size];
    }
    
    return loginLabel;
}

+(UILabel *)getYYLabelWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color{

    UILabel *loginLabel = [UILabel new];
    
    loginLabel.numberOfLines = 0;
    
    loginLabel.text = title;

    loginLabel.textColor = color;
    
    if ([name isEqualToString:kFONTNAMELIGHT]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_lightWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEREGULAR]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_regularWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEMEDIUM]) {
        
        loginLabel.font = [UIFont accodingVersionGetFont_mediumWithSize:size];
    }

    return loginLabel;
}

+(UIButton *)getButtonWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color{

    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [cancleButton setTitle:title forState:UIControlStateNormal];
    
    [cancleButton setTitleColor:color forState:UIControlStateNormal];
    
    if ([name isEqualToString:kFONTNAMELIGHT]) {
        
        cancleButton.titleLabel.font = [UIFont accodingVersionGetFont_lightWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEREGULAR]) {
        
        cancleButton.titleLabel.font = [UIFont accodingVersionGetFont_regularWithSize:size];
    }
    
    if ([name isEqualToString:kFONTNAMEMEDIUM]) {
        
        cancleButton.titleLabel.font = [UIFont accodingVersionGetFont_mediumWithSize:size];
    }
    
    return cancleButton;
}

+(UIView*)getLineView{
    
    UIView *lineView = [[UIView alloc] init];
    
    lineView.backgroundColor = [UIColor toUIColorByStr:@"D8D8D8"];
    
    return lineView;
}
@end
