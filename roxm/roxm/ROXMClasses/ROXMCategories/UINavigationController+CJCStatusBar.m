//
//  UINavigationController+CJCStatusBar.m
//  roxm
//
//  Created by 陈建才 on 2017/9/27.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import "UINavigationController+CJCStatusBar.h"

@implementation UINavigationController (CJCStatusBar)

-(UIStatusBarStyle)preferredStatusBarStyle{

    return [[self topViewController] preferredStatusBarStyle];
}

@end
