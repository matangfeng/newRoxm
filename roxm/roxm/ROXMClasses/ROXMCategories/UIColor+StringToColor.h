//
//  UIColor+StringToColor.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (StringToColor)

//将字符串转换成颜色
+(UIColor*)toUIColorByStr:(NSString*)colorStr;

//将字符串转换成颜色 并设置透明度
+(UIColor*)toUIColorByStr:(NSString*)colorStr andAlpha:(CGFloat)alpha;

@end
