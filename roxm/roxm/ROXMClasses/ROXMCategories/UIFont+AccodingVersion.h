//
//  UIFont+AccodingVersion.h
//  roxm
//
//  Created by lfy on 2017/8/4.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (AccodingVersion)

//根据系统版本获得不同的字体
+(UIFont *)accodingVersionGetFont_regularWithSize:(CGFloat)size;

+(UIFont *)accodingVersionGetFont_lightWithSize:(CGFloat)size;

+(UIFont *)accodingVersionGetFont_mediumWithSize:(CGFloat)size;

@end
