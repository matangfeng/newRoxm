//
//  UIView+CJCEasyGetSet.h
//  roxm
//
//  Created by lfy on 2017/8/3.
//  Copyright © 2017年 qunjutianxia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UILabel;

@interface UIView (CJCEasyGetSet)

@property (nonatomic, assign) CGFloat   x;
@property (nonatomic, assign) CGFloat   y;
@property (nonatomic, assign) CGFloat   width;
@property (nonatomic, assign) CGFloat   height;
@property (nonatomic, assign) CGPoint   origin;
@property (nonatomic, assign) CGSize    size;
@property (nonatomic, assign) CGFloat   bottom;
@property (nonatomic, assign) CGFloat   right;
@property (nonatomic, assign) CGFloat   centerX;
@property (nonatomic, assign) CGFloat   centerY;

//根据颜色获取一张图片
+ (UIImage *)imageWithColor:(UIColor *)color;

//根据title font fontsize titlecolor 创建一个YYlabel
+(UILabel *)getYYLabelWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color;

+(UILabel *)getSystemLabelWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color;

+(UIButton *)getButtonWithStr:(NSString *)title fontName:(NSString *)name size:(CGFloat)size color:(UIColor *)color;

//创建一条分割线 颜色为灰色（需要设置frame）
+(UIView*)getLineView;

@end
